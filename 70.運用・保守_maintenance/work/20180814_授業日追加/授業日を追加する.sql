/* dev/pre/stg毎に実行*/
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-08-13';
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-08-14';
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-08-15';
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-08-16';
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-08-17';

/*　日付を変えて繰り返し実行*/(to_dateは休校日を授業日にしたい日を設定し、from_dateはコピー元の曜日に応じた営業日を設定する)
SET @from_date='2018-07-23';
SET @to_date='2018-08-13';

SET @from_date='2018-07-24';
SET @to_date='2018-08-14';

SET @from_date='2018-07-25';
SET @to_date='2018-08-15';

SET @from_date='2018-07-26';
SET @to_date='2018-08-16';

SET @from_date='2018-07-27';
SET @to_date='2018-08-17';



/*class_shift_id insert */
INSERT INTO class_shift_work
(class_shift_id, `date`)
SELECT class_shift_id, @to_date
FROM class_shift_work
WHERE `date` = @from_date
AND disable = 0
;

/*shift_attendance insert */
INSERT INTO shift_attendance
(person_id, class_shift_work_id, is_authorized_absent, comment)
SELECT shift_attendance.person_id, csw.new_id, shift_attendance.is_authorized_absent,	shift_attendance.comment
FROM shift_attendance, 
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw
WHERE class_shift_work_id = csw.old_id
AND shift_attendance.disable = 0
;

/*class_work insert */
INSERT INTO class_work
(period_id, class_shift_work_id)
SELECT class_work.period_id, csw.new_id
FROM class_work, 
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw
WHERE class_shift_work_id = csw.old_id
AND class_work.disable = 0
;

/*attendance insert */
INSERT INTO attendance
(class_work_id,class_shift_work_id,person_id)
SELECT 
cw_new.id as new_class_work_id
,cw_new.class_shift_work_id as new_class_shift_work_id
, attendance.person_id
FROM attendance,
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @from_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_old,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @to_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_new
WHERE cw_old.class_shift_work_id = csw.old_id
AND cw_new.class_shift_work_id = csw.new_id
AND cw_old.period_id = cw_new.period_id
AND attendance.class_work_id = cw_old.id
;
