-- adminer
TRUNCATE TABLE attendance;
TRUNCATE TABLE attendance_rate;
TRUNCATE TABLE class_entry_exit;
TRUNCATE TABLE shift_attendance;

TRUNCATE TABLE class;
TRUNCATE TABLE class_group;
TRUNCATE TABLE class_group_unit;
TRUNCATE TABLE class_shift;
TRUNCATE TABLE class_shift_work;
TRUNCATE TABLE class_student;
TRUNCATE TABLE class_work;
TRUNCATE TABLE school_calendar;

/*
TRUNCATE TABLE request;
TRUNCATE TABLE application;
TRUNCATE TABLE application_approve;
TRUNCATE TABLE application_official_absence_properties;
TRUNCATE TABLE application_change_class_properties;
TRUNCATE TABLE application_change_info_job_properties;
TRUNCATE TABLE application_change_info_properties;
TRUNCATE TABLE application_issue_certificate_properties;
TRUNCATE TABLE application_leave_absence_properties;
TRUNCATE TABLE application_official_absence_properties;
TRUNCATE TABLE application_temporary_return_properties;
TRUNCATE TABLE application_withdrawal_notification_properties;
TRUNCATE TABLE external_result;
TRUNCATE TABLE external_result_detail;
*/

-- command
mysqldump -R -u midream -p midream_pre --default-character-set=utf8 --single-transaction --ignore-table=midream_pre.tablet_application_update --ignore-table=midream_pre.log > 201802261401_midream_pre.sql

-- command
mysql -u midream -p midream_pre

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/attendance.csv' INTO TABLE midream_pre.attendance FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/attendance_rate.csv' INTO TABLE midream_pre.attendance_rate FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_entry_exit.csv' INTO TABLE midream_pre.class_entry_exit FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/shift_attendance.csv' INTO TABLE midream_pre.shift_attendance FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class.csv' INTO TABLE midream_pre.class FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_group.csv' INTO TABLE midream_pre.class_group FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_group_unit.csv' INTO TABLE midream_pre.class_group_unit FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_shift.csv' INTO TABLE midream_pre.class_shift FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_shift_work.csv' INTO TABLE midream_pre.class_shift_work FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_student.csv' INTO TABLE midream_pre.class_student FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/class_work.csv' INTO TABLE midream_pre.class_work FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/command_import/school_calendar.csv' INTO TABLE midream_pre.school_calendar FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

