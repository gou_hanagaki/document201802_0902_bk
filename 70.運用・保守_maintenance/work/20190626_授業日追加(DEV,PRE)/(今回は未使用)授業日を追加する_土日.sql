/* dev/pre/stg毎に実行
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-03-24';
UPDATE school_calendar SET is_school_holiday=0 WHERE date='2018-03-25';
*/
-- --------------------------
-- school_term
-- --------------------------
-- 130:201707
-- 131:201710
-- 132:201801
-- 133:201804
-- 134:201807
-- 135:201810
-- --------------------------
-- day
-- --------------------------
-- 1:Mon
-- 2:Tue
-- 3:Wed
-- 4:Thu
-- 5:Fri
-- 6:Sat
-- 7:Sun
/*　日付を変えて繰り返し実行
SET @school_term_id='132';
SET @from_date='2018-03-12';
SET @to_date='2018-03-24';
SET @from_day='1';
SET @to_day='6';

SET @school_term_id='132';
SET @from_date='2018-03-12';
SET @to_date='2018-03-25';
SET @from_day='1';
SET @to_day='7';
*/

INSERT INTO class_shift 
(class_id,day,teacher_id,is_optional)
SELECT 
class_shift.class_id
,@to_day
,class_shift.teacher_id
,class_shift.is_optional
FROM class_shift
INNER JOIN class ON class.id=class_shift.class_id AND class.disable=0
INNER JOIN class_group ON class_group.id=class.class_group_id AND class_group.disable=0
INNER JOIN class_group_unit ON class_group_unit.id=class_group.class_group_unit_id AND class_group_unit.disable=0
INNER JOIN school_term ON school_term.id=class_group_unit.school_term_id AND school_term.disable=0
WHERE class_shift.disable = 0
AND school_term.id=@school_term_id
AND class_shift.day=@from_day
;

/*class_shift_id insert */
INSERT INTO class_shift_work
(class_shift_id,date)
SELECT 
class_shift.id
,@to_date
FROM class_shift
INNER JOIN class ON class.id=class_shift.class_id AND class.disable=0
INNER JOIN class_group ON class_group.id=class.class_group_id AND class_group.disable=0
INNER JOIN class_group_unit ON class_group_unit.id=class_group.class_group_unit_id AND class_group_unit.disable=0
INNER JOIN school_term ON school_term.id=class_group_unit.school_term_id AND school_term.disable=0
WHERE class_shift.disable = 0
AND school_term.id=@school_term_id
AND class_shift.day=@from_day
;

/*shift_attendance insert */
INSERT INTO shift_attendance
(person_id, class_shift_work_id, is_authorized_absent, comment)
SELECT shift_attendance.person_id, csw.new_id, shift_attendance.is_authorized_absent,	shift_attendance.comment
FROM shift_attendance, 
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw
WHERE class_shift_work_id = csw.old_id
AND shift_attendance.disable = 0
;

/*class_work insert */
INSERT INTO class_work
(period_id, class_shift_work_id)
SELECT class_work.period_id, csw.new_id
FROM class_work, 
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw
WHERE class_shift_work_id = csw.old_id
AND class_work.disable = 0
;

/*attendance insert */
INSERT INTO attendance
(class_work_id,class_shift_work_id,person_id)
SELECT 
cw_new.id as new_class_work_id
,cw_new.class_shift_work_id as new_class_shift_work_id
, attendance.person_id
FROM attendance,
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @from_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_old,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @to_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_new
WHERE cw_old.class_shift_work_id = csw.old_id
AND cw_new.class_shift_work_id = csw.new_id
AND cw_old.period_id = cw_new.period_id
AND attendance.class_work_id = cw_old.id
;
