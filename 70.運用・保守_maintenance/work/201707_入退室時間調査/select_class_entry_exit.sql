set @rownum:=0;
set @person_id:=null;
select
	class_name
	,student_no
	,name
	,kana
	,status
	,date
	,entry_time1
	,exit_time1
	,entry_time2
	,exit_time2
	,entry_time3
	,exit_time3
from (
	select
		a.person_id
		,student_no
		,csw.id as class_shift_work_id
		,CONCAT(p.last_name,p.first_name) as name
		,CONCAT(p.last_name_kana,p.first_name_kana) as kana
		,scd.name as status
		,csw.date as date
		,c.name as class_name
		,c.id
	from
		class_group cg
	inner join class c on c.class_group_id = cg.id and c.id >= 145
	inner join class_shift cs on cs.class_id = c.id
	inner join class_shift_work csw on csw.class_shift_id = cs.id
	inner join class_work cw on cw.class_shift_work_id = csw.id
	left join attendance a on a.class_work_id = cw.id
	inner join student s on s.person_id = a.person_id
	inner join person p on p.id = a.person_id
	inner join system_code_detail scd on scd.code1 = s.status and scd.system_code_id=2
	where true
	AND csw.date >= '2017-07-11' 
	AND csw.date <= '2017-07-21'
	group by csw.id,person_id,student_no,scd.name,csw.date
	ORDER BY c.id,student_no,csw.date
)s
left join (
	select
		class_shift_work_id
		,person_id
		,max(case cee.rownum when 1 then cee.entry_time else null end) as entry_time1
		,max(case cee.rownum when 1 then cee.exit_time else null end) as exit_time1
		,max(case cee.rownum when 2 then cee.entry_time else null end) as entry_time2
		,max(case cee.rownum when 2 then cee.exit_time else null end) as exit_time2
		,max(case cee.rownum when 3 then cee.entry_time else null end) as entry_time3
		,max(case cee.rownum when 3 then cee.exit_time else null end) as exit_time3
	from (
		select
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
		  	,@person_id:=person_id as person_id
			,class_shift_work_id
			,entry_time 
			,exit_time
		from
			class_entry_exit
		order by class_shift_work_id,person_id
	)cee
	group by class_shift_work_id,person_id
)t on t.class_shift_work_id = s.class_shift_work_id and t.person_id = s.person_id