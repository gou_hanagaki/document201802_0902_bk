
mysqldump -h rds.midream.local -u midream -p midream --default-character-set=utf8 --single-transaction 
--ignore-table=midream.tablet_application_update 
--ignore-table=midream.logs 
--ignore-table=midream.templates 
--ignore-table=midream.pdf_function 
--ignore-table=midream.function 
--ignore-table=midream.function_group 
--ignore-table=midream.permission 
--ignore-table=midream.system_code 
--ignore-table=midream.system_code_detail 
--ignore-table=midream.educational_institution 
> 20180618_for_STG_midream_real_data_only.sql



