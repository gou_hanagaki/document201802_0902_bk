
UPDATE class_shift
LEFT JOIN class on class.id = class_shift.class_id
SET class_shift.disable=1
WHERE true
and class.id >= 208
and class.id >= 246;

UPDATE class_shift_work
SET class_shift_work.disable=1
WHERE true
and class_shift_work.date >= '2018-01-01'
and class_shift_work.date <= '2018-03-31';

UPDATE class_work
LEFT JOIN class_shift_work on class_shift_work.id = class_work.class_shift_work_id
SET class_work.disable=1
WHERE true
and class_shift_work.date >= '2018-01-01'
and class_shift_work.date <= '2018-03-31';

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student on student.person_id = shift_attendance.person_id
SET shift_attendance.disable=1
WHERE true
and class_shift_work.date >= '2018-01-01'
and class_shift_work.date <= '2018-03-31';

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student on student.person_id = attendance.person_id
SET attendance.disable=1
WHERE true
and class_shift_work.date >= '2018-01-01'
and class_shift_work.date <= '2018-03-31';