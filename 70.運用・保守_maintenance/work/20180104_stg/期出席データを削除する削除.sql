SET @csw_date_from:='2018-01-01';
SET @csw_date_to:='2018-03-31';

UPDATE class_shift
LEFT JOIN class on class.id = class_shift.class_id
SET class_shift.disable=1
WHERE true
and class.id >= 208
and class.id >= 246
;

UPDATE class_shift_work
SET class_shift_work.disable=1
WHERE true
and class_shift_work.date >= @csw_date_from
and class_shift_work.date <= @csw_date_to
;

UPDATE class_work
LEFT JOIN class_shift_work on class_shift_work.id = class_work.class_shift_work_id
SET class_work.disable=1
WHERE true
and class_shift_work.date >= @csw_date_from
and class_shift_work.date <= @csw_date_to
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student on student.person_id = shift_attendance.person_id
SET shift_attendance.disable=1
WHERE true
and class_shift_work.date >= @csw_date_from
and class_shift_work.date <= @csw_date_to
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student on student.person_id = attendance.person_id
SET attendance.disable=1
WHERE true
and class_shift_work.date >= @csw_date_from
and class_shift_work.date <= @csw_date_to
;

-- ------------------------------------------

-- ------------------------------------------

SET @from_date='2018-01-04';
SET @to_date='2018-03-31';
SET @person_id='2279';

/*shift_attendance insert */
INSERT INTO shift_attendance
(person_id, class_shift_work_id)
SELECT @person_id, class_shift_work.id
FROM class_shift_work
WHERE class_shift_work.date >= @from_date
AND class_shift_work.date <= @to_date
;

/*class_work insert */
INSERT INTO class_work
(period_id, class_shift_work_id)
SELECT class_work.period_id, csw.new_id
FROM class_work
LEFT JOIN class_shift_work on class_shift_work.id =
WHERE class_shift_work_id = csw.old_id
AND class_work.disable = 0
;

/*attendance insert */
INSERT INTO attendance
(class_work_id, person_id)
SELECT cw_new.id as new_class_work_id, attendance.person_id
FROM attendance,
(SELECT csw_old.id as old_id, csw_new.id as new_id
FROM class_shift_work as csw_old, class_shift_work as csw_new
WHERE csw_old.`date` = @from_date
AND csw_new.`date` = @to_date
AND csw_old.class_shift_id = csw_new.class_shift_id
AND csw_old.disable = 0
AND csw_new.disable = 0) as csw,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @from_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_old,
(SELECT class_work.id, class_work.period_id, class_work.class_shift_work_id
FROM class_shift_work, class_work
WHERE class_shift_work.`date` = @to_date
AND class_shift_work.disable = 0
AND class_work.class_shift_work_id = class_shift_work.id
AND class_work.disable = 0) as cw_new
WHERE cw_old.class_shift_work_id = csw.old_id
AND cw_new.class_shift_work_id = csw.new_id
AND cw_old.period_id = cw_new.period_id
AND attendance.class_work_id = cw_old.id
;

UPDATE attendance
LEFT JOIN class_work ON class_work.id = attendance.class_work_id
SET attendance.class_shift_work_id = class_work.class_shift_work_id
where attendance.class_shift_work_id = 0
;