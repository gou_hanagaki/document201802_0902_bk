select * 
from student
where student_no in(1710181,1804288);

select person.*
from person
INNER JOIN student ON student.person_id = person.id
where student_no in(1710181,1804288);

select class_student.*
from class_student
INNER JOIN  person ON person.id = class_student.person_id
INNER JOIN  student ON person.id = student.person_id
where student.student_no in(1710181,1804288);

select account.*
from account
INNER JOIN person ON person.id = account.person_id
INNER JOIN  student ON person.id = student.person_id
where student.student_no in(1710181,1804288);

-----------------------------------------------------------------

UPDATE student
SET student.disable = 1
where student_no in(1710181,1804288);

UPDATE person
INNER JOIN student ON student.person_id = person.id
SET person.disable = 1
where student_no in(1710181,1804288);

UPDATE class_student
INNER JOIN  person ON person.id = class_student.person_id
INNER JOIN  student ON person.id = student.person_id
SET class_student.disable = 1
where student.student_no in(1710181,1804288);

UPDATE account
INNER JOIN  person ON person.id = account.person_id
INNER JOIN  student ON person.id = student.person_id
SET account.disable = 1
where student.student_no in(1710181,1804288);