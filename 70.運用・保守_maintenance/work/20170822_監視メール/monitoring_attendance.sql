﻿-- set @student_no='9917039';
set @student_no='';
set @rownum:=0;
set @person_id:=null;
select
--	base.class_id,base.person_id,base.class_shift_work_id
	base.class_name as 'クラス名'
	,base.student_no as '学籍番号'
	,base.name as '氏名'
	,base.kana as '氏名カナ'
	,base.student_status_nm as '在籍状況'
	,base.date as '日付'
	,case shift_attendance.is_authorized_absent when 1 then '〇' else '' end as '公欠'
	,case shift_attendance.is_leave_absent when 1 then '〇' else '' end as '休学'
	,shift_attendance.comment as 'コメント'
	,entry_time1 as '入室時間1',exit_time1 as '退室時間1'
	,entry_time2 as '入室時間2',exit_time2 as '退室時間2'
	,entry_time3 as '入室時間3',exit_time3 as '退室時間3'
--	,attendance_id1,attendance_cd1,attendance_is_authorized_absent1,attendance_is_leave_absent1,attendance_is_canceled1
	,attendance_nm1 as '1or6コマ',attendance_from1 as '1or6コマ入',attendance_to1 as '1or6コマ退'
--	,attendance_id2,attendance_cd2,attendance_is_authorized_absent2,attendance_is_leave_absent2,attendance_is_canceled2
	,attendance_nm2 as '2or7コマ',attendance_from2 as '2or7コマ入',attendance_to2 as '2or7コマ退'
--	,attendance_id3,attendance_cd3,attendance_is_authorized_absent3,attendance_is_leave_absent3,attendance_is_canceled3
	,attendance_nm3 as '3or8コマ',attendance_from3 as '3or8コマ入',attendance_to3 as '3or8コマ退'
--	,attendance_id4,attendance_cd4,attendance_is_authorized_absent4,attendance_is_leave_absent4,attendance_is_canceled4
	,attendance_nm4 as '4or9コマ',attendance_from4 as '4or9コマ入',attendance_to4 as '4or9コマ退'
from (
	select
		class.name as class_name
		,class.id as class_id
		,student.student_no
		,CONCAT(person.last_name,person.first_name) as name
		,CONCAT(person.last_name_kana,person.first_name_kana) as kana
		,student.status as student_status_cd
		,scd.name as student_status_nm
		,csw.date as date
		,coma.*
	from
		class
	inner join class_shift on class_shift.class_id = class.id and class_shift.disable = 0
	inner join class_shift_work csw on csw.class_shift_id = class_shift.id and csw.disable = 0
	left join (
		select
			class_shift_work_id
			,person_id
			,max(case when (period_id=1 or period_id=6) then attendance.id else null end) as 'attendance_id1'
			,max(case when (period_id=1 or period_id=6) then attendance_status_cd else null end) as 'attendance_cd1'
			,max(case when (period_id=1 or period_id=6) then attendance_status_nm else null end) as 'attendance_nm1'
			,max(case when (period_id=1 or period_id=6) then time_from else null end) as 'attendance_from1'
			,max(case when (period_id=1 or period_id=6) then time_to else null end) as 'attendance_to1'
			,max(case when (period_id=1 or period_id=6) then is_authorized_absent else null end) as 'attendance_is_authorized_absent1'
			,max(case when (period_id=1 or period_id=6) then is_leave_absent else null end) as 'attendance_is_leave_absent1'
			,max(case when (period_id=1 or period_id=6) then is_canceled else null end) as 'attendance_is_canceled1'
			,max(case when (period_id=2 or period_id=7) then attendance.id else null end) as 'attendance_id2'
			,max(case when (period_id=2 or period_id=7) then attendance_status_cd else null end) as 'attendance_cd2'
			,max(case when (period_id=2 or period_id=7) then attendance_status_nm else null end) as 'attendance_nm2'
			,max(case when (period_id=2 or period_id=7) then time_from else null end) as 'attendance_from2'
			,max(case when (period_id=2 or period_id=7) then time_to else null end) as 'attendance_to2'
			,max(case when (period_id=2 or period_id=7) then is_authorized_absent else null end) as 'attendance_is_authorized_absent2'
			,max(case when (period_id=2 or period_id=7) then is_leave_absent else null end) as 'attendance_is_leave_absent2'
			,max(case when (period_id=2 or period_id=7) then is_canceled else null end) as 'attendance_is_canceled2'
			,max(case when (period_id=3 or period_id=8) then attendance.id else null end) as 'attendance_id3'
			,max(case when (period_id=3 or period_id=8) then attendance_status_cd else null end) as 'attendance_cd3'
			,max(case when (period_id=3 or period_id=8) then attendance_status_nm else null end) as 'attendance_nm3'
			,max(case when (period_id=3 or period_id=8) then time_from else null end) as 'attendance_from3'
			,max(case when (period_id=3 or period_id=8) then time_to else null end) as 'attendance_to3'
			,max(case when (period_id=3 or period_id=8) then is_authorized_absent else null end) as 'attendance_is_authorized_absent3'
			,max(case when (period_id=3 or period_id=8) then is_leave_absent else null end) as 'attendance_is_leave_absent3'
			,max(case when (period_id=3 or period_id=8) then is_canceled else null end) as 'attendance_is_canceled3'
			,max(case when (period_id=4 or period_id=9) then attendance.id else null end) as 'attendance_id4'
			,max(case when (period_id=4 or period_id=9) then attendance_status_cd else null end) as 'attendance_cd4'
			,max(case when (period_id=4 or period_id=9) then attendance_status_nm else null end) as 'attendance_nm4'
			,max(case when (period_id=4 or period_id=9) then time_from else null end) as 'attendance_from4'
			,max(case when (period_id=4 or period_id=9) then time_to else null end) as 'attendance_to4'
			,max(case when (period_id=4 or period_id=9) then is_authorized_absent else null end) as 'attendance_is_authorized_absent4'
			,max(case when (period_id=4 or period_id=9) then is_leave_absent else null end) as 'attendance_is_leave_absent4'
			,max(case when (period_id=4 or period_id=9) then is_canceled else null end) as 'attendance_is_canceled4'
		from (
			select
				attendance.class_shift_work_id
				,person_id
				,period_id
				,attendance.id
				,attendance.attendance as attendance_status_cd
				,scd.name as attendance_status_nm
				,attendance.time_from
				,attendance.time_to
				,attendance.is_authorized_absent
				,attendance.is_leave_absent
				,attendance.is_canceled
			from
				attendance
			inner join class_work on class_work.id = attendance.class_work_id and class_work.disable = 0
			inner join class_shift_work csw on csw.id = class_work.class_shift_work_id and csw.disable = 0
			inner join system_code_detail scd on scd.code1 = attendance.attendance and scd.system_code_id=9 and scd.disable = 0
			where attendance.disable = 0
			order by class_work_id,person_id,period_id
		)attendance
		group by class_shift_work_id,person_id
	)coma on coma.class_shift_work_id = csw.id
	inner join student on student.person_id = coma.person_id and student.disable = 0
	inner join person on person.id = coma.person_id and person.disable = 0
	inner join system_code_detail scd on scd.code1 = student.status and scd.system_code_id=2 and scd.disable = 0
	where class.disable = 0
	and csw.date <= date(now())
	and (case when @student_no<>'' then student_no in (@student_no) else true end)
	group by student.person_id,student.student_no,scd.name,csw.date
	ORDER BY class.id,student.student_no,csw.date
)base
left join (
	select
		class_shift_work_id
		,person_id
		,max(case rownum when 1 then id else null end) as class_entry_exit_id1
		,max(case rownum when 1 then entry_time else null end) as entry_time1
		,max(case rownum when 1 then exit_time else null end) as exit_time1
		,max(case rownum when 2 then id else null end) as class_entry_exit_id2
		,max(case rownum when 2 then entry_time else null end) as entry_time2
		,max(case rownum when 2 then exit_time else null end) as exit_time2
		,max(case rownum when 3 then id else null end) as class_entry_exit_id3
		,max(case rownum when 3 then entry_time else null end) as entry_time3
		,max(case rownum when 3 then exit_time else null end) as exit_time3
	from (
		select
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
		  	,@person_id:=person_id as person_id
			,class_shift_work_id
			,entry_time 
			,exit_time
			,id
		from
			class_entry_exit
		where class_entry_exit.disable = 0
		order by class_shift_work_id,person_id
	)class_entry_exit
	group by class_shift_work_id,person_id
)entry_exit on entry_exit.class_shift_work_id = base.class_shift_work_id and entry_exit.person_id = base.person_id
left join shift_attendance on shift_attendance.class_shift_work_id = base.class_shift_work_id and shift_attendance.person_id = base.person_id
