/*
select class_work.*
from class_work
left join class_shift_work on class_shift_work.id = class_work.class_shift_work_id
WHERE class_shift_work.date = '2017-10-23' and class_work.is_canceled=0
*/
/*
select attendance.*
from class_shift_work
inner join attendance on attendance.class_shift_work_id = class_shift_work.id
WHERE class_shift_work.date = '2017-10-23' and attendance.is_canceled=0
*/


UPDATE class_work
LEFT JOIN class_shift_work ON class_shift_work.id = class_work.class_shift_work_id
SET class_work.is_canceled=1,class_work.lastup_account_id=2000,class_work.lastup_datetime='2017-10-31 20:00:00'
WHERE class_shift_work.date = '2017-10-23' and class_work.is_canceled=0;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
SET attendance.is_canceled=1,attendance.lastup_account_id=2000,attendance.lastup_datetime='2017-10-31 20:00:00'
WHERE class_shift_work.date = '2017-10-23' and attendance.is_canceled=0;

14
4