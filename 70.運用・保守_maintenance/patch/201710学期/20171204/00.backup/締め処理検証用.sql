set @start_date:='2017-11-01';
set @end_date:='2017-11-30';
set @attendance_cd:='3';
set @school_term:='2017-10-01';
SELECT 
 DATE_FORMAT(calculate_date_from,'%Y/%m') as '締め処理検証-出力月'
 ,class.name as 'クラス名'
 ,student.student_no as '学籍番号'
 ,CONCAT(person.last_name,person.first_name) as '名前'
 ,CONCAT(person.last_name_kana,person.first_name_kana) as 'フリガナ'
 ,CONCAT(person.last_name_alphabet,person.first_name_alphabet) as 'アルファベット'
 ,attendance_rate.total_time as '授業時間（コマ数）'
 ,attendance_rate.absent_time as '欠席した時間（コマ数）'
 ,attendance_rate.late_time as '遅刻/早退の回数（コマ数）'
 ,(attendance_rate.attend_time + attendance_rate.late_time)-truncate(attendance_rate.late_time/3,0) as '出席した時間（コマ数）'
 ,absent_day.absent_day as '欠席日数'
 ,attendance_rate.absent_day as 'attendance_rate欠席日数'
FROM attendance_rate
INNER JOIN student on student.id = attendance_rate.student_id and student.disable=0
INNER JOIN person on person.id = student.person_id and person.disable=0
INNER JOIN class_student on class_student.person_id = person.id
LEFT JOIN class on class.id = class_student.class_id and class.disable=0
LEFT JOIN class_group on class_group.id = class.class_group_id and class_group.disable=0
LEFT JOIN school_term on school_term.id = class_group.school_term_id and school_term.disable=0
LEFT JOIN ability on ability.id = class_group.ability_id and ability.disable=0
LEFT JOIN (
  select
   student_no
   ,SUM(absent_day) as absent_day
  from (
   select
    class.name as class_name
    ,class.time_zone_id
    ,student.student_no
    ,CONCAT(person.last_name,person.first_name) as name
    ,CONCAT(person.last_name_kana,person.first_name_kana) as kana
    ,scd.name as status
    ,csw.date as date
    ,case when(
    	attendance_cd1=0 and attendance_is_authorized_absent1=0 and attendance_is_leave_absent1=0 and attendance_is_canceled1=0 and 
    	attendance_cd2=0 and attendance_is_authorized_absent2=0 and attendance_is_leave_absent2=0 and attendance_is_canceled2=0 and 
    	attendance_cd3=0 and attendance_is_authorized_absent3=0 and attendance_is_leave_absent3=0 and attendance_is_canceled3=0 and  
    	attendance_cd4=0 and attendance_is_authorized_absent4=0 and attendance_is_leave_absent4=0 and attendance_is_canceled4=0 
    ) then 1 else 0 end as absent_day
    ,coma.*
   from
    class
   inner join class_shift on class_shift.class_id = class.id and class_shift.disable = 0
   inner join class_shift_work csw on csw.class_shift_id = class_shift.id and csw.disable = 0
   left join (
    select
     csw_date
     ,person_id
	,max(case when (period_id=1 or period_id=6) then attendance.id else null end) as 'attendance_id1'
	,max(case when (period_id=1 or period_id=6) then attendance_status_cd else null end) as 'attendance_cd1'
	,max(case when (period_id=1 or period_id=6) then attendance_status_nm else null end) as 'attendance_nm1'
	,max(case when (period_id=1 or period_id=6) then time_table_time_from else null end) as 'time_table_time_from1'
	,max(case when (period_id=1 or period_id=6) then time_table_time_to else null end) as 'time_table_time_to1'
	,max(case when (period_id=1 or period_id=6) then attendance_time_from else null end) as 'attendance_from1'
	,max(case when (period_id=1 or period_id=6) then attendance_time_to else null end) as 'attendance_to1'
	,max(case when (period_id=1 or period_id=6) then is_authorized_absent else null end) as 'attendance_is_authorized_absent1'
	,max(case when (period_id=1 or period_id=6) then is_leave_absent else null end) as 'attendance_is_leave_absent1'
	,max(case when (period_id=1 or period_id=6) then is_canceled else null end) as 'attendance_is_canceled1'
--			,max(case when (period_id=1 or period_id=6) then is_temporary_return else null end) as 'attendance_is_temporary_return1'
	,max(case when (period_id=1 or period_id=6) then class_work_id else null end) as 'class_work_id1'
	,max(case when (period_id=1 or period_id=6) then class_work_is_canceled else null end) as 'class_work_is_canceled1'
	,max(case when (period_id=2 or period_id=7) then attendance.id else null end) as 'attendance_id2'
	,max(case when (period_id=2 or period_id=7) then attendance_status_cd else null end) as 'attendance_cd2'
	,max(case when (period_id=2 or period_id=7) then attendance_status_nm else null end) as 'attendance_nm2'
	,max(case when (period_id=2 or period_id=7) then time_table_time_from else null end) as 'time_table_time_from2'
	,max(case when (period_id=2 or period_id=7) then time_table_time_to else null end) as 'time_table_time_to2'
	,max(case when (period_id=2 or period_id=7) then attendance_time_from else null end) as 'attendance_from2'
	,max(case when (period_id=2 or period_id=7) then attendance_time_to else null end) as 'attendance_to2'
	,max(case when (period_id=2 or period_id=7) then is_authorized_absent else null end) as 'attendance_is_authorized_absent2'
	,max(case when (period_id=2 or period_id=7) then is_leave_absent else null end) as 'attendance_is_leave_absent2'
	,max(case when (period_id=2 or period_id=7) then is_canceled else null end) as 'attendance_is_canceled2'
--			,max(case when (period_id=2 or period_id=7) then is_temporary_return else null end) as 'attendance_is_temporary_return2'
	,max(case when (period_id=2 or period_id=7) then class_work_id else null end) as 'class_work_id2'
	,max(case when (period_id=2 or period_id=7) then class_work_is_canceled else null end) as 'class_work_is_canceled2'
	,max(case when (period_id=3 or period_id=8) then attendance.id else null end) as 'attendance_id3'
	,max(case when (period_id=3 or period_id=8) then attendance_status_cd else null end) as 'attendance_cd3'
	,max(case when (period_id=3 or period_id=8) then attendance_status_nm else null end) as 'attendance_nm3'
	,max(case when (period_id=3 or period_id=8) then time_table_time_from else null end) as 'time_table_time_from3'
	,max(case when (period_id=3 or period_id=8) then time_table_time_to else null end) as 'time_table_time_to3'
	,max(case when (period_id=3 or period_id=8) then attendance_time_from else null end) as 'attendance_from3'
	,max(case when (period_id=3 or period_id=8) then attendance_time_to else null end) as 'attendance_to3'
	,max(case when (period_id=3 or period_id=8) then is_authorized_absent else null end) as 'attendance_is_authorized_absent3'
	,max(case when (period_id=3 or period_id=8) then is_leave_absent else null end) as 'attendance_is_leave_absent3'
	,max(case when (period_id=3 or period_id=8) then is_canceled else null end) as 'attendance_is_canceled3'
--			,max(case when (period_id=3 or period_id=8) then is_temporary_return else null end) as 'attendance_is_temporary_return3'
	,max(case when (period_id=3 or period_id=8) then class_work_id else null end) as 'class_work_id3'
	,max(case when (period_id=3 or period_id=8) then class_work_is_canceled else null end) as 'class_work_is_canceled3'
	,max(case when (period_id=4 or period_id=9) then attendance.id else null end) as 'attendance_id4'
	,max(case when (period_id=4 or period_id=9) then attendance_status_cd else null end) as 'attendance_cd4'
	,max(case when (period_id=4 or period_id=9) then attendance_status_nm else null end) as 'attendance_nm4'
	,max(case when (period_id=4 or period_id=9) then time_table_time_from else null end) as 'time_table_time_from4'
	,max(case when (period_id=4 or period_id=9) then time_table_time_to else null end) as 'time_table_time_to4'
	,max(case when (period_id=4 or period_id=9) then attendance_time_from else null end) as 'attendance_from4'
	,max(case when (period_id=4 or period_id=9) then attendance_time_to else null end) as 'attendance_to4'
	,max(case when (period_id=4 or period_id=9) then is_authorized_absent else null end) as 'attendance_is_authorized_absent4'
	,max(case when (period_id=4 or period_id=9) then is_leave_absent else null end) as 'attendance_is_leave_absent4'
	,max(case when (period_id=4 or period_id=9) then is_canceled else null end) as 'attendance_is_canceled4'
--			,max(case when (period_id=4 or period_id=9) then is_temporary_return else null end) as 'attendance_is_temporary_return4'
	,max(case when (period_id=4 or period_id=9) then class_work_id else null end) as 'class_work_id4'
	,max(case when (period_id=4 or period_id=9) then class_work_is_canceled else null end) as 'class_work_is_canceled4'
    from (
     select
		csw.date as csw_date
		,attendance.person_id
		,class_work.period_id
		,class_work.is_canceled as class_work_is_canceled
		,class_work.id as class_work_id
		,attendance.id
		,attendance.attendance as attendance_status_cd
		,scd.name as attendance_status_nm
		,attendance.time_from as attendance_time_from
		,attendance.time_to as attendance_time_to
		,attendance.is_authorized_absent
		,attendance.is_leave_absent
		,attendance.is_canceled
--				,attendance.is_temporary_return
		,time_table.time_from as time_table_time_from
		,time_table.time_to as time_table_time_to
     from
      attendance
     inner join class_work on class_work.id = attendance.class_work_id and class_work.disable = 0
     inner join class_shift_work csw on csw.id = class_work.class_shift_work_id and csw.disable = 0
     inner join system_code_detail scd on scd.code1 = attendance.attendance and scd.system_code_id=9 and scd.disable = 0
	 inner join class_shift on class_shift.id = csw.class_shift_id and class_shift.disable = 0
	 inner join class on class.id = class_shift.class_id and class.disable = 0
	 left join time_zone on time_zone.id = class.time_zone_id
	 left join time_table_group on time_table_group.id = class.time_table_group_id
	 left join time_table on time_table.time_table_group_id = time_table_group.id and time_table.period_id = class_work.period_id
     where attendance.disable = 0
    )attendance
    group by csw_date,person_id
   )coma on coma.csw_date = csw.date
   inner join student on student.person_id = coma.person_id and student.disable = 0
   inner join person on person.id = coma.person_id and person.disable = 0
   inner join system_code_detail scd on scd.code1 = student.status and scd.system_code_id=2 and scd.disable = 0
   where class.disable = 0
   and class.id >= 172
   and csw.date >= @start_date
   and csw.date <= @end_date
   group by student.person_id,student.student_no,scd.name,csw.date
   ORDER BY student.student_no,csw.date
  )base
  group by student_no
)absent_day on absent_day.student_no = student.student_no
WHERE attendance_rate.disable=0 
AND school_term.start_date= @school_term
AND calculate_date_from>= @start_date
AND calculate_date_to<= @end_date
AND attendance_rate.attendance_cd= @attendance_cd
ORDER BY ability.sequence,class.name,student.student_no
-- ORDER BY student.student_no



