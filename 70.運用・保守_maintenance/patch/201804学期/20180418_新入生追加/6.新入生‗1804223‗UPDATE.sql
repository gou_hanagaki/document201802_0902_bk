﻿SET @from_person_id:=2070;
SET @student_no:='1804223';
SET @to_person_id:=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id:=(SELECT id FROM student WHERE student_no=@student_no);
SET @start_date:='2018-04-19';
SET @end_date_intended:='2020-03-31';
SET @class_id:=259;
SET @school_term_id:=133;
SET @school_term_end_date:=(SELECT end_date FROM school_term WHERE id=@school_term_id);
SET @last_name:='BUI';
SET @first_name:='THI ANH DUONG';
SET @last_name_kana:='ブイ';
SET @first_name_kana:='ティ　アイン　ズォン';
SET @last_name_alphabet:='BUI';
SET @first_name_alphabet:='THI ANH DUONG';

INSERT INTO attendance
(
class_shift_work_id
,class_work_id
,person_id
,lastup_account_id
,create_datetime
)
SELECT
`class_shift_work_id`
,`class_work_id`
,@to_person_id
,2000
,now()
FROM `attendance`
inner join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
WHERE person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date
;

INSERT INTO shift_attendance
(
`person_id`
,`class_shift_work_id`
,`lastup_account_id`
,`create_datetime`
)
select
@to_person_id
,`class_shift_work_id`
,2000
,now()
from `shift_attendance`
inner join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
where person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date
;

INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@school_term_end_date
,2000
,now()
)
;

UPDATE student 
SET
status=4
-- ,student_type_cd=0
-- ,course_id=1
-- ,course_length_month
,start_preferred_date_intended=@school_term_id
,start_date=@start_date
,end_date_intended=@end_date_intended
,end_date='0000-00-00'
,lastup_account_id=2000
,lastup_datetime=now()
WHERE person_id=@to_person_id
;

UPDATE person 
SET
last_name=@last_name
,first_name=@first_name
,last_name_kana=@last_name_kana
,first_name_kana=@first_name_kana
,last_name_alphabet=@last_name_alphabet
,first_name_alphabet=@first_name_alphabet
,lastup_account_id=2000
,lastup_datetime=now()
WHERE person.id=@to_person_id
;

INSERT INTO attendance_rate
(
`student_id`
,`attendance_cd`
,`calculate_date_from`
,`calculate_date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
 (@student_id,1,@start_date,@start_date,2000,now()) -- 入学からの累計
,(@student_id,5,@start_date,@start_date,2000,now()) -- 当月
;

