/* 対象者
1804237　TAMANGSHYAM
変更前：4/17入学　初級１B
　　　　　↓
変更後：5/7入学　初級１F

※4/17から4/27まで出欠は破棄

締め処理前のためattendance_rateは気にしなくて良い
クラス移動の処理で5月以降は初級１Bクラスで既にdisable=1となっているため4月分のデータを論理削除
*/

/* 確認用SQL */
SET @student_no:='1804237';
SET @csw_date:='2018-04-30';
SET @class_id:=256;

SELECT student.*
FROM student
WHERE student.student_no = @student_no
;

SELECT class_student.*
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.date_to <= @csw_date
;

SELECT class_shift_work.date, shift_attendance.*
FROM shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date <= @csw_date
ORDER BY class_shift_work.date, shift_attendance.id
;

SELECT class_shift_work.date, attendance.*
FROM attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date <= @csw_date
ORDER BY class_shift_work.date, attendance.id
;

SELECT class_shift_work.date, class_entry_exit.*
FROM class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date <= @csw_date
ORDER BY class_shift_work.date, class_entry_exit.sequence
;
-- --------------------------------------------------------------------------------------------------------------

/* 更新用SQL */

SET @student_no:='1804237';
SET @start_date:='2018-05-07';
SET @csw_date:='2018-04-30';
SET @class_id:=256;

UPDATE student
SET
 student.start_date = @start_date,
 student.lastup_account_id = 2000,
 student.lastup_datetime = now()
WHERE student.student_no = @student_no
;

UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.disable=1,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.class_id = @class_id
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
SET
 shift_attendance.disable = 1,
 shift_attendance.lastup_account_id = 2000,
 shift_attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date <= @csw_date
and class.id = @class_id
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
LEFT JOIN student ON student.person_id = attendance.person_id
SET
 attendance.disable = 1,
 attendance.lastup_account_id = 2000,
 attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date <= @csw_date
and class.id = @class_id
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
SET
 class_entry_exit.disable=1,
 class_entry_exit.lastup_account_id = 2000,
 class_entry_exit.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date <= @csw_date
and class.id = @class_id
;

