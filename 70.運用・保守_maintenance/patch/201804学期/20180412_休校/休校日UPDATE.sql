SET @csw_date:='2018-04-06';

/*
SELECT date,class.name,class_work.*
FROM class_work
LEFT JOIN class_shift_work ON class_shift_work.id = class_work.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
WHERE class_shift_work.date = @csw_date
AND class.time_zone_id = @time_zone_id
;

SELECT date,class.name,attendance.*
FROM attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
WHERE class_shift_work.date = @csw_date
AND class.time_zone_id = @time_zone_id
;

SELECT date,class.name,class_entry_exit.*
FROM class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
WHERE class_shift_work.date = @csw_date
AND class.time_zone_id = @time_zone_id
;

SELECT date,class.name,class_shift_work.*
FROM class_shift_work
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
WHERE class_shift_work.date = @csw_date
AND class.time_zone_id = @time_zone_id
;
*/

UPDATE class_work
LEFT JOIN class_shift_work ON class_shift_work.id = class_work.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
SET class_work.is_canceled=1,class_work.lastup_account_id=2000,class_work.lastup_datetime=now()
WHERE class_shift_work.date = @csw_date
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
SET shift_attendance.comment='積雪のため休校',shift_attendance.lastup_account_id=2000,shift_attendance.lastup_datetime=now()
WHERE class_shift_work.date = @csw_date
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
SET attendance.is_canceled=1,attendance.lastup_account_id=2000,attendance.lastup_datetime=now()
WHERE class_shift_work.date = @csw_date
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN class_shift ON class_shift.id = class_shift_work.class_shift_id
LEFT JOIN class ON class.id = class_shift.class_id
SET class_entry_exit.disable=1,class_entry_exit.lastup_account_id=2000,class_entry_exit.lastup_datetime=now()
WHERE class_shift_work.date = @csw_date
;

UPDATE school_calendar SET is_school_holiday=1 WHERE date=