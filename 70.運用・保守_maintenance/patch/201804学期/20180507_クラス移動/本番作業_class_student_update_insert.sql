-- ,1710075
SET @student_no:='1710075';
SET @csw_date:='2018-04-30';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

SET @to_person_id:='1507';
SET @start_date:='2018-05-01';
SET @end_date:='2018-06-30';
SET @class_id:='267';
SET @school_term_id:='133';
INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;


-- ,1710068
SET @student_no:='1710068';
SET @csw_date:='2018-04-30';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

SET @to_person_id:='1500';
SET @start_date:='2018-05-01';
SET @end_date:='2018-06-30';
SET @class_id:='268';
SET @school_term_id:='133';
INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;



-- ,1710022
SET @student_no:='1710022';
SET @csw_date:='2018-04-30';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

SET @to_person_id:='1454';
SET @start_date:='2018-05-01';
SET @end_date:='2018-06-30';
SET @class_id:='252';
SET @school_term_id:='133';
INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;


-- ,1710156
SET @student_no:='1710156';
SET @csw_date:='2018-04-30';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

SET @to_person_id:='1588';
SET @start_date:='2018-05-01';
SET @end_date:='2018-06-30';
SET @class_id:='252';
SET @school_term_id:='133';
INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;


-- ,1710088
SET @student_no:='1710088';
SET @csw_date:='2018-04-30';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

SET @to_person_id:='1520';
SET @start_date:='2018-05-01';
SET @end_date:='2018-06-30';
SET @class_id:='252';
SET @school_term_id:='133';
INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;
