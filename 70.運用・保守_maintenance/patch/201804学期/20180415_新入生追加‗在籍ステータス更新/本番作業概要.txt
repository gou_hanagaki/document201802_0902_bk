﻿---------------------------------
クラス追加
---------------------------------
■class_studnet
INSERT

■student
INSERT/UPDATE
status
start_date
end_date

※「99～」から始まる学生で既存登録が無い場合は、画面から新規作成後に学生番号をアップデート

■shift_attendance
INSERT

■attendance
INSERT

---------------------------------
クラス削除
---------------------------------
■class_studnet
UPDATE
date_to
最終日を設定

■student
UPDATE
status
end_date
理由にもよる

■shift_attendance
UPDATE

■attendance
UPDATE

---------------------------------
クラス移動
---------------------------------
■class_studnet
UPDATE・INSERT
期途中の場合は既存クラスでend_dateを更新、新クラスでINSERT

■shift_attendance
UPDATE

■attendance
UPDATE
class_work_id
class_shift_work_id

■class_studnet
UPDATE

■class_entry_exit
UPDATE
期途中で翌月から変更なら移行不要。つまりいつからかが重要（日に紐付いているので旧クラスのままで良い）

---------------------------------
退学
---------------------------------
■class_studnet
UPDATE
date_to
退学日を設定

■student
UPDATE
status
end_date

■shift_attendance
UPDATE
disable=1

■attendance
UPDATE
disable=1

■class_entry_exit
UPDATE
disable=1
