/* 対象者
1604189　退学　退学日：2018/3/6
*/

/* 在籍状態
SELECT system_code_detail.code1, system_code_detail.name, system_code_detail.column_name
FROM system_code_detail
WHERE system_code_detail.system_code_id = 2
AND system_code_detail.disable = 0;

code1: name column_name
1: 予備入学 preliminary_admission
2: 入学辞退 declining
3: 不合格 failure
4: 在籍 enrollment
5: 卒業 graduate
6: 修了 completion
7: 退学 withdrawal
8: 除籍 expulsion
10: 入学希望 admission_request
11: 不入学 non_admission
12: 強制退学 forced_withdrawal
*/

/* 退学・除籍理由
SELECT system_code_detail.code1, system_code_detail.name, system_code_detail.column_name
FROM system_code_detail
WHERE system_code_detail.system_code_id = 23
AND system_code_detail.disable = 0;

★column_nameがおかしいので、今回はcodeを直接指定とする 
code1	name	column_name
1	就職	full-time_faculty_staff
2	進学	part-time_faculty_staff
3	帰国	special_lecturer
4	自主退学	office_worker
5	ビザ変更	part-time_faculty_staff
6	ビザ期限切れ	special_lecturer
7	学費未納	office_worker
8	長期欠席	part-time_faculty_staff
9	体調不良	special_lecturer
10	違法行為	office_worker
11	結婚	part-time_faculty_staff
12	妊娠	special_lecturer
13	個人都合	office_worker
14	家庭都合	part-time_faculty_staff
15	連絡なし	special_lecturer
*/


/* 確認用SQL */

SET @student_no:='1604189';
SET @csw_date:='2018-03-06';
SET @status_column_name:='withdrawal';
SET @quit_reason_cd:='0';
-- SET @quit_reason_cd_column_name:='';
SET @reason_for_exclusion:='';

SELECT student.*
FROM student
WHERE student.student_no = @student_no
;

SELECT class_student.*
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.date_to >= @csw_date
;

SELECT class_shift_work.date, shift_attendance.*
FROM shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date > @csw_date
ORDER BY class_shift_work.date, shift_attendance.id
;

SELECT class_shift_work.date, attendance.*
FROM attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date > @csw_date
ORDER BY class_shift_work.date, attendance.id
;

SELECT class_shift_work.date, class_entry_exit.*
FROM class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date > @csw_date
ORDER BY class_shift_work.date, class_entry_exit.sequence
;
-- --------------------------------------------------------------------------------------------------------------

/* 更新用SQL */

SET @student_no:='1604189';
SET @csw_date:='2018-03-06';
SET @status_column_name:='withdrawal';
SET @quit_reason_cd:='0';
-- SET @quit_reason_cd_column_name:='';
SET @reason_for_exclusion:='';

UPDATE student
SET
 student.status = (SELECT system_code_detail.code1 FROM system_code_detail WHERE system_code_detail.system_code_id = 2 AND system_code_detail.disable = 0 AND system_code_detail.column_name = @status_column_name),
 student.end_date = @csw_date,
 student.quit_reason_cd = @quit_reason_cd,
 student.reason_for_exclusion = @reason_for_exclusion,
 student.lastup_account_id = 2000,
 student.lastup_datetime = now()
WHERE student.student_no = @student_no
;

UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.date_to > @csw_date
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
SET
 shift_attendance.disable = 1,
 shift_attendance.lastup_account_id = 2000,
 shift_attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date > @csw_date
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = attendance.person_id
SET
 attendance.disable = 1,
 attendance.lastup_account_id = 2000,
 attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date > @csw_date
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
SET
 class_entry_exit.disable=1,
 class_entry_exit.lastup_account_id = 2000,
 class_entry_exit.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date > @csw_date
;

