STEP1.System8Ìf[^ði[·éìÆe[uÌì¬
STEP2.ìÆe[uÉsystem8_1_student_base.csvðimport
STEP3.UPDATEOÌstudentðæ¾
STEP4.system_codeEsystem_code_detailEstudentÌUPDATE
STEP5.UPDATEãÌstudentðæ¾
STEP6.·ª`FbN
STEP7.ìÆe[uðí

-- ------------------------------------------------------------------------------------------------------------
-- work table
-- ------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS system8_1_student_base;
CREATE TABLE `system8_1_student_base` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`wÐÔ` text NOT NULL,
`o^Ô` text NOT NULL,
`{¼Ì` text NOT NULL,
`{¼1` text NOT NULL,
`{¼2` text NOT NULL,
`tKi¼` text NOT NULL,
`«Ê` text NOT NULL,
`¢ù¥` text NOT NULL,
`¼` text NOT NULL,
`¶Nú` text NOT NULL,
`ÐîÒ¼` text NOT NULL,
`{ZÌ` text NOT NULL,
`{Z` text NOT NULL,
`{db` text NOT NULL,
`·Ô` text NOT NULL,
`·LøúÀ` text NOT NULL,
`üNú` text NOT NULL,
`¶VISAíÞ` text NOT NULL,
`VISAXVú(1)` text NOT NULL,
`VISAXVú(2)` text NOT NULL,
`VISAXVú(3)` text NOT NULL,
`VISAXVú(4)` text NOT NULL,
`üÇÂÔ` text NOT NULL,
`üÇÂú` text NOT NULL,
`Ý¯J[hÔ` text NOT NULL,
`üwú` text NOT NULL,
`²C\èú` text NOT NULL,
`R[X¼` text NOT NULL,
`NX¼` text NOT NULL,
`ÞwNú` text NOT NULL,
`ÐNú` text NOT NULL,
`ÞwÐR` text NOT NULL,
`ÝÐ²Ææª` text NOT NULL,
`ÈOZ§` text NOT NULL,
`ÈOZZ` text NOT NULL,
`»Z§` text NOT NULL,
`»ZZ` text NOT NULL,
`»Zdb` text NOT NULL,
`g@Ñdb` text NOT NULL,
`¯NÛ¯Ô` text NOT NULL,
`áQÛ¯L³` text NOT NULL,
`êê` text NOT NULL,
`²ÆãiHæ` text NOT NULL,
`ÐîÒÌ` text NOT NULL,
`ÐîÒ¼1` text NOT NULL,
`ÐîÒ¼2` text NOT NULL,
`ÐîÒZ1` text NOT NULL,
`ÐîÒZ2` text NOT NULL,
`ÐîÒTEL` text NOT NULL,
`ÐîÒFAX` text NOT NULL,
`J   Ã½Ä` text NOT NULL,
`NAT Ã½Ä` text NOT NULL,
`üwÃ½Ä` text NOT NULL,
`gªÃ½Ä` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ------------------------------------------------------------------------------------------------------------
-- 2018/04/19update
-- ------------------------------------------------------------------------------------------------------------
UPDATE system_code SET disable=1,lastup_account_id=2000,lastup_datetime=now() WHERE column_name='course'
;
UPDATE system_code_detail
LEFT JOIN system_code ON system_code.id=system_code_detail.system_code_id
SET system_code_detail.disable=1,system_code_detail.lastup_account_id=2000,system_code_detail.lastup_datetime=now()
WHERE system_code.column_name='course'
;

UPDATE student
LEFT JOIN (
	SELECT
		`wÐÔ` as student_no
		,`R[X¼` as course
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
student.course_id=	CASE KIHON.course
						WHEN 'iw2N' THEN 1
						WHEN 'iw1N9' THEN 1
						WHEN 'iw1N6' THEN 1
						WHEN 'iw1N3' THEN 1
						WHEN 'Zú6ú{ê¤C' THEN 2
						WHEN 'Zú3ú{ê¤C' THEN 2
						WHEN 'Zú2ú{ê¤C' THEN 2
						WHEN 'Zú1ú{ê¤C' THEN 2
						WHEN 'êÊú{êR[X' THEN 3
						WHEN 'êÊ2N' THEN 4
						WHEN 'êÊ1N' THEN 4
						ELSE 0
					END
,student.lastup_account_id=2000
,student.lastup_datetime=now()
;

UPDATE student
SET course_id=CASE WHEN LEFT(student_no,2)=99 THEN 2 ELSE 1 END
,lastup_account_id=2000
,lastup_datetime=now()
WHERE disable=0 AND status=4 AND course_id=0
;
-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT
	`wÐÔ` as student_no
	,`{¼1`
	,`tKi¼`
	,`{¼2`
	,`R[X¼` as course
FROM system8_1_student_base
ORDER BY student_no
;

SELECT 
	student_no
	,CONCAT(last_name,first_name)
	,CONCAT(last_name_kana,first_name_kana)
	,CONCAT(last_name_alphabet,first_name_alphabet)
	,course_id
	,course.name
	,course_length.name
FROM student
LEFT JOIN person ON person.id=student.person_id AND person.disable=0
LEFT JOIN course ON course.id=student.course_id AND course.disable=0
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='course_length'
)course_length ON course_length.code1=student.course_length_month
WHERE student.disable=0
ORDER BY student_no
;