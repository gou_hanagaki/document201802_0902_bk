﻿SET @from_person_id:=2086;
SET @to_person_id:=2309;
SET @class_id:=258;
SET @school_term_id:=133;
SET @start_date:='2018-04-17';
SET @end_date:='2018-06-30';

INSERT INTO attendance
(
class_shift_work_id
,class_work_id
,person_id
,lastup_account_id
,create_datetime
)
SELECT
`class_shift_work_id`
,`class_work_id`
,@to_person_id
,2000
,now()
FROM `attendance`
inner join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
WHERE person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date
;

INSERT INTO shift_attendance
(
`person_id`
,`class_shift_work_id`
,`lastup_account_id`
,`create_datetime`
)
select
@to_person_id
,`class_shift_work_id`
,2000
,now()
from `shift_attendance`
inner join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
where person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date
;

INSERT INTO class_student
(
`school_term_id`
,`person_id`
,`class_id`
,`date_from`
,`date_to`
,`lastup_account_id`
,`create_datetime`
)
VALUES
(
@school_term_id
,@to_person_id
,@class_id
,@start_date
,@end_date
,2000
,now()
)
;

UPDATE student 
SET
status=4
-- ,student_type_cd=0
-- ,course_id=1
-- ,course_length_month
,start_preferred_date_intended=@school_term_id
,start_date=@start_date
,end_date_intended='2020-03-31'
,end_date='0000-00-00'
,lastup_account_id=2000
,lastup_datetime=now()
WHERE person_id=@to_person_id
;

/*
UPDATE person 
SET
last_name='SHRESTHA'
,first_name='SIDDHARTHA'
,last_name_kana='シュレスタ'
,first_name_kana='シッダルタ'
,last_name_alphabet='SHRESTHA'
,first_name_alphabet='SIDDHARTHA'
,lastup_account_id=2000
,lastup_datetime=now()
WHERE person.id=@to_person_id
;
*/