/* 
�@各学生の聴解（問題１）の点数入力
�A各学生の読解（問題６）の点数入力 
*/

/*更新前確認*/
SET @add_score:='18';
SET @test_section_cd:='8';
SET @content_test_list_id:='54';
SELECT *
FROM `examination_answer`
WHERE `achievement_id` IN (9110,9111,9112,9113,9114,9115,9116,9117,9118,9119,9120,9121,9122) AND `test_section_cd` = @test_section_cd
;

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id
;


/* DB更新 */
SET @add_score:='18';
SET @test_section_cd:='8';
SET @content_test_list_id:='54';
UPDATE examination_answer 
SET
 score = '2',
 is_correct = '1'
WHERE achievement_id IN (9110,9111,9112,9113,9114,9115,9116,9117,9118,9119,9120,9121,9122) AND test_section_cd = @test_section_cd
;

UPDATE achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
SET 
 a.total_score = a.total_score + @add_score,
 ad.score = @add_score,
 ad.score_original = @add_score,
 cts.total_score = cts.total_score + @add_score
WHERE a.content_test_list_id = @content_test_list_id
;

/*更新後確認*/
SET @add_score:='18';
SET @test_section_cd:='8';
SET @content_test_list_id:='54';
SELECT *
FROM `examination_answer`
WHERE `achievement_id` IN (9110,9111,9112,9113,9114,9115,9116,9117,9118,9119,9120,9121,9122) AND `test_section_cd` = @test_section_cd
;

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id
;
-----------------------------------------------------------------------------------------------------------------
/* 
「1807184の学生」の��20の問題を正解にしてください。
*/

/*更新前確認*/
SET @add_score:='18';
SET @content_test_list_id:='54';
SET @student_no:='1807184';
SET @content_test_question_id:='1210';
SET @test_section_cd:='6';

SELECT s.student_no,a.total_score,ad.score,ad.score_original,cts.total_score,ea.score,ea.is_correct FROM achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id AND ea.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE 
s.student_no = @student_no AND ea.content_test_question_id = @content_test_question_id
;


/* DB更新 */
SET @add_score:='2';
SET @content_test_list_id:='54';
SET @student_no:='1807184';
SET @content_test_question_id:='1210';
SET @test_section_cd:='6';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id
SET 
 a.total_score = a.total_score + @add_score,
 ea.score = '2',
 ea.is_correct = '1',
 ad.score = ad.score + @add_score,
 ad.score_original = ad.score_original + @add_score,
 cts.total_score = cts.total_score + @add_score
WHERE
s.student_no = @student_no 
AND ea.content_test_question_id = @content_test_question_id
AND cts.content_test_list_id = @content_test_list_id
AND ad.test_section_cd = @test_section_cd
AND ea.test_section_cd = @test_section_cd
;

/*更新後確認*/
SET @add_score:='18';
SET @content_test_list_id:='54';
SET @student_no:='1807184';
SET @content_test_question_id:='1210';
SET @test_section_cd:='6';

SELECT s.student_no,a.total_score,ad.score,ad.score_original,cts.total_score,ea.score,ea.is_correct FROM achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id AND ea.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE 
s.student_no = @student_no AND ea.content_test_question_id = @content_test_question_id
;
-----------------------------------------------------------------------------------------------------------------
/* 
�B読解（問題５）の設問の種類の変更- 問題の配点変更
*/
SET @add_score:='6';/* 配点2 * 3問で6点 */
SET @test_section_cd_before:='6';/* 文字語彙 */
SET @test_section_cd_after:='8';/* 文作 */

/* 更新前確認 */
SELECT *
FROM `content_test_question`
WHERE `content_test_list_id` = '54' AND `id` IN (1223,1224,1225)
;

SELECT *
FROM `content_test_section`
WHERE `content_test_list_id` = '54'
;
/* DB更新 */
UPDATE content_test_question
SET test_section_cd = '8'
WHERE `content_test_list_id` = '54' AND `id` IN (1223,1224,1225)
;

UPDATE content_test_section
SET total_point = total_point - @add_score
WHERE test_section_cd = @test_section_cd_before
;

UPDATE content_test_section
SET total_point = total_point + @add_score
WHERE test_section_cd = @test_section_cd_after
;

/* 更新前確認 */
SELECT *
FROM `content_test_question`
WHERE `content_test_list_id` = '54' AND `id` IN (1223,1224,1225)
;

SELECT *
FROM `content_test_section`
WHERE `content_test_list_id` = '54'
;


/* 
�B読解（問題５）の設問の種類の変更- 学生毎の配点変更
*/

/* �B-1 examination_answer @test_section_cdの一括更新 */

SELECT *
FROM `examination_answer`
WHERE `achievement_id` IN (9110,9111,9112,9113,9114,9115,9116,9117,9118,9119,9120,9121,9122) AND `content_test_question_id` IN (1223,1224,1225)
ORDER BY `achievement_id`
;

SET @test_section_cd_after:='8';/* 文作 */
UPDATE examination_answer ea
LEFT JOIN achievement a ON ea.achievement_id = a.id
SET ea.test_section_cd = @test_section_cd_after
WHERE a.id IN (9110,9111,9112,9113,9114,9115,9116,9117,9118,9119,9120,9121,9122) AND `content_test_question_id` IN (1223,1224,1225)
;

