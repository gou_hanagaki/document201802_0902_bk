/* 
1804039
*/

/*更新前確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9110' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;


/* DB更新 */
SET @add_score:='4';
SET @content_test_list_id:='54';
SET @student_no:='1804039';
SET @content_test_question_id_1:='1193';
SET @content_test_question_id_2:='1194';
SET @test_section_cd:='8';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id
SET 
 a.total_score = a.total_score - @add_score,
 ea.score = '0',
 ea.is_correct = '0',
 ad.score = ad.score - @add_score,
 ad.score_original = ad.score_original - @add_score,
 cts.total_score = cts.total_score - @add_score
WHERE
s.student_no = @student_no 
AND ea.content_test_question_id IN (@content_test_question_id_1,@content_test_question_id_2)
AND cts.content_test_list_id = @content_test_list_id
AND ad.test_section_cd = @test_section_cd
AND ea.test_section_cd = @test_section_cd
;


/*更新後確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9110' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';
SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id

-----------------------------------------------------------------------------------------------------------------
/* 
「1807184の学生」
*/

/*更新前確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9117' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;



/* DB更新 */
SET @add_score:='4';
SET @content_test_list_id:='54';
SET @student_no:='1807184';
SET @content_test_question_id_1:='1226';
SET @content_test_question_id_2:='1228';
SET @test_section_cd:='8';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id
SET 
 a.total_score = a.total_score - @add_score,
 ea.score = '0',
 ea.is_correct = '0',
 ad.score = ad.score - @add_score,
 ad.score_original = ad.score_original - @add_score,
 cts.total_score = cts.total_score - @add_score
WHERE
s.student_no = @student_no 
AND ea.content_test_question_id IN (@content_test_question_id_1,@content_test_question_id_2)
AND cts.content_test_list_id = @content_test_list_id
AND ad.test_section_cd = @test_section_cd
AND ea.test_section_cd = @test_section_cd
;


/*更新後確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9117' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;

-----------------------------------------------------------------------------------------------------------------

/* 
1810060
*/

/*更新前確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9118' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;



/* DB更新 */
SET @add_score:='2';
SET @content_test_list_id:='54';
SET @student_no:='1810060';
SET @content_test_question_id_1:='1196';
SET @test_section_cd:='8';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id
SET 
 a.total_score = a.total_score - @add_score,
 ea.score = '0',
 ea.is_correct = '0',
 ad.score = ad.score - @add_score,
 ad.score_original = ad.score_original - @add_score,
 cts.total_score = cts.total_score - @add_score
WHERE
s.student_no = @student_no 
AND ea.content_test_question_id = @content_test_question_id_1
AND cts.content_test_list_id = @content_test_list_id
AND ad.test_section_cd = @test_section_cd
AND ea.test_section_cd = @test_section_cd
;


/*更新後確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9118' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;

-----------------------------------------------------------------------------------------------------------------
/* 
1904097
*/

/*更新前確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9120' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;



/* DB更新 */
SET @add_score:='2';
SET @content_test_list_id:='54';
SET @student_no:='1904097';
SET @content_test_question_id_1:='1226';
SET @test_section_cd:='8';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id
SET 
 a.total_score = a.total_score - @add_score,
 ea.score = '0',
 ea.is_correct = '0',
 ad.score = ad.score - @add_score,
 ad.score_original = ad.score_original - @add_score,
 cts.total_score = cts.total_score - @add_score
WHERE
s.student_no = @student_no 
AND ea.content_test_question_id = @content_test_question_id_1
AND cts.content_test_list_id = @content_test_list_id
AND ad.test_section_cd = @test_section_cd
AND ea.test_section_cd = @test_section_cd
;


/*更新後確認*/
SELECT *
FROM `examination_answer`
WHERE `achievement_id` = '9120' AND `test_section_cd` = '8' AND `content_test_question_id` IN (1193,1194);
SET @test_section_cd:='8';
SET @content_test_list_id:='54';

SELECT a.total_score,ad.score,ad.score_original,cts.total_score
FROM achievement a
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd = @test_section_cd
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE a.content_test_list_id = @content_test_list_id;

-----------------------------------------------------------------------------------------------------------------