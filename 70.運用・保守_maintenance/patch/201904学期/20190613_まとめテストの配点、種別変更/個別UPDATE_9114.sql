/* �B-2 examination_answer @test_section_cdの一括更新 */
SET @content_test_list_id:='54';
SET @content_test_question_id:='1223,1224,1225';
SET @test_section_cd_before:='6';/* 文字語彙 */
SET @test_section_cd_after:='8';/* 文作 */

SET @achievement_id:='9114';
SET @calc_point:='6';

SELECT s.student_no,a.id,a.total_score as a_total_score, cts.total_score as cts_total_score ,ad.test_section_cd as test_section_cd, ad.score as ad_score,ad.score_original as ad_score_original 
FROM achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id 
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id 
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE 
a.id = @achievement_id AND ea.content_test_question_id IN (@content_test_question_id)
AND ad.test_section_cd IN (@test_section_cd_before,@test_section_cd_after)
;

UPDATE achievement_detail ad
SET 
ad.score = ad.score - @calc_point,
ad.score_original = ad.score_original - @calc_point
WHERE ad.achievement_id = @achievement_id AND ad.test_section_cd = @test_section_cd_before
;
UPDATE achievement_detail ad
SET 
ad.score = ad.score + @calc_point,
ad.score_original = ad.score_original + @calc_point
WHERE ad.achievement_id = @achievement_id AND ad.test_section_cd = @test_section_cd_after
;

SELECT s.student_no,a.id,a.total_score as a_total_score, cts.total_score as cts_total_score ,ad.test_section_cd as test_section_cd, ad.score as ad_score,ad.score_original as ad_score_original 
FROM achievement a
LEFT JOIN student s ON s.person_id = a.person_id
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id 
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id 
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id AND cts.content_test_list_id = @content_test_list_id
WHERE 
a.id = @achievement_id AND ea.content_test_question_id IN (@content_test_question_id)
AND ad.test_section_cd IN (@test_section_cd_before,@test_section_cd_after)
;

