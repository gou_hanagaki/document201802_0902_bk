/* �X�V�Ώ�SELECT */

SET @login_id:= 'C190601';

SELECT *
FROM person p
LEFT JOIN account a ON p.id = a.person_id
LEFT JOIN staff s ON s.person_id = p.id
LEFT JOIN staff_ability sa ON sa.staff_id = s.id
LEFT JOIN staff_qualification sq ON sq.staff_id = s.id
WHERE a.login_id = @login_id;


UPDATE person p
LEFT JOIN account a ON p.id = a.person_id
LEFT JOIN staff s ON s.person_id = p.id
LEFT JOIN staff_ability sa ON sa.staff_id = s.id
LEFT JOIN staff_qualification sq ON sq.staff_id = s.id
SET p.disable = '1',
a.disable = '1',
s.disable = '1',
sa.disable = '1',
sq.disable = '1'
WHERE a.login_id = @login_id;