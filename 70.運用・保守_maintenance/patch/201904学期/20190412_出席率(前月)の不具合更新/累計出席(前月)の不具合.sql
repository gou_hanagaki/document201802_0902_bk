
/* �@attendance_cd 1,2 で累計出席に差分がある　かつ　3月月締め対象の学生を抽出 */
SELECT s.student_no,s.status, s.id, ar1.rate_time as '出席率(昨日)' , ar1.calculate_date_to as '出席率(昨日)_date_to' , ar2.rate_time as '出席率(前月)' , ar2.calculate_date_to as '出席率(前月)_date_to', (CAST(ar2.total_time AS SIGNED) - CAST(ar1.total_time AS SIGNED))
FROM student s
LEFT JOIN attendance_rate ar1 ON s.id = ar1.student_id
LEFT JOIN attendance_rate ar2 ON ar2.student_id = s.id and ar2.attendance_cd = '2'
WHERE 
ar1.attendance_cd = '1'
AND ar2.rate_time != ar1.rate_time 
AND ar1.calculate_date_to LIKE '2019-03%'
AND ar2.calculate_date_to LIKE '2019-03%'
ORDER BY s.student_no;



/* �Aattendance_cd=3　かつ　3月月締め対象の重複レコードを削除 */
SELECT s.student_no, attendance_rate.*
FROM `attendance_rate`
INNER JOIN student s ON s.id = attendance_rate.student_id
WHERE `calculate_date_to` = '2019-03-19' AND `attendance_cd` = '3';

SELECT s.student_no, attendance_rate.*
FROM `attendance_rate`
INNER JOIN student s ON s.id = attendance_rate.student_id
WHERE `calculate_date_to` = '2019-03-20' AND `attendance_cd` = '3';



UPDATE attendance_rate
SET disable = '1'
WHERE calculate_date_to = '2019-03-19' AND `attendance_cd` = '3';

UPDATE attendance_rate
SET disable = '1'
WHERE calculate_date_to = '2019-03-20' AND `attendance_cd` = '3';

/* �B　画面より出席率更新（全員）と月締め処理(3月)を実施 */

/* �C　attendance_cd=1 が最新化されてないデータが抽出される */
SELECT s.student_no,s.status, s.id, ar1.rate_time as '出席率(昨日)' , ar1.calculate_date_to as '出席率(昨日)_date_to' , ar2.rate_time as '出席率(前月)' , ar2.calculate_date_to as '出席率(前月)_date_to', (CAST(ar2.total_time AS SIGNED) - CAST(ar1.total_time AS SIGNED))
FROM student s
LEFT JOIN attendance_rate ar1 ON s.id = ar1.student_id
LEFT JOIN attendance_rate ar2 ON ar2.student_id = s.id and ar2.attendance_cd = '2'
WHERE 
ar1.attendance_cd = '1'
AND ar2.rate_time != ar1.rate_time 
AND ar1.calculate_date_to LIKE '2019-03%'
AND ar2.calculate_date_to LIKE '2019-03%'
ORDER BY s.student_no;

/* �D　上記抽出された学生を出席率更新(学籍番号毎)でattendance_cd=1を最新化 */


/* �E　再度、下記SQLを実行し、差分がないことを確認 */
SELECT s.student_no,s.status, s.id, ar1.rate_time as '出席率(昨日)' , ar1.calculate_date_to as '出席率(昨日)_date_to' , ar2.rate_time as '出席率(前月)' , ar2.calculate_date_to as '出席率(前月)_date_to', (CAST(ar2.total_time AS SIGNED) - CAST(ar1.total_time AS SIGNED))
FROM student s
LEFT JOIN attendance_rate ar1 ON s.id = ar1.student_id
LEFT JOIN attendance_rate ar2 ON ar2.student_id = s.id and ar2.attendance_cd = '2'
WHERE 
ar1.attendance_cd = '1'
AND ar2.rate_time != ar1.rate_time 
AND ar1.calculate_date_to LIKE '2019-03%'
AND ar2.calculate_date_to LIKE '2019-03%'
ORDER BY s.student_no;




------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------

SET @student_no:='1610046';
SET @student_no:='1704082';
SET @student_no:='1707060';
SET @student_no:='1710139';
SET @student_no:='1804019';


UPDATE attendance_rate ar2
LEFT JOIN attendance_rate ar1 ON ar1.student_id = ar2.student_id AND ar1.attendance_cd = '1'
LEFT JOIN student s ON s.id = ar2.student_id

SET ar2.rate_time = ar1.rate_time,
ar2.total_time = ar1.total_time,
ar2.attend_time = ar1.attend_time,
ar2.absent_time = ar1.absent_time,
ar2.late_time = ar1.late_time,
ar2.rate_day = ar1.rate_day,
ar2.total_day = ar1.total_day,
ar2.attend_day = ar1.attend_day,
ar2.absent_day = ar1.absent_day,
ar2.lastup_account_id=2000, 
ar2.lastup_datetime=now()
WHERE ar2.attendance_cd = '2' and s.student_no =@student_no
;


