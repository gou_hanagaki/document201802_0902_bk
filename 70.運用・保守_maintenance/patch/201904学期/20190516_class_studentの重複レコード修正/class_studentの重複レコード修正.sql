SET @person_id := '2566';
SET @date_to1:= '2018-06-22';
SET @date_to2:= '2018-09-28';
SET @date_to3:= '2018-10-31';
SET @date_to4:= '2018-11-30';
SET @date_to5:= '2018-12-20';
SET @date_to6:= '2019-01-31';
SET @date_to7:= '2019-03-18';

UPDATE class_student
SET date_to = @date_to1
WHERE person_id = @person_id
AND id = '4944';

UPDATE class_student
SET date_to = @date_to2
WHERE person_id = @person_id
AND id = '5898';

UPDATE class_student
SET date_to = @date_to3
WHERE person_id = @person_id
AND id = '7258';

UPDATE class_student
SET date_to = @date_to4
WHERE person_id = @person_id
AND id = '8438';

UPDATE class_student
SET date_to = @date_to5
WHERE person_id = @person_id
AND id = '9686';

UPDATE class_student
SET date_to = @date_to6
WHERE person_id = @person_id
AND id = '10469';

UPDATE class_student
SET date_to = @date_to7
WHERE person_id = @person_id
AND id = '11725';
