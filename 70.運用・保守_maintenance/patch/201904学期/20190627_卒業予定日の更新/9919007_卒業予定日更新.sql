SET @student_no:= '9919007';
SET @end_date:= '2019-09-30';


UPDATE student
SET end_date_intended = @end_date
WHERE student_no = @student_no;