/* 
�@achivement_detail の重複レコードを論理削除
 */

/* 確認 */
SET @student_no:='1804108';
SET @total_score:='73';
SET @content_test_list_id:='72';
SELECT ad.*
FROM achievement_detail ad
LEFT JOIN achievement a ON a.id = ad.achievement_id AND a.content_test_list_id = @content_test_list_id
LEFT JOIN student s ON s.person_id = a.person_id
WHERE s.student_no = @student_no;
;

/* DB更新 */
SET @student_no:='1804108';
SET @total_score:='73';
SET @content_test_list_id:='72';
UPDATE achievement_detail ad
LEFT JOIN achievement a ON a.id = ad.achievement_id AND a.content_test_list_id = @content_test_list_id
LEFT JOIN student s ON s.person_id = a.person_id
SET ad.disable ='1'
WHERE s.student_no = @student_no
AND ad.id IN ('22146','22147')
;

/* 
�Aexamination_answerテーブルの重複レコードを物理削除（EXCEL参照）
 */


/* 
�B　achievement、achievement_detail、content_test_statusテーブルの合計点(score)を更新
 */

/* 確認 */
SET @student_no:='1804108';
SET @total_score:='73';
SET @content_test_list_id:='72';
SET @test_section_cd:='1';
SELECT  s.student_no,a.total_score,ad.score,ad.score_original,cts.total_score, SUM(ea.score)
FROM achievement a
LEFT JOIN student s ON s.person_id = a.person_id 
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd =@test_section_cd AND ad.disable ='0' 
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id AND ea.person_id = a.person_id AND ea.disable ='0'
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id  AND cts.content_test_list_id = @content_test_list_id
WHERE 
a.content_test_list_id =@content_test_list_id AND s.student_no = @student_no
;

/* DB更新 */
SET @student_no:='1804108';
SET @total_score:='73';
SET @content_test_list_id:='72';
SET @test_section_cd:='1';
UPDATE achievement a
LEFT JOIN student s ON s.person_id = a.person_id 
LEFT JOIN achievement_detail ad ON ad.achievement_id = a.id AND ad.test_section_cd =@test_section_cd AND ad.disable ='0'
LEFT JOIN examination_answer ea ON ea.achievement_id = a.id AND ea.person_id = a.person_id AND ea.disable ='0'
LEFT JOIN content_test_status cts ON cts.person_id = a.person_id  AND cts.content_test_list_id = @content_test_list_id
SET 
 a.total_score = @total_score,
 ad.score = @total_score,
 ad.score_original = @total_score,
 cts.total_score = @total_score
WHERE
a.content_test_list_id =@content_test_list_id AND s.student_no = @student_no;