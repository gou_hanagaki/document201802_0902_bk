※attendance,shift_attendanceは6/22以降レコード作成がなかったため、class_student,studentのみ変更


SET @csw_date:='2018-06-22';
SET @school_term:='133';

SELECT *
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no in ('9918004','9918005','9918006','9918007')
AND class_student.disable = 0
AND class_student.school_term_id  = @school_term
AND class_student.date_from <= @csw_date
AND class_student.date_to >= @csw_date;

SELECT *
FROM student
WHERE student.student_no in ('9918004','9918005','9918006','9918007');


-----------------------------------------------------------------------------------------------------------------

SET @csw_date:='2018-06-22';
SET @school_term:='133';
UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no  in ('9918004','9918005','9918006','9918007')
AND class_student.disable = 0
AND class_student.school_term_id  = @school_term
AND class_student.date_from <= @csw_date
AND class_student.date_to >= @csw_date

;
SET @csw_date:='2018-06-22';
SET @school_term:='133';
UPDATE student
SET status = '7', 
end_date = @csw_date,
final_judgement = '9',
final_judgement_all_attend = '9',
final_judgement_all_absent = '9',
lastup_account_id=2000,
lastup_datetime=now()
WHERE student_no in ('9918004','9918005','9918006','9918007')
;

