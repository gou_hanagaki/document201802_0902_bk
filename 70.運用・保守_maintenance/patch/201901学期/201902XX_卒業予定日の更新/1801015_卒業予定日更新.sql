SET @student_no:= '1801015';
SET @end_date:= '2019-03-31';


UPDATE student
SET end_date_intended = @end_date
WHERE student_no = @student_no;