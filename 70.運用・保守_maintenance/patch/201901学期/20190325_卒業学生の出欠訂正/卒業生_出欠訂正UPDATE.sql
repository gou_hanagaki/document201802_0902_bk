SET @student_no:=1710088;
SET @csw_date:='2019-03-11';


select shift_attendance.*
from shift_attendance
left join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
left join student on student.person_id = shift_attendance.person_id
WHERE student.student_no = @student_no
and class_shift_work.date = @csw_date
;

select attendance.*
from attendance
left join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
left join student on student.person_id = attendance.person_id
WHERE student.student_no = @student_no
and class_shift_work.date = @csw_date
;

select class_entry_exit.*
from class_entry_exit
left join class_shift_work on class_shift_work.id = class_entry_exit.class_shift_work_id
left join student on student.person_id = class_entry_exit.person_id
WHERE student.student_no = @student_no
and class_shift_work.date = @csw_date
;
-----------------------------------------------------------------------------------------------------------------

SET @student_no:=1710088;
SET @csw_date:='2019-03-11';
SET @attendance:='4';
SET @attendance_id1:='1017657';
SET @attendance_id2:='1017658';
SET @time_from1:='10:40:00';
SET @time_to1:='11:24:59';
SET @time_from2:='11:35:00';
SET @time_to2:='12:19:59';
SET @entry_time:= '10:30:00';


UPDATE attendance
SET attendance.attendance = @attendance,attendance.time_from = @time_from1, attendance.time_to = @time_to1,lastup_account_id=2000, attendance.lastup_datetime=now()
WHERE attendance.id = @attendance_id1
;

UPDATE attendance
SET attendance.attendance = @attendance,attendance.time_from = @time_from2, attendance.time_to = @time_to2,lastup_account_id=2000, attendance.lastup_datetime=now()
WHERE attendance.id = @attendance_id2
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student on student.person_id = class_entry_exit.person_id
SET class_entry_exit.entry_time = @entry_time, class_entry_exit.lastup_account_id=2000,class_entry_exit.lastup_datetime=now()
WHERE student.student_no = @student_no
AND class_shift_work.date = @csw_date
AND class_entry_exit.sequence = '0'
;
