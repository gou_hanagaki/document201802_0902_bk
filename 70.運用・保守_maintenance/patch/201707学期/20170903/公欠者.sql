set @rownum:=0;
set @person_id:=null;
select
	base.time_zone_name
	,base.time_table_group_name
	,base.class_name as '�N���X��'
	,base.student_no as '�w�Дԍ�'
/*
	,base.name as '����'
	,base.kana as '�����J�i'
	,base.student_status_nm as '�ݐЏ�'
	,base.date as '���t'
	,case shift_attendance.is_authorized_absent when 1 then '�Z' else '' end as '����'
	,case shift_attendance.is_leave_absent when 1 then '�Z' else '' end as '�x�w'
	,shift_attendance.comment as '�R�����g'
	,entry_time1 as '��������1'
--	,'' as '�����p'
	,exit_time1 as '�ގ�����1'
--	,'' as '�����p'
	,entry_time2 as '��������2'
--	,'' as '�����p'
	,exit_time2 as '�ގ�����2'
--	,'' as '�����p'
	,entry_time3 as '��������3'
--	,'' as '�����p'
	,exit_time3 as '�ގ�����3'
--	,'' as '�����p'
*/
	,attendance_id1,attendance_cd1,attendance_is_authorized_absent1,attendance_is_leave_absent1,attendance_is_canceled1
--	,class_work_id1,class_work_is_canceled1
--	,time_table_time_from1,time_table_time_to1
	,attendance_nm1 as '1or6�R�}',attendance_from1 as '1or6�R�}��',attendance_to1 as '1or6�R�}��'
--	,'' as '�����p','' as '�����p'
	,attendance_id2,attendance_cd2,attendance_is_authorized_absent2,attendance_is_leave_absent2,attendance_is_canceled2
--	,class_work_id2,class_work_is_canceled2
--	,time_table_time_from2,time_table_time_to2
--	,attendance_nm2 as '2or7�R�}',attendance_from2 as '2or7�R�}��',attendance_to2 as '2or7�R�}��'
--	,'' as '�����p','' as '�����p'
	,attendance_id3,attendance_cd3,attendance_is_authorized_absent3,attendance_is_leave_absent3,attendance_is_canceled3
--	,class_work_id3,class_work_is_canceled3
--	,time_table_time_from3,time_table_time_to3
--	,attendance_nm3 as '3or8�R�}',attendance_from3 as '3or8�R�}��',attendance_to3 as '3or8�R�}��'
--	,'' as '�����p','' as '�����p'
	,attendance_id4,attendance_cd4,attendance_is_authorized_absent4,attendance_is_leave_absent4,attendance_is_canceled4
--	,class_work_id4,class_work_is_canceled4
--	,time_table_time_from4,time_table_time_to4
--	,attendance_nm4 as '4or9�R�}',attendance_from4 as '4or9�R�}��',attendance_to4 as '4or9�R�}��'
--	,'' as '�����p','' as '�����p'
--	,base.class_time_zone_id,base.class_time_table_group_id
--	,base.class_id,base.person_id,base.class_shift_work_id
	,shift_attendance.id,shift_attendance.is_authorized_absent,shift_attendance.is_leave_absent
from (
	select
		class.name as class_name
		,class.id as class_id
		,class.time_zone_id as class_time_zone_id
		,time_zone.name as time_zone_name
		,class.time_table_group_id as class_time_table_group_id
		,time_table_group.name as time_table_group_name
		,student.student_no
		,CONCAT(person.last_name,person.first_name) as name
		,CONCAT(person.last_name_kana,person.first_name_kana) as kana
		,student.status as student_status_cd
		,scd.name as student_status_nm
		,csw.date as date
		,coma.*
	from
		class
	inner join class_shift on class_shift.class_id = class.id and class_shift.disable = 0
	inner join class_shift_work csw on csw.class_shift_id = class_shift.id and csw.disable = 0
	left join (
		select
			class_shift_work_id
			,person_id
			,max(case when (period_id=1 or period_id=6) then attendance.id else null end) as 'attendance_id1'
			,max(case when (period_id=1 or period_id=6) then attendance_status_cd else null end) as 'attendance_cd1'
			,max(case when (period_id=1 or period_id=6) then attendance_status_nm else null end) as 'attendance_nm1'
			,max(case when (period_id=1 or period_id=6) then time_table_time_from else null end) as 'time_table_time_from1'
			,max(case when (period_id=1 or period_id=6) then time_table_time_to else null end) as 'time_table_time_to1'
			,max(case when (period_id=1 or period_id=6) then attendance_time_from else null end) as 'attendance_from1'
			,max(case when (period_id=1 or period_id=6) then attendance_time_to else null end) as 'attendance_to1'
			,max(case when (period_id=1 or period_id=6) then is_authorized_absent else null end) as 'attendance_is_authorized_absent1'
			,max(case when (period_id=1 or period_id=6) then is_leave_absent else null end) as 'attendance_is_leave_absent1'
			,max(case when (period_id=1 or period_id=6) then is_canceled else null end) as 'attendance_is_canceled1'
			,max(case when (period_id=1 or period_id=6) then class_work_id else null end) as 'class_work_id1'
			,max(case when (period_id=1 or period_id=6) then class_work_is_canceled else null end) as 'class_work_is_canceled1'
			,max(case when (period_id=2 or period_id=7) then attendance.id else null end) as 'attendance_id2'
			,max(case when (period_id=2 or period_id=7) then attendance_status_cd else null end) as 'attendance_cd2'
			,max(case when (period_id=2 or period_id=7) then attendance_status_nm else null end) as 'attendance_nm2'
			,max(case when (period_id=2 or period_id=7) then time_table_time_from else null end) as 'time_table_time_from2'
			,max(case when (period_id=2 or period_id=7) then time_table_time_to else null end) as 'time_table_time_to2'
			,max(case when (period_id=2 or period_id=7) then attendance_time_from else null end) as 'attendance_from2'
			,max(case when (period_id=2 or period_id=7) then attendance_time_to else null end) as 'attendance_to2'
			,max(case when (period_id=2 or period_id=7) then is_authorized_absent else null end) as 'attendance_is_authorized_absent2'
			,max(case when (period_id=2 or period_id=7) then is_leave_absent else null end) as 'attendance_is_leave_absent2'
			,max(case when (period_id=2 or period_id=7) then is_canceled else null end) as 'attendance_is_canceled2'
			,max(case when (period_id=2 or period_id=7) then class_work_id else null end) as 'class_work_id2'
			,max(case when (period_id=2 or period_id=7) then class_work_is_canceled else null end) as 'class_work_is_canceled2'
			,max(case when (period_id=3 or period_id=8) then attendance.id else null end) as 'attendance_id3'
			,max(case when (period_id=3 or period_id=8) then attendance_status_cd else null end) as 'attendance_cd3'
			,max(case when (period_id=3 or period_id=8) then attendance_status_nm else null end) as 'attendance_nm3'
			,max(case when (period_id=3 or period_id=8) then time_table_time_from else null end) as 'time_table_time_from3'
			,max(case when (period_id=3 or period_id=8) then time_table_time_to else null end) as 'time_table_time_to3'
			,max(case when (period_id=3 or period_id=8) then attendance_time_from else null end) as 'attendance_from3'
			,max(case when (period_id=3 or period_id=8) then attendance_time_to else null end) as 'attendance_to3'
			,max(case when (period_id=3 or period_id=8) then is_authorized_absent else null end) as 'attendance_is_authorized_absent3'
			,max(case when (period_id=3 or period_id=8) then is_leave_absent else null end) as 'attendance_is_leave_absent3'
			,max(case when (period_id=3 or period_id=8) then is_canceled else null end) as 'attendance_is_canceled3'
			,max(case when (period_id=3 or period_id=8) then class_work_id else null end) as 'class_work_id3'
			,max(case when (period_id=3 or period_id=8) then class_work_is_canceled else null end) as 'class_work_is_canceled3'
			,max(case when (period_id=4 or period_id=9) then attendance.id else null end) as 'attendance_id4'
			,max(case when (period_id=4 or period_id=9) then attendance_status_cd else null end) as 'attendance_cd4'
			,max(case when (period_id=4 or period_id=9) then attendance_status_nm else null end) as 'attendance_nm4'
			,max(case when (period_id=4 or period_id=9) then time_table_time_from else null end) as 'time_table_time_from4'
			,max(case when (period_id=4 or period_id=9) then time_table_time_to else null end) as 'time_table_time_to4'
			,max(case when (period_id=4 or period_id=9) then attendance_time_from else null end) as 'attendance_from4'
			,max(case when (period_id=4 or period_id=9) then attendance_time_to else null end) as 'attendance_to4'
			,max(case when (period_id=4 or period_id=9) then is_authorized_absent else null end) as 'attendance_is_authorized_absent4'
			,max(case when (period_id=4 or period_id=9) then is_leave_absent else null end) as 'attendance_is_leave_absent4'
			,max(case when (period_id=4 or period_id=9) then is_canceled else null end) as 'attendance_is_canceled4'
			,max(case when (period_id=4 or period_id=9) then class_work_id else null end) as 'class_work_id4'
			,max(case when (period_id=4 or period_id=9) then class_work_is_canceled else null end) as 'class_work_is_canceled4'
		from (
			select
				attendance.class_shift_work_id
				,attendance.person_id
				,class_work.period_id
				,class_work.is_canceled as class_work_is_canceled
				,class_work.id as class_work_id
				,attendance.id
				,attendance.attendance as attendance_status_cd
				,scd.name as attendance_status_nm
				,attendance.time_from as attendance_time_from
				,attendance.time_to as attendance_time_to
				,attendance.is_authorized_absent
				,attendance.is_leave_absent
				,attendance.is_canceled
				,time_table.time_from as time_table_time_from
				,time_table.time_to as time_table_time_to
			from
				attendance
			inner join system_code_detail scd on scd.code1 = attendance.attendance and scd.system_code_id=9 and scd.disable = 0
			inner join class_work on class_work.id = attendance.class_work_id and class_work.disable = 0
			inner join class_shift_work csw on csw.id = class_work.class_shift_work_id and csw.disable = 0
			inner join class_shift on class_shift.id = csw.class_shift_id and class_shift.disable = 0
			inner join class on class.id = class_shift.class_id and class.disable = 0
			left join time_zone on time_zone.id = class.time_zone_id
			left join time_table_group on time_table_group.id = class.time_table_group_id
			left join time_table on time_table.time_table_group_id = time_table_group.id and time_table.period_id = class_work.period_id
			where attendance.disable = 0
			order by class_work_id,person_id,period_id
		)attendance
		group by class_shift_work_id,person_id
	)coma on coma.class_shift_work_id = csw.id
	inner join student on student.person_id = coma.person_id and student.disable = 0
	inner join person on person.id = coma.person_id and person.disable = 0
	inner join system_code_detail scd on scd.code1 = student.status and scd.system_code_id=2 and scd.disable = 0
	inner join time_zone on time_zone.id = class.time_zone_id
	inner join time_table_group on time_table_group.id = class.time_table_group_id
	where class.disable = 0
	and csw.date >= '2017-08-01'
	and csw.date <= '2017-08-31'
	group by student.person_id,student.student_no,scd.name,csw.date
	ORDER BY class.id,student.student_no,csw.date
)base
left join (
	select
		class_shift_work_id
		,person_id
		,max(case rownum when 1 then id else null end) as class_entry_exit_id1
		,max(case rownum when 1 then entry_time else null end) as entry_time1
		,max(case rownum when 1 then exit_time else null end) as exit_time1
		,max(case rownum when 2 then id else null end) as class_entry_exit_id2
		,max(case rownum when 2 then entry_time else null end) as entry_time2
		,max(case rownum when 2 then exit_time else null end) as exit_time2
		,max(case rownum when 3 then id else null end) as class_entry_exit_id3
		,max(case rownum when 3 then entry_time else null end) as entry_time3
		,max(case rownum when 3 then exit_time else null end) as exit_time3
	from (
		select
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
		  	,@person_id:=person_id as person_id
			,class_shift_work_id
			,entry_time 
			,exit_time
			,id
		from
			class_entry_exit
		where class_entry_exit.disable = 0
		order by class_shift_work_id,person_id
	)class_entry_exit
	group by class_shift_work_id,person_id
)entry_exit on entry_exit.class_shift_work_id = base.class_shift_work_id and entry_exit.person_id = base.person_id
left join shift_attendance on shift_attendance.class_shift_work_id = base.class_shift_work_id and shift_attendance.person_id = base.person_id
where shift_attendance.is_authorized_absent = 1
