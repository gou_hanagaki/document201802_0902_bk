set @from_date:='2017-07-11';
set @to_date:='2017-07-31';
set @rownum:=0;
set @person_id:=null;
select
	base.class_id
	,base.person_id
	,base.student_id
	,base.student_no
	,entry_time1
	,base.class_name as 'クラス名'
	,base.student_no as '学籍番号'
	,base.name as '氏名'
	,base.kana as '氏名カナ'
	,base.status as '在籍状況'
	,base.date as '日付'
	,case shift_attendance.is_authorized_absent when 1 then '〇' else '' end as '公休'
	,shift_attendance.comment as '公休コメント'
	,cee_id1,entry_time1 as '入室時間1',exit_time1 as '退室時間1'
	,cee_id2,entry_time2 as '入室時間2',exit_time2 as '退室時間2'
	,cee_id3,entry_time3 as '入室時間3',exit_time3 as '退室時間3'
	,1coma as '1コマ目',1coma_in as '1コマ目入室',1coma_out as '1コマ目退室'
	,2coma as '2コマ目',2coma_in as '2コマ目入室',2coma_out as '2コマ目退室'
	,3coma as '3コマ目',3coma_in as '3コマ目入室',3coma_out as '3コマ目退室'
	,4coma as '4コマ目',4coma_in as '4コマ目入室',4coma_out as '4コマ目退室'
	,6coma as '6コマ目',6coma_in as '6コマ目入室',6coma_out as '6コマ目退室'
	,7coma as '7コマ目',7coma_in as '7コマ目入室',7coma_out as '7コマ目退室'
	,8coma as '8コマ目',8coma_in as '8コマ目入室',8coma_out as '8コマ目退室'
	,9coma as '9コマ目',9coma_in as '9コマ目入室',9coma_out as '9コマ目退室'
from (
	select
		class.id as class_id
		,student.id as student_id
		,class.name as class_name
		,student.student_no
		,CONCAT(person.last_name,person.first_name) as name
		,CONCAT(person.last_name_kana,person.first_name_kana) as kana
		,scd.name as status
		,csw.date as date
		,coma.*
	from
		class
	inner join class_shift on class_shift.class_id = class.id and class_shift.disable = 0
	inner join class_shift_work csw on csw.class_shift_id = class_shift.id and csw.disable = 0
	left join (
		select
			class_shift_work_id
			,person_id
			,max(case period_id when 1 then attendance.in_out else null end) as '1coma'
			,max(case period_id when 1 then time_from else null end) as '1coma_in'
			,max(case period_id when 1 then time_to else null end) as '1coma_out'
			,max(case period_id when 2 then attendance.in_out else null end) as '2coma'
			,max(case period_id when 2 then time_from else null end) as '2coma_in'
			,max(case period_id when 2 then time_to else null end) as '2coma_out'
			,max(case period_id when 3 then attendance.in_out else null end) as '3coma'
			,max(case period_id when 3 then time_from else null end) as '3coma_in'
			,max(case period_id when 3 then time_to else null end) as '3coma_out'
			,max(case period_id when 4 then attendance.in_out else null end) as '4coma'
			,max(case period_id when 4 then time_from else null end) as '4coma_in'
			,max(case period_id when 4 then time_to else null end) as '4coma_out'
			,max(case period_id when 6 then attendance.in_out else null end) as '6coma'
			,max(case period_id when 6 then time_from else null end) as '6coma_in'
			,max(case period_id when 6 then time_to else null end) as '6coma_out'
			,max(case period_id when 7 then attendance.in_out else null end) as '7coma'
			,max(case period_id when 7 then time_from else null end) as '7coma_in'
			,max(case period_id when 7 then time_to else null end) as '7coma_out'
			,max(case period_id when 8 then attendance.in_out else null end) as '8coma'
			,max(case period_id when 8 then time_from else null end) as '8coma_in'
			,max(case period_id when 8 then time_to else null end) as '8coma_out'
			,max(case period_id when 9 then attendance.in_out else null end) as '9coma'
			,max(case period_id when 9 then time_from else null end) as '9coma_in'
			,max(case period_id when 9 then time_to else null end) as '9coma_out'
		from (
			select
				class_shift_work_id
				,person_id
				,period_id
				,attendance
				,scd.name as 'in_out'
				,time_from
				,time_to
			from
				attendance
			inner join class_work on class_work.id = attendance.class_work_id and class_work.disable = 0
			inner join class_shift_work csw on csw.id = class_work.class_shift_work_id and csw.disable = 0
			inner join system_code_detail scd on scd.code1 = attendance.attendance and scd.system_code_id=9 and scd.disable = 0
			where attendance.disable = 0
			order by class_work_id,person_id,period_id
		)attendance
		group by class_shift_work_id,person_id
	)coma on coma.class_shift_work_id = csw.id
	inner join student on student.person_id = coma.person_id and student.disable = 0
	inner join person on person.id = coma.person_id and person.disable = 0
	inner join system_code_detail scd on scd.code1 = student.status and scd.system_code_id=2 and scd.disable = 0
	where class.disable = 0
	and class.id >= 145
	and csw.date >= @from_date
	and csw.date <= @to_date
	group by student.person_id,student.student_no,scd.name,csw.date,class.id
	ORDER BY class.id,student.student_no,csw.date
)base
left join (
	select
		class_shift_work_id
		,person_id
		,max(case rownum when 1 then id else null end) as cee_id1
		,max(case rownum when 1 then entry_time else null end) as entry_time1
		,max(case rownum when 1 then exit_time else null end) as exit_time1
		,max(case rownum when 2 then id else null end) as cee_id2
		,max(case rownum when 2 then entry_time else null end) as entry_time2
		,max(case rownum when 2 then exit_time else null end) as exit_time2
		,max(case rownum when 3 then id else null end) as cee_id3
		,max(case rownum when 3 then entry_time else null end) as entry_time3
		,max(case rownum when 3 then exit_time else null end) as exit_time3
	from (
		select
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
		  	,@person_id:=person_id as person_id
			,class_shift_work_id
			,entry_time 
			,exit_time
			,id
		from
			class_entry_exit
		where class_entry_exit.disable = 0
		order by class_shift_work_id,person_id
	)cee
	group by class_shift_work_id,person_id
)entry_exit on entry_exit.class_shift_work_id = base.class_shift_work_id and entry_exit.person_id = base.person_id
left join shift_attendance on shift_attendance.class_shift_work_id = base.class_shift_work_id and shift_attendance.person_id = base.person_id
