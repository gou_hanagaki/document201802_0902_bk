select
	attendance.person_id
	,student_no
	,csw.id as class_shift_work_id
	,CONCAT(p.last_name,p.first_name) as name
	,CONCAT(p.last_name_kana,p.first_name_kana) as kana
	,scd.name as status
	,start_date
	,end_date_intended
	,end_date
	,csw.date as date
	,c.name as class_name
	,c.id
	,attendance.id
from
	class c
inner join class_shift cs on cs.class_id = c.id
inner join class_shift_work csw on csw.class_shift_id = cs.id
inner join class_work cw on cw.class_shift_work_id = csw.id
left join attendance on attendance.class_work_id = cw.id
inner join student s on s.person_id = attendance.person_id
inner join person p on p.id = attendance.person_id
inner join system_code_detail scd on scd.code1 = s.status and scd.system_code_id=2
where true
AND c.id >= 145
AND csw.date >= '2017-07-11' 
AND csw.date <= '2017-07-31'
AND csw.date < s.start_date
ORDER BY c.id,student_no,csw.date