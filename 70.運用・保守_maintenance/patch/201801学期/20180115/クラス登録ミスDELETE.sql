SET @student_no:=1604172;


select shift_attendance.*
from shift_attendance
left join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
left join student on student.person_id = shift_attendance.person_id
WHERE student_no = @student_no
and shift_attendance.disable=1;

select attendance.*
from attendance
left join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
left join student on student.person_id = attendance.person_id
WHERE student_no = @student_no
and attendance.disable=1;

select class_entry_exit.*
from class_entry_exit
left join class_shift_work on class_shift_work.id = class_entry_exit.class_shift_work_id
left join student on student.person_id = class_entry_exit.person_id
WHERE student_no = @student_no
and class_entry_exit.disable=1;





DELETE target_table FROM shift_attendance target_table
LEFT JOIN class_shift_work ON class_shift_work.id = target_table.class_shift_work_id
LEFT JOIN student on student.person_id = target_table.person_id
WHERE student_no = @student_no
and target_table.disable=1;

DELETE target_table FROM attendance target_table
LEFT JOIN class_shift_work ON class_shift_work.id = target_table.class_shift_work_id
LEFT JOIN student on student.person_id = target_table.person_id
WHERE student_no = @student_no
and target_table.disable=1;

DELETE target_table FROM class_entry_exit target_table
LEFT JOIN class_shift_work ON class_shift_work.id = target_table.class_shift_work_id
LEFT JOIN student on student.person_id = target_table.person_id
WHERE student_no = @student_no
and target_table.disable=1;



