/* 対象者
1801026　蔡琴　１月２３日入学（１月２３日から出席を取る）ということにして頂きたいです。
*/



/* 確認用SQL */

SET @student_no:='1801026';
SET @csw_date:='2018-01-23';

SELECT student.*
FROM student
WHERE student.student_no = @student_no
;

SELECT class_student.*
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.date_to >= @csw_date
;

SELECT class_shift_work.date, shift_attendance.*
FROM shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date < @csw_date
ORDER BY class_shift_work.date, shift_attendance.id
;

SELECT class_shift_work.date, attendance.*
FROM attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = attendance.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date < @csw_date
ORDER BY class_shift_work.date, attendance.id
;

SELECT class_shift_work.date, class_entry_exit.*
FROM class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
WHERE student.student_no = @student_no
AND class_shift_work.date < @csw_date
ORDER BY class_shift_work.date, class_entry_exit.sequence
;
-- --------------------------------------------------------------------------------------------------------------

/* 更新用SQL */

SET @student_no:='1801026';
SET @csw_date:='2018-01-23';

UPDATE student
SET
 student.start_date = @csw_date,
 student.lastup_account_id = 2000,
 student.lastup_datetime = now()
WHERE student.student_no = @student_no
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = shift_attendance.person_id
SET
 shift_attendance.disable = 1,
 shift_attendance.lastup_account_id = 2000,
 shift_attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date < @csw_date
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student ON student.person_id = attendance.person_id
SET
 attendance.disable = 1,
 attendance.lastup_account_id = 2000,
 attendance.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date < @csw_date
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student ON student.person_id = class_entry_exit.person_id
SET
 class_entry_exit.disable=1,
 class_entry_exit.lastup_account_id = 2000,
 class_entry_exit.lastup_datetime = now()
WHERE student.student_no = @student_no
and class_shift_work.date < @csw_date
;

