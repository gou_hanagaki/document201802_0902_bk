﻿INSERT INTO attendance
(
class_shift_work_id
,class_work_id
,person_id
,attendance
,time_from
,time_to
,lastup_account_id
,create_datetime
,lastup_datetime
,disable
)
SELECT
`class_shift_work_id`
,`class_work_id`
,2041
,0
,'0000-00-00 00:00:00'
,'0000-00-00 00:00:00'
,0
,now()
,now()
,0
FROM `attendance`
inner join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
WHERE person_id = 1122
and class.id = 223
and class_shift_work.date >= '2018-03-01'
-- and class_shift_work.date <= '2018-02-28'
;

INSERT INTO shift_attendance
(
`person_id`
,`class_shift_work_id`
,`is_authorized_absent`
,`comment`
,`lastup_account_id`
,`create_datetime`
,`lastup_datetime`
,`disable`
)
select
2041
,`class_shift_work_id`
,0
,''
,0
,now()
,now()
,0
from `shift_attendance`
inner join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
WHERE person_id = 1122
and class.id = 223
and class_shift_work.date >= '2018-03-01'
-- and class_shift_work.date <= '2018-02-28'
;
