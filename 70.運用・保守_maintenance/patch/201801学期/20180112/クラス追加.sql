﻿SET @from_person_id:=1345;
SET @to_person_id:=1439;
SET @class_id:=189;
SET @start_date:='2017-12-04';

INSERT INTO attendance
(
class_shift_work_id
,class_work_id
,person_id
,attendance
,time_from
,time_to
,lastup_account_id
,create_datetime
)
SELECT
`class_shift_work_id`
,`class_work_id`
,@to_person_id
,0
,'0000-00-00 00:00:00'
,'0000-00-00 00:00:00'
,2000
,now()
FROM `attendance`
inner join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
WHERE person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date;

INSERT INTO shift_attendance
(
`person_id`
,`class_shift_work_id`
,`is_authorized_absent`
,`comment`
,`lastup_account_id`
,`create_datetime`
)
select
@to_person_id
,`class_shift_work_id`
,0
,''
,2000
,now()
from `shift_attendance`
inner join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
inner join class_shift on class_shift.id = class_shift_work.class_shift_id
inner join class on class.id = class_shift.class_id
where person_id = @from_person_id
and class.id = @class_id
and class_shift_work.date >= @start_date;
