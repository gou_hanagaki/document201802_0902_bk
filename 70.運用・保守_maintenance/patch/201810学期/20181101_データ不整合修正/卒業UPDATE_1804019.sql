SET @student_no:='1804019';
SET @csw_date:='2018-10-01';
SET @school_term:='135';

SELECT *
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.disable = 1
AND class_student.school_term_id  = @school_term
and class_student.id = 7347
;

select shift_attendance.*,class_shift_work.date
from shift_attendance
left join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
left join student on student.person_id = shift_attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and shift_attendance.id IN (237228,237238,237248,237258,237268,237229,237239,237249,237259,237269,237230,237240,237250)
ORDER BY date
;

select attendance.*,class_shift_work.date
from attendance
left join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
left join student on student.person_id = attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and attendance.id IN (957541,957542,957539,957540,957580,957581,957582,957579,957619,957620,957621,957622,957662,957659,957660,957661,957700,957701,957702,957699,957546,957543,957544,957545,957585,957586,957583,957584,957624,957625,957626,957623,957663,957664,957665,957666,957705,957706,957703,957704,957547,957548,957549,957550,957590,957587,957588,957589,957629,957630,957627,957628)

ORDER BY date
;

select class_entry_exit.*,class_shift_work.date
from class_entry_exit
left join class_shift_work on class_shift_work.id = class_entry_exit.class_shift_work_id
left join student on student.person_id = class_entry_exit.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and class_entry_exit.disable = 1
ORDER BY date
;

-------------------------------------------------------------------
SET @student_no:='1804019';
SET @csw_date:='2018-10-01';
SET @school_term:='135';

UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.disable = 0
WHERE student.student_no = @student_no
AND class_student.id  = 7347
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student on student.person_id = shift_attendance.person_id
SET shift_attendance.disable= 0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and shift_attendance.disable = 1
and shift_attendance.id IN (237228,237238,237248,237258,237268,237229,237239,237249,237259,237269,237230,237240,237250)
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student on student.person_id = attendance.person_id
SET attendance.disable= 0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and attendance.disable = 1
and attendance.id IN (957541,957542,957539,957540,957580,957581,957582,957579,957619,957620,957621,957622,957662,957659,957660,957661,957700,957701,957702,957699,957546,957543,957544,957545,957585,957586,957583,957584,957624,957625,957626,957623,957663,957664,957665,957666,957705,957706,957703,957704,957547,957548,957549,957550,957590,957587,957588,957589,957629,957630,957627,957628)
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student on student.person_id = class_entry_exit.person_id
SET class_entry_exit.disable= 0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and class_entry_exit.disable = 1


