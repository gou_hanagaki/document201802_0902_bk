SET @student_no:='1704113';
SET @csw_date:='2018-10-01';
SET @school_term:='135';

SELECT *
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.disable = 1
AND class_student.school_term_id  = @school_term
;

select shift_attendance.*,class_shift_work.date
from shift_attendance
left join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
left join student on student.person_id = shift_attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and shift_attendance.disable = 1
ORDER BY date
;

select attendance.*,class_shift_work.date
from attendance
left join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
left join student on student.person_id = attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and attendance.disable = 1
ORDER BY date
;

select class_entry_exit.*,class_shift_work.date
from class_entry_exit
left join class_shift_work on class_shift_work.id = class_entry_exit.class_shift_work_id
left join student on student.person_id = class_entry_exit.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and class_entry_exit.disable = 1
ORDER BY date
;

-------------------------------------------------------------------
SET @student_no:='1704113';
SET @csw_date:='2018-10-01';
SET @school_term:='135';

UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET class_student.disable = 0
WHERE student.student_no = @student_no
AND class_student.id  = 6908
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student on student.person_id = shift_attendance.person_id
SET shift_attendance.disable=0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and shift_attendance.disable = 1
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student on student.person_id = attendance.person_id
SET attendance.disable=0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and attendance.disable = 1
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student on student.person_id = class_entry_exit.person_id
SET class_entry_exit.disable=0
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
and class_entry_exit.disable = 1
;

