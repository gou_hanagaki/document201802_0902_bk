SET @new_holyday1:='2019-04-30';
SET @new_holyday2:='2019-05-01';
SET @new_holyday3:='2019-05-02';
SET @new_holyday4:='2019-10-22';
SET @non_holyday:='2019-12-23';
SET @new_holyday5:='2019-08-12';


SELECT *
FROM school_calendar
WHERE date IN (@new_holyday1,@new_holyday2,@new_holyday3,@new_holyday4,@non_holyday,@new_holyday5);



-----------------------------------------------------------------------------
SET @new_holyday1:='2019-04-30';
SET @new_holyday2:='2019-05-01';
SET @new_holyday3:='2019-05-02';
SET @new_holyday4:='2019-10-22';
SET @non_holyday:='2019-12-23';
SET @new_holyday5:='2019-08-12';

UPDATE school_calendar
SET is_public_holiday = '1', is_school_holiday = '1'
WHERE date IN (@new_holyday1,@new_holyday2,@new_holyday3,@new_holyday4,@new_holyday5);


UPDATE school_calendar
SET is_public_holiday = '0', is_school_holiday ='0'
WHERE date = @non_holyday;
