SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_get_student_info`;
delimiter //
CREATE PROCEDURE `sp_get_student_info`(
    IN `param_student_no` VARCHAR(128)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Get current info of student'
BEGIN
    SELECT person_id, id INTO @person_id, @student_id FROM student WHERE student_no = param_student_no and disable = 0;
    /* get current class */
    SET @class_id = COALESCE((SELECT class_id FROM class_student WHERE disable = 0 and person_id = @person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1), '');
    IF  @class_id = '' THEN
        SET @class_id =  COALESCE((SELECT class_id FROM class_student WHERE disable = 0 and person_id = @person_id and date_to < CURRENT_DATE() order by date_to desc, date_from desc, id desc limit 1), '');
    END IF;
    IF  @class_id <> '' THEN
         SET @jlpt_level = (SELECT ETL.name 
                                FROM 
                                    class C 
                                    inner join class_group CG on C.class_group_id = CG.id
                                    inner join ability A on A.id = CG.ability_id
                                    inner join external_test_level ETL on ETL.id = A.external_test_level_id
                                where C.id = @class_id and ETL.`disable` = 0 and A.`disable` = 0 and CG.`disable` = 0);
    END IF;
    SET @class_name = (SELECT name FROM class WHERE id = @class_id);
    SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
    SET @teacher_account_id = (SELECT id FROM account WHERE person_id = @teacher_person_id);
    SET @teacher_name = (SELECT CONCAT(last_name, ' ', first_name) as full_name FROM person WHERE id = @teacher_person_id);
    SET @total_leave_absent = (SELECT count(DISTINCT CSW.date) FROM shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id WHERE person_id = @person_id and SA.is_leave_absent = 1);
     SELECT residence_card_no, permit_other_activitiy_cd, end_date INTO @residence_card_no, @permit_other_activitiy_cd, @visa_expired_date FROM visa WHERE person_id = @person_id AND disable = 0 ORDER BY id DESC LIMIT 1;
     SET @sales_staff_person_id = (SELECT sales_staff_person_id FROM student WHERE student_no = param_student_no and disable = 0);

    SELECT
          P.last_name,
          P.first_name,
        CONCAT(P.last_name, ' ', P.first_name) as full_name,
        P.last_name_kana,
        P.first_name_kana,
        CONCAT(P.last_name_kana, ' ', P.first_name_kana) as full_name_kana,
        P.last_name_alphabet,
        P.first_name_alphabet,
        CONCAT(P.last_name_alphabet, ' ', P.first_name_alphabet) as full_name_alphabet,
        P.sex_cd,
        P.birthday,
        P.phone_no,
        P.phone_no2,
        P.mobile_no,
        P.zipcode,
        P.address1,
        P.address2,
        P.address3,
        P.mail_address1,
        P.mail_address2,
        P.sns_account,
        P.sns_type,
        P.`type`,
        C.name as country_name,
        C.english_name as english_country_name,
        I.home_country_phone_no1,
        I.home_country_phone_no2,
        I.home_country_address1,
        I.home_country_address2,
        I.home_country_address3,
        I.other_activity_result,
        A.attend_day,
        A.absent_day,
        A.total_day,
        A.rate_time,
        S.*,
        CH.amount,
        CH.pay_off_date,
        @class_id as class_id,
        @class_name as class_name,
        @teacher_person_id as teacher_person_id,
        @teacher_name as teacher_name,
        @person_id as person_id,
        @total_leave_absent as total_leave_absent,
        @teacher_account_id as teacher_account_id,
        @residence_card_no as residence_card_no,
        @visa_expired_date as visa_expired_date,
        @permit_other_activitiy_cd as permit_other_activitiy_cd,
        @sales_staff_person_id as sales_staff_person_id,
        @jlpt_level as jlpt_level
    FROM
        person P
        LEFT JOIN country C ON P.country_id = C.id
        INNER JOIN student S on P.id = S.person_id
        LEFT JOIN immigration I on P.id = I.person_id
        LEFT JOIN charge CH on CH.student_id = S.id
        LEFT JOIN (SELECT
                         rate_time,
                         student_id,
                         attend_day,
                         absent_day,
                         total_day
                     FROM attendance_rate WHERE student_id= @student_id and attendance_cd = 1) A ON S.id = A.student_id

    WHERE
        P.id = @person_id
    and P.disable = 0
    and S.disable = 0;
END//
delimiter ;