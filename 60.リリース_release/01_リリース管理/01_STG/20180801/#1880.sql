ALTER table `application_official_absence_properties`
ADD COLUMN `authorized_absent_school_section1` VARCHAR(128) NOT NULL DEFAULT '' AFTER `authorized_absent_school_section`,
ADD COLUMN `authorized_absent_school_section2` VARCHAR(128) NOT NULL DEFAULT '' AFTER `authorized_absent_school_section1`,
ADD COLUMN `authorized_absent_laboratory` VARCHAR(128) NOT NULL DEFAULT '' AFTER `authorized_absent_school_section2`;