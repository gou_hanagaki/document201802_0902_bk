
ALTER TABLE `school`
    ADD COLUMN `international_phone_no2` VARCHAR(20)  NULL DEFAULT '' AFTER `international_fax_no`,
    ADD COLUMN `international_fax_no2` VARCHAR(20)  NULL DEFAULT '' AFTER `international_phone_no2`,
    ADD COLUMN `immigr_mng_num_first` VARCHAR(4) NOT NULL DEFAULT '' AFTER `applicant_mobile_number`,
    ADD COLUMN `immigr_mng_num_second` VARCHAR(4) NOT NULL DEFAULT '' AFTER `immigr_mng_num_first`,
    ADD COLUMN `fiscal_year` VARCHAR(4) NOT NULL DEFAULT '' AFTER `immigr_mng_num_second`,
    ADD COLUMN `vacation_period_from1` DATE NOT NULL DEFAULT '0000-00-00' AFTER `fiscal_year`,
    ADD COLUMN `vacation_period_to1` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from1`,
    ADD COLUMN `vacation_period_from2` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to1`,
    ADD COLUMN `vacation_period_to2` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from2`,
    ADD COLUMN `vacation_period_from3` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to2`,
    ADD COLUMN `vacation_period_to3` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from3`,
    ADD COLUMN `vacation_period_from4` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to3`,
    ADD COLUMN `vacation_period_to4` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from4`,
    ADD COLUMN `vacation_period_from5` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to4`,
    ADD COLUMN `vacation_period_to5` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from5`,
    ADD COLUMN `vacation_period_from6` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to5`,
    ADD COLUMN `vacation_period_to6` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from6`,
    ADD COLUMN `vacation_period_from7` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to6`,
    ADD COLUMN `vacation_period_to7` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from7`,
    ADD COLUMN `vacation_period_from8` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to7`,
    ADD COLUMN `vacation_period_to8` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from8`,
    ADD COLUMN `vacation_period_from9` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_to8`,
    ADD COLUMN `vacation_period_to9` DATE NOT NULL DEFAULT '0000-00-00' AFTER `vacation_period_from9`,
    MODIFY COLUMN `country_id` int(10) NULL DEFAULT '0',
    MODIFY COLUMN `zipcode2` varchar(12) NULL DEFAULT '',
    MODIFY COLUMN `phone_no2` varchar(16) NULL DEFAULT '',
    MODIFY COLUMN `fax_no2` varchar(16) NULL DEFAULT '',
    MODIFY COLUMN `address2` varchar(255) NULL DEFAULT '',
    MODIFY COLUMN `english_address2` varchar(128) NULL DEFAULT '',
    MODIFY COLUMN `applicant` varchar(128) NOT NULL DEFAULT '';

/* Set value default*/
set @name       = '学校基本情報メンテナンス';
set @link       = 'staff/school_information_master/show';
set @sequence   = 14;
set @is_in_menu = 1;
set @active     = 0;
/* Set value default:  end*/

/* Set function group システム管理 */
set @setup_function_group_id = 12;
SET @addCol =(SELECT IF((SELECT COUNT(*)
                          FROM `function`
                          WHERE `function`.path = @link and `function`.function_group_id = @setup_function_group_id and `function`.disable = @active
                         )>0,
                         "SELECT 1",
                         "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
                         VALUES (@setup_function_group_id, @name, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Set value default*/
set @name       = '学校基本情報メンテナンス';
set @link       = 'staff/school_information_master/edit';
set @sequence   = 14;
set @is_in_menu = 0;
set @active     = 0;
/* Set value default:  end*/

/* Set function group システム管理 */
set @setup_function_group_id = 12;
SET @addCol =(SELECT IF((SELECT COUNT(*)
                          FROM `function`
                          WHERE `function`.path = @link and `function`.function_group_id = @setup_function_group_id and `function`.disable = @active
                         )>0,
                         "SELECT 1",
                         "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
                         VALUES (@setup_function_group_id, @name, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;