INSERT INTO `function`
            (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
            SELECT 1, '学生進路先検索一覧', 'staff/after_graduation_course/search_list', 1, 5, 9999, NOW(), NOW(), 0 FROM DUAL 
            WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '1' AND path = 'staff/after_graduation_course/search_list' AND DISABLE = 0 );

DELETE FROM  `function` WHERE path = 'staff/student/after_graduation_course_list'  AND DISABLE = 0;