/* Set value default*/
SET @title          = '�h����{��������' ;
SET @link           = 'staff/homework/search_list' ;
SET @sequence       = 5 ;
SET @is_in_menu     = 1 ;
SET @active         = 0 ;
SET @setup_function_group_id = 7 ; /* Set function group ����R���e���c */
SET @add_record = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `function`
            INNER JOIN function_group ON function_group.id = `function`.function_group_id AND function_group.`disable` = @active
            WHERE `function`.`disable` = @active AND `function`.path = @link AND `function`.function_group_id = @setup_function_group_id
        ) > 0,
        "SELECT 1",
        "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@setup_function_group_id, @title, @link, @is_in_menu, @sequence, 0, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @add_record ;
EXECUTE alterIfNotExists ;
DEALLOCATE PREPARE alterIfNotExists ;

/* Set value default*/
SET @title          = '����w�K�^�u' ;
SET @link           = 'staff/homework/show' ;
SET @sequence       = 5 ;
SET @is_in_menu     = 0 ;
SET @setup_function_group_id = 1 ; /* Set function group �w���Ǘ� */

SET @add_record = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `function`
            INNER JOIN function_group ON function_group.id = `function`.function_group_id AND function_group.`disable` = @active
            WHERE `function`.`disable` =  @active AND `function`.path = @link AND `function`.function_group_id = @setup_function_group_id
        ) > 0,
        "SELECT 1",
        "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@setup_function_group_id, @title, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @add_record;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Set value default*/
SET @title          = '����w�K�^�u' ;
SET @link           = 'staff/homework/show' ;
SET @sequence       = 5 ;
SET @is_in_menu     = 0 ;
SET @setup_function_group_id = 15 ; /* Set function group �w���Ǘ� */

SET @add_record = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `function`
            INNER JOIN function_group ON function_group.id = `function`.function_group_id AND function_group.`disable` =  @active
            WHERE `function`.`disable` =  @active AND `function`.path = @link AND `function`.function_group_id = @setup_function_group_id
        ) > 0,
        "SELECT 1",
        "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@setup_function_group_id, @title, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @add_record;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Insert system_code*/
set @name        ='�h��';
set @column_name = 'homework_status';
/* Set value default:  end*/

SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code`
            WHERE `system_code`.column_name = @column_name and `system_code`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code(name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@name, @column_name, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Get id system code*/
SET @system_code_id = (SELECT `id` FROM `system_code` WHERE column_name = 'homework_status' AND system_code.disable = @active);

-- Code1 = 1 read
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'read' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 1, 0, 0, 0, '�ǂ�', 'read', 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Code1 = 2 downloaded
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'downloaded' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 2, 0, 0, 0, '�_�E�����[�h��', 'downloaded', 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Code1 = 5 done
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'done' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 5, 0, 0, 0, '�e�X�g���ʑ��M��', 'done', 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Code1 = 6 sent_test_result
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'sent_test_result' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 6, 0, 0, 0, '�e�X�g���ʖ��_���M��', 'sent_test_result', 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Insert row homework into table test if it not exist
SET @addCol = (SELECT
    IF (
        (
            SELECT COUNT(*)
            FROM `test`
            WHERE `test`.column_name = 'homework' AND `test`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO test(name, column_name, sequence, lastup_account_id, create_datetime, lastup_datetime)
            VALUES ('�h��', 'homework', 0, 2000, NOW(), NOW())" 
    )
);

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Insert system_code homework_is_publish*/
set @name        ='���J/����J';
set @column_name = 'homework_is_publish';

SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code`
            WHERE `system_code`.column_name = @column_name and `system_code`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code(name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@name, @column_name, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Get id system code*/
SET @system_code_id = (SELECT `id` FROM `system_code` WHERE column_name = 'homework_is_publish' AND system_code.disable = @active);

-- public
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'public' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 1, 0, 0, 0, '���J', 'public', 2000, NOW(), NOW())" 
    )
);
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- private
SET @addCol = (SELECT 
    IF (
        (
            SELECT COUNT(*)
            FROM `system_code_detail`
            WHERE `system_code_detail`.system_code_id = @system_code_id AND `column_name` = 'private' AND `system_code_detail`.disable = @active
        ) > 0,
        "SELECT 1",
        "INSERT INTO system_code_detail(system_code_id, code1, code2, code3, sequence, name, column_name, lastup_account_id, create_datetime, lastup_datetime)
            VALUES (@system_code_id, 0, 0, 0, 0, '����J', 'private', 2000, NOW(), NOW())" 
    )
);
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;