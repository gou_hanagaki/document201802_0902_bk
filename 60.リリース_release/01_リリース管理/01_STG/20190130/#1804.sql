CREATE TABLE IF NOT EXISTS `after_graduation_course` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `after_graduation_course_cd` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `person_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `class_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `attendance_rate` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `base_date` DATE NOT NULL DEFAULT '0000-00-00',
    `school_name` VARCHAR(255) NOT NULL DEFAULT '',
    `faculty` VARCHAR(255) NOT NULL DEFAULT '',
    `speciality` VARCHAR(255) NOT NULL DEFAULT '',
    `after_graduation_status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `comment` TEXT NULL,
    `status_exclusive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `recommendation_general` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `test_result` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `company_name` VARCHAR(255) NOT NULL DEFAULT '',
    `lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    INDEX `person_id` (`person_id`),
    INDEX `class_id` (`class_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- START ADD after_graduation_course_cd INTO SYSTEM_CODE
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code
     WHERE column_name = 'after_graduation_course_type'
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code(name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES ('学生情報', 'after_graduation_course_type', 99999999, NOW(), NOW())" 
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @after_graduation_course_type_id = (SELECT id FROM system_code WHERE system_code.column_name = 'after_graduation_course_type');

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'educational_information' AND system_code_id = @after_graduation_course_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@after_graduation_course_type_id, 1, '進学情報', 'educational_information', 99999999, NOW(), NOW())" 
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'job_information' AND system_code_id = @after_graduation_course_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@after_graduation_course_type_id, 2, '就職情報', 'job_information', 99999999, NOW(), NOW())" 
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/*
Update function table
*/
set @title ='学生カルテ詳細(進学・就職)';
set @link  = 'staff/student/after_graduation_course';

/* Set function group 学生用タブレット */
set @function_group = '学生用タブレット';
SET @addCol =(SELECT IF( (SELECT COUNT(*)
     FROM `function`
     WHERE `function`.path = @link  and `function`.function_group_id in (SELECT id
                    FROM function_group
                    WHERE function_group.name = @function_group)
      )>0,
     "SELECT 1",
     "INSERT INTO function(function_group_id, name, path, lastup_account_id, create_datetime, lastup_datetime) 
       VALUES ( (SELECT id
                    FROM function_group
                    WHERE function_group.name = @function_group),
                    @title, @link,2000, NOW(), NOW())" 
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Set function group 学生管理 */
set @function_group = '学生管理';
SET @addCol =(SELECT IF( (SELECT COUNT(*)
     FROM `function`
     WHERE `function`.path = @link and `function`.function_group_id in (SELECT id
                    FROM function_group
                    WHERE function_group.name = @function_group)
      )>0,
     "SELECT 1",
     "INSERT INTO function(function_group_id, name, path, lastup_account_id, create_datetime, lastup_datetime) 
       VALUES ( (SELECT id
                    FROM function_group
                    WHERE function_group.name = @function_group),
                    @title, @link,2000, NOW(), NOW())" 
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;