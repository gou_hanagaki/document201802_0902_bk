SET @systme_name = '���E/���';
SET @system_column_name = "nominate";
SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                            VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @insert_id= LAST_INSERT_ID();

SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 1, 1, '���E���', 'general', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail1;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 3, 2, '���E�w��Z', 'recommend', 2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addColDetail2;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail3 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 4, 3, 'AO', 'ao', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail3;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail4 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 2, 4, '���', 'others', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail4;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/*-------------------------------------------------------------------------------------------*/

SET @systme_name = '���E����';
SET @system_column_name = "exclusive_application";
SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                            VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @insert_id= LAST_INSERT_ID();

SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 1, '���', 'staff_status_exclusive_application', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail1;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 2, '����', 'staff_status_job_application', 2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addColDetail2;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/*-------------------------------------------------------------------------------------------*/

SET @systme_name = '�󌱌���';
SET @system_column_name = "exam_results";
SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                            VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @insert_id= LAST_INSERT_ID();
SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 1, '���i', 'already_check_result', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail1;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 2, '�s���i', 'not_already_check_result', 2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addColDetail2;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/*-------------------------------------------------------------------------------------------*/

SET @systme_name = '�i�H��';
SET @system_column_name = "status_after_graduation";
SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                            VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @insert_id= LAST_INSERT_ID();

SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 1, '���w/����', 'status_after_graduation_admission', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail1;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                       " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                           VALUES (@insert_id, 2, '����', 'status_after_graduation_decline', 2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addColDetail2;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;