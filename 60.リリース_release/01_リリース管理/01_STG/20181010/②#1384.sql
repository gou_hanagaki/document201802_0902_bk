SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_major_university_japanese_language'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_curriculum_university_japanese'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_jlpt_staff'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_teacher_420_hours_japanese_language'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_university_graduation'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_jlpt_student'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_examination_eju'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_practical_japanese_test'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN  external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_pjc_bridge'
AND external_test_level.disable = 0;

SET @i := 0;
update external_test_level
INNER JOIN external_test ON external_test.id = external_test_level.external_test_id  AND external_test.`disable` = 0
set level_sequence = (@i := @i + 1)
WHERE external_test.column_name = 'external_certificate_of_applicant_etc'
AND external_test_level.disable = 0;