ALTER TABLE `attendance_rate`
    ADD INDEX `attendance_cd` (`attendance_cd`),
    ADD INDEX `calculate_date_from` (`calculate_date_from`),
    ADD INDEX `calculate_date_to` (`calculate_date_to`),
    ADD INDEX `lastup_datetime` (`lastup_datetime`);

ALTER TABLE `external_result`
    ADD INDEX `status` (`disable`, `apply_status`, `input_status`, `result_status`);

ALTER TABLE `class_student`
    ADD INDEX `date` (`date_from`,`date_to`);

ALTER TABLE `student`
    ADD INDEX `start_date` (`start_date`),
    ADD INDEX `end_date` (`end_date`);

ALTER TABLE `external_result`
    ADD INDEX `external_test_level_id` (`external_test_level_id`);

ALTER TABLE `student`
    ADD INDEX `agent_id` (`agent_id`);