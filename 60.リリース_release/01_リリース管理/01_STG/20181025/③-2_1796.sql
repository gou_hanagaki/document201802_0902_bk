SET @helper_id = (SELECT id FROM `system_code` WHERE `column_name` = 'helper');
INSERT INTO system_code_detail (`system_code_id`, `code1`,`sequence`, `name`, `column_name`, `create_datetime`, `lastup_datetime`) 
VALUES (@helper_id, 2, 1, 'ヘッダーのヘルプボタン', 'header_helper', NOW(), NOW());