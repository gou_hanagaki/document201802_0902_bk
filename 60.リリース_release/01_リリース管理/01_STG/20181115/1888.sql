INSERT INTO `function`
(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
SELECT 5, 'ajax_get_attend_days_by_school_calendar', 'staff/application/ajax_get_attend_days_by_school_calendar', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '5' AND path = 'staff/application/ajax_get_attend_days_by_school_calendar' AND disable = 0 );

INSERT INTO `function`
(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
SELECT 5, 'ajax_get_attend_days_by_student_real_attendance', 'staff/application/ajax_get_attend_days_by_student_real_attendance', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '5' AND path = 'staff/application/ajax_get_attend_days_by_student_real_attendance' AND disable = 0 );

INSERT INTO `function`
(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
SELECT 15, 'ajax_get_attend_days_by_school_calendar', 'student/application/ajax_get_attend_days_by_school_calendar', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '15' AND path = 'student/application/ajax_get_attend_days_by_school_calendar' AND disable = 0 );

INSERT INTO `function`
(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
SELECT 15, 'ajax_get_attend_days_by_student_real_attendance', 'staff/application/ajax_get_attend_days_by_student_real_attendance', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '15' AND path = 'student/application/ajax_get_attend_days_by_student_real_attendance' AND disable = 0 );