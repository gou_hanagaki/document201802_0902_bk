BEGIN;

ALTER TABLE `student_part_time_job`
    ADD COLUMN `is_main_part_time_job` TINYINT NOT NULL DEFAULT '0' AFTER `person_id`;

UPDATE student_part_time_job ptj
JOIN student st ON st.priority_student_part_time_job_id = ptj.id 
SET is_main_part_time_job = 1
WHERE st.`disable` = 0;

ALTER TABLE `student`
    DROP COLUMN `priority_student_part_time_job_id`;

COMMIT;