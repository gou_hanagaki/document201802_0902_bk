CREATE TABLE `shift_upload` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `display_from` DATE NOT NULL DEFAULT '0000-00-00',
    `display_to` DATE NOT NULL DEFAULT '0000-00-00',
    `upload_file_name` VARCHAR(128) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
    `date_from` DATE NOT NULL DEFAULT '0000-00-00',
    `date_to` DATE NOT NULL DEFAULT '0000-00-00',
    `file_path` VARCHAR(128) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
    `lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
    `disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
);