-- Add column to table student
ALTER TABLE `student`
    ADD COLUMN `is_insured` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `is_use_dormitory`;

-- Create table hospitalization_history
CREATE TABLE `hospitalization_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hospitalization_from` date NOT NULL DEFAULT '0000-00-00',
  `hospitalization_to` date NOT NULL DEFAULT '0000-00-00',
  `hospital_name` varchar(128) NOT NULL DEFAULT '',
  `disease_name_or_reason` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8