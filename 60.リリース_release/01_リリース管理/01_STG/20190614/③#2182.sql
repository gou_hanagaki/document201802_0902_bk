ALTER TABLE `application_temporary_return_properties`
ADD COLUMN `temporary_return_is_confirm` TINYINT (1) UNSIGNED NOT NULL DEFAULT '0' AFTER `temporary_return_comment`;

UPDATE `application_type` SET `name`='一時出国届', `name_1`='一時出国' WHERE `column_name`='temporary_return';