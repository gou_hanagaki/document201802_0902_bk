ALTER TABLE `penalty`
    ADD COLUMN `penalty_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `penalty_content`;

ALTER TABLE `penalty`
    ADD INDEX `person_id` (`person_id`);

SET @system_name = '賞の種類のコンテンツ';
SET @system_column_name = "reward_penalty_comment_type";
SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
SET @addCol = IF (@codeExsit > 0, 'SELECT 1',
" INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`)
VALUES (@system_name, @system_column_name, 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @insert_id= LAST_INSERT_ID();

SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
" INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`)
VALUES (@insert_id, 0, 0, '賞', 'penalty', 2000, NOW(), NOW())");

PREPARE alterIfNotExists FROM @addColDetail1;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
" INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`)
VALUES (@insert_id, 1, 1, '罰', 'reward', 2000, NOW(), NOW())");
PREPARE alterIfNotExists FROM @addColDetail2;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;