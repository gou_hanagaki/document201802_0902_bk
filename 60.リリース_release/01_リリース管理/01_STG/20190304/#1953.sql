   SET @systme_name = '寮の使用';
    SET @system_column_name = "use_dormitory";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '使用', 'use', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '未使用', 'unused', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '国保加入';
    SET @system_column_name = "national_health_insurance";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '加入', 'buy_insurance', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '未加入', 'participation_finished', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '同伴者の有無';
    SET @system_column_name = "is_partner";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '有', 'yes', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '無', 'no', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '過去の出入国歴';
    SET @system_column_name = "is_past_entry_departure";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '有', 'yes', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '無', 'no', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '犯罪歴の有無';
    SET @system_column_name = "is_crime";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '有', 'yes', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '無', 'no', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '退去強制又は出国命令による出国の有無';
    SET @system_column_name = "is_departure_of_deportation";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '有', 'yes', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '無', 'no', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    SET @systme_name = '在日親族及び同居者同居予定';
    SET @system_column_name = "is_intended_to_reside";
    SET @codeExsit = (SELECT COUNT(*) FROM system_code WHERE column_name = @system_column_name AND `disable` = 0);
    SET @addCol     =  IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                                VALUES (@systme_name, @system_column_name,  2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addCol;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @insert_id= LAST_INSERT_ID();

    SET @addColDetail1 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 1, 1, '有', 'yes', 2000, NOW(), NOW())");

    PREPARE alterIfNotExists FROM @addColDetail1;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

    SET @addColDetail2 = IF (@codeExsit > 0, 'SELECT 1',
                           " INSERT INTO  `system_code_detail` (`system_code_id`, `code1`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) 
                               VALUES (@insert_id, 0, 2, '無', 'no', 2000, NOW(), NOW())");
    PREPARE alterIfNotExists FROM @addColDetail2;
    EXECUTE alterIfNotExists;
    DEALLOCATE PREPARE alterIfNotExists;

/************************************************************************/

    ALTER TABLE `student` CHANGE `weekly_study_hours` `weekly_study_hours` FLOAT UNSIGNED DEFAULT 0 NOT NULL; 

/************************************************************************/

    SET @id := (SELECT id FROM system_code WHERE column_name = 'part_time_job_type_cd' AND `disable` = 0);
    UPDATE system_code_detail SET `name` = '翻訳', column_name = 'Translation' WHERE column_name = 'Translation_Interpretation' AND system_code_id = @id;
    UPDATE system_code_detail SET `sequence` = 3 WHERE column_name = 'Language_teaching' AND system_code_id = @id;
    UPDATE system_code_detail SET `sequence` = 4 WHERE column_name = 'Others' AND system_code_id = @id;
    INSERT INTO system_code_detail(`system_code_id`, code1, sequence, `name`, `column_name`) VALUES (@id, 4, 2, '通訳', 'Interpretation');

/************************************************************************/

    ALTER TABLE hospitalization_history MODIFY disease_name_or_reason VARCHAR(255) DEFAULT '' NOT NULL;
    ALTER TABLE student MODIFY memo VARCHAR(255) DEFAULT '' NOT NULL;

/************************************************************************/