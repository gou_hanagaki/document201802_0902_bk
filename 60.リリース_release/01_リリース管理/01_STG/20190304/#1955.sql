DROP FUNCTION IF EXISTS `fn_get_real_attend_day`;
DELIMITER //
CREATE FUNCTION `fn_get_real_attend_day`(
    `pr_person_id` INT,
    `pr_from_date` DATE,
    `pr_to_date` DATE
) RETURNS int(11)
    DETERMINISTIC
    COMMENT 'get real attend day from date to date'
BEGIN
-- add
    SET @max_date= ((SELECT MAX(date) FROM school_calendar WHERE disable = 0));
  IF((pr_from_date > @max_date) OR (pr_to_date > @max_date)) THEN 
          RETURN -2;
  END IF;
-- add
  IF (pr_from_date > pr_to_date) THEN
    SET @real_attend = -1;
  ELSE
    SET @real_attend = (SELECT COUNT(date)
                FROM school_calendar
                WHERE
                  disable = 0
                  AND is_school_holiday = 0
                  AND is_public_holiday = 0
                  AND date >= pr_from_date
                  AND date <= pr_to_date);
    END IF;
    RETURN (@real_attend);
END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `sp_get_real_attend_by_student_no`;

DELIMITER //
CREATE PROCEDURE `sp_get_real_attend_by_student_no`(
    IN `pr_student_no` varchar(128),
    IN `pr_from_date` date,
    IN `pr_to_date` date

)
    COMMENT 'Get real attend of current class'
get_attend:BEGIN
    SET @person_id = (SELECT person_id FROM student WHERE student_no = pr_student_no and disable = 0);
    IF @person_id IS NULL THEN
        SELECT 1 as error, 'student not found' as errmsg;
        LEAVE get_attend;
    END IF;

    SET @real_attend = fn_get_real_attend_day(pr_student_no, pr_from_date, pr_to_date);
-- add
    IF @real_attend = 0 THEN 
        SELECT 3 as error, 'all_date_not_working' as errmsg;
        LEAVE get_attend;
    END IF;
-- add
    IF @real_attend > 0 THEN
        /* count attendance data */
        SET @attend_count = (SELECT
                                        count(CSW.date)
                                    FROM
                                        shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
                                    WHERE
                                        SA.person_id = @person_id
                                        and CSW.date >= pr_from_date
                                        and CSW.date <= pr_to_date
                                        and SA.disable = 0
                                        and CSW.disable = 0);
        /* check attendance valid */
        IF @real_attend > @attend_count THEN
            SELECT 2 as error, 'attendance data not found' as errmsg;
            LEAVE get_attend;
        END IF;

        /* valid result */
        SELECT 0 as error,  @real_attend as real_attend_day;
    ELSE
        SELECT 2 as error, 'attendance data not found' as errmsg;
    END IF;
END//
DELIMITER ;