INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`,
`create_datetime`, `lastup_datetime`) 
VALUES ('22', '1', '学生', 'student', '2000', NOW(), NOW());

INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`,
`create_datetime`, `lastup_datetime`) 
VALUES ('22', '12', 'エージェント', 'agent', '2000', NOW(), NOW());

UPDATE system_code_detail 
SET code2 = 1
WHERE system_code_detail.column_name NOT IN ('student', 'agent')
AND system_code_detail.system_code_id = 22;

UPDATE person 
INNER JOIN agent ON person.id = agent.person_id
SET person.type = 12
WHERE person.type = 9 
AND person.disable = 0
AND agent.disable = 0;

--UPDATE person SET `type` = 12 WHERE `type` = 9 AND disable = 0;--