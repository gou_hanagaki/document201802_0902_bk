ALTER TABLE `student`
    ADD COLUMN `memo` TEXT NOT NULL AFTER `ability_id`;  

INSERT INTO `function` (`function_group_id`, `name`, `path`, `sequence`, `create_datetime`, `lastup_datetime`)
VALUES (1, 'メモ画面', 'staff/student/memo_info', 1, NOW(), NOW()), 
       (1, 'メモ編集画面', 'staff/student/memo_edit', 1, NOW(), NOW());