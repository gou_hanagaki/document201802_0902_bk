INSERT INTO `system_code` (`name`, `column_name`,`create_datetime`,`lastup_datetime`)
VALUE ("�ؖ����萔��","certificate_issuance_fee",NOW(),NOW());

SET @system_code_id = (SELECT id from system_code where system_code.column_name="certificate_issuance_fee");

INSERT INTO system_code_detail (`system_code_id`,`code1`,`code2`,`sequence`,`name`,`column_name`,`create_datetime`,`lastup_datetime`)
VALUES 
(@system_code_id,300,1,1,"����(���{��)","graduation_japanese",NOW(),NOW()),
(@system_code_id,300,1,2,"����(�p��)","graduation_english",NOW(),NOW()),
(@system_code_id,300,1,3,"���ƌ���(���{��)","graduation_prospect_japanese",NOW(),NOW()),
(@system_code_id,300,1,4,"���ƌ���(�p��)","graduation_prospect_english",NOW(),NOW()),
(@system_code_id,300,1,5,"�C��(���{��)","completion_japanese",NOW(),NOW()),
(@system_code_id,300,1,6,"�C��(�p��)","completion_english",NOW(),NOW()),
(@system_code_id,300,1,7,"�݊w(���{��)","studying_abroad_japanese",NOW(),NOW()),
(@system_code_id,300,1,8,"�݊w(�p��)","studying_abroad_japanese_english",NOW(),NOW()),
(@system_code_id,500,1,9,"���{��\��","japanese_ability",NOW(),NOW()),
(@system_code_id,300,1,10,"�w�Ɛ���(���{��)","academic_record_japanese",NOW(),NOW()),
(@system_code_id,300,1,11,"�w�Ɛ���(�p��)","academic_record_english",NOW(),NOW()),

(@system_code_id,300,2,1,"�ݐЁi���{��j","enrollment_japanese",NOW(),NOW()),
(@system_code_id,300,2,2,"�ݐЁi�p��j","enrollment_english",NOW(),NOW()),
(@system_code_id,500,2,3,"���E��(���E����)","letter_of_recommendation",NOW(),NOW()),
(@system_code_id,300,2,4,"�ʊw","going_to_school",NOW(),NOW()),
(@system_code_id,100,2,5,"�x�Ɋ���","vacation_period",NOW(),NOW()),
(@system_code_id,500,2,6,"�w���؍Ĕ��s","student_card_reissue",NOW(),NOW()),
(@system_code_id,500,2,7,"�w�Z�󉟈�","school_seal",NOW(),NOW()),
(@system_code_id,2000,2,8,"�|��@","translation_type_1",NOW(),NOW()),
(@system_code_id,3000,2,9,"�|��A","translation_type_2",NOW(),NOW())

;