﻿ALTER TABLE `visa` ADD COLUMN `is_pending` TINYINT (1) UNSIGNED DEFAULT 0 NOT NULL AFTER `permit_other_activitiy_cd`;
ALTER TABLE `visa`
    ADD COLUMN `application_work_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `end_date`;