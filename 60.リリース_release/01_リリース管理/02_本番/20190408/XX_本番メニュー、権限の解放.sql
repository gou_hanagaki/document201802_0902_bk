/* 入学生一括登録 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/bulk_register';
/* 合格通知書発行 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/issue_passing_certificate';
/* 入国管理情報確認 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/confirm_for_immigration_bureau';
/* 在留資格認定証明書交付申請 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/apply_residence_status';
/* 入学許可証発行 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/issue_acceptance_letter';
/* 入学プレースメントテスト */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/placement_exam';
/* 申請番号付与 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/student/student_application_no_list';
/* 中長期在留者の受け入れ情報 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/grant_residence';
/* 請求書発行 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/issue_invoice';
/* 入金確認 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/confirm_payment';
/* 資格外活動許可申請 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/permission_engage_other_activity';
/* 在留資格更新 */
UPDATE function
SET is_in_menu = '1' WHERE path ='staff/admission/update_residence_status';


/* permissionテーブルの権限更新 */
UPDATE permission SET is_permitted = '1'  WHERE id = '14';
UPDATE permission SET is_permitted = '1'  WHERE id = '16';
UPDATE permission SET is_permitted = '1'  WHERE id = '17';
UPDATE permission SET is_permitted = '1'  WHERE id = '18';
UPDATE permission SET is_permitted = '1'  WHERE id = '19';
UPDATE permission SET is_permitted = '1'  WHERE id = '42';
UPDATE permission SET is_permitted = '1'  WHERE id = '43';
UPDATE permission SET is_permitted = '1'  WHERE id = '44';
UPDATE permission SET is_permitted = '1'  WHERE id = '45';
UPDATE permission SET is_permitted = '1'  WHERE id = '46';
UPDATE permission SET is_permitted = '1'  WHERE id = '47';
UPDATE permission SET is_permitted = '1'  WHERE id = '48';
UPDATE permission SET is_permitted = '1'  WHERE id = '49';


