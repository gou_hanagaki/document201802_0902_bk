﻿UPDATE `function` SET `name` = '入学生一括登録', `lastup_datetime` = NOW() WHERE `path` = 'staff/admission/bulk_register';

UPDATE system_code_detail scd
JOIN system_code sc ON sc.id = scd.system_code_id 
SET scd.column_name = CASE 
    WHEN scd.name = '就職' THEN 'employment' 
    WHEN scd.name = '進学' THEN 'admission' 
    WHEN scd.name = '帰国' THEN 'return_home' 
    WHEN scd.name = '自主退学' THEN 'voluntarily_withdrawing' 
    WHEN scd.name = 'ビザ変更' THEN 'visa_change' 
    WHEN scd.name = 'ビザ期限切れ' THEN 'visa_expired' 
    WHEN scd.name = '学費未納' THEN 'tuition_fee_not_paid' 
    WHEN scd.name = '長期欠席' THEN 'long_term_absence' 
    WHEN scd.name = '体調不良' THEN 'poor_physical_condition' 
    WHEN scd.name = '違法行為' THEN 'illegal_act' 
    WHEN scd.name = '結婚' THEN 'marriage' 
    WHEN scd.name = '妊娠' THEN 'pregnancy' 
    WHEN scd.name = '個人都合' THEN 'personal_convenience' 
    WHEN scd.name = '家庭都合' THEN 'family_convenience' 
    WHEN scd.name = '連絡なし' THEN 'no_contact' 
END
    , scd.lastup_datetime = NOW()
WHERE sc.column_name = 'Reason_for_withdrawal_exclusion_reason'
AND scd.`disable` = 0
AND sc.`disable`  = 0;


ALTER TABLE `immigration`   
  ADD COLUMN `residence_application_required_years_until` VARCHAR(128) DEFAULT ''  NOT NULL AFTER `residence_application_date`;

ALTER TABLE `supporter` ADD COLUMN `supporter_relationship_others_comment` VARCHAR (128) DEFAULT '' NOT NULL AFTER `supporter_relationship_cd`;

