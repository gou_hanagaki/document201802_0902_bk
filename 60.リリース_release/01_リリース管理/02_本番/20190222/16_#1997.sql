SET @ModelID = (select `sequence` from `function_group` where name = '教育コンテンツ');
    SET @IDEXPEL = (select id from `function_group` where name = '学生用タブレット');
    update `function_group`
        set sequence = sequence + 1
        where sequence >= @ModelID + 1 and id != @IDEXPEL;
    INSERT INTO `function_group` (`name`, `sequence`, `lastup_account_id`, `create_datetime`) VALUES ('自宅学習コンテンツ', @ModelID + 1, '2000', CURDATE());
    SET @IDINSERT = LAST_INSERT_ID();
    update `function`
        set function_group_id = @IDINSERT
        where path 
        IN(
          'staff/video_content/show'
        , 'staff/video_content/video_exam_get'
        , 'staff/Video_content/update_exam'
        , 'staff/video_content/set_up_public_class'
        , 'staff/video_content/load_list'
        , 'staff/Video_content/insert_video_exam'
        , 'staff/video_content/edit_public_class'
        , 'staff/video_content/edit'
        , 'staff/video_content/create_exam_video'
        , 'staff/video_content/add'
        , 'staff/homework_exam/update_information_test'
        , 'staff/homework_exam/update_homework_exam'
        , 'staff/homework_exam/set_up_public_class'
        , 'staff/homework_exam/load_list'
        , 'staff/homework_exam/list_student_homework'
        , 'staff/homework_exam/insert_homework_exam'
        , 'staff/homework_exam/homework_exam_get'
        , 'staff/homework_exam/edit_public_class'
        , 'staff/homework_exam/edit'
        , 'staff/homework_exam/create_homework_exam'
        , 'staff/homework_exam/add'
        , 'staff/homework_exam'
        , 'staff/homework/ajax_get_class_by_school_term_with_person_type'
        );
    INSERT INTO `permission` (`account_group_id`, `function_group_id`, `is_permitted`, create_datetime) 
    VALUES ('1', @IDINSERT, '0', CURDATE())
                ,('2', @IDINSERT, '1', CURDATE())
        , ('3', @IDINSERT, '1', CURDATE())
        , ('4', @IDINSERT, '1', CURDATE())
        , ('5', @IDINSERT, '1', CURDATE())
        , ('6', @IDINSERT, '1', CURDATE())
        , ('7', @IDINSERT, '1', CURDATE())
        , ('8', @IDINSERT, '1', CURDATE())
        , ('9', @IDINSERT, '1', CURDATE())
        , ('10', @IDINSERT, '1', CURDATE())
        ;