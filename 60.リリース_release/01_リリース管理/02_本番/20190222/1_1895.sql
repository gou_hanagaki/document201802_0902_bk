﻿/* Set value default*/
set @name       = '学校データメンテナンス';
set @link       = 'staff/educational_institution/school_data';
set @sequence   = 5;
set @is_in_menu = 1;
set @active     = 0;
/* Set value default:  end*/

/* Set function group システム管理 */
set @setup_function_group_id = 13;
SET @addCol =(SELECT IF((SELECT COUNT(*)
                          FROM `function`
                          WHERE `function`.path = @link and `function`.function_group_id = @setup_function_group_id and `function`.disable = @active
                         )>0,
                         "SELECT 1",
                         "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
                         VALUES (@setup_function_group_id, @name, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

/* Set value default*/
set @name       = '学校データメンテナンス';
set @link       = 'staff/educational_institution/export_errors_pdf';
set @sequence   = 5;
set @is_in_menu = 0;
set @active     = 0;
/* Set value default:  end*/

/* Set function group システム管理 */
set @setup_function_group_id = 13;
SET @addCol =(SELECT IF((SELECT COUNT(*)
                          FROM `function`
                          WHERE `function`.path = @link and `function`.function_group_id = @setup_function_group_id and `function`.disable = @active
                         )>0,
                         "SELECT 1",
                         "INSERT INTO function(function_group_id, name, path, is_in_menu, sequence, lastup_account_id, create_datetime, lastup_datetime)
                         VALUES (@setup_function_group_id, @name, @link, @is_in_menu, @sequence, 2000, NOW(), NOW())" 
));

PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

-- Create table temp
CREATE TABLE IF NOT EXISTS `log_import_education` (
    `code` VARCHAR(512) NULL DEFAULT '',
    `error_type` VARCHAR(512) NULL DEFAULT '',
    `error_name` VARCHAR(512) NULL DEFAULT '',
    `error_data` VARCHAR(512) NULL DEFAULT '',
    `error_content` VARCHAR(512) NULL DEFAULT '',
    `token` VARCHAR(512) NOT NULL,
    `create_datetime` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/* Update phone and fax hyphen */

update educational_institution SET fax = REPLACE(fax, '–', '-'), phone = REPLACE(phone, '–', '-'), zipcode = REPLACE(zipcode, '–', '-');
update educational_institution SET fax = REPLACE(fax, 'ｰ', '-'), phone = REPLACE(phone, 'ｰ', '-'), zipcode = REPLACE(zipcode, 'ｰ', '-');
update educational_institution SET fax = REPLACE(fax, '‒', '-'), phone = REPLACE(phone, '‒', '-'), zipcode = REPLACE(zipcode, '‒', '-');
update educational_institution SET fax = REPLACE(fax, '　', ''), phone = REPLACE(phone, '　', ''), zipcode = REPLACE(zipcode, '　', '');
update educational_institution SET fax = REPLACE(fax, '－', '-'), phone = REPLACE(phone, '－', '-'), zipcode = REPLACE(zipcode, '－', '-');
update educational_institution SET fax = REPLACE(fax, '‐', '-'), phone = REPLACE(phone, '‐', '-'), zipcode = REPLACE(zipcode, '‐', '-');
update educational_institution SET fax = REPLACE(fax, '−', '-'), phone = REPLACE(phone, '−', '-'), zipcode = REPLACE(zipcode, '−', '-');

/* Add ajax permission*/
INSERT INTO `function`(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`,`lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) SELECT 13, 'ajax_educational_institution', 'staff/educational_institution/import_educational_institution', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
            WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '13' AND path = 'staff/educational_institution/import_educational_institution' AND disable = 0 );

INSERT INTO `function`
            (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) 
            SELECT 13, 'ajax_delete_error', 'staff/educational_institution/ajax_delete_error_log_by_token', 0, 1, 9999, NOW(), NOW(), 0 FROM DUAL 
            WHERE NOT EXISTS(SELECT * FROM `function` WHERE function_group_id = '13' AND path = 'staff/educational_institution/ajax_delete_error_log_by_token' AND disable = 0 );