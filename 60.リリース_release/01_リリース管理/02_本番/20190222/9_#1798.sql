ALTER TABLE `agent` 
  ADD COLUMN `office_name` VARCHAR (128) DEFAULT '' NOT NULL AFTER `document_charge_qq_no`,
  ADD COLUMN `office_name_kana` VARCHAR (128) DEFAULT '' NOT NULL AFTER `office_name`,
  ADD COLUMN `office_name_alphabet` VARCHAR (128) DEFAULT '' NOT NULL AFTER `office_name_kana` ;

ALTER TABLE `agent`
ADD COLUMN `office_name_native` VARCHAR(128) NOT NULL DEFAULT '' AFTER `office_name_alphabet`;

ALTER TABLE `agent`
ADD COLUMN `register_code` VARCHAR(128) NOT NULL DEFAULT '' AFTER `office_name_native`;

UPDATE person
JOIN agent ON agent.`person_id` = person.id 
SET person.`sns_account` = agent.`in_charge_qq_no` 
WHERE person.`sns_account` = '' AND agent.`disable` = 0