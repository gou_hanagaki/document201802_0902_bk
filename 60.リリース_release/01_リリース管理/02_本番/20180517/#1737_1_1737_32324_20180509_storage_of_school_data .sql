-- START create table
CREATE TABLE IF NOT EXISTS `educational_institution` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`educational_institution_type` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`head_quater` VARCHAR(50) NOT NULL DEFAULT '',
	`name` VARCHAR(255) NOT NULL DEFAULT '',
	`campus` VARCHAR(50) NOT NULL DEFAULT '',
	`faculty` VARCHAR(50) NOT NULL DEFAULT '',
	`is_postgraduate` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`operator_type` VARCHAR(50) NOT NULL DEFAULT '',
	`researching_major` VARCHAR(255) NOT NULL DEFAULT '',
	`speciality` VARCHAR(255) NOT NULL DEFAULT '',
	`course` VARCHAR(40) NOT NULL DEFAULT '',
	`comment` VARCHAR(40) NOT NULL DEFAULT '',
	`is_independent` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`researching_field` VARCHAR(60) NOT NULL DEFAULT '',
	`university_affiliated` VARCHAR(40) NOT NULL DEFAULT '',
	`faculty_name` VARCHAR(40) NOT NULL DEFAULT '',
	`faculty_affiliated` VARCHAR(40) NOT NULL DEFAULT '',
	`prefecture_code` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`district` VARCHAR(20) NOT NULL DEFAULT '',
	`city_code` VARCHAR(8) NOT NULL DEFAULT '',
	`zipcode` VARCHAR(12) NOT NULL DEFAULT '',
	`address` VARCHAR(255) NOT NULL DEFAULT '',
	`phone` VARCHAR(16) NOT NULL DEFAULT '',
	`fax` VARCHAR(16) NOT NULL DEFAULT '',
	`k_code` VARCHAR(16) NOT NULL DEFAULT '',
	`department_name` VARCHAR(20) NOT NULL DEFAULT '',
	`people_cnt` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `educational_institution_type` (`educational_institution_type`),
	INDEX `prefecture_code` (`prefecture_code`)
);

-- END create table

-- START ADD educational_institution_type INTO SYSTEM_CODE
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code
     WHERE column_name = 'educational_institution_type'
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code(name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES ('校種', 'educational_institution_type', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @educational_institution_type_id = (SELECT id FROM system_code WHERE system_code.column_name = 'educational_institution_type');

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'university' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 1, '大学', 'university', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'postgraduate' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 2, '大学院', 'postgraduate', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'vocational_school' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 3, '専修学校', 'vocational_school', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'industrial_school' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 4, '大学校', 'industrial_school', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'college_of_technology' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 5, '短期大学', 'college_of_technology', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'university_specify' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 6, '各種学校', 'university_specify', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;

SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM system_code_detail
     WHERE column_name = 'short_term_university' AND system_code_id = @educational_institution_type_id
    ) > 0,
    "SELECT 1",
    "INSERT INTO system_code_detail(system_code_id, code1, name, column_name, lastup_account_id, create_datetime, lastup_datetime) VALUES (@educational_institution_type_id, 7, '高等専門学校', 'short_term_university', 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
-- END ADD education_system_type INTO SYSTEM_CODE


--  START INSERT FUNCTION PATH
SET @function_group_id = (SELECT id FROM function_group WHERE function_group.name = 'システム管理');
SET @addCol = (SELECT IF(
    (SELECT COUNT(*)
     FROM function
     WHERE path = 'staff/import/educational_institution_upload_excel'
    ) > 0,
    "SELECT 1",
    "INSERT INTO function(function_group_id, path, is_in_menu, lastup_account_id, create_datetime, lastup_datetime) VALUES (@function_group_id, 'staff/import/educational_institution_upload_excel', 0, 2000, NOW(), NOW())"
));
PREPARE alterIfNotExists FROM @addCol;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
-- -- END INSERT FUNCTION PATH


-- START UPDATE prefectures
SET @system_code_id = (SELECT id FROM system_code WHERE column_name = 'prefectures');

UPDATE system_code_detail
SET
  column_name = LOWER(column_name)
WHERE
  system_code_id = @system_code_id;

SET @id_system_detail_code = (SELECT id
FROM system_code_detail
WHERE (column_name = 'aomori' or column_name = 'Aomori' or column_name = 'okinawa')
AND system_code_id = @system_code_id
ORDER BY id DESC  LIMIT 1);

UPDATE system_code_detail
SET
  column_name = 'okinawa'
WHERE
  id = @id_system_detail_code;
-- END UPDATE prefectures