-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `application_issue_certificate_properties`;
CREATE TABLE `application_issue_certificate_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `certificate_zipcode` varchar(12) NOT NULL DEFAULT '',
  `certificate_immigration_application_no` varchar(12) NOT NULL DEFAULT '',
  `certificate_address1` varchar(255) NOT NULL DEFAULT '',
  `certificate_address2` varchar(255) NOT NULL DEFAULT '',
  `certificate_address3` varchar(255) NOT NULL DEFAULT '',
  `certificate_school_route_line` varchar(255) NOT NULL DEFAULT '',
  `certificate_boarding_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_boarding_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_vacation_period_1` date NOT NULL DEFAULT '0000-00-00',
  `certificate_vacation_period_2` date NOT NULL DEFAULT '0000-00-00',
  `certificate_school_route_from` varchar(128) NOT NULL DEFAULT '',
  `certificate_school_route_to` varchar(128) NOT NULL DEFAULT '',
  `certificate_not_recommend_reason` mediumtext NOT NULL,
  `certificate_purpose_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `certificate_purpose_other` varchar(255) NOT NULL DEFAULT '',
  `certificate_submission_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `certificate_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_college_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_college_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_company_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_part_time_job_company_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_other` varchar(128) NOT NULL DEFAULT '',
  `certificate_quantity` int(10) NOT NULL DEFAULT '0',
  `total_issue_fee` int(10) NOT NULL DEFAULT '0',
  `reception_school_building` int(10) NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-12-25 17:43:04
