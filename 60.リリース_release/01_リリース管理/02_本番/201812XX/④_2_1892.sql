SET @helper_id = (SELECT system_code.id FROM system_code WHERE column_name = 'helper');

INSERT INTO system_code_detail (`system_code_id`, `code1`,`sequence`, `name`, `column_name`, `create_datetime`, `lastup_datetime`)
VALUES (@helper_id, 5,1,'証明書発行願入力画面', 'application_condition', NOW(), NOW());