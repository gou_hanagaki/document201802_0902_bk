-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: midream_work
-- ------------------------------------------------------
-- Server version	5.6.33-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability`
--

DROP TABLE IF EXISTS `ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ability_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `next_ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `color` char(6) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `main_text` text NOT NULL,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `text3` text NOT NULL,
  `text4` text NOT NULL,
  `text5` text NOT NULL,
  `text6` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_level_id` (`external_test_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ability_backup`
--

DROP TABLE IF EXISTS `ability_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability_backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_group_id` int(11) NOT NULL DEFAULT '0',
  `next_ability` int(10) unsigned NOT NULL DEFAULT '0',
  `color` char(6) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `main_text` text NOT NULL,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `text3` text NOT NULL,
  `text4` text NOT NULL,
  `text5` text NOT NULL,
  `text6` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_level_id` (`test_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ability_group`
--

DROP TABLE IF EXISTS `ability_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ability_students_detail`
--

DROP TABLE IF EXISTS `ability_students_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability_students_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `before_term_ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_group_unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_down` int(10) unsigned NOT NULL DEFAULT '0',
  `class_up` int(10) unsigned NOT NULL DEFAULT '0',
  `graduation_leave` int(10) unsigned NOT NULL DEFAULT '0',
  `from_university` int(10) unsigned NOT NULL DEFAULT '0',
  `to_university` int(10) unsigned NOT NULL DEFAULT '0',
  `from_postgraduate` int(10) unsigned NOT NULL DEFAULT '0',
  `to_postgraduate` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` int(10) unsigned NOT NULL DEFAULT '0',
  `students_number` int(10) unsigned NOT NULL DEFAULT '0',
  `university_postgraduate` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `login_id` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `login_attempt_fail` int(10) unsigned NOT NULL DEFAULT '0',
  `locked_expire_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`),
  KEY `person_id` (`person_id`),
  KEY `login_id` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_group`
--

DROP TABLE IF EXISTS `account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_password_log`
--

DROP TABLE IF EXISTS `account_password_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_password_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `password` varchar(64) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_title`
--

DROP TABLE IF EXISTS `account_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_title` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `is_school_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement`
--

DROP TABLE IF EXISTS `achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_date` date NOT NULL DEFAULT '0000-00-00',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `is_additional_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `test_date` (`test_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement_backup`
--

DROP TABLE IF EXISTS `achievement_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `year_month` date NOT NULL DEFAULT '0000-00-00',
  `place` int(3) unsigned NOT NULL DEFAULT '0',
  `certification_no` varchar(128) NOT NULL DEFAULT '',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_additional_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_edit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `test_level_id` (`test_level_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `class_work_id` (`class_work_id`),
  KEY `content_test_list_id` (`content_test_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement_detail`
--

DROP TABLE IF EXISTS `achievement_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_original` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `test_section_cd` (`test_section_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement_detail_backup`
--

DROP TABLE IF EXISTS `achievement_detail_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_detail_backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_id` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_original` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `test_section_id` (`test_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `after_graduation_course`
--

DROP TABLE IF EXISTS `after_graduation_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `after_graduation_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `after_graduation_course_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance_rate` int(10) unsigned NOT NULL DEFAULT '0',
  `base_date` date NOT NULL DEFAULT '0000-00-00',
  `school_name` varchar(255) NOT NULL DEFAULT '',
  `faculty` varchar(255) NOT NULL DEFAULT '',
  `speciality` varchar(255) NOT NULL DEFAULT '',
  `after_graduation_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `status_exclusive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommendation_general` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `test_result` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `company_name` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `individual_corporate_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `representative_last_name` varchar(128) NOT NULL DEFAULT '',
  `representative_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `representative_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `in_charge_division` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name` varchar(128) NOT NULL DEFAULT '',
  `in_charge_first_name` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `in_charge_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `in_charge_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `in_charge_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `in_charge_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `in_charge_mail_address` varchar(255) NOT NULL DEFAULT '',
  `in_charge_qq_no` varchar(20) NOT NULL DEFAULT '',
  `school_in_charge` int(10) unsigned NOT NULL DEFAULT '0',
  `school_in_charge_comment` text NOT NULL,
  `commission_fee` int(10) unsigned NOT NULL DEFAULT '0',
  `commission_fee_comment` text NOT NULL,
  `document_charge_last_name` varchar(128) NOT NULL DEFAULT '',
  `document_charge_first_name` varchar(128) NOT NULL DEFAULT '',
  `document_charge_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `document_charge_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `document_charge_qq_no` varchar(12) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `applicant_list`
--

DROP TABLE IF EXISTS `applicant_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_preferred_date_intended` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `applicant_numbering_status`
--

DROP TABLE IF EXISTS `applicant_numbering_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant_numbering_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_preferred_date_intended` int(10) unsigned NOT NULL DEFAULT '0',
  `is_numbering_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_no` varchar(10) NOT NULL DEFAULT '',
  `application_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_date` date NOT NULL DEFAULT '0000-00-00',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status_desc` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `charge_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `print_date` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `request_id` (`request_id`),
  KEY `application_type_id` (`application_type_id`),
  KEY `charge_id` (`charge_id`),
  KEY `application_no` (`application_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_approve`
--

DROP TABLE IF EXISTS `application_approve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_approve` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_approver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `approver_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `action` text NOT NULL,
  `interview_id` int(10) unsigned NOT NULL DEFAULT '0',
  `approval_date` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_document` varchar(255) NOT NULL DEFAULT '',
  `reject_reason` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `application_approver_id` (`application_approver_id`),
  KEY `interview_id` (`interview_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_approver`
--

DROP TABLE IF EXISTS `application_approver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_approver` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `approver_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `application_type_id` (`application_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_change_class_properties`
--

DROP TABLE IF EXISTS `application_change_class_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_change_class_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_change_month` date NOT NULL DEFAULT '0000-00-00',
  `is_class_change_teacher_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_teacher_confirm` date NOT NULL DEFAULT '0000-00-00',
  `class_change_time_zone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason_comment` text NOT NULL,
  `class_change_comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_change_info_job_properties`
--

DROP TABLE IF EXISTS `application_change_info_job_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_change_info_job_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) unsigned NOT NULL DEFAULT '0',
  `application_id` int(11) unsigned NOT NULL DEFAULT '0',
  `is_request_to_apply` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_request_to_delete` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `company` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_type_cd` tinyint(4) NOT NULL DEFAULT '0',
  `part_time_job_type_other` varchar(128) NOT NULL DEFAULT '',
  `job_length` varchar(128) NOT NULL DEFAULT '',
  `working_hours_per_week` time NOT NULL DEFAULT '00:00:00',
  `industry_cd` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `company_zipcode` varchar(12) NOT NULL DEFAULT '',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(20) NOT NULL DEFAULT '',
  `agent` varchar(50) NOT NULL DEFAULT '',
  `work_from_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `salary` float unsigned NOT NULL DEFAULT '0',
  `salary_unit_cd` float unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL,
  `lastup_datetime` datetime NOT NULL,
  `lastup_account_id` int(11) unsigned NOT NULL DEFAULT '0',
  `disable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_change_info_properties`
--

DROP TABLE IF EXISTS `application_change_info_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_change_info_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) unsigned NOT NULL,
  `first_name` varchar(128) NOT NULL DEFAULT '',
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `name_font` varchar(128) NOT NULL DEFAULT '',
  `first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `mail_address` varchar(255) NOT NULL DEFAULT '',
  `sns_account` varchar(255) NOT NULL DEFAULT '',
  `sns_type` varchar(50) NOT NULL DEFAULT '',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(11) unsigned NOT NULL DEFAULT '0',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_issue_certificate_properties`
--

DROP TABLE IF EXISTS `application_issue_certificate_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_issue_certificate_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `certificate_zipcode` varchar(12) NOT NULL DEFAULT '',
  `certificate_immigration_application_no` varchar(12) NOT NULL DEFAULT '',
  `certificate_address1` varchar(255) NOT NULL DEFAULT '',
  `certificate_address2` varchar(255) NOT NULL DEFAULT '',
  `certificate_address3` varchar(255) NOT NULL DEFAULT '',
  `certificate_school_route_line` varchar(255) NOT NULL DEFAULT '',
  `certificate_boarding_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_boarding_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_vacation_period_1` date NOT NULL DEFAULT '0000-00-00',
  `certificate_vacation_period_2` date NOT NULL DEFAULT '0000-00-00',
  `certificate_school_route_from` varchar(128) NOT NULL DEFAULT '',
  `certificate_school_route_to` varchar(128) NOT NULL DEFAULT '',
  `certificate_not_recommend_reason` mediumtext NOT NULL,
  `certificate_purpose_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `certificate_purpose_other` varchar(255) NOT NULL DEFAULT '',
  `certificate_submission_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `certificate_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_college_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_college_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_section_1` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_section_2` varchar(128) NOT NULL DEFAULT '',
  `certificate_company_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_part_time_job_company_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_other` varchar(128) NOT NULL DEFAULT '',
  `certificate_quantity` int(10) NOT NULL DEFAULT '0',
  `total_issue_fee` int(10) NOT NULL DEFAULT '0',
  `reception_school_building` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_leave_absence_properties`
--

DROP TABLE IF EXISTS `application_leave_absence_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_leave_absence_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `leave_absence_date_from` date NOT NULL DEFAULT '0000-00-00',
  `leave_absence_date_to` date NOT NULL DEFAULT '0000-00-00',
  `leave_absence_approval_date_from` date NOT NULL DEFAULT '0000-00-00',
  `leave_absence_approval_date_to` date NOT NULL DEFAULT '0000-00-00',
  `leave_absence_return_country_phone_no` varchar(20) NOT NULL DEFAULT '',
  `leave_absence_return_country_mail_address` varchar(128) NOT NULL DEFAULT '',
  `leave_absence_return_country_address1` varchar(255) NOT NULL DEFAULT '',
  `leave_absence_return_country_address2` varchar(255) NOT NULL DEFAULT '',
  `leave_absence_return_country_address3` varchar(255) NOT NULL DEFAULT '',
  `leave_absence_reason_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `leave_absence_reason_detail` text NOT NULL,
  `leave_absence_note` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_official_absence_properties`
--

DROP TABLE IF EXISTS `application_official_absence_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_official_absence_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `authorized_absent_date_from` date NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_date_to` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_from` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_to` date NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_reason_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `authorized_absent_school_name` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_school_section` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_school_section1` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_school_section2` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_laboratory` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_professor` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_ceremony_relation` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_company` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_temporary_return_properties`
--

DROP TABLE IF EXISTS `application_temporary_return_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_temporary_return_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `temporary_return_date_from` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_date_to` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_actual_date_from` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_actual_date_to` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_flight_name_go` varchar(50) NOT NULL DEFAULT '',
  `temporary_return_flight_name_comeback` varchar(50) NOT NULL DEFAULT '',
  `temporary_return_reason` text NOT NULL,
  `temporary_return_contact_name` varchar(128) NOT NULL DEFAULT '',
  `temporary_return_contact_phone_no` varchar(255) NOT NULL DEFAULT '',
  `temporary_return_address` varchar(255) NOT NULL DEFAULT '',
  `temporary_return_address2` varchar(255) NOT NULL DEFAULT '',
  `temporary_return_address3` varchar(255) NOT NULL DEFAULT '',
  `temporary_return_comment` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_type`
--

DROP TABLE IF EXISTS `application_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL DEFAULT '',
  `name_1` varchar(225) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `is_certification` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `prefix_character` varchar(3) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_withdrawal_notification_properties`
--

DROP TABLE IF EXISTS `application_withdrawal_notification_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_withdrawal_notification_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `leaving_desire_date` date NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_date` date NOT NULL DEFAULT '0000-00-00',
  `leaving_date` date NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_flight` varchar(255) NOT NULL DEFAULT '',
  `leaving_return_country_phone_no` varchar(20) NOT NULL DEFAULT '',
  `leaving_return_country_mail_address` varchar(255) NOT NULL DEFAULT '',
  `leaving_case` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `art_work`
--

DROP TABLE IF EXISTS `art_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `art_work_title` varchar(255) NOT NULL DEFAULT '',
  `student_comment` text NOT NULL,
  `teacher_comment` text NOT NULL,
  `art_work_file` varchar(255) NOT NULL DEFAULT '',
  `art_work_path` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_work_id` (`class_work_id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_edit`
--

DROP TABLE IF EXISTS `attendance_edit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_edit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_work_id` (`class_work_id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_edit_history`
--

DROP TABLE IF EXISTS `attendance_edit_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_edit_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `original_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `original_attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `edit_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `original_time_from` time NOT NULL DEFAULT '00:00:00',
  `edit_time_from` time NOT NULL DEFAULT '00:00:00',
  `original_time_to` time NOT NULL DEFAULT '00:00:00',
  `edit_time_to` time NOT NULL DEFAULT '00:00:00',
  `edit_reason` varchar(255) NOT NULL DEFAULT '',
  `reject_reason` varchar(255) NOT NULL DEFAULT '',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attendance_id` (`attendance_id`),
  KEY `original_staff_person_id` (`original_staff_person_id`),
  KEY `edit_staff_person_id` (`edit_staff_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_history`
--

DROP TABLE IF EXISTS `attendance_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shift_attendance_history_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_attendance` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_time_from` time NOT NULL DEFAULT '00:00:00',
  `original_time_to` time NOT NULL DEFAULT '00:00:00',
  `original_is_authorized_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_leave_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_canceled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_temporary_return` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_attendance` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_time_from` time NOT NULL DEFAULT '00:00:00',
  `edit_time_to` time NOT NULL DEFAULT '00:00:00',
  `edit_is_authorized_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_is_leave_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_is_canceled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_is_temporary_return` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shift_attendance_history_id` (`shift_attendance_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_rate`
--

DROP TABLE IF EXISTS `attendance_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_time` int(7) unsigned NOT NULL DEFAULT '0',
  `total_time` int(10) unsigned NOT NULL DEFAULT '0',
  `attend_time` int(10) unsigned NOT NULL DEFAULT '0',
  `absent_time` int(10) unsigned NOT NULL DEFAULT '0',
  `late_time` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_day` int(7) unsigned NOT NULL DEFAULT '0',
  `total_day` int(10) unsigned NOT NULL DEFAULT '0',
  `attend_day` int(10) unsigned NOT NULL DEFAULT '0',
  `absent_day` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `calculate_date_from` date NOT NULL DEFAULT '0000-00-00',
  `calculate_date_to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `attendance_cd` (`attendance_cd`),
  KEY `calculate_date_from` (`calculate_date_from`),
  KEY `calculate_date_to` (`calculate_date_to`),
  KEY `lastup_datetime` (`lastup_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_rate_rank`
--

DROP TABLE IF EXISTS `attendance_rate_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_rate_rank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` char(6) NOT NULL DEFAULT '',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_tokens`
--

DROP TABLE IF EXISTS `auth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `selector` varchar(12) NOT NULL DEFAULT '',
  `token` varchar(64) NOT NULL DEFAULT '',
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `charge`
--

DROP TABLE IF EXISTS `charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_title_id` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `pay_off_date` date NOT NULL DEFAULT '0000-00-00',
  `comment` text NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `confirm_date` date NOT NULL DEFAULT '0000-00-00',
  `paid_person_name` varchar(128) NOT NULL DEFAULT '',
  `confirm_person_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `account_title_id` (`account_title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_room` varchar(128) NOT NULL DEFAULT '',
  `text_book` varchar(255) NOT NULL DEFAULT '',
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `next_education_guidance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allocation_number` int(10) NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_person_id` (`teacher_person_id`),
  KEY `class_group_id` (`class_group_id`),
  KEY `time_zone_id` (`time_zone_id`),
  KEY `time_table_group_id` (`time_table_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_activity`
--

DROP TABLE IF EXISTS `class_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_work_id` (`class_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_entry_exit`
--

DROP TABLE IF EXISTS `class_entry_exit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_entry_exit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_time` time NOT NULL DEFAULT '00:00:00',
  `exit_time` time NOT NULL DEFAULT '00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_shift_work_id` (`class_shift_work_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_entry_exit_history`
--

DROP TABLE IF EXISTS `class_entry_exit_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_entry_exit_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shift_attendance_history_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_entry_time` time NOT NULL DEFAULT '00:00:00',
  `original_exit_time` time NOT NULL DEFAULT '00:00:00',
  `edit_entry_time` time NOT NULL DEFAULT '00:00:00',
  `edit_exit_time` time NOT NULL DEFAULT '00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `shift_attendance_history_id` (`shift_attendance_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group`
--

DROP TABLE IF EXISTS `class_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `main_teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`),
  KEY `main_teacher_person_id` (`main_teacher_person_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `date_from` (`date_from`),
  KEY `date_to` (`date_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group_copy`
--

DROP TABLE IF EXISTS `class_group_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group_copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `main_teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`),
  KEY `main_teacher_person_id` (`main_teacher_person_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `date_from` (`date_from`),
  KEY `date_to` (`date_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group_template`
--

DROP TABLE IF EXISTS `class_group_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group_unit`
--

DROP TABLE IF EXISTS `class_group_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment1` text NOT NULL,
  `comment2` text NOT NULL,
  `comment3` text NOT NULL,
  `comment4` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift`
--

DROP TABLE IF EXISTS `class_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `day` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `teacher_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_optional` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_request_level`
--

DROP TABLE IF EXISTS `class_shift_request_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_request_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_school_term_id` (`teacher_school_term_id`),
  KEY `test_level_id` (`external_test_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_request_time_zone`
--

DROP TABLE IF EXISTS `class_shift_request_time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_request_time_zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `day` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `possible_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_school_term_id` (`teacher_school_term_id`),
  KEY `time_zone_id` (`time_zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_tentative`
--

DROP TABLE IF EXISTS `class_shift_tentative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_tentative` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_room` varchar(128) NOT NULL DEFAULT '',
  `main_teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `day1` int(10) unsigned NOT NULL DEFAULT '0',
  `day1_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day2` int(10) unsigned NOT NULL DEFAULT '0',
  `day2_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day3` int(10) unsigned NOT NULL DEFAULT '0',
  `day3_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day4` int(10) unsigned NOT NULL DEFAULT '0',
  `day4_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day5` int(10) unsigned NOT NULL DEFAULT '0',
  `day5_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day6` int(10) unsigned NOT NULL DEFAULT '0',
  `day6_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `day7` int(10) unsigned NOT NULL DEFAULT '0',
  `day7_optional` int(10) unsigned NOT NULL DEFAULT '0',
  `data_number` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment1` text NOT NULL,
  `comment2` text NOT NULL,
  `comment3` text NOT NULL,
  `comment4` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_work`
--

DROP TABLE IF EXISTS `class_shift_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_shift_id` (`class_shift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_student`
--

DROP TABLE IF EXISTS `class_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `class_id` (`class_id`),
  KEY `date` (`date_from`,`date_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_template`
--

DROP TABLE IF EXISTS `class_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `class_group_template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_room` varchar(128) NOT NULL DEFAULT '',
  `text_book` varchar(255) NOT NULL DEFAULT '',
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_group_template_id` (`class_group_template_id`),
  KEY `time_zone_id` (`time_zone_id`),
  KEY `time_table_group_id` (`time_table_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_work`
--

DROP TABLE IF EXISTS `class_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `period_id` (`period_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_level`
--

DROP TABLE IF EXISTS `content_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_level_ability`
--

DROP TABLE IF EXISTS `content_level_ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_level_ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_level_id` (`content_level_id`),
  KEY `ability_id` (`ability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_marksheet_score`
--

DROP TABLE IF EXISTS `content_marksheet_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_marksheet_score` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content_test_section_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_score_step` varchar(256) DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_section_id` (`content_test_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_answer`
--

DROP TABLE IF EXISTS `content_test_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_answer_selection`
--

DROP TABLE IF EXISTS `content_test_answer_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_answer_selection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_list`
--

DROP TABLE IF EXISTS `content_test_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` varchar(256) NOT NULL DEFAULT '',
  `name` varchar(256) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `time_test` int(10) unsigned NOT NULL DEFAULT '0',
  `public_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `marksheet_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `public_date` varchar(256) NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_question`
--

DROP TABLE IF EXISTS `content_test_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `test_title` text NOT NULL,
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `big_question_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `question_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_kind` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_example` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `question` text NOT NULL,
  `hint` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_list_id` (`content_test_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_section`
--

DROP TABLE IF EXISTS `content_test_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `test_section_cd` (`test_section_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_status`
--

DROP TABLE IF EXISTS `content_test_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(3) unsigned NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_cd` int(3) unsigned zerofill NOT NULL DEFAULT '000',
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_cd` (`country_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(255) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duration`
--

DROP TABLE IF EXISTS `duration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` date NOT NULL DEFAULT '0000-00-00',
  `to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educational_institution`
--

DROP TABLE IF EXISTS `educational_institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educational_institution` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `educational_institution_type` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `head_quater` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `campus` varchar(50) NOT NULL DEFAULT '',
  `faculty` varchar(50) NOT NULL DEFAULT '',
  `is_postgraduate` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `operator_type` varchar(50) NOT NULL DEFAULT '',
  `researching_major` varchar(255) NOT NULL DEFAULT '',
  `speciality` varchar(255) NOT NULL DEFAULT '',
  `course` varchar(40) NOT NULL DEFAULT '',
  `comment` varchar(40) NOT NULL DEFAULT '',
  `is_independent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `researching_field` varchar(60) NOT NULL DEFAULT '',
  `university_affiliated` varchar(40) NOT NULL DEFAULT '',
  `faculty_name` varchar(40) NOT NULL DEFAULT '',
  `faculty_affiliated` varchar(40) NOT NULL DEFAULT '',
  `prefecture_code` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `district` varchar(20) NOT NULL DEFAULT '',
  `city_code` varchar(8) NOT NULL DEFAULT '',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(16) NOT NULL DEFAULT '',
  `fax` varchar(16) NOT NULL DEFAULT '',
  `k_code` varchar(16) NOT NULL DEFAULT '',
  `department_name` varchar(255) NOT NULL DEFAULT '',
  `people_cnt` int(10) unsigned NOT NULL DEFAULT '0',
  `is_target_in_list` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `homepage` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `educational_institution_type` (`educational_institution_type`),
  KEY `prefecture_code` (`prefecture_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `color` char(6) NOT NULL DEFAULT '',
  `school_year_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `school_year_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `is_on_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id_from` (`school_year_id_from`),
  KEY `school_year_id_to` (`school_year_id_to`),
  KEY `function_group_id` (`is_on_holiday`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_type_account_group`
--

DROP TABLE IF EXISTS `event_type_account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type_account_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `account_group_id` (`account_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `examination_answer`
--

DROP TABLE IF EXISTS `examination_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `answer_position` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_comment` text NOT NULL,
  `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `score` varchar(10) NOT NULL DEFAULT '',
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`),
  KEY `person_id` (`person_id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `selected_content_test_answer_id` (`content_test_answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `excel_3_external_result_jlpt_N123`
--

DROP TABLE IF EXISTS `excel_3_external_result_jlpt_N123`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `excel_3_external_result_jlpt_N123` (
  `No` text NOT NULL,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `実施月` text NOT NULL,
  `レベル` text NOT NULL,
  `クラス` text NOT NULL,
  `学籍番号` text NOT NULL,
  `氏名` text NOT NULL,
  `読み方` text NOT NULL,
  `国籍` text NOT NULL,
  `性別` text NOT NULL,
  `言語知識` text NOT NULL,
  `読解` text NOT NULL,
  `聴解` text NOT NULL,
  `合計` text NOT NULL,
  `合否` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `excel_4_external_result_jlpt_N45`
--

DROP TABLE IF EXISTS `excel_4_external_result_jlpt_N45`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `excel_4_external_result_jlpt_N45` (
  `No` text NOT NULL,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `実施月` text NOT NULL,
  `レベル` text NOT NULL,
  `クラス` text NOT NULL,
  `学籍番号` text NOT NULL,
  `氏名` text NOT NULL,
  `読み方` text NOT NULL,
  `国籍` text NOT NULL,
  `性別` text NOT NULL,
  `言語知識・読解` text NOT NULL,
  `聴解` text NOT NULL,
  `合計` text NOT NULL,
  `合否` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `excel_5_external_result_eju`
--

DROP TABLE IF EXISTS `excel_5_external_result_eju`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `excel_5_external_result_eju` (
  `No` text NOT NULL,
  `school_term_id` text NOT NULL,
  `実施月` text NOT NULL,
  `クラス` text NOT NULL,
  `学生番号` text NOT NULL,
  `氏名` text NOT NULL,
  `国籍` text NOT NULL,
  `性別` text NOT NULL,
  `聴解・聴読解` text NOT NULL,
  `読解` text NOT NULL,
  `合計` text NOT NULL,
  `記述` text NOT NULL,
  `総合科目` text NOT NULL,
  `物理` text NOT NULL,
  `化学` text NOT NULL,
  `生物` text NOT NULL,
  `数学1` text NOT NULL,
  `数学2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_result`
--

DROP TABLE IF EXISTS `external_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_result` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_date` varchar(7) NOT NULL DEFAULT '',
  `apply_status` int(2) unsigned DEFAULT '0',
  `input_status` int(2) unsigned NOT NULL DEFAULT '0',
  `result_status` int(2) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `test_country_name` varchar(128) NOT NULL DEFAULT '',
  `external_test_level_id` int(10) unsigned DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`disable`,`apply_status`,`input_status`,`result_status`),
  KEY `external_test_level_id` (`external_test_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_result_detail`
--

DROP TABLE IF EXISTS `external_result_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_result_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_result_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_section_id` int(10) unsigned DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `external_result_id` (`external_result_id`),
  KEY `external_section_cd` (`external_test_level_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test`
--

DROP TABLE IF EXISTS `external_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test_ave_eju`
--

DROP TABLE IF EXISTS `external_test_ave_eju`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test_ave_eju` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_date` varchar(7) NOT NULL DEFAULT '',
  `reading` int(10) unsigned NOT NULL DEFAULT '0',
  `listening` int(10) unsigned NOT NULL DEFAULT '0',
  `sub_japanese` int(10) unsigned NOT NULL DEFAULT '0',
  `writing` int(10) unsigned NOT NULL DEFAULT '0',
  `all_japanese` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test_level`
--

DROP TABLE IF EXISTS `external_test_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `total_threshold_point` int(10) unsigned NOT NULL DEFAULT '0',
  `total_perfect_score` int(10) unsigned NOT NULL DEFAULT '0',
  `level_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `external_test_id` (`external_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test_level_section`
--

DROP TABLE IF EXISTS `external_test_level_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test_level_section` (
  `id` int(10) unsigned NOT NULL,
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `section_name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `threshold_point` int(10) unsigned NOT NULL DEFAULT '0',
  `perfect_score` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family_in_japan`
--

DROP TABLE IF EXISTS `family_in_japan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_in_japan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `relationship` varchar(128) NOT NULL DEFAULT '',
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `first_name` varchar(128) NOT NULL DEFAULT '',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_intended_to_reside` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `employment` varchar(128) NOT NULL DEFAULT '',
  `residence_card_no` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function`
--

DROP TABLE IF EXISTS `function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `function_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `path` varchar(128) NOT NULL DEFAULT '',
  `is_in_menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `function_group_id` (`function_group_id`),
  KEY `is_in_menu` (`is_in_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function_group`
--

DROP TABLE IF EXISTS `function_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helper_content_management`
--

DROP TABLE IF EXISTS `helper_content_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helper_content_management` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_code_detail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(256) NOT NULL DEFAULT '',
  `style` varchar(128) NOT NULL DEFAULT '',
  `content` text,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `holiday_setting`
--

DROP TABLE IF EXISTS `holiday_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` char(6) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homework`
--

DROP TABLE IF EXISTS `homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_publish` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_acount_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `homework_status`
--

DROP TABLE IF EXISTS `homework_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homework_status` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `homework_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `score_rate` int(5) unsigned NOT NULL DEFAULT '0',
  `start_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_times` int(10) unsigned NOT NULL DEFAULT '0',
  `lastsubmit_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `homework_id` (`homework_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospitalization_history`
--

DROP TABLE IF EXISTS `hospitalization_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospitalization_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hospitalization_from` date NOT NULL DEFAULT '0000-00-00',
  `hospitalization_to` date NOT NULL DEFAULT '0000-00-00',
  `hospital_name` varchar(128) NOT NULL DEFAULT '',
  `disease_name_or_reason` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `immigration`
--

DROP TABLE IF EXISTS `immigration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immigration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `immigration_application_no` varchar(45) NOT NULL DEFAULT '',
  `visa_type` int(2) unsigned NOT NULL DEFAULT '0',
  `passport_no` varchar(128) NOT NULL DEFAULT '',
  `passport_expired_date` date NOT NULL DEFAULT '0000-00-00',
  `occupation_before_admission` varchar(128) NOT NULL DEFAULT '',
  `other_activity_result` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `other_activity_date` date NOT NULL DEFAULT '0000-00-00',
  `other_activity_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `other_activity_expiration_date` date NOT NULL DEFAULT '0000-00-00',
  `kanji_residence_card_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `acceptance_notification_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admission_permission_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `update_desired_extension_period` varchar(128) NOT NULL DEFAULT '',
  `update_required_years_until_graduation` varchar(128) NOT NULL DEFAULT '',
  `birth_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `birth_address1` varchar(255) NOT NULL DEFAULT '',
  `birth_address2` varchar(255) NOT NULL DEFAULT '',
  `birth_address3` varchar(255) NOT NULL DEFAULT '',
  `home_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `home_country_zipcode` varchar(20) NOT NULL DEFAULT '',
  `home_country_address1` varchar(255) NOT NULL DEFAULT '',
  `home_country_address2` varchar(255) NOT NULL DEFAULT '',
  `home_country_address3` varchar(255) NOT NULL DEFAULT '',
  `home_country_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `home_country_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `entry_date` date NOT NULL DEFAULT '0000-00-00',
  `scheduled_landing_port` varchar(128) NOT NULL DEFAULT '',
  `duration_of_stay` varchar(128) NOT NULL DEFAULT '',
  `is_partner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `acceptance_notification_output` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `intended_visa_apply_place` varchar(255) NOT NULL DEFAULT '',
  `is_past_entry_departure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `past_entry_times` int(3) unsigned NOT NULL DEFAULT '0',
  `last_entry_date` date NOT NULL DEFAULT '0000-00-00',
  `last_departure_date` date NOT NULL DEFAULT '0000-00-00',
  `is_crime` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `crime_content` varchar(128) NOT NULL DEFAULT '',
  `is_departure_of_deportation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `departure_of_deportation_times` int(3) unsigned NOT NULL DEFAULT '0',
  `last_departure_of_deportation_date` date NOT NULL DEFAULT '0000-00-00',
  `total_education_years` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education_status` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education_name` varchar(255) NOT NULL DEFAULT '',
  `last_school_education_graduation_date` date NOT NULL DEFAULT '0000-00-00',
  `last_school_education_other` varchar(128) NOT NULL DEFAULT '',
  `jp_lang_ability_test_name` varchar(128) NOT NULL DEFAULT '',
  `jp_lang_ability_level_or_score` varchar(128) NOT NULL DEFAULT '',
  `ability_proof_institution_name` varchar(255) NOT NULL DEFAULT '',
  `ability_proof_enrollment_start_date` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_enrollment_end_date` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_other_comment` varchar(128) NOT NULL DEFAULT '',
  `support_pay_amount_self` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_living_abroad` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_living_japan` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_scholarship` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_others` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_others_comment` varchar(128) NOT NULL DEFAULT '',
  `carrying_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_from_abroad_name` varchar(128) NOT NULL DEFAULT '',
  `carrying_from_abroad_date` date NOT NULL DEFAULT '0000-00-00',
  `remittance_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_remittance_other_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `supporter_comment` varchar(128) NOT NULL DEFAULT '',
  `supporter_rate` varchar(128) NOT NULL DEFAULT '',
  `provide_scholarship_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `provide_scholarship_name` varchar(128) NOT NULL DEFAULT '',
  `provide_scholarship_other` varchar(128) NOT NULL DEFAULT '',
  `plans_after_graduation_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `plans_after_graduation_other` varchar(128) NOT NULL DEFAULT '',
  `graduate_organization_name` varchar(128) NOT NULL DEFAULT '',
  `graduate_organization_school_type` varchar(128) NOT NULL DEFAULT '',
  `graduate_organization_graduation_date` date NOT NULL DEFAULT '0000-00-00',
  `graduate_organization_school_name` varchar(128) NOT NULL DEFAULT '',
  `residence_application_date` date NOT NULL DEFAULT '0000-00-00',
  `residence_application_result` int(2) unsigned NOT NULL DEFAULT '0',
  `residence_application_cancel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `residence_application_embassy_denial` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `residence_application_remarks` varchar(128) NOT NULL DEFAULT '',
  `residence_application_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `part_time_job_date` date NOT NULL DEFAULT '0000-00-00',
  `part_time_job_result` int(2) unsigned NOT NULL DEFAULT '0',
  `residence_application_update_date` date NOT NULL DEFAULT '0000-00-00',
  `residence_application_update_result` int(2) unsigned NOT NULL DEFAULT '0',
  `residence_update_reason` varchar(128) NOT NULL DEFAULT '',
  `residence_update_desired_extension_period` varchar(128) NOT NULL DEFAULT '',
  `residence_update_required_years_until_grad` varchar(128) NOT NULL DEFAULT '',
  `residence_update_result` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `residence_update_date` date NOT NULL DEFAULT '0000-00-00',
  `residence_update_is_outputted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `residence_update_is_outputted_date` date NOT NULL DEFAULT '0000-00-00',
  `other_activity_required_years` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interview`
--

DROP TABLE IF EXISTS `interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `comment` text NOT NULL,
  `interviewer_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `interviewer_person_id` (`interviewer_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(128) NOT NULL DEFAULT '',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `invoice_date` date NOT NULL DEFAULT '0000-00-00',
  `payment_period_from` date NOT NULL DEFAULT '0000-00-00',
  `payment_period_to` date NOT NULL DEFAULT '0000-00-00',
  `payment_due_date` date NOT NULL DEFAULT '0000-00-00',
  `invoice_admission_fee` int(10) NOT NULL DEFAULT '0',
  `enrollment_fee` int(10) NOT NULL DEFAULT '0',
  `facility_fee` int(10) NOT NULL DEFAULT '0',
  `equipment_fee` int(10) NOT NULL DEFAULT '0',
  `tuition_fee` int(10) NOT NULL DEFAULT '0',
  `dormitory_fee` int(10) NOT NULL DEFAULT '0',
  `reserve_cost1` int(10) NOT NULL DEFAULT '0',
  `reserve_cost2` int(10) NOT NULL DEFAULT '0',
  `reserve_cost3` int(10) NOT NULL DEFAULT '0',
  `subtotal_fee` bigint(20) NOT NULL DEFAULT '0',
  `art_class_payment_period` varchar(128) NOT NULL DEFAULT '',
  `art_class_tuition_fee` int(10) NOT NULL DEFAULT '0',
  `total_fee` bigint(20) NOT NULL DEFAULT '0',
  `remarks` varchar(255) NOT NULL DEFAULT '',
  `cancellation_check` tinyint(1) NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `late_threshold`
--

DROP TABLE IF EXISTS `late_threshold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `late_threshold` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `late_attend` int(3) unsigned NOT NULL DEFAULT '0',
  `absent_late` int(3) unsigned NOT NULL DEFAULT '0',
  `late_as_absent` int(3) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_import_education`
--

DROP TABLE IF EXISTS `log_import_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_import_education` (
  `code` varchar(512) DEFAULT '',
  `error_type` varchar(512) DEFAULT '',
  `error_name` varchar(512) DEFAULT '',
  `error_data` varchar(512) DEFAULT '',
  `error_content` varchar(512) DEFAULT '',
  `token` varchar(512) NOT NULL,
  `create_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL DEFAULT '',
  `method` varchar(255) NOT NULL DEFAULT '',
  `params` text,
  `ip_address` varchar(45) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `rtime` float DEFAULT '0',
  `authorized` varchar(1) NOT NULL DEFAULT '0',
  `response_code` smallint(3) DEFAULT '0',
  `response` text,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxrow`
--

DROP TABLE IF EXISTS `maxrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxrow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maxrow` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_coma`
--

DROP TABLE IF EXISTS `meeting_coma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_coma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coma_id` int(11) NOT NULL DEFAULT '0',
  `sequence` tinyint(4) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_list`
--

DROP TABLE IF EXISTS `meeting_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL,
  `meeting_name` varchar(50) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_teacher_id` int(11) DEFAULT NULL,
  `coma` tinyint(1) NOT NULL DEFAULT '0',
  `auto` tinyint(1) NOT NULL DEFAULT '0',
  `saved` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_resource`
--

DROP TABLE IF EXISTS `meeting_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL DEFAULT '0',
  `teacher_id` int(11) NOT NULL DEFAULT '0',
  `teacher_type` tinyint(4) NOT NULL DEFAULT '0',
  `meeting_id` int(11) NOT NULL DEFAULT '0',
  `coma` int(11) NOT NULL DEFAULT '0',
  `auto` tinyint(4) DEFAULT '0',
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_schedule`
--

DROP TABLE IF EXISTS `meeting_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_id` int(11) NOT NULL,
  `class_group_unit_id` int(11) NOT NULL,
  `meeting_name` varchar(50) NOT NULL DEFAULT '',
  `class_teacher_id` int(11) DEFAULT NULL,
  `coma` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `color_code` varchar(9) NOT NULL DEFAULT 'ffffff',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_schedule_teachers`
--

DROP TABLE IF EXISTS `meeting_schedule_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_schedule_teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL DEFAULT '0',
  `teacher_id` int(11) NOT NULL DEFAULT '0',
  `teacher_type` tinyint(4) NOT NULL DEFAULT '0',
  `meeting_id` int(11) NOT NULL DEFAULT '0',
  `coma` int(11) NOT NULL DEFAULT '0',
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_setting`
--

DROP TABLE IF EXISTS `meeting_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL DEFAULT '0',
  `max_coma` tinyint(4) NOT NULL DEFAULT '0',
  `general_meeting` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_setting_class`
--

DROP TABLE IF EXISTS `meeting_setting_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_setting_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `meeting_name` varchar(50) NOT NULL,
  `class_teacher_id` int(11) DEFAULT NULL,
  `class_teacher_type` tinyint(4) DEFAULT NULL,
  `class_teacher_non_participation` tinyint(1) NOT NULL DEFAULT '0',
  `non_meeting` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `meeting_setting_class_teachers`
--

DROP TABLE IF EXISTS `meeting_setting_class_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_setting_class_teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(11) NOT NULL DEFAULT '0',
  `teacher_id` int(11) NOT NULL DEFAULT '0',
  `teacher_type` tinyint(4) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `non_participation` tinyint(4) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastup_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monthly_status`
--

DROP TABLE IF EXISTS `monthly_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthly_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` date NOT NULL DEFAULT '0000-00-00',
  `is_attendance_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(512) NOT NULL DEFAULT '',
  `is_publish` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_level_id` int(10) NOT NULL DEFAULT '0',
  `is_examination` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `examination_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `examination_id` (`examination_id`),
  KEY `content_level_id` (`content_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movie_status`
--

DROP TABLE IF EXISTS `movie_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `movie_id` (`movie_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notice_management`
--

DROP TABLE IF EXISTS `notice_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice_management` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text,
  `date_start` date DEFAULT '0000-00-00',
  `date_finish` date DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned DEFAULT '0',
  `create_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notice_management_account_group`
--

DROP TABLE IF EXISTS `notice_management_account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice_management_account_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notice_management_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned DEFAULT '0',
  `create_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `notice_management_id` (`notice_management_id`),
  KEY `account_group_id` (`account_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operation_history`
--

DROP TABLE IF EXISTS `operation_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `mac_address` varchar(20) NOT NULL DEFAULT '',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(128) NOT NULL DEFAULT '',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL DEFAULT '0000-00-00',
  `payment_type` tinyint(1) NOT NULL DEFAULT '0',
  `payment_amount` int(10) NOT NULL DEFAULT '0',
  `payment_remaining` int(10) NOT NULL DEFAULT '0',
  `commission_fee` int(10) NOT NULL DEFAULT '0',
  `payment_admission_fee` int(10) NOT NULL DEFAULT '0',
  `remarks` varchar(255) NOT NULL DEFAULT '''''',
  `lastup_account_id` int(10) NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pdf_function`
--

DROP TABLE IF EXISTS `pdf_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdf_function` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'Screen name',
  `link` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link of screen, example: ''link1;link2;''',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Display order',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Type of pdf (Unused)',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `penalty`
--

DROP TABLE IF EXISTS `penalty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penalty` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `penalty_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `penalty_content` text,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `period` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `duration_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `is_extra` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `duration_id` (`duration_id`),
  KEY `time_zone_id` (`time_zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `function_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_permitted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`),
  KEY `function_group_id` (`function_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(3) unsigned NOT NULL DEFAULT '0',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `native_language` varchar(128) NOT NULL DEFAULT '',
  `native_language_en` varchar(255) NOT NULL DEFAULT '',
  `parent_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `sex_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `married_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `address1_en` varchar(255) NOT NULL DEFAULT '',
  `address2_en` varchar(255) NOT NULL DEFAULT '',
  `address3_en` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(20) NOT NULL DEFAULT '',
  `phone_no2` varchar(20) NOT NULL DEFAULT '',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `previous_address_zipcode` varchar(12) NOT NULL DEFAULT '',
  `previous_address1` varchar(255) NOT NULL DEFAULT '',
  `previous_address2` varchar(255) NOT NULL DEFAULT '',
  `previous_address3` varchar(255) NOT NULL DEFAULT '',
  `mail_address1` varchar(255) NOT NULL DEFAULT '',
  `mail_address2` varchar(255) NOT NULL DEFAULT '',
  `sns_account` varchar(255) NOT NULL DEFAULT '',
  `sns_type` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `first_name` varchar(128) NOT NULL DEFAULT '',
  `name_font` varchar(255) NOT NULL DEFAULT '',
  `last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `abbreviation_name` varchar(128) NOT NULL DEFAULT '',
  `last_name_native` varchar(128) NOT NULL DEFAULT '',
  `first_name_native` varchar(128) NOT NULL DEFAULT '',
  `photo` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `parent_person_id` (`parent_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quick_menu`
--

DROP TABLE IF EXISTS `quick_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quick_menu_detail`
--

DROP TABLE IF EXISTS `quick_menu_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_menu_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quick_menu_id` int(10) unsigned NOT NULL DEFAULT '0',
  `function_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `quick_menu_id` (`quick_menu_id`),
  KEY `function_id` (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_no` varchar(128) NOT NULL DEFAULT '',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reward`
--

DROP TABLE IF EXISTS `reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reward_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reward_penalty_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_printed` tinyint(1) unsigned DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_number` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(128) NOT NULL DEFAULT '',
  `principal_name` varchar(128) NOT NULL DEFAULT '',
  `english_principal_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `country_id` int(10) DEFAULT '0',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `zipcode2` varchar(12) DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) DEFAULT '',
  `english_address` varchar(128) NOT NULL DEFAULT '',
  `english_address2` varchar(128) DEFAULT '',
  `phone_no` varchar(16) NOT NULL DEFAULT '',
  `phone_no2` varchar(16) DEFAULT '',
  `fax_no` varchar(16) NOT NULL DEFAULT '',
  `fax_no2` varchar(16) DEFAULT '',
  `international_phone_no` varchar(20) NOT NULL DEFAULT '',
  `international_fax_no` varchar(20) NOT NULL DEFAULT '',
  `international_phone_no2` varchar(20) DEFAULT '',
  `international_fax_no2` varchar(20) DEFAULT '',
  `school_type_certification_no` varchar(4) NOT NULL DEFAULT '',
  `immigration_office` varchar(255) NOT NULL DEFAULT '',
  `immigration_office_department` varchar(255) NOT NULL DEFAULT '',
  `applicant` varchar(128) NOT NULL DEFAULT '',
  `relationship_with_applicant` varchar(128) NOT NULL DEFAULT '',
  `applicant_mobile_number` varchar(16) NOT NULL DEFAULT '',
  `immigr_mng_num_first` varchar(4) NOT NULL DEFAULT '',
  `immigr_mng_num_second` varchar(4) NOT NULL DEFAULT '',
  `fiscal_year` varchar(4) NOT NULL DEFAULT '',
  `vacation_period_from1` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to1` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from2` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to2` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from3` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to3` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from4` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to4` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from5` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to5` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from6` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to6` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from7` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to7` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from8` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to8` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_from9` date NOT NULL DEFAULT '0000-00-00',
  `vacation_period_to9` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_calendar`
--

DROP TABLE IF EXISTS `school_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `is_public_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_school_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_event`
--

DROP TABLE IF EXISTS `school_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `event_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `remark` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`),
  KEY `event_type_id` (`event_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record`
--

DROP TABLE IF EXISTS `school_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(11) NOT NULL DEFAULT '0',
  `person_id` int(11) NOT NULL DEFAULT '0',
  `observation_internal` text NOT NULL,
  `observation_internal_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `observation_external` text NOT NULL,
  `observation_external_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `observation_external_english` text NOT NULL,
  `observation_external_english_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `homework_submission_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `observation_internal_account_id` (`observation_internal_account_id`),
  KEY `observation_external_account_id` (`observation_external_account_id`),
  KEY `observation_external_english_account_id` (`observation_external_english_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record_detail`
--

DROP TABLE IF EXISTS `school_record_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_record_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_record_section_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grade` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_record_id` (`school_record_id`),
  KEY `school_record_section_id` (`school_record_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record_section`
--

DROP TABLE IF EXISTS `school_record_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_term`
--

DROP TABLE IF EXISTS `school_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`),
  KEY `school_term_type_id` (`school_term_type_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_term_type`
--

DROP TABLE IF EXISTS `school_term_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_term_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `month` int(2) unsigned NOT NULL DEFAULT '0',
  `sequence` int(2) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_year`
--

DROP TABLE IF EXISTS `school_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_year` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` date NOT NULL DEFAULT '0000-00-00',
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shift_attendance`
--

DROP TABLE IF EXISTS `shift_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift_attendance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shift_attendance_history`
--

DROP TABLE IF EXISTS `shift_attendance_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift_attendance_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status_cd` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_authorized_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_leave_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_is_temporary_return` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `original_comment` text NOT NULL,
  `edit_is_authorized_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_is_leave_absent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_is_temporary_return` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `edit_comment` text NOT NULL,
  `reject_reason` text NOT NULL,
  `approval_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `approval_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `status_cd` (`status_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shift_upload`
--

DROP TABLE IF EXISTS `shift_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift_upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_from` date NOT NULL DEFAULT '0000-00-00',
  `display_to` date NOT NULL DEFAULT '0000-00-00',
  `upload_file_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `file_path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `staff_no` varchar(128) NOT NULL DEFAULT '',
  `employment_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `entry_date` date NOT NULL DEFAULT '0000-00-00',
  `retirement_date` date NOT NULL DEFAULT '0000-00-00',
  `retirement_reason` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `staff_no` (`staff_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff_ability`
--

DROP TABLE IF EXISTS `staff_ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_group_id` int(11) NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff_qualification`
--

DROP TABLE IF EXISTS `staff_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_qualification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `external_test_level_id` (`external_test_level_id`),
  KEY `lastup_account_id` (`lastup_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `agent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sales_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `student_no` varchar(128) NOT NULL DEFAULT '',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0',
  `course_length_month` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `memo` text NOT NULL,
  `start_date_intended` date NOT NULL DEFAULT '0000-00-00',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date_intended` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `weekly_study_hours` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `final_judgement` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status_print` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `final_judgement_all_attend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `final_judgement_all_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quit_reason_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `reason_for_exclusion` varchar(1000) NOT NULL DEFAULT '',
  `student_type_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `after_graduate_school` varchar(255) NOT NULL DEFAULT '',
  `general_observation` text NOT NULL,
  `general_observation_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `general_observation_date` date NOT NULL DEFAULT '0000-00-00',
  `start_preferred_date_intended` int(10) unsigned NOT NULL DEFAULT '0',
  `part_time_job_name` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address1` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address2` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address3` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_phone_no` varchar(20) NOT NULL DEFAULT '',
  `continuous_absent_count` int(3) unsigned NOT NULL DEFAULT '0',
  `continuous_absent_class_id` int(3) unsigned NOT NULL DEFAULT '0',
  `old_class_name` varchar(128) NOT NULL DEFAULT '',
  `is_old_class` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_attendance_exclusion` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `art_class_name` varchar(128) NOT NULL DEFAULT '',
  `is_use_dormitory` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_insured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `important_points` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `course_id` (`course_id`),
  KEY `ability_id` (`ability_id`),
  KEY `student_no` (`student_no`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `agent_id` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student_dropout`
--

DROP TABLE IF EXISTS `student_dropout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_dropout` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `original_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `edit_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `edit_date` date NOT NULL DEFAULT '0000-00-00',
  `dropout_reason` text NOT NULL,
  `is_update` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student_interview`
--

DROP TABLE IF EXISTS `student_interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_interview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `interview_title` varchar(128) NOT NULL DEFAULT '',
  `interview_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `in_the_interview_attendance_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `in_the_interview_class_name` varchar(128) NOT NULL DEFAULT '',
  `interviewer_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment_attendance_status` text NOT NULL,
  `comment_after_graduate` text NOT NULL,
  `comment_other` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student_part_time_job`
--

DROP TABLE IF EXISTS `student_part_time_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_part_time_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL DEFAULT '0',
  `is_main_part_time_job` tinyint(4) NOT NULL DEFAULT '0',
  `company` varchar(128) NOT NULL DEFAULT '',
  `part_time_job_type_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `part_time_job_type_other` varchar(128) NOT NULL DEFAULT '',
  `job_length` varchar(128) NOT NULL DEFAULT '',
  `working_hours_per_week` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_industry_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `company_zipcode` varchar(12) NOT NULL DEFAULT '',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(255) NOT NULL DEFAULT '',
  `agent` varchar(255) NOT NULL DEFAULT '',
  `work_from_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_0` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_1` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_2` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_3` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_4` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_5` time NOT NULL DEFAULT '00:00:00',
  `work_from_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `work_to_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `break_time_day_6` time NOT NULL DEFAULT '00:00:00',
  `hourly_wage` float NOT NULL DEFAULT '0',
  `monthly_wage` float NOT NULL DEFAULT '0',
  `salary` float NOT NULL DEFAULT '0',
  `part_time_job_salary_unit_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_request_to_apply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(11) NOT NULL DEFAULT '0',
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `supporter`
--

DROP TABLE IF EXISTS `supporter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supporter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `first_name` varchar(128) NOT NULL DEFAULT '',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(20) NOT NULL DEFAULT '',
  `employer_name` varchar(128) NOT NULL DEFAULT '',
  `employer_phone_no` varchar(20) NOT NULL DEFAULT '',
  `annual_income_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `supporter_relationship_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system8`
--

DROP TABLE IF EXISTS `system8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system8` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_no` varchar(128) NOT NULL DEFAULT '',
  `system8_id` varchar(128) NOT NULL DEFAULT '',
  `name_font` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `name_kana` varchar(128) NOT NULL DEFAULT '',
  `name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `sex` varchar(2) NOT NULL DEFAULT '',
  `married` varchar(4) NOT NULL DEFAULT '',
  `country` varchar(128) NOT NULL DEFAULT '',
  `birthday` varchar(128) NOT NULL DEFAULT '00000000',
  `agent` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_address_font` varchar(255) NOT NULL DEFAULT '',
  `legal_registered_address` varchar(255) NOT NULL DEFAULT '',
  `legal_registered_phone_no` varchar(20) NOT NULL DEFAULT '',
  `passport_no` varchar(128) NOT NULL DEFAULT '',
  `expiry_date` varchar(128) NOT NULL DEFAULT '00000000',
  `entry_date` varchar(128) NOT NULL DEFAULT '00000000',
  `visa_type` varchar(4) NOT NULL DEFAULT '',
  `visa_update_date1` varchar(128) NOT NULL DEFAULT '00000000',
  `visa_update_date2` varchar(128) NOT NULL DEFAULT '00000000',
  `visa_update_date3` varchar(128) NOT NULL DEFAULT '00000000',
  `visa_update_date4` varchar(128) NOT NULL DEFAULT '00000000',
  `immigration_no` varchar(128) NOT NULL DEFAULT '',
  `immigration_date` varchar(128) NOT NULL DEFAULT '00000000',
  `residence_no` varchar(128) NOT NULL DEFAULT '',
  `start_date` varchar(128) NOT NULL DEFAULT '00000000',
  `end_date_intended` varchar(128) NOT NULL DEFAULT '00000000',
  `course_name` varchar(128) NOT NULL DEFAULT '',
  `class_name` varchar(128) NOT NULL DEFAULT '',
  `end_date1` varchar(128) NOT NULL DEFAULT '00000000',
  `end_date2` varchar(128) NOT NULL DEFAULT '00000000',
  `reason` varchar(255) NOT NULL DEFAULT '',
  `status_name` varchar(4) NOT NULL DEFAULT '',
  `previous_zipcode` varchar(12) NOT NULL DEFAULT '',
  `previous_address` varchar(255) NOT NULL DEFAULT '',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(20) NOT NULL DEFAULT '',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `national_health_insurance_no` varchar(128) NOT NULL DEFAULT '',
  `accident_insurance` varchar(4) NOT NULL DEFAULT '',
  `native_language` varchar(128) NOT NULL DEFAULT '',
  `after_graduate_school` varchar(255) NOT NULL DEFAULT '',
  `agent_font` varchar(255) NOT NULL DEFAULT '',
  `agent_name1` varchar(128) NOT NULL DEFAULT '',
  `agent_name2` varchar(128) NOT NULL DEFAULT '',
  `agent_address1` varchar(255) NOT NULL DEFAULT '',
  `agent_address2` varchar(255) NOT NULL DEFAULT '',
  `agent_phone_no` varchar(20) NOT NULL DEFAULT '',
  `agent_fax_no` varchar(20) NOT NULL DEFAULT '',
  `j_test` varchar(128) NOT NULL DEFAULT '',
  `nat_test` varchar(128) NOT NULL DEFAULT '',
  `start_test` varchar(128) NOT NULL DEFAULT '',
  `class_shift_test` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system8_1_student_base`
--

DROP TABLE IF EXISTS `system8_1_student_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system8_1_student_base` (
  `学籍番号` text NOT NULL,
  `登録番号` text NOT NULL,
  `本国氏名書体` text NOT NULL,
  `本国氏名1` text NOT NULL,
  `本国氏名2` text NOT NULL,
  `フリガナ名` text NOT NULL,
  `性別` text NOT NULL,
  `未既婚` text NOT NULL,
  `国名` text NOT NULL,
  `生年月日` text NOT NULL,
  `紹介者名` text NOT NULL,
  `本国住所書体` text NOT NULL,
  `本国住所` text NOT NULL,
  `本国電話` text NOT NULL,
  `旅券番号` text NOT NULL,
  `旅券有効期限` text NOT NULL,
  `入国年月日` text NOT NULL,
  `生VISA種類` text NOT NULL,
  `VISA更新日(1)` text NOT NULL,
  `VISA更新日(2)` text NOT NULL,
  `VISA更新日(3)` text NOT NULL,
  `VISA更新日(4)` text NOT NULL,
  `入管許可番号` text NOT NULL,
  `入管許可日` text NOT NULL,
  `在留カード番号` text NOT NULL,
  `入学日` text NOT NULL,
  `卒修予定日` text NOT NULL,
  `コース名` text NOT NULL,
  `クラス名` text NOT NULL,
  `退学年月日` text NOT NULL,
  `除籍年月日` text NOT NULL,
  `退学除籍理由` text NOT NULL,
  `在籍卒業区分` text NOT NULL,
  `以前住所〒` text NOT NULL,
  `以前住所住` text NOT NULL,
  `現住所〒` text NOT NULL,
  `現住所住` text NOT NULL,
  `現住所電話` text NOT NULL,
  `携　帯電話` text NOT NULL,
  `国民健康保険番号` text NOT NULL,
  `障害保険有無` text NOT NULL,
  `母国語` text NOT NULL,
  `卒業後進路先` text NOT NULL,
  `紹介者書体` text NOT NULL,
  `紹介者名1` text NOT NULL,
  `紹介者名2` text NOT NULL,
  `紹介者住所1` text NOT NULL,
  `紹介者住所2` text NOT NULL,
  `紹介者TEL` text NOT NULL,
  `紹介者FAX` text NOT NULL,
  `J   ﾃｽﾄ` text NOT NULL,
  `NAT ﾃｽﾄ` text NOT NULL,
  `入学ﾃｽﾄ` text NOT NULL,
  `組分ﾃｽﾄ` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system8_2_student_other`
--

DROP TABLE IF EXISTS `system8_2_student_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system8_2_student_other` (
  `学籍番号` text NOT NULL,
  `登録番号` text NOT NULL,
  `本国氏名書体` text NOT NULL,
  `本国氏名1` text NOT NULL,
  `本国氏名2` text NOT NULL,
  `フリガナ名` text NOT NULL,
  `性別` text NOT NULL,
  `未既婚` text NOT NULL,
  `国名` text NOT NULL,
  `生年月日` text NOT NULL,
  `紹介者名` text NOT NULL,
  `保護者名書体` text NOT NULL,
  `保護者名` text NOT NULL,
  `保護者住所書体` text NOT NULL,
  `保護者住所` text NOT NULL,
  `保護者電話` text NOT NULL,
  `保護者続柄` text NOT NULL,
  `保護者職業` text NOT NULL,
  `本人最終学歴` text NOT NULL,
  `本人最終卒業日` text NOT NULL,
  `本人最終修学年数` text NOT NULL,
  `本人最終職業` text NOT NULL,
  `本人最終会社名` text NOT NULL,
  `修学予定年数` text NOT NULL,
  `卒業後予定` text NOT NULL,
  `希望進路先(1)` text NOT NULL,
  `希望進路先(2)` text NOT NULL,
  `希望進路先(3)` text NOT NULL,
  `確認年月日1` text NOT NULL,
  `職場名1` text NOT NULL,
  `職場場所1` text NOT NULL,
  `職場電話1` text NOT NULL,
  `職種1` text NOT NULL,
  `時給1` text NOT NULL,
  `勤務時間1` text NOT NULL,
  `許可1` text NOT NULL,
  `確認年月日2` text NOT NULL,
  `職場名2` text NOT NULL,
  `職場場所2` text NOT NULL,
  `職場電話2` text NOT NULL,
  `職種2` text NOT NULL,
  `時給2` text NOT NULL,
  `勤務時間2` text NOT NULL,
  `許可2` text NOT NULL,
  `確認年月日3` text NOT NULL,
  `職場名3` text NOT NULL,
  `職場場所3` text NOT NULL,
  `職場電話3` text NOT NULL,
  `職種3` text NOT NULL,
  `時給3` text NOT NULL,
  `勤務時間3` text NOT NULL,
  `許可3` text NOT NULL,
  `確認年月日4` text NOT NULL,
  `職場名4` text NOT NULL,
  `職場場所4` text NOT NULL,
  `職場電話4` text NOT NULL,
  `職種4` text NOT NULL,
  `時給4` text NOT NULL,
  `勤務時間4` text NOT NULL,
  `許可4` text NOT NULL,
  `確認年月日5` text NOT NULL,
  `職場名5` text NOT NULL,
  `職場場所5` text NOT NULL,
  `職場電話5` text NOT NULL,
  `職種5` text NOT NULL,
  `時給5` text NOT NULL,
  `勤務時間5` text NOT NULL,
  `許可5` text NOT NULL,
  `確認年月日6` text NOT NULL,
  `職場名6` text NOT NULL,
  `職場場所6` text NOT NULL,
  `職場電話6` text NOT NULL,
  `職種6` text NOT NULL,
  `時給6` text NOT NULL,
  `勤務時間6` text NOT NULL,
  `許可6` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system8_3_immigration`
--

DROP TABLE IF EXISTS `system8_3_immigration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system8_3_immigration` (
  `学籍番号` text NOT NULL,
  `登録番号` text NOT NULL,
  `本国氏名書体` text NOT NULL,
  `本国氏名1` text NOT NULL,
  `本国氏名2` text NOT NULL,
  `フリガナ名` text NOT NULL,
  `性別` text NOT NULL,
  `未既婚` text NOT NULL,
  `国名` text NOT NULL,
  `生年月日` text NOT NULL,
  `紹介者名` text NOT NULL,
  `出生地書体` text NOT NULL,
  `出生地` text NOT NULL,
  `本国書体` text NOT NULL,
  `本国居住地` text NOT NULL,
  `査証申請予定地書体` text NOT NULL,
  `査証申請予定地` text NOT NULL,
  `過去出入国歴有無` text NOT NULL,
  `過去出入国回数` text NOT NULL,
  `直近出入国年月日～` text NOT NULL,
  `～直近出入国年月日` text NOT NULL,
  `卒業までの所要年数` text NOT NULL,
  `最終学歴状況（在・卒・休）` text NOT NULL,
  `最終学歴状況（学校種別）` text NOT NULL,
  `学校名書体` text NOT NULL,
  `学校名` text NOT NULL,
  `卒業・見込年月日` text NOT NULL,
  `日本語能力試験（級）` text NOT NULL,
  `日本語能力留学試験得点` text NOT NULL,
  `日本語能力Ｊテスト結果` text NOT NULL,
  `日本語教育機関書体` text NOT NULL,
  `日本語教育機関名` text NOT NULL,
  `日本語教育年月日～` text NOT NULL,
  `～日本語教育年月日` text NOT NULL,
  `支弁方法＿本人負担額` text NOT NULL,
  `支弁方法＿外国から送金額` text NOT NULL,
  `支弁方法＿外国から携行額` text NOT NULL,
  `支弁方法＿携行者名書体` text NOT NULL,
  `支弁方法＿携行者` text NOT NULL,
  `支弁方法＿携行時期` text NOT NULL,
  `支弁方法＿在日支弁負担額` text NOT NULL,
  `支弁方法＿奨学金` text NOT NULL,
  `支弁方法＿その他` text NOT NULL,
  `支弁方法＿欄外入学金他前払` text NOT NULL,
  `支弁者名1書体` text NOT NULL,
  `支弁者名1` text NOT NULL,
  `支弁者住所1書体` text NOT NULL,
  `支弁者住所1` text NOT NULL,
  `支弁者電話1` text NOT NULL,
  `支弁者勤務先1書体` text NOT NULL,
  `支弁者勤務先1` text NOT NULL,
  `支弁者勤務先電話1` text NOT NULL,
  `支弁者年収1` text NOT NULL,
  `支弁者申請者との関係1` text NOT NULL,
  `支弁者名2書体` text NOT NULL,
  `支弁者名2` text NOT NULL,
  `支弁者住所2書体` text NOT NULL,
  `支弁者住所2` text NOT NULL,
  `支弁者電話2` text NOT NULL,
  `支弁者勤務先2書体` text NOT NULL,
  `支弁者勤務先2` text NOT NULL,
  `支弁者勤務先2電話` text NOT NULL,
  `支弁者年収2` text NOT NULL,
  `支弁者申請者との関係2` text NOT NULL,
  `職業` text NOT NULL,
  `旅券番号` text NOT NULL,
  `旅券＿有効期限` text NOT NULL,
  `修学年数` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_code`
--

DROP TABLE IF EXISTS `system_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_code_detail`
--

DROP TABLE IF EXISTS `system_code_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_code_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_code_id` int(10) unsigned NOT NULL DEFAULT '0',
  `code1` int(3) unsigned NOT NULL DEFAULT '0',
  `code2` int(3) unsigned NOT NULL DEFAULT '0',
  `code3` int(3) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_code_id` (`system_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tablet_application_update`
--

DROP TABLE IF EXISTS `tablet_application_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tablet_application_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version_name` varchar(128) NOT NULL DEFAULT '',
  `download_url` varchar(255) NOT NULL DEFAULT '',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_latest_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_latest_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teacher_class_history`
--

DROP TABLE IF EXISTS `teacher_class_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_class_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `total_class_time` int(10) unsigned NOT NULL DEFAULT '0',
  `period_wage` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teacher_school_term`
--

DROP TABLE IF EXISTS `teacher_school_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_school_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `period_wage` int(10) unsigned NOT NULL DEFAULT '0',
  `work_days_week` int(10) unsigned NOT NULL DEFAULT '0',
  `work_times_week` int(10) unsigned NOT NULL DEFAULT '0',
  `is_full_day_request` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment_request` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `school_term_id` (`school_term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_number` int(10) unsigned NOT NULL DEFAULT '0',
  `display_file_name` varchar(50) NOT NULL DEFAULT '',
  `physical_file_name` varchar(50) NOT NULL DEFAULT '',
  `orientation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pdf_function_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The screen use this template (foreign key)',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temporary_attendance_time`
--

DROP TABLE IF EXISTS `temporary_attendance_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temporary_attendance_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_level`
--

DROP TABLE IF EXISTS `test_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_section`
--

DROP TABLE IF EXISTS `test_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_level_id` (`test_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table`
--

DROP TABLE IF EXISTS `time_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `period_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_break` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `time_table_group_id` (`time_table_group_id`),
  KEY `period_id` (`period_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table_group`
--

DROP TABLE IF EXISTS `time_table_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_table_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone`
--

DROP TABLE IF EXISTS `time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_attendance`
--

DROP TABLE IF EXISTS `tmp_20180507_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_attendance` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_class_entry_exit`
--

DROP TABLE IF EXISTS `tmp_20180507_class_entry_exit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_class_entry_exit` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_time` time NOT NULL DEFAULT '00:00:00',
  `exit_time` time NOT NULL DEFAULT '00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_class_shift_work`
--

DROP TABLE IF EXISTS `tmp_20180507_class_shift_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_class_shift_work` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_class_student`
--

DROP TABLE IF EXISTS `tmp_20180507_class_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_class_student` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_class_work`
--

DROP TABLE IF EXISTS `tmp_20180507_class_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_class_work` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `period_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tmp_20180507_shift_attendance`
--

DROP TABLE IF EXISTS `tmp_20180507_shift_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_20180507_shift_attendance` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transfer_class_properties`
--

DROP TABLE IF EXISTS `transfer_class_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfer_class_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_change_month` date NOT NULL DEFAULT '0000-00-00',
  `is_class_change_teacher_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_teacher_confirm` date DEFAULT '0000-00-00',
  `class_change_time_zone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason_comment` text,
  `class_change_comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visa`
--

DROP TABLE IF EXISTS `visa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `visa_length_of_stay` varchar(128) NOT NULL DEFAULT '',
  `residence_card_no` varchar(128) NOT NULL DEFAULT '',
  `permit_other_activitiy_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'midream_work'
--
/*!50003 DROP FUNCTION IF EXISTS `fn_get_id_by_column_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` FUNCTION `fn_get_id_by_column_name`(
  `pr_system_code_name` VARCHAR(100),
  `pr_system_code_detail_name` VARCHAR(100)

) RETURNS int(11)
BEGIN
    SET @id = (SELECT id FROM application_type WHERE column_name = pr_system_code_detail_name);
    IF @id IS NULL THEN
      SET @id = 0;
    END IF;
    RETURN @id;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_real_attend_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` FUNCTION `fn_get_real_attend_day`(
	`pr_person_id` INT,
	`pr_from_date` DATE,
	`pr_to_date` DATE
) RETURNS int(11)
    DETERMINISTIC
    COMMENT 'get real attend day from date to date'
BEGIN
  IF (pr_from_date > pr_to_date) THEN
    SET @real_attend = -1;
  ELSE
    SET @real_attend = (SELECT COUNT(date)
                FROM school_calendar
                WHERE
                  disable = 0
                  AND is_school_holiday = 0
                  AND is_public_holiday = 0
                  AND date >= pr_from_date
                  AND date <= pr_to_date);
	END IF;
	RETURN (@real_attend);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_system_code` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` FUNCTION `fn_get_system_code`(
	`pr_system_code_name` VARCHAR(100),
	`pr_system_code_detail_name` VARCHAR(100)

) RETURNS int(11)
    DETERMINISTIC
    COMMENT 'get system code detail'
BEGIN
  SET @value = (SELECT
            SCD.code1
          FROM
            system_code_detail SCD INNER JOIN system_code SC ON SCD.system_code_id = SC.id
          WHERE
            SC.column_name = pr_system_code_name
            AND SCD.column_name = pr_system_code_detail_name);
  IF (@value IS NULL) THEN
    RETURN 0;
  ELSE
    RETURN @value;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_application_leave_absence_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_get_application_leave_absence_info`(
	IN `pr_application_id` INT

)
    COMMENT 'Get application leave absence info'
BEGIN
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @full_time_teacher = fn_get_system_code('staff_classification','full-time_faculty_staff');
	 SET @part_time_teacher = fn_get_system_code('staff_classification','part-time_faculty_staff');
	 SET @special_teacher = fn_get_system_code('staff_classification','special_lecturer');

	 
	 SELECT
		 	application_approver_id, approval_date
		 INTO
		 	@teacher_id, @teacher_approval_date
		 FROM
		 	application_approve
		 WHERE
		 	application_id = pr_application_id
			 AND (approver_cd = @full_time_teacher OR approver_cd = @part_time_teacher OR approver_cd = @special_teacher)
	 	LIMIT 1;

	 SET @teacher_name = (SELECT CONCAT(last_name, ' ', first_name) as full_name FROM person WHERE id = @teacher_id);

    SELECT
        A.application_no,
        A.application_type_id,
        A.application_date,
        A.status_cd,
        ALAP.leave_absence_date_from as request_leaving_date_from,
        ALAP.leave_absence_date_to as request_leaving_date_to,
        ALAP.leave_absence_approval_date_from as approval_leaving_date_from,
        ALAP.leave_absence_approval_date_to as approval_leaving_date_to,
        ALAP.leave_absence_return_country_phone_no as leaving_return_country_phone_no,
        ALAP.leave_absence_return_country_mail_address as leaving_return_country_mail_address,
        ALAP.leave_absence_return_country_address1 as leaving_return_country_address1,
        ALAP.leave_absence_return_country_address2 as leaving_return_country_address2,
        ALAP.leave_absence_return_country_address3 as leaving_return_country_address3,
        ALAP.leave_absence_reason_id as reason_absence,
        ALAP.leave_absence_reason_detail as reason_absence_detail,
        ALAP.leave_absence_note as school_type_note,
        

        A2.g7_approval_date,
        A2.g7_confirmed_document as confirmed_document_secretary_general,
        A2.g7_reject_reason as reject_reason_secretary_general,
        A2.g7_comment as comment_secretary_general,
        A2.g7_status_cd as status_cd_secretary_general,

        A3.g6_approval_date,
        A3.g6_confirmed_document as confirmed_document_college_chief,
        A3.g6_reject_reason as reject_reason_college_chief,
        A3.g6_comment as comment_college_chief,
        A3.g6_status_cd status_cd_college_chief,

        A4.g9_approval_date,
        A4.g9_confirmed_document as confirmed_document_head_school,
        A4.g9_reject_reason as reject_reason_head_school,
        A4.g9_comment as comment_head_school,
        A4.g9_status_cd as status_cd_head_school,

        @teacher_approval_date as leave_absence_teacher_confirm_date,
        @teacher_name as leave_absence_teacher_confirm_name,
        @teacher_id as teacher_id

	 FROM
        application A
        INNER JOIN application_leave_absence_properties ALAP ON A.id = ALAP.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g7_approval_date,
                confirmed_document as g7_confirmed_document,
                reject_reason as g7_reject_reason,
                comment as g7_comment,
                status_cd as g7_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','ceo') and disable = 0) A2 ON A.id = A2.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g6_approval_date,
                confirmed_document as g6_confirmed_document,
                reject_reason as g6_reject_reason,
                comment as g6_comment,
                status_cd as g6_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','education_chief') and disable = 0) A3 ON A.id = A3.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g9_approval_date,
                confirmed_document as g9_confirmed_document,
                reject_reason as g9_reject_reason,
                comment as g9_comment,
                status_cd as g9_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','head_teacher') and disable = 0) A4 ON A.id = A4.application_id
    WHERE A.id = pr_application_id and A.application_type_id = @app_type_id and A.disable = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_real_attend_by_student_no` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_get_real_attend_by_student_no`(IN `pr_student_no` varchar(128), IN `pr_from_date` date, IN `pr_to_date` date)
    COMMENT 'Get real attend of current class'
get_attend:BEGIN
	SET @person_id = (SELECT person_id FROM student WHERE student_no = pr_student_no);
	IF @person_id IS NULL THEN
		SELECT 1 as error, 'student not found' as errmsg;
		LEAVE get_attend;
	END IF;

	SET @real_attend = fn_get_real_attend_day(pr_student_no, pr_from_date, pr_to_date);
	IF @real_attend > 0 THEN
		/* count attendance data */
		SET @attend_count = (SELECT
						count(CSW.date)
					FROM
						shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
					WHERE
						SA.person_id = @person_id
						and CSW.date >= pr_from_date
						and CSW.date <= pr_to_date
						and SA.disable = 0
						and CSW.disable = 0);
		/* check attendance valid */
		IF @real_attend > @attend_count THEN
			SELECT 2 as error, 'attendance data not found' as errmsg;
			LEAVE get_attend;
		END IF;

		/* check class_student Max date valid */
		SET @current_class_last_date = (SELECT
						MAX(CSW.date)
					FROM
						shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
					WHERE
						SA.person_id = @person_id
						and CSW.date >= pr_from_date
						and CSW.date <= pr_to_date
						and SA.disable = 0
						and CSW.disable = 0);
		IF pr_to_date > @current_class_last_date THEN
			SELECT 2 as error, 'beyond the last day current_class' as errmsg;
			LEAVE get_attend;
		END IF;

		/* valid result */
		SELECT 0 as error,  @real_attend as real_attend_day;
	ELSE
		SELECT 2 as error, 'attendance data not found' as errmsg;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_student_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_get_student_info`(
    IN `param_student_no` VARCHAR(128)
)
    COMMENT 'Get current info of student'
BEGIN
    SELECT person_id, id INTO @person_id, @student_id FROM student WHERE student_no = param_student_no and disable = 0;
    /* get current class */
    SET @class_id = COALESCE((SELECT class_id FROM class_student WHERE disable = 0 and person_id = @person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1), '');
    IF  @class_id = '' THEN
        SET @class_id =  COALESCE((SELECT class_id FROM class_student WHERE disable = 0 and person_id = @person_id and date_to < CURRENT_DATE() order by date_to desc, date_from desc, id desc limit 1), '');
    END IF;
    IF  @class_id <> '' THEN
         SET @jlpt_level = (SELECT ETL.name 
                                FROM 
                                    class C 
                                    inner join class_group CG on C.class_group_id = CG.id
                                    inner join ability A on A.id = CG.ability_id
                                    inner join external_test_level ETL on ETL.id = A.external_test_level_id
                                where C.id = @class_id and ETL.`disable` = 0 and A.`disable` = 0 and CG.`disable` = 0);
    END IF;
    SET @class_name = (SELECT name FROM class WHERE id = @class_id);
    SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
    SET @teacher_account_id = (SELECT id FROM account WHERE person_id = @teacher_person_id);
    SET @teacher_name = (SELECT CONCAT(last_name, ' ', first_name) as full_name FROM person WHERE id = @teacher_person_id);
    SET @total_leave_absent = (SELECT count(DISTINCT CSW.date) FROM shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id WHERE person_id = @person_id and SA.is_leave_absent = 1);
     SELECT residence_card_no, permit_other_activitiy_cd, end_date INTO @residence_card_no, @permit_other_activitiy_cd, @visa_expired_date FROM visa WHERE person_id = @person_id AND disable = 0 ORDER BY id DESC LIMIT 1;
     SET @sales_staff_person_id = (SELECT sales_staff_person_id FROM student WHERE student_no = param_student_no and disable = 0);

    SELECT
          P.last_name,
          P.first_name,
        CONCAT(P.last_name, ' ', P.first_name) as full_name,
        P.last_name_kana,
        P.first_name_kana,
        CONCAT(P.last_name_kana, ' ', P.first_name_kana) as full_name_kana,
        P.last_name_alphabet,
        P.first_name_alphabet,
        CONCAT(P.last_name_alphabet, ' ', P.first_name_alphabet) as full_name_alphabet,
        P.sex_cd,
        P.birthday,
        P.phone_no,
        P.phone_no2,
        P.mobile_no,
        P.zipcode,
        P.address1,
        P.address2,
        P.address3,
        P.mail_address1,
        P.mail_address2,
        P.sns_account,
        P.sns_type,
        P.`type`,
        C.name as country_name,
        C.english_name as english_country_name,
        I.home_country_phone_no1,
        I.home_country_phone_no2,
        I.home_country_address1,
        I.home_country_address2,
        I.home_country_address3,
        I.other_activity_result,
        A.attend_day,
        A.absent_day,
        A.total_day,
        A.rate_time,
        S.*,
        CH.amount,
        CH.pay_off_date,
        @class_id as class_id,
        @class_name as class_name,
        @teacher_person_id as teacher_person_id,
        @teacher_name as teacher_name,
        @person_id as person_id,
        @total_leave_absent as total_leave_absent,
        @teacher_account_id as teacher_account_id,
        @residence_card_no as residence_card_no,
        @visa_expired_date as visa_expired_date,
        @permit_other_activitiy_cd as permit_other_activitiy_cd,
        @sales_staff_person_id as sales_staff_person_id,
        @jlpt_level as jlpt_level
    FROM
        person P
        LEFT JOIN country C ON P.country_id = C.id
        INNER JOIN student S on P.id = S.person_id
        LEFT JOIN immigration I on P.id = I.person_id
        LEFT JOIN charge CH on CH.student_id = S.id
        LEFT JOIN (SELECT
                         rate_time,
                         student_id,
                         attend_day,
                         absent_day,
                         total_day
                     FROM attendance_rate WHERE student_id= @student_id and attendance_cd = 1) A ON S.id = A.student_id

    WHERE
        P.id = @person_id
    and P.disable = 0
    and S.disable = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_total_absent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_get_total_absent`(
	IN `param_student_no` VARCHAR(128)
)
    COMMENT 'Get total absent of student'
BEGIN
    SET @student_id = (select person_id from student where student_no = param_student_no);
    SELECT absent_day
        FROM attendance_rate
        WHERE disable = 0
            AND student_id = @student_id
            AND attendance_cd = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_leave_absence_application` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_insert_leave_absence_application`(
	IN `pr_student_no` VARCHAR(128)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_charge_id` INT
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255)
    ,
	IN `pr_leaving_reason_id` INT,
	IN `pr_lastup_account_id` INT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_note` TEXT

)
    COMMENT 'Insert new leaving application'
BEGIN
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    START TRANSACTION;
	     
	     SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

        
        SET @person_id = (SELECT person_id FROM student WHERE student_no = pr_student_no);
        SET @str_request_no = (select max(request_no) from request);
        IF (@str_request_no IS NULL) THEN
            SET @str_request_no = 1;
        END IF;
        SET @request_no = CAST(@str_request_no AS SIGNED) + 1;
        SET @str_request_no = RIGHT(CONCAT('00000000', CAST(@request_no AS CHAR(8))), 8);

        
        INSERT INTO request(request_no, person_id, lastup_account_id, lastup_datetime, create_datetime, disable)
            VALUES(
                @str_request_no, @person_id, pr_lastup_account_id, now(), now(), 0
            );
        SET @last_request_id = LAST_INSERT_ID();

        
        
        
        INSERT INTO application(
                    request_id,
                    
                    application_type_id,
                    application_date,
                    status_cd,
                    charge_id,
                    lastup_account_id,
                    create_datetime,
                    lastup_datetime
                )
               VALUES(
                    @last_request_id,
                    
                    @app_type_id,
                    pr_application_date,
                    1,
                    pr_charge_id,
                    pr_lastup_account_id,
                    now(),
                    now()
                );
			SET @last_application_id = LAST_INSERT_ID();

		
		INSERT INTO application_leave_absence_properties(
						  lastup_account_id,
                    create_datetime,
                    lastup_datetime,
                    application_id,
		              leave_absence_reason_id,
                    leave_absence_date_from,
                    leave_absence_date_to,
                    leave_absence_return_country_phone_no,
                    leave_absence_return_country_mail_address,
                    leave_absence_return_country_address1,
                    leave_absence_return_country_address2,
                    leave_absence_return_country_address3,
                    leave_absence_reason_detail,
                    leave_absence_approval_date_from,
                    leave_absence_approval_date_to,
                    leave_absence_note
                )
               VALUES(
                    pr_lastup_account_id,
                    now(),
                    now(),
                    @last_application_id,
                    pr_leaving_reason_id,
                    pr_leaving_date,
                    pr_leaving_return_country_date,
                    pr_leaving_return_country_phone_no,
                    pr_leaving_return_country_mail_address,
                    pr_leaving_return_country_address1,
                    pr_leaving_return_country_address2,
                    pr_leaving_return_country_address3,
                    pr_leaving_reason_detail,
                    pr_leaving_approval_date_from,
                    pr_leaving_approval_date_to,
                    pr_leaving_note
                );

		
		INSERT INTO application_approve(application_id, approver_cd, action, comment, lastup_account_id, create_datetime, lastup_datetime) VALUES
			(@last_application_id, fn_get_system_code('staff_classification','ceo'), '', '', pr_lastup_account_id, now(), now()),
			(@last_application_id, fn_get_system_code('staff_classification','education_chief'), '', '', pr_lastup_account_id, now(), now()),
			(@last_application_id, fn_get_system_code('staff_classification','head_teacher'), '', '', pr_lastup_account_id, now(), now());

		
		

    
    SET @success = 0;
    CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_date, pr_leaving_return_country_date, '休学予定', '', pr_lastup_account_id, @success);

    IF (`_rollback`) OR (@success = 0) OR (@app_type_id = 0) THEN
        ROLLBACK;
        ALTER TABLE request auto_increment = 1;
        ALTER TABLE application auto_increment = 1;
        ALTER TABLE application_approve auto_increment = 1;
        select 2 as error, _rollback, @success, @app_type_id, @last_application_id,  @str_request_no ;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_cancel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_cancel`(
	IN `pr_application_id` INT
,
	IN `pr_lastup_account_id` INT
)
    COMMENT 'Cancel leave absence application'
cancel_leaving_app:BEGIN
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	 
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    SET @secretary_general_remand = fn_get_system_code('application_status', 'secretary_general_remand');
    SET @secretariat_input = fn_get_system_code('application_status', 'secretariat_input');

    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE cancel_leaving_app;
    END IF;

    SET @success = 1;
    START TRANSACTION;
    
    IF (@status_cd <> @secretary_general_remand) AND (@status_cd <> @secretariat_input) THEN
        SELECT 2 as error, 'Cancel not yet ready' as errmsg;
        COMMIT;
        LEAVE cancel_leaving_app;
    ELSE
            
            SET @request_id = (select request_id from application where id = pr_application_id);
            SET @person_id = (select person_id from request where id = @request_id);
            SET @from_date = (select leave_absence_date_from from application_leave_absence_properties where application_id = pr_application_id);
            SET @to_date = (select leave_absence_date_to from application_leave_absence_properties where application_id = pr_application_id);

            UPDATE application
            SET
                status_cd = fn_get_system_code('application_status', 'secretariat_remand'),
                lastup_account_id = pr_lastup_account_id,
                lastup_datetime = now()
            WHERE
                id = pr_application_id and (status_cd = @secretary_general_remand OR status_cd = @secretariat_input) and disable = 0;

            
            CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, @from_date, @to_date, '', '休学予定', pr_lastup_account_id, @success);

    END IF;

    IF (`_rollback`) OR (@success = 0) THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group6_approval` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group6_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT
)
    COMMENT 'Education Chief approval leave absence application'
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretary_general_approved = fn_get_system_code('application_status' , 'secretary_general_approved');
	 SET @school_chief_remand = fn_get_system_code('application_status' , 'school_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    
    IF (@status_cd <> @secretary_general_approved) and (@status_cd <> @school_chief_remand) THEN
        SELECT 2 as error, 'Secretary-General not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'education_chief_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
    			application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;


    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
            reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification', 'education_chief');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group6_reject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group6_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT
)
    COMMENT 'Education Chief reject leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretary_general_approved = fn_get_system_code('application_status' , 'secretary_general_approved');
	 SET @school_chief_remand = fn_get_system_code('application_status' , 'school_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    
    IF (@status_cd <> @secretary_general_approved) and (@status_cd <> @school_chief_remand) THEN
        SELECT 2 as error, 'Secretary General not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'education_chief_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
            application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
            confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
            approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification', 'education_chief');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group7_approval` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group7_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
    COMMENT 'Ceo approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
		
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretariat_input = fn_get_system_code('application_status' , 'secretariat_input');
	 SET @education_chief_remand = fn_get_system_code('application_status' , 'education_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    
    IF (@status_cd <> @secretariat_input) AND (@status_cd <> @education_chief_remand) THEN
        SELECT 2 as error, 'flow approval error' as errmsg;
        LEAVE update_app;
    ELSE

    		
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'secretary_general_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
             application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
                reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'ceo');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group7_reject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group7_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
    COMMENT 'Ceo reject leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretariat_input = fn_get_system_code('application_status' , 'secretariat_input');
	 SET @education_chief_remand = fn_get_system_code('application_status' , 'education_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    
    IF (@status_cd <> @secretariat_input) AND (@status_cd <> @education_chief_remand) THEN
        SELECT 2 as error, 'Status not yet reject' as errmsg;
        LEAVE update_app;
    ELSE

    		
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'secretary_general_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
                confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
                approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'ceo');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group9_approval` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group9_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` DATE,
	IN `pr_leaving_approval_date_to` DATE,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
    COMMENT 'Head of School approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @education_chief_approved = fn_get_system_code('application_status' , 'education_chief_approved');


    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;



    START TRANSACTION;
    
    IF @status_cd <> @education_chief_approved THEN
    		COMMIT;
        SELECT 2 as error, 'Chief not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE
    		 
		    SET @request_id = (SELECT request_id FROM application WHERE id = pr_application_id);
		    SET @person_id = (SELECT person_id FROM request WHERE id = @request_id);

    		

    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'school_chief_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;


    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
                reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'head_teacher');

	 		
	 		SET @real_attend_day = fn_get_real_attend_day(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to);




	 		
	 		SET @success = 0;
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_date, pr_leaving_return_country_date, '', '休学予定', pr_account_id, @success);

			
			SELECT max(SA.is_authorized_absent), max(SA.is_leave_absent), max(SA.is_temporary_return)
			INTO
				@a1, @a2, @a3
			FROM
				shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and CSW.date in
					(SELECT date FROM school_calendar
					WHERE
						disable = 0
						AND is_school_holiday = 0
						AND is_public_holiday = 0
						AND date >= pr_leaving_approval_date_from
						AND date <= pr_leaving_approval_date_to);

			IF (@a1 > 0) OR (@a2 > 0) OR (@a3 > 0) THEN
				ROLLBACK;
				select 3 as error, @a1 as is_authorized_absent, @a2 as is_leave_absent, @a3 as is_temporary_return, 'data error' as errmsg;
				LEAVE update_app;
			END IF;


	 		
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to, '休学', '', pr_account_id, @success);

         
         UPDATE shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			SET SA.is_leave_absent = 1,
				SA.lastup_account_id = pr_account_id,
				SA.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and SA.comment = '休学'
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

         
         UPDATE attendance A INNER JOIN class_shift_work CSW ON A.class_shift_work_id = CSW.id
			SET A.is_leave_absent = 1,
                 A.attendance = 0,
				A.lastup_account_id = pr_account_id,
				A.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and A.disable = 0
				and CSW.disable = 0
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group9_reject` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_group9_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
    COMMENT 'Head of School approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @education_chief_approved = fn_get_system_code('application_status' , 'education_chief_approved');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    
    IF @status_cd <> @education_chief_approved THEN
        SELECT 2 as error, 'College Chief not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'school_chief_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
                confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
                approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'head_teacher');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_leave_absence_application_teacher_confirm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_leave_absence_application_teacher_confirm`(
	IN `pr_application_id` INT
,
	IN `pr_account_id` INT

)
    COMMENT 'Teacher confirm leave absence form'
teacher_confirm:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

	
	SET @id = (SELECT id FROM application WHERE id = pr_application_id AND disable = 0 AND application_type_id = @app_type_id);
	IF @id IS NULL THEN
		SELECT 1 as error, 'application not found' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	
	SET @request_id = (SELECT request_id FROM application WHERE id = pr_application_id AND disable = 0 AND application_type_id = @app_type_id);
	SET @student_person_id = (SELECT person_id FROM request WHERE id = @request_id);
	SET @class_id = (SELECT class_id FROM class_student WHERE person_id = @student_person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1);
   SET @class_name = (SELECT name FROM class WHERE id = @class_id);
   SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
   SET @teacher_person_type = (SELECT `type` FROM person WHERE id = @teacher_person_id);
   

	
	IF @teacher_person_id IS NULL THEN
		SELECT 2 as error, 'teacher not found' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	
	SET @check_confirmed = (SELECT 1 FROM application_approve WHERE application_id = @id
										AND (approver_cd = fn_get_system_code('staff_classification', 'full-time_faculty_staff')
												OR approver_cd = fn_get_system_code('staff_classification', 'part-time_faculty_staff')
												OR approver_cd = fn_get_system_code('staff_classification', 'special_lecturer')
												)
									);
	IF @check_confirmed = 1 THEN
		SELECT 3 as error, 'teacher confirmed' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	

	START TRANSACTION;
	INSERT INTO application_approve(
		application_id,
		application_approver_id,
		approver_cd,
		status_cd,
		action,
		interview_id,
		approval_date,
		confirmed_document,
		reject_reason,
		comment,
		lastup_account_id,
		create_datetime,
		lastup_datetime)
	VALUES(
		@id,
		@teacher_person_id,
		@teacher_person_type,
		1,
		'',
		0,
		CURRENT_DATE(),
		'',
		'',
		'',
		pr_account_id,
		now(),
		now()
	);

	
	

	IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_comment_to_shift_attendance_by_person_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_update_comment_to_shift_attendance_by_person_id`(
	IN `pr_person_id` INT,
	IN `pr_from_date` DATE,
	IN `pr_to_date` DATE,
	IN `pr_new_comment` TEXT,
	IN `pr_old_comment` TEXT,
	IN `pr_account_id` INT,
	OUT `return_success_status` TINYINT

)
    COMMENT 'when submit leaving application then update column comment in table shift_attendance'
update_shift_attendance_comment:BEGIN

	UPDATE shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
	SET SA.comment = pr_new_comment,
		SA.lastup_account_id = pr_account_id,
		SA.lastup_datetime = now()
	WHERE
		person_id = pr_person_id
		and SA.disable = 0
		and CSW.disable = 0
		and SA.comment = pr_old_comment
		and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_from_date AND date <= pr_to_date)
	;

	SET return_success_status = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_leave_absence_application` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`midream`@`localhost` PROCEDURE `sp_update_leave_absence_application`(
	IN `pr_application_id` INT
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
,
	IN `pr_lastup_account_id` INT


)
    COMMENT 'Update leaving application info'
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;

	
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

	SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

	 START TRANSACTION;
    UPDATE application
    SET
    	  status_cd = 1,
        application_date = pr_application_date,
        lastup_account_id = pr_lastup_account_id,
        lastup_datetime = now()
    WHERE
        id = pr_application_id;


	
	UPDATE application_leave_absence_properties
	SET
		lastup_account_id = pr_lastup_account_id,
		lastup_datetime = now(),
		leave_absence_date_from = pr_leaving_date,
       leave_absence_date_to = pr_leaving_return_country_date,
       leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
       leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
       leave_absence_return_country_address1 = pr_leaving_return_country_address1,
       leave_absence_return_country_address2 = pr_leaving_return_country_address2,
       leave_absence_return_country_address3 = pr_leaving_return_country_address3,
       leave_absence_reason_id = pr_leaving_reason,
       leave_absence_reason_detail = pr_leaving_reason_detail,
       leave_absence_note = pr_leaving_note
	WHERE application_id = pr_application_id;

	IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-25 18:35:39
