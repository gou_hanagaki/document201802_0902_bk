SET @helper_id = (SELECT system_code.id FROM system_code WHERE column_name = 'helper');
INSERT INTO system_code_detail (`system_code_id`, `code1`,`sequence`, `name`, `column_name`, `create_datetime`, `lastup_datetime`) 
VALUES (@helper_id, 3,1, '出席一覧画面', 'attendance_list', NOW(), NOW());
INSERT INTO system_code_detail (`system_code_id`, `code1`,`sequence`, `name`, `column_name`, `create_datetime`, `lastup_datetime`) 
VALUES (@helper_id, 4,1,'出欠訂正一覧画面', 'correct_attendance_list', NOW(), NOW());