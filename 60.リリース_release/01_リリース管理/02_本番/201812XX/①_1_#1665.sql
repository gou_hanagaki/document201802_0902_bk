ALTER TABLE application_issue_certificate_properties
ADD COLUMN `certificate_quantity` INT(10) NOT NULL DEFAULT '0' AFTER `certificate_other`;

ALTER TABLE application_issue_certificate_properties
ADD COLUMN `total_issue_fee` INT(10) NOT NULL DEFAULT '0' AFTER `certificate_quantity`;

ALTER TABLE application_issue_certificate_properties
ADD COLUMN `reception_school_building` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `total_issue_fee`;

ALTER TABLE educational_institution
ADD COLUMN `is_target_in_list` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `people_cnt`;

UPDATE application_type SET application_type.name = "���E��(���p)" WHERE application_type.column_name ="letter_of_recommendation";

SET @current_max_sequence = (SELECT MAX(sequence) FROM  application_type);
INSERT INTO application_type (name,column_name,is_certification,sequence,create_datetime,lastup_datetime)
VALUES
("���E��(����p)","letter_of_recommendation_type_multiple",1,@current_max_sequence+1,NOW(),NOW()), 
("�|��@","translation_type_1",1,@current_max_sequence+2,NOW(),NOW()),
("�|��A","translation_type_2",1,@current_max_sequence+3,NOW(),NOW());

INSERT INTO system_code (name,column_name,create_datetime,lastup_datetime)
VALUES ("��t�Z��","reception_school_building",NOW(),NOW());

SET @system_code_id = (SELECT system_code.id FROM system_code WHERE system_code.column_name = 'reception_school_building');
INSERT INTO system_code_detail (system_code_id,code1,name,column_name,create_datetime,lastup_datetime)
VALUES
(@system_code_id,1,"�{��","main_building",NOW(),NOW()),
(@system_code_id,2,"2����","secondary_building",NOW(),NOW());