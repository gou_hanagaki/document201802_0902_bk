select * from function
WHERE function_group_id = 5
and disable = 1
and path in ('staff/application/search_student','staff/application/narrow_down','staff/report/certificates_output_list');
--------------------------------------
UPDATE function
SET  is_in_menu = 1, disable = 0
WHERE function_group_id = 5
and disable = 1
and path in ('staff/application/search_student','staff/application/narrow_down','staff/report/certificates_output_list');