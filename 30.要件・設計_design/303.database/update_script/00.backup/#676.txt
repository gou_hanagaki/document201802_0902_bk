-- #676 
-- -----------------------------------------------------
-- Table `school_term`
-- -----------------------------------------------------
ALTER TABLE school_term DROP COLUMN `is_confirmed_class_edit`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_shift_make`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_class_assign`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_shift_request`;
ALTER TABLE school_term DROP COLUMN `is_confirmed`;

-- -----------------------------------------------------
-- Table `class_group`
-- -----------------------------------------------------
ALTER TABLE class_group ADD `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_special;

-- -----------------------------------------------------
-- Table `class_student`
-- -----------------------------------------------------
ALTER TABLE class_student ADD `school_term_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;

-- -----------------------------------------------------
-- Table `teacher_school_term`
-- -----------------------------------------------------
ALTER TABLE teacher_school_term ADD `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_full_day_request;
ALTER TABLE teacher_school_term DROP COLUMN `is_absent`;
