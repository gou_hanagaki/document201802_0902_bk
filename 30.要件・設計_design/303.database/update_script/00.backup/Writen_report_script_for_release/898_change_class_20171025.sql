-- -----------------------------------------------------
-- Table `application_change_class_properties`
-- -----------------------------------------------------
CREATE TABLE `application_change_class_properties` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`application_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`class_change_month` DATE NOT NULL DEFAULT '0000-00-00',
	`is_class_change_teacher_confirm` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`date_teacher_confirm` DATE NOT NULL DEFAULT '0000-00-00',
	`class_change_time_zone` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`class_change_reason` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`class_change_reason_comment` TEXT NOT NULL,
	`class_change_comment` TEXT NOT NULL,
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `application_id` (`application_id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;