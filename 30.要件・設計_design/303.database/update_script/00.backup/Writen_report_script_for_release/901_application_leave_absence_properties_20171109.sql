-- -----------------------------------------------------
-- Table `application_leave_absence_properties`
-- -----------------------------------------------------
CREATE TABLE `application_leave_absence_properties` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`application_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`leave_absence_date_from` DATE NOT NULL DEFAULT '0000-00-00',
	`leave_absence_date_to` DATE NOT NULL DEFAULT '0000-00-00',
	`leave_absence_approval_date_from` DATE NOT NULL DEFAULT '0000-00-00',
	`leave_absence_approval_date_to` DATE NOT NULL DEFAULT '0000-00-00',
	`leave_absence_return_country_phone_no` VARCHAR(20) NOT NULL DEFAULT '',
	`leave_absence_return_country_mail_address` VARCHAR(128) NOT NULL DEFAULT '',
	`leave_absence_return_country_address1` VARCHAR(255) NOT NULL DEFAULT '',
	`leave_absence_return_country_address2` VARCHAR(255) NOT NULL DEFAULT '',
	`leave_absence_return_country_address3` VARCHAR(255) NOT NULL DEFAULT '',
	`leave_absence_reason_id` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`leave_absence_reason_detail` TEXT NOT NULL,
	`leave_absence_note` TEXT NOT NULL,
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `application_id` (`application_id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

