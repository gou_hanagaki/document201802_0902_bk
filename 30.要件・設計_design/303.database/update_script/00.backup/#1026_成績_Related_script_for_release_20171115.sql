-- ==============================================
-- For writen report (Script also exist on #1026)
-- ==============================================

ALTER TABLE `school_record`   
  DROP COLUMN `class_student_id`, 
  ADD COLUMN `school_term_id` int(10) unsigned NOT NULL DEFAULT 0 AFTER `id`,
  ADD COLUMN `person_id` int(10) unsigned NOT NULL DEFAULT 0 AFTER `school_term_id`, 
  DROP INDEX `class_student_id`;

-- Update column_name for school_record_section
ALTER TABLE school_record_section ADD COLUMN column_name varchar(128) NOT NULL default '' AFTER name;
ALTER TABLE school_record_section ADD INDEX column_name (column_name);

UPDATE `school_record_section` SET `column_name` = 'vocabulary_grammar' WHERE `name` = '文字語彙';
UPDATE `school_record_section` SET `column_name` = 'grammar' WHERE `name` = '文法';
UPDATE `school_record_section` SET `column_name` = 'reading' WHERE `name` = '読解';
UPDATE `school_record_section` SET `column_name` = 'listening' WHERE `name` = '聴解';
UPDATE `school_record_section` SET `column_name` = 'conversation' WHERE `name` = '会話';
UPDATE `school_record_section` SET `column_name` = 'writing' WHERE `name` = '作文';
UPDATE `school_record_section` SET `column_name` = 'normal' WHERE `name` = '平常点';
UPDATE `school_record_section` SET `column_name` = 'comprehension' WHERE `name` = '総合';

-- Update column_name for time_zone
ALTER TABLE time_zone ADD COLUMN column_name varchar(128) NOT NULL default '' AFTER name;
ALTER TABLE time_zone ADD INDEX column_name (column_name);

UPDATE `time_zone` SET `column_name` = 'morning' WHERE `name` = '午前'; 
UPDATE `time_zone` SET `column_name` = 'afternoon' WHERE `name` = '午後'; 
UPDATE `time_zone` SET `column_name` = 'lunch_break' WHERE `name` = '昼休'; 
UPDATE `time_zone` SET `column_name` = 'other' WHERE `name` = 'その他';


-- ==============================================
-- For #1029 (Script existed on pre env) 
-- Don't need to run this scrip on pre env
-- ==============================================
ALTER TABLE account_group ADD COLUMN column_name varchar(128) NOT NULL default '' AFTER name;
ALTER TABLE account_group ADD INDEX column_name (column_name);

-- Update column_name for account_group
UPDATE   `account_group` SET `column_name`='student' WHERE `name`='学生';
UPDATE   `account_group` SET `column_name`='full_time_faculty_staff' WHERE  `name`='常勤教職員';
UPDATE   `account_group` SET `column_name`='part_time_faculty_staff' WHERE  `name`='非常勤教職員';
UPDATE   `account_group` SET `column_name`='office_worker' WHERE  `name`='事務職員';
UPDATE   `account_group` SET `column_name`='part_time_job' WHERE  `name`='事務職員アルバイト';
UPDATE   `account_group` SET `column_name`='ceo' WHERE  `name`='事務局長';
UPDATE   `account_group` SET `column_name`='president' WHERE  `name`='理事長';
UPDATE   `account_group` SET `column_name`='head_teacher' WHERE  `name`='校長';
UPDATE   `account_group` SET `column_name`='sys_admin' WHERE  `name`= 'システム管理者';
UPDATE   `account_group` SET `column_name`='education_chief' WHERE  `name`= '教務主任';
