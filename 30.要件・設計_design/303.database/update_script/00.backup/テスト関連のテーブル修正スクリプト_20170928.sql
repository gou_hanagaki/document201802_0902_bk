/* ability_groupのcolumn属性がおかしかったので修正 */
ALTER TABLE ability_group MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE ability_group MODIFY name varchar(128) NOT NULL DEFAULT '';
ALTER TABLE ability_group MODIFY sequence int(10) unsigned NOT NULL DEFAULT '0';
ALTER TABLE ability_group MODIFY lastup_account_id int(10) unsigned NOT NULL DEFAULT '0';
ALTER TABLE ability_group MODIFY create_datetime datetime NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE ability_group MODIFY lastup_datetime datetime NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE ability_group MODIFY disable tinyint(1) unsigned NOT NULL DEFAULT '0';

/* create table external_test */
/* データの移行もあるので、dev環境のdumpを使って他環境に入れた方が良い
CREATE TABLE `external_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
);
*/

/* create table external_test_level */
/* データの移行もあるので、dev環境のdumpを使って他環境に入れた方が良い
CREATE TABLE `external_test_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `external_test_id` (`external_test_id`)
);
*/

/* alter table ability */
/* ★★external_testとexternal_test_levelのデータ移行が先★★ */
ALTER TABLE ability MODIFY `ability_group_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE ability CHANGE `test_level_id` `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER ability_group_id;
ALTER TABLE ability CHANGE `next_ability` `next_ability_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER external_test_level_id;

/* drop table test */
DROP TABLE test;
/* create table test */
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1)unsigned  NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
);
INSERT INTO test SET name='まとめテスト', create_datetime = now(), lastup_datetime = now();
INSERT INTO test SET name='期末テスト', create_datetime = now(), lastup_datetime = now();
INSERT INTO test SET name='プレースメントテスト', create_datetime = now(), lastup_datetime = now();
INSERT INTO test SET name='クラスアップテスト', create_datetime = now(), lastup_datetime = now();

/* alter table content_test_list */
ALTER TABLE content_test_list ADD test_id int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE content_test_list MODIFY `name` VARCHAR(256) NOT NULL DEFAULT '';
ALTER TABLE content_test_list ADD total_point int(10) unsigned NOT NULL DEFAULT '0' AFTER name;
ALTER TABLE content_test_list ADD threshold_rate int(7) unsigned NOT NULL DEFAULT '0' AFTER total_point;
ALTER TABLE content_test_list MODIFY time_test int(10) unsigned NOT NULL DEFAULT '0' AFTER threshold_rate;
ALTER TABLE content_test_list ADD public_type tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER time_test;
ALTER TABLE content_test_list ADD sequence int(10) unsigned NOT NULL DEFAULT '0' AFTER public_type;
/* create table content_test_list */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(256) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `time_test` int(10) unsigned NOT NULL DEFAULT '0',
  `public_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`)
);
*/

/* alter table content_test_question */
ALTER TABLE content_test_question CHANGE `test_list_id` `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE content_test_question ADD test_section_cd int(3) unsigned NOT NULL DEFAULT '0' AFTER content_test_list_id;
ALTER TABLE content_test_question CHANGE `question_sec` `question_sequence` int(10) unsigned NOT NULL DEFAULT '0' AFTER test_kind;
ALTER TABLE content_test_question MODIFY `question` TEXT NOT NULL AFTER score_kind;
ALTER TABLE content_test_question DROP COLUMN `test_section_id`;
ALTER TABLE content_test_question DROP COLUMN `is_answer`;
/* create table content_test_question */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `test_title` text NOT NULL,
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `question_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_kind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:all, 1: one',
  `question` text NOT NULL,
  `hint` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_list_id` (`content_test_list_id`)
);
*/

/* alter table content_test_answer */
ALTER TABLE content_test_answer CHANGE `sequence` `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0' AFTER content_test_question_id;
ALTER TABLE content_test_answer CHANGE `answer_sec` `answer_selection` varchar(50) NOT NULL DEFAULT '' AFTER hint;
/* create table content_test_answer */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
)
*/

/* create table achievement */
CREATE TABLE `achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_date` date NOT NULL DEFAULT '0000-00-00',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `is_additional_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `test_date` (`test_date`)
)

/* create table achievement_detail */
CREATE TABLE `achievement_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_original` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `test_section_cd` (`test_section_cd`)
);

/* create table content_test_status */
CREATE TABLE `content_test_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(3) unsigned NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `person_id` (`person_id`)
);


/* create table content_test_section */
CREATE TABLE `content_test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `test_section_cd` (`test_section_cd`)
);

/* alter table examination_answer */
ALTER TABLE examination_answer ADD test_section_cd int(3) unsigned NOT NULL DEFAULT '0' AFTER achievement_id;
ALTER TABLE examination_answer CHANGE `answer_sec` `answer_position` int(10) unsigned NOT NULL DEFAULT '0' AFTER test_section_cd;
ALTER TABLE examination_answer CHANGE `selected_content_test_answer_id` `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER answer_comment;
ALTER TABLE examination_answer MODIFY `test_kind` VARCHAR(50) NOT NULL DEFAULT '' AFTER score;
/* create table examination_answer */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `examination_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `answer_position` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_comment` text NOT NULL,
  `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `score` varchar(10) NOT NULL DEFAULT '',
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`),
  KEY `person_id` (`person_id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `selected_content_test_answer_id` (`content_test_answer_id`)
);
*/

/* drop table test_level */
DROP TABLE test_level;

/* drop table class_activity */
DROP TABLE class_activity;




/* INSERT system_code + system_code_datail */
INSERT INTO system_code SET name = '試験科目', column_name = 'test_section', create_datetime = now(), lastup_datetime=now();
SET @ver_id =  LAST_INSERT_ID();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 1, name = '文字語彙', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 2, name = '文法', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 3, name = '文作', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 4, name = '読解', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 5, name = '聴解', create_datetime = now(), lastup_datetime = now();


ALTER TABLE class_shift_request_level
CHANGE test_level_id external_test_level_id int(10) unsigned NOT NULL DEFAULT '0'

CREATE TABLE `content_test_answer_selection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
);

ALTER TABLE content_test_answer DROP COLUMN hint;
ALTER TABLE content_test_answer DROP COLUMN answer_selection;