﻿-- -----------------------------------------------------
-- Table `system_code_detail`
-- -----------------------------------------------------
INSERT INTO `system_code_detail` 
(`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES
(2,	10,	'入学希望', 'admission_request',2000, now()),
(2,	11,	'不入学', 'non_admission',2000, now()),
(2,	12,	'強制退学', 'forced_withdrawal',2000, now());

-- -----------------------------------------------------
-- Table `function_group`
-- -----------------------------------------------------
INSERT INTO `function_group` (`id`, `name`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES
(12,	'入学',	12,	2000,	now(),	now());

-- -----------------------------------------------------
-- Table `function`
-- -----------------------------------------------------
INSERT INTO `function` (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`) VALUES
(12,	'入学生CSV一括登録',	'',	0,	1,	2000,	now()),
(12,	'合格証書発行',	'staff/admission/issue_passing_certificate',	1,	2,	2000,	now()),
(12,	'入管情報確認',	'staff/admission/confirm_for_immigration_bureau',	1,	3,	2000,	now()),
(12,	'申請番号付与',	'staff/admission/grantapplicationnumber',	0,	4,	2000,	now()),
(12,	'入管帳票管理',	'staff/admission/immigrationformmanagement',	0,	5,	2000,	now()),
(12,	'在留資格認定証申請',	'',	0,	6,	2000,	now()),
(12,	'中長期在留者の受け入れに関する届出',	'staff/admission/grant_residence',	1,	7,	2000,	now()),
(12,	'請求書発行',	'',	0,	8,	2000,	now()),
(12,	'入学許可書発行',	'',	0,	9,	2000,	now()),
(12,	'入金確認',	'',	0,	10,	2000,	now());

-- -----------------------------------------------------
-- Table `permission`
-- -----------------------------------------------------
INSERT INTO `permission` (`account_group_id`, `function_group_id`, `is_permitted`, `lastup_account_id`, `create_datetime`) VALUES
(1,	12,	0,	2000,	now()),
(2,	12,	0,	2000,	now()),
(3,	12,	0,	2000,	now()),
(4,	12,	1,	2000,	now()),
(5,	12,	1,	2000,	now()),
(6,	12,	1,	2000,	now()),
(7,	12,	1,	2000,	now()),
(8,	12,	1,	2000,	now()),
(9,	12,	1,	2000,	now()),
(10,	12,	1,	2000,	now());
