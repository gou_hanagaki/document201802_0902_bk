﻿-- -----------------------------------------------------
-- Table `immigration`
-- -----------------------------------------------------
DROP TABLE immigration;

CREATE TABLE `immigration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `immigration_application_no` VARCHAR(45) NOT NULL DEFAULT '',
  `visa_type` int(2) unsigned NOT NULL DEFAULT '0',
  `passport_no` VARCHAR(128) NOT NULL DEFAULT '',
  `passport_expired_date` date NOT NULL DEFAULT '0000-00-00',
  `occupation_before_admission` VARCHAR(128) NOT NULL DEFAULT '',
  `birth_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `birth_address1` VARCHAR(255) NOT NULL DEFAULT '',
  `birth_address2` VARCHAR(255) NOT NULL DEFAULT '',
  `birth_address3` VARCHAR(255) NOT NULL DEFAULT '',
  `home_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `home_country_zipcode` VARCHAR(20) NOT NULL DEFAULT '',
  `home_country_address1` VARCHAR(255) NOT NULL DEFAULT '',
  `home_country_address2` VARCHAR(255) NOT NULL DEFAULT '',
  `home_country_address3` VARCHAR(255) NOT NULL DEFAULT '',
  `home_country_phone_no1` VARCHAR(20) NOT NULL DEFAULT '',
  `home_country_phone_no2` VARCHAR(20) NOT NULL DEFAULT '',
  `entry_date` date NOT NULL DEFAULT '0000-00-00',
  `scheduled_landing_port` VARCHAR(128) NOT NULL DEFAULT '',
  `duration_of_stay` VARCHAR(128) NOT NULL DEFAULT '',
  `is_partner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `intended_visa_apply_place` VARCHAR(255) NOT NULL DEFAULT '',
  `is_past_entry_departure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `past_entry_times` int(3) unsigned NOT NULL DEFAULT '0',
  `last_entry_date` date NOT NULL DEFAULT '0000-00-00',
  `last_departure_date` date NOT NULL DEFAULT '0000-00-00',
  `is_crime` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `crime_content` VARCHAR(128) NOT NULL DEFAULT '',
  `is_departure_of_deportation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `departure_of_deportation_times` int(3) unsigned NOT NULL DEFAULT '0',
  `last_departure_of_deportation_date` date NOT NULL DEFAULT '0000-00-00',
  `total_education_years` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education_status` int(2) unsigned NOT NULL DEFAULT '0',
  `last_school_education_name` VARCHAR(255) NOT NULL DEFAULT '',
  `last_school_education_graduation_date` date NOT NULL DEFAULT '0000-00-00',
  `last_school_education_other` VARCHAR(128) NOT NULL DEFAULT '',
  `jp_lang_ability_test_name` VARCHAR(128) NOT NULL DEFAULT '',
  `jp_lang_ability_level_or_score` VARCHAR(128) NOT NULL DEFAULT '',
  `ability_proof_institution_name` VARCHAR(255) NOT NULL DEFAULT '',
  `ability_proof_enrollment_start_date` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_enrollment_end_date` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_other_comment` VARCHAR(128) NOT NULL DEFAULT '',
  `support_pay_amount_self` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_living_abroad` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_living_japan` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_scholarship` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_others` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_from_abroad_name` VARCHAR(128) NOT NULL DEFAULT '',
  `carrying_from_abroad_date` date NOT NULL DEFAULT '0000-00-00',
  `remittance_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_remittance_other_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `provide_scholarship_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `provide_scholarship_name` VARCHAR(128) NOT NULL DEFAULT '',
  `provide_scholarship_other` VARCHAR(128) NOT NULL DEFAULT '',
  `plans_after_graduation_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `graduate_organization_name` VARCHAR(128) NOT NULL DEFAULT '',
  `graduate_organization_school_type` VARCHAR(128) NOT NULL DEFAULT '',
  `graduate_organization_graduation_date` date NOT NULL DEFAULT '0000-00-00',
  `graduate_organization_school_name` VARCHAR(128) NOT NULL DEFAULT '',
  `residence_application_date` date NOT NULL DEFAULT '0000-00-00',
  `residence_application_result` int(2) unsigned NOT NULL DEFAULT '0',
  `residence_update_reason` VARCHAR(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
);

-- -----------------------------------------------------
-- Table `visa`
-- -----------------------------------------------------
CREATE TABLE `visa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `visa_length_of_stay` int(2) unsigned NOT NULL DEFAULT '0',
  `residence_card_no` VARCHAR(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
);

-- -----------------------------------------------------
-- Table `supporter`
-- -----------------------------------------------------
CREATE TABLE `supporter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `last_name` VARCHAR(128) NOT NULL DEFAULT '',
  `first_name` VARCHAR(128) NOT NULL DEFAULT '',
  `address1` VARCHAR(255) NOT NULL DEFAULT '',
  `address2` VARCHAR(255) NOT NULL DEFAULT '',
  `address3` VARCHAR(255) NOT NULL DEFAULT '',
  `phone_no` VARCHAR(20) NOT NULL DEFAULT '',
  `employer_name` VARCHAR(128) NOT NULL DEFAULT '',
  `employer_phone_no` VARCHAR(20) NOT NULL DEFAULT '',
  `annual_income_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `supporter_relationship_cd` int(2) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
);

-- -----------------------------------------------------
-- Table `family_in_japan`
-- -----------------------------------------------------
CREATE TABLE `family_in_japan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `relationship` VARCHAR(128) NOT NULL DEFAULT '',
  `last_name` VARCHAR(128) NOT NULL DEFAULT '',
  `first_name` VARCHAR(128) NOT NULL DEFAULT '',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_intended_to_reside` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `employment` VARCHAR(128) NOT NULL DEFAULT '',
  `residence_card_no` VARCHAR(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
);

-- -----------------------------------------------------
-- Table `student`
-- -----------------------------------------------------
ALTER TABLE `student`
ADD COLUMN `start_scheduled_date` date NOT NULL DEFAULT '0000-00-00' AFTER `ability_id`;

-- -----------------------------------------------------
-- Table `system_code`
-- -----------------------------------------------------
UPDATE system_code SET column_name='visa_type',lastup_account_id=2000,lastup_datetime=now() where name='ビザの種類';