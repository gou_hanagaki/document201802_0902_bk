﻿SELECT COUNT(*) FROM application;
SELECT COUNT(*) FROM application_approve;
SELECT COUNT(*) FROM application_change_class_properties;
SELECT COUNT(*) FROM application_issue_certificate_properties;
SELECT COUNT(*) FROM application_leave_absence_properties;
SELECT COUNT(*) FROM application_official_absence_properties;
SELECT COUNT(*) FROM application_temporary_return_properties;
SELECT COUNT(*) FROM application_withdrawal_notification_properties;

/*
TRUNCATE TABLE application;
TRUNCATE TABLE application_approve;
TRUNCATE TABLE application_change_class_properties;
TRUNCATE TABLE application_issue_certificate_properties;
TRUNCATE TABLE application_leave_absence_properties;
TRUNCATE TABLE application_official_absence_properties;
TRUNCATE TABLE application_temporary_return_properties;
TRUNCATE TABLE application_withdrawal_notification_properties;
*/