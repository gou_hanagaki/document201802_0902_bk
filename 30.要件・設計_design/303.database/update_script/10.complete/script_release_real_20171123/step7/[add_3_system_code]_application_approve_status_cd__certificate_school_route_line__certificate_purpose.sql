-- Insert systemcode for application_approve_status_cd
INSERT INTO system_code (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES 
('申請書の承認ステータス', 'application_approve_status_cd', 0 , now(),now());
set @id = LAST_INSERT_ID();
INSERT INTO system_code_detail (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@id, '1', '承認ステータス', 'approve_status_cd', 0 , now(),now());
INSERT INTO system_code_detail (`system_code_id`, `code1`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@id, '2', '差戻しステータス', 'remand_status_cd', 0 , now(),now());

-- insert system_code certificate_school_route_line
INSERT INTO `system_code` ( `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
( '学校ルートライン', 'certificate_school_route_line', 0, now(), now(), 0);
set @id = (SELECT id FROM system_code WHERE column_name = 'certificate_school_route_line');
INSERT INTO `system_code_detail` ( `system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
( @id, 1, 0, 0, 0, '都営新宿線', 'toei_shinjuku_line', 0, now(), now(), 0),
( @id, 2, 0, 0, 0, '都営浅草線', 'toei_asakusa_line', 0, now(), now(), 0),
( @id, 3, 0, 0, 0, '都営大江戸線', 'toei_oedo_line', 0, now(), now(), 0),
( @id, 4, 0, 0, 0, '都営三田線', 'toei_mita_line', 0, now(), now(), 0),
( @id, 5, 0, 0, 0, '左記路線以外', 'other_than_the_left_route', 0, now(), now(), 0);

-- insert system_code certificate_purpose
INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES 
('目的', 'certificate_purpose',0 , now(),now());
SET @system_code_id = LAST_INSERT_ID();
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 1, '進学', 'college',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 2, '就職', 'employment',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 3, 'ビザ更新', 'visa_update',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 4, 'ビザ変更', 'visa_change',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 5, 'アルバイト', 'part_time_job',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 6, 'その他', 'other',0 , now(),now());
