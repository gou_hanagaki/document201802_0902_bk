-- -----------------------------------------------------
-- Table `application_temporary_return_properties`
-- -----------------------------------------------------
CREATE TABLE `application_temporary_return_properties` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`application_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`temporary_return_date_from` DATE NOT NULL DEFAULT '0000-00-00',
	`temporary_return_date_to` DATE NOT NULL DEFAULT '0000-00-00',
	`temporary_return_actual_date_from` DATE NOT NULL DEFAULT '0000-00-00',
	`temporary_return_actual_date_to` DATE NOT NULL DEFAULT '0000-00-00',
	`temporary_return_flight_name_go` VARCHAR(50) NOT NULL DEFAULT '',
	`temporary_return_flight_name_comeback` VARCHAR(50) NOT NULL DEFAULT '',
	`temporary_return_reason` TEXT NOT NULL,
	`temporary_return_contact_name` VARCHAR(128) NOT NULL DEFAULT '',
	`temporary_return_contact_phone_no` VARCHAR(255) NOT NULL DEFAULT '',
	`temporary_return_address` VARCHAR(255) NOT NULL DEFAULT '',
	`temporary_return_address2` VARCHAR(255) NOT NULL DEFAULT '',
	`temporary_return_address3` VARCHAR(255) NOT NULL DEFAULT '',
	`is_temporary_return_main_teacher_confirm` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`is_temporary_return_office_staff_confirm` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`temporary_return_comment` VARCHAR(128) NOT NULL DEFAULT '',
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `application_id` (`application_id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;