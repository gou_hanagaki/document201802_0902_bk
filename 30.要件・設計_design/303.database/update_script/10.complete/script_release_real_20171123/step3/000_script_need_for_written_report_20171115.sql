-- -----------------------------------------------------
-- Table `application_leave_absence_properties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`request_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`application_no` VARCHAR(10) NOT NULL DEFAULT '',
	`application_type_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`application_date` DATE NOT NULL DEFAULT '0000-00-00',
	`status_cd` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`charge_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`is_paid` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`print_date` DATE NOT NULL DEFAULT '0000-00-00',
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `request_id` (`request_id`),
	INDEX `application_type_id` (`application_type_id`),
	INDEX `charge_id` (`charge_id`),
	INDEX `application_no` (`application_no`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- UPDATE/INSERT data for system_code_detail
-- -----------------------------------------------------

-- insert system_code_detail for 'application_status'
SET @system_code_id = (SELECT id FROM system_code WHERE column_name = 'application_status' and disable = 0);

INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 0, 0, 0, 0, '証明書発行待ち', 'waiting_certificate_issuance', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 6, 0, 0, 0, '学校長差戻し', 'school_chief_remand', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 7, 0, 0, 0, '教務主任差戻し', 'education_chief_remand', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 8, 0, 0, 0, '事務局長差戻し', 'secretary_general_remand', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 9, 0, 0, 0, '事務局差戻し', 'secretariat_remand', 0, now(), now(), 0);


-- update system code application status
UPDATE system_code_detail
SET
	column_name = (CASE WHEN code1 = 0 THEN 'waiting_certificate_issuance'
			WHEN code1 = 1 THEN 'secretariat_input'
			WHEN code1 = 2 THEN 'secretary_general_approved'
			WHEN code1 = 3 THEN 'education_chief_approved'
			WHEN code1 = 4 THEN 'school_chief_approved'
			WHEN code1 = 5 THEN 'issued_certificate'
			WHEN code1 = 6 THEN 'school_chief_remand'
			WHEN code1 = 7 THEN 'education_chief_remand'
			WHEN code1 = 8 THEN 'secretary_general_remand'
			WHEN code1 = 9 THEN 'secretariat_remand'
		END)
WHERE
	system_code_id = @system_code_id;

-- Insert new row system_code 'staff_classification'
SET @system_code_id = (select id from system_code where column_name = "staff_classification" and disable = 0 );

INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 9, 0, 0, 0, '事務局長', 'ceo', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 10, 0, 0, 0, '教務主任', 'education_chief', 0, now(), now(), 0);
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (@system_code_id, 11, 0, 0, 0, '事務職員アルバイト', 'part-time_job', 0, now(), now(), 0);

-- Insert new application_type
INSERT INTO `application_type` (`name`, `is_certification`, `sequence`, `prefix_character`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
('休学届', 0, 27, '', 0, now(), now(), 0),
('在籍（日本語）', 1, 11, '', 0, now(), now(), 0),
('在籍（英語）', 1, 12, '', 0, now(), now(), 0);

-- -----------------------------------------------------
-- UPDATE for application_type
-- -----------------------------------------------------
ALTER TABLE `application_type` ADD COLUMN `column_name` VARCHAR(128) NOT NULL DEFAULT '' AFTER `name`;
ALTER TABLE `application_type` ADD INDEX `column_name` (`column_name`);

UPDATE application_type SET column_name = 'graduation_japanese', `sequence` = 1 WHERE name = '卒業(日本語)';
UPDATE application_type SET column_name = 'graduation_english', `sequence` = 2 WHERE name = '卒業(英語)';
UPDATE application_type SET column_name = 'graduation_prospect_japanese', `sequence` = 3 WHERE name = '卒業見込(日本語)';
UPDATE application_type SET column_name = 'graduation_prospect_english', `sequence` = 4 WHERE name = '卒業見込(英語)';
UPDATE application_type SET column_name = 'completion_japanese', `sequence` = 5 WHERE name = '修了(日本語)';
UPDATE application_type SET column_name = 'completion_english', `sequence` = 6 WHERE name = '修了(英語)';
UPDATE application_type SET column_name = 'studying_abroad_japanese', `sequence` = 9 WHERE name = '在学(日本語)';
UPDATE application_type SET column_name = 'studying_abroad_japanese_english', `sequence` = 10 WHERE name = '在学(英語)';
UPDATE application_type SET column_name = 'going_to_school', `sequence` = 13 WHERE name = '通学';
UPDATE application_type SET column_name = 'vacation_period', `sequence` = 14 WHERE name = '休暇期間';
UPDATE application_type SET column_name = 'japanese_ability', `sequence` = 15 WHERE name = '日本語能力';
UPDATE application_type SET column_name = 'academic_record_japanese', `sequence` = 16 WHERE name = '学業成績(日本語)';
UPDATE application_type SET column_name = 'academic_record_english', `sequence` = 17 WHERE name = '学業成績(英語)';
UPDATE application_type SET column_name = 'official_absence', `sequence` = 18 WHERE name = '公欠願';
UPDATE application_type SET column_name = 'student_card_reissue', `sequence` = 19 WHERE name = '学生証再発行';
UPDATE application_type SET column_name = 'letter_of_recommendation', `sequence` = 20 WHERE name = '推薦書';
UPDATE application_type SET column_name = 'prospect_expected_japanese', `sequence` = 7 WHERE name = '修了見込(日本語)';
UPDATE application_type SET column_name = 'prospect_expected_english', `sequence` = 8 WHERE name = '修了見込(英語)';
UPDATE application_type SET column_name = 'school_seal', `sequence` = 21 WHERE name = '学校印押印';
UPDATE application_type SET column_name = 'reason_for_not_issuing_letter_of_recommendation', `sequence` = 22 WHERE name = '推薦書不発行理由書';
UPDATE application_type SET column_name = 'temporary_return', `sequence` = 23 WHERE name = '一時帰国届';
UPDATE application_type SET column_name = 'change_personal_information', `sequence` = 24 WHERE name = '個人情報変更届';
UPDATE application_type SET column_name = 'change_class', `sequence` = 25 WHERE name = 'クラス移動申請書';
UPDATE application_type SET column_name = 'withdrawal', `sequence` = 26 WHERE name = '退学届';

UPDATE application_type SET column_name = 'leave_absence', `sequence` = 27 WHERE name = '休学届';
UPDATE application_type SET column_name = 'enrollment_japanese', `sequence` = 11 WHERE name = '在籍（日本語）';
UPDATE application_type SET column_name = 'enrollment_english', `sequence` = 12 WHERE name = '在籍（英語）';

-- #25981 Disable system_code_detail 'application_type_id'
SET @id = (select id from system_code  where column_name = 'application_type_id' and disable = 0);

UPDATE system_code SET disable = 1 WHERE id= @id;
UPDATE system_code_detail SET disable = 1 WHERE system_code_id= @id;
