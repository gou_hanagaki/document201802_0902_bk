-- -----------------------------------------------------
-- Table `application_issue_certificate_properties`
-- -----------------------------------------------------
CREATE TABLE `application_issue_certificate_properties` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`application_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`certificate_zipcode` VARCHAR(12) NOT NULL DEFAULT '',
	`certificate_immigration_application_no` VARCHAR(12) NOT NULL DEFAULT '',
	`certificate_address1` VARCHAR(255) NOT NULL DEFAULT '',
	`certificate_address2` VARCHAR(255) NOT NULL DEFAULT '',
	`certificate_address3` VARCHAR(255) NOT NULL DEFAULT '',
	`certificate_school_route_line` VARCHAR(255) NOT NULL DEFAULT '',
	`certificate_boarding_section_1` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_boarding_section_2` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_vacation_period_1` DATE NOT NULL DEFAULT '0000-00-00',
	`certificate_vacation_period_2` DATE NOT NULL DEFAULT '0000-00-00',
	`certificate_school_route_from` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_school_route_to` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_not_recommend_reason` MEDIUMTEXT NOT NULL,
	`certificate_purpose_cd` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`certificate_purpose_other` VARCHAR(255) NOT NULL DEFAULT '',
	`certificate_submission_cd` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	`certificate_college_name` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_college_section_1` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_college_section_2` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_postgraduate_name` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_postgraduate_section_1` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_postgraduate_section_2` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_career_college_name` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_career_college_section_1` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_career_college_section_2` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_company_name` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_part_time_job_company_name` VARCHAR(128) NOT NULL DEFAULT '',
	`certificate_other` VARCHAR(128) NOT NULL DEFAULT '',
	`lastup_account_id` INT(10) NOT NULL DEFAULT '0',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `application_id` (`application_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Change function group for certificates_output_list
SET @path= 'staff/report/certificates_output_list';
SET @name_group = '事務局';

UPDATE  `function` as f
SET f.function_group_id = (SELECT fg.id
FROM function_group fg
Where fg.name = @name_group)
WHERE f.path = @path