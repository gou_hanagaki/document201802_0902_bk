-- -----------------------------------------------------
-- Table `student`
-- -----------------------------------------------------
ALTER TABLE `student`
ADD COLUMN `reason_for_exclusion` varchar(128) NOT NULL DEFAULT '' AFTER `quit_reason_cd`;