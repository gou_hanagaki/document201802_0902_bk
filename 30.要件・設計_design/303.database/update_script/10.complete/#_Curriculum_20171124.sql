-- #676 
-- -----------------------------------------------------
-- Table `school_term`
-- -----------------------------------------------------
ALTER TABLE school_term DROP COLUMN `is_confirmed_class_edit`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_shift_make`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_class_assign`;
ALTER TABLE school_term DROP COLUMN `is_confirmed_shift_request`;
ALTER TABLE school_term DROP COLUMN `is_confirmed`;

-- -----------------------------------------------------
-- Table `class_student`
-- -----------------------------------------------------
ALTER TABLE class_student ADD `school_term_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;

-- -----------------------------------------------------
-- Table `teacher_school_term`
-- -----------------------------------------------------
ALTER TABLE teacher_school_term ADD `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_full_day_request;
ALTER TABLE teacher_school_term DROP COLUMN `is_absent`;

-- #689
-- -----------------------------------------------------
-- Table `class_group_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `class_group_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment1` text NOT NULL,
  `comment2` text NOT NULL,
  `comment3` text NOT NULL,
  `comment4` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `class_shift`
-- -----------------------------------------------------
ALTER TABLE `class_shift`
ADD COLUMN `is_optional` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `teacher_id`;

-- -----------------------------------------------------
-- Table `class_group`
-- -----------------------------------------------------
ALTER TABLE class_group ADD `class_group_unit_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;

-- #694
-- -----------------------------------------------------
-- Table `teacher_school_term`
-- -----------------------------------------------------
ALTER TABLE teacher_school_term ADD `work_days_week` int(10) unsigned NOT NULL DEFAULT '0' AFTER period_wage;
ALTER TABLE teacher_school_term ADD `work_times_week` int(10) unsigned NOT NULL DEFAULT '0' AFTER work_days_week;

-- #737
-- -----------------------------------------------------
-- Table `teacher_school_term`
-- -----------------------------------------------------
ALTER TABLE teacher_school_term ADD `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_full_day_request;

-- #946
-- -----------------------------------------------------
-- Table `class_group_unit`
-- -----------------------------------------------------
INSERT INTO class_group_unit (school_term_id, date_from, date_to , lastup_datetime)
SELECT DISTINCT school_term_id, date_from, date_to , CURDATE()
FROM class_group
WHERE class_group.class_group_unit_id = 0;

-- -----------------------------------------------------
-- Table `class_group`
-- -----------------------------------------------------
UPDATE class_group 
LEFT JOIN class_group_unit  ON class_group.date_from = class_group_unit.date_from AND class_group.date_to = class_group_unit.date_to
SET class_group.class_group_unit_id = class_group_unit.id
WHERE
class_group.class_group_unit_id = 0;

-- -----------------------------------------------------
-- Table `class_group_unit`
-- -----------------------------------------------------
UPDATE class_group_unit SET is_confirmed = 1 WHERE class_group_unit.date_from < CURDATE();

INSERT INTO `class_group_template` (`id`, `ability_id`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, 1,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(2, 2,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(3, 3,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(4, 4,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(5, 5,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(6, 6,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(7, 7,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(8, 8,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(9, 9,  0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(10,  10, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(11,  11, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(12,  12, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(13,  13, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(14,  14, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(15,  15, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(16,  16, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(17,  17, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(18,  18, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(19,  19, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0),
(20,  20, 0,  '2017-06-13 12:18:04',  '2017-06-13 12:18:04',  0);

INSERT INTO `class_template` (`id`, `name`, `class_group_template_id`, `time_zone_id`, `class_room`, `text_book`, `time_table_group_id`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, '大学1',  10, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(2, '初級2',  2,  1,  '2号館',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(4, '初中級',  3,  1,  '402',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(5, '中級1',  4,  1,  '202',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(7, '中級2',  5,  1,  '101',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(9, '中上級1', 6,  1,  '205',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(10,  '中上級2', 7,  1,  '206',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  '上級1',  8,  1,  '207',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(12,  '上級2',  9,  1,  '502',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(27,  '大学2',  11, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(28,  '大学3',  12, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(29,  '大学院1', 13, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(30,  '大学院2', 14, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(31,  '大学院3', 15, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(32,  '超級1',  16, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(33,  '超級2',  17, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(34,  '超級3',  18, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(35,  '超級4',  19, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(36,  '超級5',  20, 1,  '403',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(37,  '初級1',  1,  1,  '2号館',  '1',  1,  2017, '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0);


-- function_group
INSERT INTO `function_group` (`id`, `name`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(11,  'カリキュラム', 11, 2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0);

-- function
INSERT INTO `function` (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(11,  'クラス分トップ画面',  'staff/curriculum/student_classify_top',  1,  6,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  '未クラス分け学生画面', 'staff/curriculum/student_classify_new',  0,  7,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  '各クラス学生画面', 'staff/curriculum/student_classify_edit', 0,  8,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'クラス編成画面',  'staff/curriculum/bulk_class_create', 1,  1,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'クラス追加画面',  'staff/curriculum/class_create',  0,  2,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'シフト作成画面',  'staff/curriculum/create_class_shift',  1,  5,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'シフト希望画面',  'staff/curriculum/shift_request_list',  1,  3,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'シフト希望詳細画面',  'staff/curriculum/shift_request_detail',  0,  4,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  '授業スケジュール', 'staff/curriculum/schedule',  0,  9,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  '授業スケジュール配布用画面',  'staff/curriculum/schedule_for_distribution', 0,  10, 2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0),
(11,  'レベル一覧画面',  'staff/curriculum/level_list',  1,  0,  2000, '2017-10-04 00:00:00',  '0000-00-00 00:00:00',  0);

INSERT INTO `permission` (`account_group_id`, `function_group_id`, `is_permitted`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, 11, 0,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(2, 11, 0,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(3, 11, 0,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(4, 11, 0,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(5, 11, 0,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(6, 11, 1,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(7, 11, 1,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(8, 11, 1,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(9, 11, 1,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0),
(10,  11, 1,  0,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00',  0);