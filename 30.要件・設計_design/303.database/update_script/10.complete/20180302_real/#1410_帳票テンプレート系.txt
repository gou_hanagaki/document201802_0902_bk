CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_number` int(10) unsigned NOT NULL DEFAULT '0',
  `display_file_name` varchar(50) NOT NULL DEFAULT '',
  `physical_file_name` varchar(50) NOT NULL DEFAULT '',
  `orientation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------
-- http://mid-dev-redmine.fraise.jp/issues/1410
-- +
-- http://mid-dev-redmine.fraise.jp/issues/1410#note-17
-- --------------------------------------------
ALTER TABLE `school` 
ADD COLUMN `applicant` VARCHAR (128) COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `immigration_office_department`,
ADD COLUMN `relationship_with_applicant` VARCHAR (128) COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `applicant`;
