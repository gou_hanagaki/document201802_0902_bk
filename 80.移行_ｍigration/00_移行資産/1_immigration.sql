TRUNCATE TABLE immigration;
INSERT INTO immigration(
person_id
,birth_address1 -- 出生地 住所 1
,intended_visa_apply_place
,is_past_entry_departure
,past_entry_times -- 過去の出入回数
,last_entry_date -- 直近の出入国歴from
,last_departure_date -- 直近の出入国歴to
,last_school_education_status -- 最終学歴在籍状況
,last_school_education -- 最終学歴
,last_school_education_name -- 学校名
,last_school_education_graduation_date -- 卒業又は卒業見込み年月
,ability_proof_institution_name
,ability_proof_enrollment_start_date
,ability_proof_enrollment_end_date
,support_pay_amount_self
,remittance_from_abroad_amount
,carrying_from_abroad_amount
,carrying_from_abroad_name
,carrying_from_abroad_date
,support_pay_amount_living_japan
,support_pay_amount_scholarship
,support_pay_amount_others
,occupation_before_admission
,total_education_years
,home_country_id
,home_country_address1
,home_country_phone_no1
,passport_no
,passport_expired_date
,entry_date
,visa_type
,immigration_application_no
)
SELECT
person_id
,birth_address1
,intended_visa_apply_place
,CASE is_past_entry_departure
	WHEN '有' THEN 1
	ELSE 0
END as is_past_entry_departure
,past_entry_times
,last_entry_date
,last_departure_date
,CASE last_school_education_status
	WHEN '卒業' THEN 1
	WHEN '在学中' THEN 2
	WHEN '休学中' THEN 3
	WHEN '中退' THEN 4
	ELSE 0
END as last_school_education_status
,CASE last_school_education
	WHEN '大学院（博士）' THEN 1
	WHEN '大学院（修士）' THEN 2
	WHEN '大学' THEN 3
	WHEN '短期大学'THEN 4
	WHEN '専門学校' THEN 5
	WHEN '高等学校' THEN 6
	WHEN '中学校' THEN 7
	WHEN '小学校' THEN 8
	WHEN 'その他' THEN 9
	WHEN '短大' THEN 4
	WHEN '不明' THEN 0
	ELSE 0
END as last_school_education
,last_school_education_name
,CASE
	WHEN CHAR_LENGTH(IMM.last_school_education_graduation_date)=6 THEN LAST_DAY(CONCAT(LEFT(IMM.last_school_education_graduation_date, 4),'-',RIGHT(IMM.last_school_education_graduation_date, 2),'-','01'))
	ELSE IMM.last_school_education_graduation_date
END as last_school_education_graduation_date
,ability_proof_institution_name
,CASE
	WHEN CHAR_LENGTH(IMM.ability_proof_enrollment_start_date)=6 THEN CONCAT(LEFT(IMM.ability_proof_enrollment_start_date, 4),'-',RIGHT(IMM.ability_proof_enrollment_start_date, 2),'-','01')
	ELSE IMM.ability_proof_enrollment_start_date
END as ability_proof_enrollment_start_date
,CASE
	WHEN CHAR_LENGTH(IMM.ability_proof_enrollment_end_date)=6 THEN LAST_DAY(CONCAT(LEFT(IMM.ability_proof_enrollment_end_date, 4),'-',RIGHT(IMM.ability_proof_enrollment_end_date, 2),'-','01'))
	ELSE IMM.ability_proof_enrollment_end_date
END as ability_proof_enrollment_end_date
,support_pay_amount_self
,remittance_from_abroad_amount
,carrying_from_abroad_amount
,carrying_from_abroad_name
,carrying_from_abroad_date
,support_pay_amount_living_japan
,support_pay_amount_scholarship
,support_pay_amount_others
,occupation_before_admission
,total_education_years
,home_country_id
,home_country_address1
,home_country_phone_no1
,passport_no
,passport_expired_date
,entry_date
,CASE
--	WHEN '転校生' THEN 0
--	WHEN '人文知識' THEN 13
--	WHEN '技術・人文' THEN 13
--	WHEN '短期' THEN 19
--	WHEN '短期30days' THEN 19
--	WHEN '短期90日' THEN 19
--	WHEN '短期滞在90' THEN 19
--	WHEN '留学・短期' THEN 20
--	WHEN '投資' THEN 8
--	WHEN '日本人の配' THEN 25
--	WHEN '日本人配偶' THEN 25
--	WHEN '定住者' THEN 27
-- 20190122 以下追加
	WHEN IMM.visa_type LIKE '%転校生%' THEN 0
	WHEN IMM.visa_type LIKE '%人文%' THEN 13
	WHEN IMM.visa_type LIKE '%技術%' THEN 13
	WHEN IMM.visa_type LIKE '%短期%' THEN 19
	WHEN IMM.visa_type LIKE '%定住%' THEN 27
	WHEN IMM.visa_type LIKE '%留学%' THEN 20
	WHEN IMM.visa_type LIKE '%投資%' THEN 8
	WHEN IMM.visa_type LIKE '%日本人%' THEN 25
	WHEN IMM.visa_type LIKE '%特定%' THEN 23
	WHEN IMM.visa_type LIKE '%経営%' THEN 8
	WHEN IMM.visa_type LIKE '%芸術%' THEN 4
	WHEN IMM.visa_type LIKE '%家族%' THEN 22
	ELSE visa_type.code1
END as visa_type
,REPLACE(immigration_application_no,'東学認','')
FROM(
	SELECT
		student.person_id as person_id
		,system8_3_immigration.`出生地` as birth_address1 
		,system8_3_immigration.`査証申請予定地` as intended_visa_apply_place
		,system8_3_immigration.`過去出入国歴有無` as is_past_entry_departure
		,system8_3_immigration.`過去出入国回数` as past_entry_times
		,system8_3_immigration.`直近出入国年月日～` as last_entry_date
		,system8_3_immigration.`～直近出入国年月日` as last_departure_date
		,system8_3_immigration.`最終学歴状況（在・卒・休）` as last_school_education_status
		,system8_3_immigration.`最終学歴状況（学校種別）` as last_school_education
		,system8_3_immigration.`学校名` as last_school_education_name
		,system8_3_immigration.`卒業・見込年月日` as last_school_education_graduation_date
		,system8_3_immigration.`日本語教育機関名` as ability_proof_institution_name
		,system8_3_immigration.`日本語教育年月日～` as ability_proof_enrollment_start_date
		,system8_3_immigration.`～日本語教育年月日` as ability_proof_enrollment_end_date
		,system8_3_immigration.`支弁方法＿本人負担額` as support_pay_amount_self
		,system8_3_immigration.`支弁方法＿外国から送金額` as remittance_from_abroad_amount
		,system8_3_immigration.`支弁方法＿外国から携行額` as carrying_from_abroad_amount
		,system8_3_immigration.`支弁方法＿携行者` as carrying_from_abroad_name
		,system8_3_immigration.`支弁方法＿携行時期` as carrying_from_abroad_date
		,system8_3_immigration.`支弁方法＿在日支弁負担額` as support_pay_amount_living_japan
		,system8_3_immigration.`支弁方法＿奨学金` as support_pay_amount_scholarship
		,system8_3_immigration.`支弁方法＿その他` as support_pay_amount_others
		,system8_3_immigration.`職業` as occupation_before_admission
		,system8_3_immigration.`修学年数` as total_education_years
		,system8_1_student_base.`国名` as home_country_id
		,system8_1_student_base.`本国住所` as home_country_address1
		,system8_1_student_base.`本国電話` as home_country_phone_no1
		,system8_1_student_base.`旅券番号` as passport_no
		,system8_1_student_base.`旅券有効期限` as passport_expired_date
		,system8_1_student_base.`入国年月日` as entry_date
		,system8_1_student_base.`生VISA種類` as visa_type
		,system8_1_student_base.`入管許可番号` as immigration_application_no
	FROM system8_1_student_base
	INNER JOIN student ON student.student_no=system8_1_student_base.`学籍番号`
	LEFT JOIN system8_3_immigration ON system8_3_immigration.`学籍番号`=system8_1_student_base.`学籍番号`
)IMM
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='last_school_education_status'
)last_school_education_status_cd ON last_school_education_status_cd.name=IMM.last_school_education_status
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='visa_type'
)visa_type ON visa_type.name=IMM.visa_type
;

UPDATE immigration
LEFT JOIN student ON student.person_id=immigration.person_id
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN country ON country.name=system8_1_student_base.`国名` AND country.disable = 0
SET immigration.home_country_id=country.id
,immigration.lastup_account_id=2000
,immigration.lastup_datetime=now()
;







-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT `生VISA種類`
FROM `system8_1_student_base`
LEFT JOIN system_code_detail scd ON scd.name=system8_1_student_base.`生VISA種類`
WHERE scd.name is null
GROUP BY `生VISA種類`

SELECT 
student.person_id as person_id
,system8_3_immigration.`出生地` as birth_address1 
,system8_3_immigration.`査証申請予定地` as intended_visa_apply_place
,system8_3_immigration.`過去出入国歴有無` as is_past_entry_departure
,system8_3_immigration.`過去出入国回数` as past_entry_times
,system8_3_immigration.`直近出入国年月日～` as last_entry_date
,system8_3_immigration.`～直近出入国年月日` as last_departure_date
,system8_3_immigration.`最終学歴状況（在・卒・休）` as last_school_education_status
,system8_3_immigration.`最終学歴状況（学校種別）` as last_school_education
,system8_3_immigration.`学校名` as last_school_education_name
,system8_3_immigration.`卒業・見込年月日` as last_school_education_graduation_date
,system8_3_immigration.`日本語教育機関名` as ability_proof_institution_name
,system8_3_immigration.`日本語教育年月日～` as ability_proof_enrollment_start_date
,system8_3_immigration.`～日本語教育年月日` as ability_proof_enrollment_end_date
,system8_3_immigration.`支弁方法＿本人負担額` as support_pay_amount_self
,system8_3_immigration.`支弁方法＿外国から送金額` as remittance_from_abroad_amount
,system8_3_immigration.`支弁方法＿外国から携行額` as carrying_from_abroad_amount
,system8_3_immigration.`支弁方法＿携行者` as carrying_from_abroad_name
,system8_3_immigration.`支弁方法＿携行時期` as carrying_from_abroad_date
,system8_3_immigration.`支弁方法＿在日支弁負担額` as support_pay_amount_living_japan
,system8_3_immigration.`支弁方法＿奨学金` as support_pay_amount_scholarship
,system8_3_immigration.`支弁方法＿その他` as support_pay_amount_others
,system8_3_immigration.`職業` as occupation_before_admission
,system8_3_immigration.`修学年数` as total_education_years
FROM system8_3_immigration -- 在留資格項目.CSV
LEFT JOIN student ON student.student_no=system8_3_immigration.`学籍番号`
ORDER BY student.person_id
;

SELECT 
student.person_id as person_id
,system8_1_student_base.`本国住所` as home_country_address1
,system8_1_student_base.`本国電話` as home_country_phone_no1
,system8_1_student_base.`旅券番号` as passport_no
,system8_1_student_base.`旅券有効期限` as passport_expired_date
,system8_1_student_base.`入国年月日` as entry_date
,system8_1_student_base.`生VISA種類` as visa_type
,system8_1_student_base.`入管許可番号` as immigration_application_no
FROM system8_1_student_base -- 生徒基本項目.CSV
LEFT JOIN student ON student.student_no=system8_1_student_base.`学籍番号`
ORDER BY student.person_id
;

SELECT 
immigration.person_id
,birth_address1 -- 出生地 住所 1
,intended_visa_apply_place
,CASE is_past_entry_departure
	WHEN 0 THEN '無'
	WHEN 1 THEN '有'
END as is_past_entry_departure
,past_entry_times -- 過去の出入回数
,last_entry_date -- 直近の出入国歴from
,last_departure_date -- 直近の出入国歴to
,scd_last_school_education_status.name as last_school_education_status -- 最終学歴在籍状況
,scd_last_school_education.name as last_school_education -- 最終学歴
,last_school_education_name -- 学校名
,last_school_education_graduation_date -- 卒業又は卒業見込み年月
,ability_proof_institution_name
,ability_proof_enrollment_start_date
,ability_proof_enrollment_end_date
,support_pay_amount_self
,remittance_from_abroad_amount
,carrying_from_abroad_amount
,carrying_from_abroad_name
,carrying_from_abroad_date
,support_pay_amount_living_japan
,support_pay_amount_scholarship
,support_pay_amount_others
,occupation_before_admission
,total_education_years
FROM student
LEFT JOIN immigration ON immigration.person_id=student.person_id
LEFT JOIN (
        SELECT system_code_detail.code1,system_code_detail.name
        FROM system_code
        INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
        WHERE system_code.disable=0
        AND system_code_detail.disable=0 
        AND system_code.column_name='last_school_education_status' -- 最終学歴_在籍状態
)scd_last_school_education_status ON scd_last_school_education_status.code1 =immigration.last_school_education_status 
LEFT JOIN (
        SELECT system_code_detail.code1,system_code_detail.name
        FROM system_code
        INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
        WHERE system_code.disable=0
        AND system_code_detail.disable=0 
        AND system_code.column_name='last_school_education' -- 最終学歴
)scd_last_school_education ON scd_last_school_education.code1 =immigration.last_school_education 
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY student.person_id
;

SELECT 
immigration.person_id
,home_country_address1
,home_country_phone_no1
,passport_no
,passport_expired_date
,entry_date
,scd_visa_type.name as visa_type
,immigration_application_no
FROM student
LEFT JOIN immigration ON immigration.person_id=student.person_id
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='visa_type'
)scd_visa_type ON scd_visa_type.code1=immigration.visa_type
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY student.person_id
;



