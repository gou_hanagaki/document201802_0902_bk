TRUNCATE TABLE external_result;
INSERT INTO external_result(
person_id
-- ,class_id
,external_test_id
,test_date
,apply_status
,input_status
,result_status
,total_score
,test_country_name
,external_test_level_id
)
SELECT
-- student.student_no
student.person_id
,6 as external_test_id
,`実施月` as test_date
,CASE 
	WHEN (`言語知識`='欠席' AND `読解`='欠席' AND `聴解`='欠席') THEN 1
	ELSE 2
END as apply_status
,2 as input_status
,CASE `合否`
	WHEN '○' THEN 1
	ELSE 0
END as result_status
,`合計` -- total_score
,'日本' as test_country_name
,CASE `レベル`
	WHEN 'N1' THEN 6
	WHEN 'N2' THEN 7
	WHEN 'N3' THEN 8
	WHEN 'N4' THEN 9
	WHEN 'N5' THEN 10
END as external_test_level_id
FROM excel_3_external_result_jlpt_N123
INNER JOIN student ON student.student_no=excel_3_external_result_jlpt_N123.`学籍番号`
ORDER BY `school_term_id`,`レベル`
;

INSERT INTO external_result(
person_id
-- ,class_id
,external_test_id
,test_date
,apply_status
,input_status
,result_status
,total_score
,test_country_name
,external_test_level_id
)
SELECT
-- student.student_no
student.person_id
,6 as external_test_id
,`実施月` as test_date
,CASE 
	WHEN (`言語知識・読解`='欠席' AND `聴解`='欠席') THEN 1
	ELSE 2
END as apply_status
,2 as input_status
,CASE `合否`
	WHEN '○' THEN 1
	ELSE 0
END as result_status
,`合計` -- total_score
,'日本' as test_country_name
,CASE `レベル`
	WHEN 'N1' THEN 6
	WHEN 'N2' THEN 7
	WHEN 'N3' THEN 8
	WHEN 'N4' THEN 9
	WHEN 'N5' THEN 10
END as external_test_level_id
FROM excel_4_external_result_jlpt_N45
INNER JOIN student ON student.student_no=excel_4_external_result_jlpt_N45.`学籍番号`
ORDER BY `school_term_id`,`レベル`
;

INSERT INTO external_result(
person_id
-- ,class_id
,external_test_id
,test_date
,apply_status
,input_status
,result_status
,total_score
,test_country_name
,external_test_level_id
)
SELECT
student.person_id
,7 as external_test_id
,`実施月` as test_date
,CASE 
	WHEN (`聴解・聴読解`='欠席' AND `読解`='欠席' AND `記述`='欠席') THEN 1
	ELSE 2
END as apply_status
,2 as input_status
,0 as result_status
,`聴解・聴読解`+`読解`+`記述`+`総合科目`+`物理`+`化学`+`生物`+`数学1`+`数学2` as total_score
,'日本' as test_country_name
,11 as external_test_level_id
FROM excel_5_external_result_eju
INNER JOIN student ON student.student_no=excel_5_external_result_eju.`学生番号`
;