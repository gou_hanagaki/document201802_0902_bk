ALTER TABLE content_test_list MODIFY `ability_id` VARCHAR(256) NOT NULL DEFAULT '';

TRUNCATE TABLE content_test_list;
INSERT INTO content_test_list (`test_id`,`ability_id`,`name`,`total_point`,`comment`) 
VALUES 
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20160401', 200, 125),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20160402', 180, 125),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20160701', 200, 126),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20160702', 180, 126),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20161001', 200, 127),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20161002', 180, 127),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170101', 200, 128),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170102', 180, 128),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170401', 200, 129),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170402', 180, 129),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170701', 200, 130),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20170702', 180, 130),
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20171001', 200, 131),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20171002', 180, 131)
;

TRUNCATE TABLE content_test_section;
INSERT INTO `content_test_section` 
(`content_test_list_id`, `test_section_cd`, `total_point`, `sequence`, `lastup_account_id`, `create_datetime`) 
VALUES
(0,	1,	100,	20160401,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20160401,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20160402,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20160402,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20160402,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20160701,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20160701,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20160702,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20160702,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20160702,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20161001,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20161001,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20161002,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20161002,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20161002,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20170101,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20170101,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20170102,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20170102,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20170102,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20170401,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20170401,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20170402,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20170402,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20170402,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20170701,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20170701,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20170702,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20170702,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20170702,	2000,	'2018-03-26 00:00:00'),
(0,	1,	100,	20171001,	2000,	'2018-03-26 00:00:00'),
(0,	2,	100,	20171001,	2000,	'2018-03-26 00:00:00'),
(0,	3,	60,	20171002,	2000,	'2018-03-26 00:00:00'),
(0,	4,	60,	20171002,	2000,	'2018-03-26 00:00:00'),
(0,	5,	60,	20171002,	2000,	'2018-03-26 00:00:00')
;

UPDATE content_test_section
INNER JOIN content_test_list ON content_test_list.name=content_test_section.sequence
SET content_test_section.content_test_list_id=content_test_list.id
;

TRUNCATE TABLE achievement;
INSERT INTO achievement(
person_id
,content_test_list_id
,class_id
,test_date
,total_score
)
SELECT
-- student.student_no
-- ,content_test_list.test_id
-- ,content_test_list.comment
student.person_id
,content_test_list.name as content_test_list_id
,class_id
,TEST_DATE.end_date as test_date
,CASE content_test_list.test_id
	WHEN 2 THEN excel_2_term_end_exam_list.`期末テスト合計`
	WHEN 5 THEN excel_2_term_end_exam_list.`日本語能力試験合計`
END as total_score
FROM excel_2_term_end_exam_list
INNER JOIN content_test_list ON content_test_list.comment=excel_2_term_end_exam_list.school_term_id
INNER JOIN excel_1_school_record ON excel_1_school_record.`学籍番号`=excel_2_term_end_exam_list.`学籍番号` AND excel_1_school_record.school_term_id=excel_2_term_end_exam_list.school_term_id
INNER JOIN student ON student.student_no=excel_2_term_end_exam_list.`学籍番号`
LEFT JOIN (
	SELECT
		school_term.id as school_term_id
		,school_term.start_date as test_date
		,class.id as class_id
		,class.name as class_name
		,class.teacher_person_id as teacher_person_id
		,school_term.end_date
	FROM school_term
	INNER JOIN class_group ON class_group.school_term_id = school_term.id
	INNER JOIN class ON class.class_group_id = class_group.id
	INNER JOIN class_student ON class_student.class_id = class.id AND class_student.disable=0
	GROUP BY school_term.id,class.id,class.name,class.teacher_person_id
)TEST_DATE ON TEST_DATE.school_term_id=excel_1_school_record.school_term_id AND TEST_DATE.class_name=excel_1_school_record.`クラス`
ORDER BY content_test_list.id,content_test_list.test_id,TEST_DATE.class_id,student.student_no
;

/*
UPDATE achievement
INNER JOIN content_test_list ON content_test_list.name=achievement.content_test_list_id
SET achievement.content_test_list_id=content_test_list.id
;

UPDATE content_test_section SET sequence='';
;

UPDATE content_test_list SET disable=1
;
*/

-- --------------------------------------------------------------------------------------------------------------------------------------------------
-- add 2017/07 2017/10 2018/01
-- --------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO content_test_list (`test_id`,`ability_id`,`name`,`total_point`,`comment`) 
VALUES 
(2, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20180101', 200, 132),
(5, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', '20180102', 180, 132)
;

INSERT INTO `content_test_section` 
(`content_test_list_id`, `test_section_cd`, `total_point`, `sequence`, `lastup_account_id`, `create_datetime`) 
VALUES
(0,	1,	100,	20180101,	2000,	'2018-04-05 00:00:00'),
(0,	2,	100,	20180101,	2000,	'2018-04-05 00:00:00'),
(0,	3,	60,	20180102,	2000,	'2018-04-05 00:00:00'),
(0,	4,	60,	20180102,	2000,	'2018-04-05 00:00:00'),
(0,	5,	60,	20180102,	2000,	'2018-04-05 00:00:00')
;

UPDATE content_test_section
INNER JOIN content_test_list ON content_test_list.name=content_test_section.sequence
SET content_test_section.content_test_list_id=content_test_list.id
WHERE content_test_section.sequence IN (20180101,20180102)
;

INSERT INTO achievement(
person_id
,content_test_list_id
,class_id
,test_date
,total_score
)
SELECT
-- student.student_no
-- ,content_test_list.test_id
-- ,content_test_list.comment
student.person_id
,content_test_list.name as content_test_list_id
,class_id
,TEST_DATE.end_date as test_date
,CASE content_test_list.test_id
	WHEN 2 THEN excel_2_term_end_exam_list.`期末テスト合計`
	WHEN 5 THEN excel_2_term_end_exam_list.`日本語能力試験合計`
END as total_score
FROM excel_2_term_end_exam_list
INNER JOIN content_test_list ON content_test_list.comment=excel_2_term_end_exam_list.school_term_id
INNER JOIN excel_1_school_record ON excel_1_school_record.`学籍番号`=excel_2_term_end_exam_list.`学籍番号` AND excel_1_school_record.school_term_id=excel_2_term_end_exam_list.school_term_id
INNER JOIN student ON student.student_no=excel_2_term_end_exam_list.`学籍番号`
LEFT JOIN (
	SELECT
		school_term.id as school_term_id
		,school_term.start_date as test_date
		,class.id as class_id
		,class.name as class_name
		,class.teacher_person_id as teacher_person_id
		,school_term.end_date
	FROM school_term
	INNER JOIN class_group ON class_group.school_term_id = school_term.id
	INNER JOIN class ON class.class_group_id = class_group.id
	INNER JOIN class_student ON class_student.class_id = class.id AND class_student.disable=0
	GROUP BY school_term.id,class.id,class.name,class.teacher_person_id
)TEST_DATE ON TEST_DATE.school_term_id=excel_1_school_record.school_term_id AND TEST_DATE.class_name=excel_1_school_record.`クラス`
WHERE excel_2_term_end_exam_list.school_term_id IN (130,131,132)
ORDER BY content_test_list.id,content_test_list.test_id,TEST_DATE.class_id,student.student_no
;

UPDATE achievement
INNER JOIN content_test_list ON content_test_list.name=achievement.content_test_list_id
SET achievement.content_test_list_id=content_test_list.id
WHERE achievement.content_test_list_id IN (20170701,20170702,20171001,20171002,20180101,20180102)
;

