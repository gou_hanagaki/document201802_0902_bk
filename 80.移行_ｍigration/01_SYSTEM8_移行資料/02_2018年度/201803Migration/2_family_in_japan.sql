-- 日本にいる親族？保護者？
TRUNCATE TABLE family_in_japan;
INSERT INTO family_in_japan(
person_id
,relationship -- 続柄
,SUBSTRING_INDEX(name, '　', 1) as last_name -- 姓
,TRIM(LEADING '　' FROM REPLACE(name,SUBSTRING_INDEX(name, '　', 1),'')) as first_name -- 名
-- ,birthday -- 生年月日
,country_id -- 国籍・地域
-- ,is_intended_to_reside -- 同居予定
,employment -- 勤務先
-- ,residence_card_no -- 在留カード番号
)
SELECT
student.person_id
,system8_2_student_other.`保護者名` as name
,system8_2_student_other.`保護者住所`
,system8_2_student_other.`保護者電話`
,system8_2_student_other.`保護者続柄` as relationship
,system8_2_student_other.`保護者職業` as employment
FROM system8_2_student_other
LEFT JOIN student ON student.student_no=system8_2_student_other.`学籍番号`
WHERE true
AND NOT ( true
AND system8_2_student_other.`保護者名` =''
)
;

-- ------------------------------------------------------------------------------
-- 個別UPDATE
-- ------------------------------------------------------------------------------
UPDATE family_in_japan
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN system8_2_student_other ON system8_2_student_other.`学籍番号`=student.student_no
LEFT JOIN country ON country.name=system8_2_student_other.`国名` AND country.disable = 0
SET person.country_id=country.id
,person.lastup_account_id=2000
,person.lastup_datetime=now()
;



