-- -------------------------------------------------------------
-- 2016/04
-- -------------------------------------------------------------
/*
SET @student_no='1604207';
SET @person_id=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id=(SELECT id FROM student WHERE student_no=@student_no);
SELECT * FROM person WHERE id=@person_id;
SELECT * FROM student WHERE person_id=@person_id;
SELECT class.*,class_student.* FROM class_student INNER JOIN class ON class.id=class_student.class_id WHERE person_id=@person_id;

SET @student_no='1604211';
SET @person_id=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id=(SELECT id FROM student WHERE student_no=@student_no);
SELECT * FROM person WHERE id=@person_id;
SELECT * FROM student WHERE person_id=@person_id;
SELECT class.*,class_student.* FROM class_student INNER JOIN class ON class.id=class_student.class_id WHERE person_id=@person_id;
*/
SELECT * FROM class_student
LEFT JOIN student ON student.person_id=class_student.person_id
WHERE school_term_id=125 AND student_no IN (1604211,1604207)
;

UPDATE class_student
SET class_student.person_id=964
WHERE class_student.school_term_id=125 AND person_id=960 AND class_id=14
;

SELECT *
FROM excel_2_term_end_exam_list
WHERE `学籍番号` IN (1604211,1604207)
;

UPDATE excel_2_term_end_exam_list
SET `学籍番号`='1604211' 
WHERE school_term_id='125' AND `氏　名`='曲　思頴'
;

-- -------------------------------------------------------------
-- 2017/04
-- -------------------------------------------------------------
/*
SET @student_no='1604238';
SET @person_id=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id=(SELECT id FROM student WHERE student_no=@student_no);
SELECT * FROM person WHERE id=@person_id;
SELECT * FROM student WHERE person_id=@person_id;
SELECT class.*,class_student.* FROM class_student INNER JOIN class ON class.id=class_student.class_id WHERE person_id=@person_id;
SELECT * FROM attendance_rate WHERE student_id=@student_id ORDER BY attendance_cd,calculate_date_from;

SET @student_no='1704096';
SET @person_id=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id=(SELECT id FROM student WHERE student_no=@student_no);
SELECT * FROM person WHERE id=@person_id;
SELECT * FROM student WHERE person_id=@person_id;
SELECT class.*,class_student.* FROM class_student INNER JOIN class ON class.id=class_student.class_id WHERE person_id=@person_id;
SELECT * FROM attendance_rate WHERE student_id=@student_id ORDER BY attendance_cd,calculate_date_from;

SET @student_no='1704112';
SET @person_id=(SELECT person_id FROM student WHERE student_no=@student_no);
SET @student_id=(SELECT id FROM student WHERE student_no=@student_no);
SELECT * FROM person WHERE id=@person_id;
SELECT * FROM student WHERE person_id=@person_id;
SELECT class.*,class_student.* FROM class_student INNER JOIN class ON class.id=class_student.class_id WHERE person_id=@person_id;
SELECT * FROM attendance_rate WHERE student_id=@student_id ORDER BY attendance_cd,calculate_date_from;
*/
INSERT INTO `class_student` 
(`school_term_id`, `person_id`, `class_id`, `date_from`, `date_to`, `lastup_account_id`, `create_datetime`) 
VALUES
(129, 990, 125, '2017-04-01', '2017-06-30', 2000, now()), -- 1604238
(129, 1281, 144, '2017-06-09', '2017-06-30', 2000, now()), -- 1704096
(129, 1297, 144, '2017-06-12', '2017-06-30', 2000, now())  -- 1704112
;

INSERT INTO `class_student` 
(`school_term_id`, `person_id`, `class_id`, `date_from`, `date_to`, `lastup_account_id`, `create_datetime`) 
VALUES
(129, 990, 125, '2017-04-01', '2017-06-30', 2000, now()), -- 1604238
(129, 1281, 144, '2017-06-09', '2017-06-30', 2000, now()), -- 1704096
(129, 1297, 144, '2017-06-12', '2017-06-30', 2000, now())  -- 1704112
;



INSERT INTO achievement(
person_id
,content_test_list_id
,class_id
,test_date
,total_score
)
VALUES
(964, 3,  14, '2016-06-30',   78),
(964, 4,  14, '2016-06-30',   136),

(990, 11,  125, '2017-06-30',   113),
(990, 12,  125, '2017-06-30',   101),

(1281, 11,  144, '2017-06-30',   150),
(1281, 12,  144, '2017-06-30',   109),

(1297, 11,  144, '2017-06-30',   163),
(1297, 12,  144, '2017-06-30',   156)

;


INSERT INTO achievement_detail(
achievement_id
,test_section_cd
,score
)
VALUES
(8964, 1, 0),
(8964, 2, 78),
(8965, 3, 42),
(8965, 4, 42),
(8965, 5, 52),

(8966, 1, 42),
(8966, 2, 71),
(8967, 3, 27),
(8967, 4, 23),
(8967, 5, 51),

(8968, 1, 73),
(8968, 2, 77),
(8969, 3, 33),
(8969, 4, 40),
(8969, 5, 36),

(8970, 1, 88),
(8970, 2, 75),
(8971, 3, 46),
(8971, 4, 50),
(8971, 5, 60)
;