SELECT * 
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN account ON account.person_id=person.id
WHERE account.login_id IS NULL
;

-- 新入生
INSERT INTO account(
account_group_id
,person_id
,login_id
,password
)
SELECT 
'1'
,person.id
,student.student_no
,'305E4F55CE823E111A46A9D500BCB86C'
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN account ON account.person_id=person.id
WHERE account.login_id IS NULL
;
