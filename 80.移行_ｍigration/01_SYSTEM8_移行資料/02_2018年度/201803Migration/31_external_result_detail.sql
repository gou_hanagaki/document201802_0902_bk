TRUNCATE TABLE external_result_detail;
INSERT INTO external_result_detail(
external_result_id
,external_test_level_section_id
,score
)
SELECT
-- JLPT.student_no
-- ,JLPT.school_term_id
-- ,JLPT.test_date
external_result.id
,JLPT.external_test_level_section_id
,JLPT.score
FROM external_result
LEFT JOIN student ON student.person_id=external_result.person_id
LEFT JOIN external_test ON external_test.id=external_result.external_test_id
LEFT JOIN external_test_level ON external_test_level.id=external_result.external_test_level_id
LEFT JOIN (
    SELECT
        excel_3_external_result_jlpt_N123.school_term_id
        ,excel_3_external_result_jlpt_N123.`実施月` as test_date
        ,excel_3_external_result_jlpt_N123.`学籍番号` as student_no
        ,jlpt_test_section.external_test_level_section_id as external_test_level_section_id
        ,jlpt_test_section.external_test_level_id as external_test_level_id
        ,CASE jlpt_test_section.section_name
            WHEN '言語知識' THEN excel_3_external_result_jlpt_N123.`言語知識`
            WHEN '読解' THEN excel_3_external_result_jlpt_N123.`読解`
            WHEN '聴解' THEN excel_3_external_result_jlpt_N123.`聴解`
        END as score
        ,excel_3_external_result_jlpt_N123.`No`
    FROM excel_3_external_result_jlpt_N123 
    CROSS JOIN (
        SELECT
            id as external_test_level_section_id
            ,external_test_level_id
            ,section_name
        FROM external_test_level_section
        WHERE external_test_level_id IN (6,7,8)
    )jlpt_test_section
    ORDER BY school_term_id,external_test_level_id,`No`
)JLPT ON JLPT.test_date=external_result.test_date AND JLPT.student_no=student.student_no AND JLPT.external_test_level_id=external_result.external_test_level_id
WHERE score is not null
ORDER BY JLPT.school_term_id,JLPT.test_date,JLPT.external_test_level_id
;

INSERT INTO external_result_detail(
external_result_id
,external_test_level_section_id
,score
)
SELECT
-- JLPT.student_no
-- ,JLPT.school_term_id
-- ,JLPT.test_date
external_result.id
,JLPT.external_test_level_section_id
,JLPT.score
FROM external_result
LEFT JOIN student ON student.person_id=external_result.person_id
LEFT JOIN external_test ON external_test.id=external_result.external_test_id
LEFT JOIN external_test_level ON external_test_level.id=external_result.external_test_level_id
LEFT JOIN (
    SELECT
        excel_4_external_result_jlpt_N45.school_term_id
        ,excel_4_external_result_jlpt_N45.`実施月` as test_date
        ,excel_4_external_result_jlpt_N45.`学籍番号` as student_no
        ,jlpt_test_section.external_test_level_section_id as external_test_level_section_id
        ,jlpt_test_section.external_test_level_id as external_test_level_id
        ,CASE jlpt_test_section.section_name
            WHEN '言語知識・読解' THEN excel_4_external_result_jlpt_N45.`言語知識・読解`
            WHEN '聴解' THEN excel_4_external_result_jlpt_N45.`聴解`
        END as score
        ,excel_4_external_result_jlpt_N45.`No`
    FROM excel_4_external_result_jlpt_N45 
    CROSS JOIN (
        SELECT
            id as external_test_level_section_id
            ,external_test_level_id
            ,section_name
        FROM external_test_level_section
        WHERE external_test_level_id IN (9,10)
    )jlpt_test_section
    ORDER BY school_term_id,external_test_level_id,`No`
)JLPT ON JLPT.test_date=external_result.test_date AND JLPT.student_no=student.student_no AND JLPT.external_test_level_id=external_result.external_test_level_id
WHERE score!='欠席'
ORDER BY JLPT.school_term_id,JLPT.test_date,JLPT.external_test_level_id
;

INSERT INTO external_result_detail(
external_result_id
,external_test_level_section_id
,score
)
SELECT
-- EJU.student_no
-- ,EJU.school_term_id
-- ,EJU.test_date
external_result.id
,EJU.external_test_level_section_id
,EJU.score
FROM external_result
LEFT JOIN student ON student.person_id=external_result.person_id
LEFT JOIN external_test ON external_test.id=external_result.external_test_id
LEFT JOIN external_test_level ON external_test_level.id=external_result.external_test_level_id
LEFT JOIN (
    SELECT
        excel_5_external_result_eju.school_term_id
        ,excel_5_external_result_eju.`実施月` as test_date
        ,excel_5_external_result_eju.`学生番号` as student_no
        ,EJU_test_section.external_test_level_section_id as external_test_level_section_id
        ,EJU_test_section.external_test_level_id as external_test_level_id
        ,CASE EJU_test_section.section_name
            WHEN '聴解・聴読解' THEN excel_5_external_result_eju.`聴解・聴読解`
            WHEN '読解' THEN excel_5_external_result_eju.`読解`
            WHEN '記述' THEN excel_5_external_result_eju.`記述`
            WHEN '総合科目' THEN excel_5_external_result_eju.`総合科目`
            WHEN '物理' THEN excel_5_external_result_eju.`物理`
            WHEN '化学' THEN excel_5_external_result_eju.`化学`
            WHEN '生物' THEN excel_5_external_result_eju.`生物`
            WHEN '数学1' THEN excel_5_external_result_eju.`数学1`
            WHEN '数学2' THEN excel_5_external_result_eju.`数学2`
        END as score
    FROM excel_5_external_result_eju 
    CROSS JOIN (
        SELECT
            id as external_test_level_section_id
            ,external_test_level_id
            ,section_name
        FROM external_test_level_section
        WHERE external_test_level_id IN (11)
    )EJU_test_section
    ORDER BY school_term_id,external_test_level_id
)EJU ON EJU.test_date=external_result.test_date AND EJU.student_no=student.student_no AND EJU.external_test_level_id=external_result.external_test_level_id
-- WHERE score!='欠席' AND score>=0
WHERE score REGEXP '^[0-9]+$'
ORDER BY EJU.school_term_id,EJU.test_date,EJU.external_test_level_id
;