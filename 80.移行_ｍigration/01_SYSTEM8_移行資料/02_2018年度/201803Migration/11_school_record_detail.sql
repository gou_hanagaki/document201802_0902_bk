TRUNCATE TABLE school_record_detail;
INSERT INTO school_record_detail(
school_record_id
,school_record_section_id
,grade
)
SELECT
school_record.id
-- SRS.school_term_id
-- ,SRS.student_no
,school_record_section_id
,system_code_detail.code1 as grade
-- ,SRS.school_record_section_name
FROM (
    SELECT
        excel_1_school_record.school_term_id
        ,excel_1_school_record.`学籍番号` as student_no
        ,school_record_section.id as school_record_section_id
        ,CASE school_record_section.name
            WHEN '文字語彙' then excel_1_school_record.`文字語彙`
            WHEN '文法' then excel_1_school_record.`文法`
            WHEN '読解' then excel_1_school_record.`読解`
            WHEN '聴解' then excel_1_school_record.`聴解`
            WHEN '会話' then excel_1_school_record.`会話`
            WHEN '作文' then excel_1_school_record.`作文`
            WHEN '平常点' then excel_1_school_record.`平常点`
            WHEN '総合' then excel_1_school_record.`総合`
        END as school_record_section_name
    FROM excel_1_school_record 
    CROSS JOIN school_record_section
    ORDER BY excel_1_school_record.school_term_id,excel_1_school_record.`学籍番号`
)SRS
LEFT JOIN student ON student.student_no=SRS.student_no
LEFT JOIN school_record ON school_record.school_term_id=SRS.school_term_id AND school_record.person_id=student.person_id
LEFT JOIN system_code_detail ON system_code_detail.name=school_record_section_name
LEFT JOIN system_code ON system_code.id=system_code_detail.system_code_id
WHERE system_code.column_name='school_record_grade'
-- ORDER BY school_record.id,school_record_section_id
ORDER BY SRS.school_term_id,SRS.student_no,school_record_section_id
;

-- ----------------------------------------------------------------------------------------------------------------
-- add 2018/01
-- ----------------------------------------------------------------------------------------------------------------
INSERT INTO school_record_detail(
school_record_id
,school_record_section_id
,grade
)
SELECT
school_record.id
-- SRS.school_term_id
-- ,SRS.student_no
,school_record_section_id
,system_code_detail.code1 as grade
-- ,SRS.school_record_section_name
FROM (
    SELECT
        excel_1_school_record.school_term_id
        ,excel_1_school_record.`学籍番号` as student_no
        ,school_record_section.id as school_record_section_id
        ,CASE school_record_section.name
            WHEN '文字語彙' then excel_1_school_record.`文字語彙`
            WHEN '文法' then excel_1_school_record.`文法`
            WHEN '読解' then excel_1_school_record.`読解`
            WHEN '聴解' then excel_1_school_record.`聴解`
            WHEN '会話' then excel_1_school_record.`会話`
            WHEN '作文' then excel_1_school_record.`作文`
            WHEN '平常点' then excel_1_school_record.`平常点`
            WHEN '総合' then excel_1_school_record.`総合`
        END as school_record_section_name
    FROM excel_1_school_record 
    CROSS JOIN school_record_section
    ORDER BY excel_1_school_record.school_term_id,excel_1_school_record.`学籍番号`
)SRS
LEFT JOIN student ON student.student_no=SRS.student_no
LEFT JOIN school_record ON school_record.school_term_id=SRS.school_term_id AND school_record.person_id=student.person_id
LEFT JOIN system_code_detail ON system_code_detail.name=school_record_section_name
LEFT JOIN system_code ON system_code.id=system_code_detail.system_code_id
WHERE system_code.column_name='school_record_grade' AND SRS.school_term_id=132
-- ORDER BY school_record.id,school_record_section_id
ORDER BY SRS.school_term_id,SRS.student_no,school_record_section_id
;