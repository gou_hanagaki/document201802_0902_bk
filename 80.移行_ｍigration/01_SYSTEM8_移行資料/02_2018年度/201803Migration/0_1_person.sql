-- 新入生
INSERT INTO person(
type
-- ,country_id
,native_language
-- ,parent_person_id
,birthday
,sex_cd
,married_cd
,zipcode
,address_country_id
,address1
-- ,address2
-- ,address3
,phone_no
-- ,phone_no2
,mobile_no
,previous_address_zipcode
,previous_address1
-- ,previous_address2
-- ,previous_address3
-- ,mail_address1
-- ,mail_address2
,last_name
,first_name
,name_font
,last_name_kana
,first_name_kana
,last_name_alphabet
,first_name_alphabet
-- ,legal_registered_last_name
-- ,legal_registered_first_name
-- ,legal_registered_last_name_kana
-- ,legal_registered_first_name_kana
-- ,legal_registered_last_name_alphabet
-- ,legal_registered_first_name_alphabet
-- ,abbreviation_name
-- ,last_name_native -- 姓_母国語
-- ,first_name_native -- 名_母国語
,lastup_account_id
)
SELECT 
1 as type -- 1:学生
,KIHON.native_language
,KIHON.birthday
,CASE KIHON.sex
	WHEN '男' THEN 1
	WHEN '女' THEN 2
	ELSE 0
END as sex_cd
,CASE KIHON.married
	WHEN '未婚' THEN 1
	WHEN '既婚' THEN 2
	ELSE 0
END as married_cd
,KIHON.zipcode
,'156' as address_country_id -- 156:日本
,KIHON.address1
,KIHON.phone_no
,KIHON.mobile_no
,KIHON.previous_address_zipcode
,KIHON.previous_address1
,KIHON.name as last_name
,KIHON.name as first_name
,KIHON.name_font
,KIHON.name_kana as last_name_kana
,KIHON.name_kana as first_name_kana
,KIHON.name_en as last_name_alphabet
,KIHON.name_en as first_name_alphabet
,KIHON.student_no as lastup_account_id
FROM (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日` as birthday
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
WHERE student.student_no IS NULL
;

-- ------------------------------------------------------------------------------
-- 在校生
-- ------------------------------------------------------------------------------
UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日`
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
-- person.type
-- person.country_id
person.native_language=KIHON.native_language
-- ,person.parent_person_id
,person.birthday=KIHON.`生年月日`
,person.sex_cd=	CASE KIHON.sex
					WHEN '男' THEN 1
					WHEN '女' THEN 2
					ELSE 0
				END
,person.married_cd=	CASE KIHON.married
						WHEN '未婚' THEN 1
						WHEN '既婚' THEN 2
						ELSE 0
					END
,person.zipcode=KIHON.zipcode
,person.address_country_id='156' -- 156:日本
,person.address1=KIHON.address1
-- ,person.address2
-- ,person.address3
-- ,person.phone_no=KIHON.phone_no -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.phone_no2 -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.mobile_no=KIHON.mobile_no -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
,person.previous_address_zipcode=KIHON.previous_address_zipcode
,person.previous_address1=KIHON.previous_address1
-- ,person.previous_address2
-- ,person.previous_address3
-- ,person.mail_address1
-- ,person.mail_address2
-- ,person.last_name -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
,person.name_font=KIHON.name_font
-- ,person.last_name_kana -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_kana -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.last_name_alphabet -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_alphabet -- 名前（漢字、英字、読み方）と電話番号は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.legal_registered_last_name -- 戸籍
-- ,person.legal_registered_first_name -- 戸籍
-- ,person.legal_registered_last_name_kana -- 戸籍
-- ,person.legal_registered_first_name_kana -- 戸籍
-- ,person.legal_registered_last_name_alphabet -- 戸籍
-- ,person.legal_registered_first_name_alphabet -- 戸籍
-- ,person.abbreviation_name -- 表示用
-- ,person.last_name_native -- 姓_母国語
-- ,person.first_name_native -- 名_母国語
-- ,person.lastup_account_id=2000
-- ,person.lastup_datetime=now()
;

-- ------------------------------------------------------------------------------
-- 個別UPDATE
-- ------------------------------------------------------------------------------
UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN country ON country.name=system8_1_student_base.`国名` AND country.disable = 0
SET person.country_id=	CASE system8_1_student_base.`国名`
							WHEN '台湾' THEN 127
							ELSE country.id
						END
-- ,person.lastup_account_id=2000
-- ,person.lastup_datetime=now()
;

-- ------------------------------------------------------------------------------
-- 追加
-- ------------------------------------------------------------------------------
UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN country ON country.name=system8_1_student_base.`国名` AND country.disable = 0
SET person.country_id=	CASE system8_1_student_base.`国名`
							WHEN '台湾' THEN 127
							WHEN 'ネパール' THEN 160
							WHEN 'バングラデシュ' THEN 179
							WHEN 'ウズベキスタン' THEN 31
							WHEN '中国' THEN 133
							ELSE country.id
						END
WHERE system8_1_student_base.`国名` IN ('台湾','ネパール','バングラデシュ','ウズベキスタン','中国')

-- ------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------
SELECT
`学籍番号` as student_no
,person.id
,1 as type
,`国名` 
,`母国語`
,`生年月日` as birthday
,`性別` as sex
,`未既婚` as married
,`現住所〒` as zipcode
,`現住所住` as address1
,`現住所電話` as phone_no
,`携　帯電話` as mobile_no
,`以前住所〒` as previous_address_zipcode
,`以前住所住` as previous_address1
,`本国氏名1` as name
,`本国氏名書体` as name_font
,`フリガナ名` as name_kana
,`本国氏名2` as name_en
-- ,`本国住所` 
-- ,`本国電話` -- 移行対象外？
-- ,`紹介者名` 
-- ,`本国住所書体` 
-- ,`旅券番号` 
-- ,`旅券有効期限` 
-- ,`入国年月日` 
-- ,`生VISA種類` 
-- ,`登録番号` 
-- ,`VISA更新日(1)` 
-- ,`VISA更新日(2)` 
-- ,`VISA更新日(3)` 
-- ,`VISA更新日(4)` 
-- ,`入管許可番号` 
-- ,`入管許可日` 
-- ,`在留カード番号` 
-- ,`入学日` as start_date
-- ,`卒修予定日` as end_date_intended
-- ,`コース名` as course
-- ,`クラス名` 
-- ,`退学年月日` as end_date1
-- ,`除籍年月日` as end_date2
-- ,`退学除籍理由` as reason_for_exclusion
-- ,`在籍卒業区分` as status_name
-- ,`国民健康保険番号` 
-- ,`障害保険有無` 
-- ,`卒業後進路先` as after_graduate_school
-- ,`紹介者書体` 
-- ,`紹介者名1` 
-- ,`紹介者名2` 
-- ,`紹介者住所1` 
-- ,`紹介者住所2` 
-- ,`紹介者TEL` 
-- ,`紹介者FAX` 
-- ,`J   ﾃｽﾄ` 
-- ,`NAT ﾃｽﾄ` 
-- ,`入学ﾃｽﾄ` 
-- ,`組分ﾃｽﾄ` 
FROM system8_1_student_base KIHON
LEFT JOIN student ON student.student_no=KIHON.`学籍番号`
LEFT JOIN person ON person.id=student.person_id
ORDER BY student.person_id
;

SELECT 
student.student_no
,person.`id`
,`type`
,country.name as `country_id`
,`native_language`
-- ,`native_language_en`
-- ,`parent_person_id`
,`birthday`
,gender.name as `sex_cd`
,marital_status.name as `married_cd`
,`zipcode`
-- ,`address_country_id`
,`address1`
-- ,`address2`
-- ,`address3`
-- ,`address1_en`
-- ,`address2_en`
-- ,`address3_en`
,`phone_no`
-- ,`phone_no2`
,`mobile_no`
,`previous_address_zipcode`
,`previous_address1`
-- ,`previous_address2`
-- ,`previous_address3`
-- ,`mail_address1`
-- ,`mail_address2`
-- ,`sns_account`
-- ,`sns_type`
,`last_name`
,`first_name`
,`name_font`
,`last_name_kana`
,`first_name_kana`
,`last_name_alphabet`
,`first_name_alphabet`
-- ,`legal_registered_last_name`
-- ,`legal_registered_first_name`
-- ,`legal_registered_last_name_kana`
-- ,`legal_registered_first_name_kana`
-- ,`legal_registered_last_name_alphabet`
-- ,`legal_registered_first_name_alphabet`
-- ,`abbreviation_name`
-- ,`last_name_native`
-- ,`first_name_native`
-- ,`photo`
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN country ON country.id=person.country_id AND country.disable = 0
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='gender'
)gender ON gender.code1=person.sex_cd
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='marital_status'
)marital_status ON marital_status.code1=person.married_cd
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id
