DROP TABLE IF EXISTS `test_section`;

/*
ALTER TABLE student DROP COLUMN part_time_job_name;
ALTER TABLE student DROP COLUMN part_time_job_address1;
ALTER TABLE student DROP COLUMN part_time_job_address2;
ALTER TABLE student DROP COLUMN part_time_job_address3;
ALTER TABLE student DROP COLUMN part_time_job_phone_no;
*/

/*
ALTER TABLE `student`
ADD COLUMN `part_time_job_name` VARCHAR(128) NOT NULL DEFAULT '' AFTER `priority_student_part_time_job_id`,
ADD COLUMN `part_time_job_address1` VARCHAR(128) NOT NULL DEFAULT '' AFTER `part_time_job_name`,
ADD COLUMN `part_time_job_address2` VARCHAR(128) NOT NULL DEFAULT '' AFTER `part_time_job_address1`,
ADD COLUMN `part_time_job_address3` VARCHAR(128) NOT NULL DEFAULT '' AFTER `part_time_job_address2`,
ADD COLUMN `part_time_job_phone_no` VARCHAR(128) NOT NULL DEFAULT '' AFTER `part_time_job_address3`
;
*/
/*
DELETE FROM person WHERE id >= 2287;
ALTER TABLE person AUTO_INCREMENT = 2287;
*/
