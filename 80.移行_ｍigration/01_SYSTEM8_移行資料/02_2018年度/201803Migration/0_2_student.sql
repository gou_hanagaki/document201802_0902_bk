-- 新入生
INSERT INTO student(
person_id
-- ,agent_id
-- ,sales_staff_person_id
,student_no
,course_id
,course_length_month
-- ,ability_id
-- ,start_date_intended
,start_date
,end_date_intended
-- ,end_date
-- ,weekly_study_hours
,status
-- ,final_judgement -- 卒業判定
-- ,status_print -- 卒業修了証印刷済みフラグ
,quit_reason_cd
,reason_for_exclusion
,student_type_cd -- 学生種別 0:進学 1:短期
,after_graduate_school
-- ,general_observation -- 総合所見
-- ,general_observation_account_id -- 評価者
-- ,general_observation_date -- 評価日
-- ,priority_student_part_time_job_id -- 優先アルバイト情報ID
-- ,start_preferred_date_intended -- 入学希望年学期 
-- ,part_time_job_name
-- ,part_time_job_address1
-- ,part_time_job_address2
-- ,part_time_job_address3
-- ,part_time_job_phone_no
-- ,continuous_absent_count
-- ,continuous_absent_class_id
-- ,old_class_name
-- ,is_old_class
-- ,is_attendance_exclusion -- 出欠除外フラグ
-- ,art_class_name -- 美術クラス名
-- ,is_use_dormitory -- 寮使用有無
)
SELECT 
person.id as person_id
,KIHON.student_no
/*
,CASE KIHON.course
	WHEN '進学2年' THEN 2
	WHEN '進学1年9ヶ月' THEN 2
	WHEN '進学1年6ヶ月' THEN 2
	WHEN '進学1年3ヶ月' THEN 2
	WHEN '短期6ヶ月日本語研修' THEN 3
	WHEN '短期3ヶ月日本語研修' THEN 3
	WHEN '短期2ヶ月日本語研修' THEN 3
	WHEN '短期1ヶ月日本語研修' THEN 3
	WHEN '一般日本語コース' THEN 4
	WHEN '一般2年' THEN 1
	WHEN '一般1年' THEN 1
	ELSE 0
END as course_id
*/
,CASE KIHON.course
	WHEN '進学2年' THEN 1
	WHEN '進学1年9ヶ月' THEN 2
	WHEN '進学1年6ヶ月' THEN 3
	WHEN '進学1年3ヶ月' THEN 4
	WHEN '短期6ヶ月日本語研修' THEN 6
	WHEN '短期3ヶ月日本語研修' THEN 7
	WHEN '短期2ヶ月日本語研修' THEN 9
	WHEN '短期1ヶ月日本語研修' THEN 8
	WHEN '一般日本語コース' THEN 5
	WHEN '一般2年' THEN 1
	WHEN '一般1年' THEN 5
	ELSE 0
END as course_length_month
,KIHON.start_date
,KIHON.end_date_intended
,CASE KIHON.status_name
	WHEN '予入' THEN 1
	WHEN '不入' THEN 11
	WHEN '在籍' THEN 4
	WHEN '卒業' THEN 5
	WHEN '修了' THEN 6
	WHEN '退学' THEN 7
	WHEN '除籍' THEN 8
	ELSE 3
END as status
,'' as quit_reason_cd
,KIHON.reason_for_exclusion
,CASE
	WHEN LEFT(KIHON.student_no,2)=99 THEN 1
	ELSE 0
END as student_type_cd
,KIHON.after_graduate_school
FROM (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` 
		,`本国氏名1` 
		,`本国氏名2` 
		,`フリガナ名` 
		,`性別` 
		,`未既婚` 
		,`国名` 
		,`生年月日` 
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` 
		,`以前住所住` 
		,`現住所〒` 
		,`現住所住` 
		,`現住所電話` 
		,`携　帯電話` 
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` 
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
LEFT JOIN person ON person.lastup_account_id=KIHON.student_no
WHERE student.student_no IS NULL
;
/*
SELECT * FROM person
INNER JOIN student ON student.student_no=person.lastup_account_id
;

UPDATE person
INNER JOIN student ON student.student_no=person.lastup_account_id
SET person.lastup_account_id=2000
;
*/
-- ------------------------------------------------------------------------------
-- 在校生
-- ------------------------------------------------------------------------------
UPDATE student
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` 
		,`本国氏名1` 
		,`本国氏名2` 
		,`フリガナ名` 
		,`性別` 
		,`未既婚` 
		,`国名` 
		,`生年月日` 
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` as old_class_name
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` 
		,`以前住所住` 
		,`現住所〒` 
		,`現住所住` 
		,`現住所電話` 
		,`携　帯電話` 
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` 
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
-- student.person_id
-- ,student.agent_id
-- ,student.sales_staff_person_id
-- ,student.student_no
/*
student.course_id=	CASE KIHON.course
						WHEN '進学2年' THEN 2
						WHEN '進学1年9ヶ月' THEN 2
						WHEN '進学1年6ヶ月' THEN 2
						WHEN '進学1年3ヶ月' THEN 2
						WHEN '短期6ヶ月日本語研修' THEN 3
						WHEN '短期3ヶ月日本語研修' THEN 3
						WHEN '短期2ヶ月日本語研修' THEN 3
						WHEN '短期1ヶ月日本語研修' THEN 3
						WHEN '一般日本語コース' THEN 4
						WHEN '一般2年' THEN 1
						WHEN '一般1年' THEN 1
						ELSE 0
					END
*/
,student.course_length_month=	CASE KIHON.course
									WHEN '進学2年' THEN 1
									WHEN '進学1年9ヶ月' THEN 2
									WHEN '進学1年6ヶ月' THEN 3
									WHEN '進学1年3ヶ月' THEN 4
									WHEN '短期6ヶ月日本語研修' THEN 6
									WHEN '短期3ヶ月日本語研修' THEN 7
									WHEN '短期2ヶ月日本語研修' THEN 9
									WHEN '短期1ヶ月日本語研修' THEN 8
									WHEN '一般日本語コース' THEN 5
									WHEN '一般2年' THEN 1
									WHEN '一般1年' THEN 5
									ELSE 0
								END
-- ,student.ability_id
-- ,student.start_date_intended
-- ,student.start_date=CASE 
--						WHEN (student.status!=4) THEN KIHON.start_date
--					END
-- ,student.end_date_intended=KIHON.end_date_intended
-- ,student.end_date=	CASE 
--						WHEN (KIHON.end_date1!=00000000) THEN KIHON.end_date1
--						WHEN (KIHON.end_date2!=00000000) THEN KIHON.end_date2
--					END
-- ,student.weekly_study_hours
,student.status=CASE KIHON.status_name
					WHEN '予入' THEN 1
					WHEN '不入' THEN 11
					WHEN '在籍' THEN 4
					WHEN '卒業' THEN 5
					WHEN '修了' THEN 6
					WHEN '退学' THEN 7
					WHEN '除籍' THEN 8
					ELSE 3
				END
-- ,student.final_judgement -- 卒業判定
-- ,student.status_print -- 卒業修了証印刷済みフラグ
-- ,student.quit_reason_cd
,student.reason_for_exclusion=KIHON.reason_for_exclusion
,student.student_type_cd=	CASE -- 学生種別 0:進学 1:短期
								WHEN LEFT(KIHON.student_no,2)=99 THEN 1
								ELSE 0
							END 
,student.after_graduate_school=KIHON.after_graduate_school
-- ,student.general_observation -- 総合所見
-- ,student.general_observation_account_id -- 評価者
-- ,student.general_observation_date -- 評価日
-- ,student.priority_student_part_time_job_id -- 優先アルバイト情報ID
-- ,student.start_preferred_date_intended -- 入学希望年学期 
-- ,student.part_time_job_name
-- ,student.part_time_job_address1
-- ,student.part_time_job_address2
-- ,student.part_time_job_address3
-- ,student.part_time_job_phone_no
-- ,student.continuous_absent_count
-- ,student.continuous_absent_class_id
,student.old_class_name=KIHON.old_class_name
-- ,student.is_old_class
-- ,student.is_attendance_exclusion -- 出欠除外フラグ
-- ,student.art_class_name -- 美術クラス名
-- ,student.is_use_dormitory -- 寮使用有無
,student.lastup_account_id=2000
,student.lastup_datetime=now()
;
-- ------------------------------------------------------------------------------
-- 個別UPDATE
-- ------------------------------------------------------------------------------
UPDATE student
INNER JOIN school_term ON school_term.name=CONCAT('20',SUBSTRING(student.student_no FROM 1 FOR 2),'/',SUBSTRING(student.student_no FROM 3 FOR 2))
SET student.start_preferred_date_intended=school_term.id -- 入学希望年学期 
,student.lastup_account_id=2000
,student.lastup_datetime=now()
;

UPDATE student
INNER JOIN school_term ON school_term.start_date<=student.start_date AND  school_term.end_date>=student.start_date
SET student.start_preferred_date_intended=school_term.id -- 入学希望年学期 
,student.lastup_account_id=2000
,student.lastup_datetime=now()
WHERE start_preferred_date_intended =0
;

UPDATE student
INNER JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
SET student.end_date=system8_1_student_base.`退学年月日` -- 入学希望年学期 
,student.lastup_account_id=2000
,student.lastup_datetime=now()
WHERE system8_1_student_base.`在籍卒業区分` ='退学' 
AND str_to_date(system8_1_student_base.`退学年月日`,'%Y%m%d') < str_to_date('20170711','%Y%m%d')
;

-- SELECT id,student_no,start_date,start_preferred_date_intended FROM student;

-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT
	`学籍番号`
	,`入学日`
	,`卒修予定日`
	,`コース名`
	,`クラス名` 
	,`退学年月日`
	,`除籍年月日`
	,`退学除籍理由`
	,`在籍卒業区分`
	,`卒業後進路先`
FROM student
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN person ON person.id=student.person_id
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id

SELECT
student.student_no
,student.start_date
,student.end_date_intended
,CONCAT(course.name,course_length.name)
,student.old_class_name
,student.end_date
,student.end_date
,student.reason_for_exclusion
,enrollment_status.name
,student.after_graduate_school
FROM student
LEFT JOIN person ON person.id=student.person_id
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='course'
)course ON course.code1=student.course_id
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='course_length'
)course_length ON course_length.code1=student.course_length_month
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='enrollment_status'
)enrollment_status ON enrollment_status.code1=student.status
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id



SELECT
	`学籍番号`
	,`入学日`
	,`卒修予定日`
	,`退学年月日`
	,`除籍年月日`
	,`在籍卒業区分`
FROM student
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN person ON person.id=student.person_id
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id

SELECT
student.student_no
,student.start_date
,student.end_date_intended
,student.end_date
,student.end_date
,enrollment_status.name
FROM student
LEFT JOIN person ON person.id=student.person_id
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='enrollment_status'
)enrollment_status ON enrollment_status.code1=student.status
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id

-- ------------------------------------------------------------------------------------------------------------
-- 2018/04/19update
-- ------------------------------------------------------------------------------------------------------------
UPDATE system_code SET disable=1,lastup_account_id=2000,lastup_datetime=now() WHERE column_name='course'
;
UPDATE system_code_detail
LEFT JOIN system_code ON system_code.id=system_code_detail.system_code_id
SET system_code_detail.disable=1,system_code_detail.lastup_account_id=2000,system_code_detail.lastup_datetime=now()
WHERE system_code.column_name='course'
;

UPDATE student
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`コース名` as course
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
student.course_id=	CASE KIHON.course
						WHEN '進学2年' THEN 1
						WHEN '進学1年9ヶ月' THEN 1
						WHEN '進学1年6ヶ月' THEN 1
						WHEN '進学1年3ヶ月' THEN 1
						WHEN '短期6ヶ月日本語研修' THEN 2
						WHEN '短期3ヶ月日本語研修' THEN 2
						WHEN '短期2ヶ月日本語研修' THEN 2
						WHEN '短期1ヶ月日本語研修' THEN 2
						WHEN '一般日本語コース' THEN 3
						WHEN '一般2年' THEN 4
						WHEN '一般1年' THEN 4
						ELSE 0
					END
,student.lastup_account_id=2000
,student.lastup_datetime=now()
;

UPDATE student
SET course_id=CASE WHEN LEFT(student_no,2)=99 THEN 2 ELSE 1 END
,lastup_account_id=2000
,lastup_datetime=now()
WHERE disable=0 AND status=4 AND course_id=0
;
-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT
	`学籍番号` as student_no
	,`本国氏名1`
	,`フリガナ名`
	,`本国氏名2`
	,`コース名` as course
FROM system8_1_student_base
ORDER BY student_no
;

SELECT 
	student_no
	,CONCAT(last_name,first_name)
	,CONCAT(last_name_kana,first_name_kana)
	,CONCAT(last_name_alphabet,first_name_alphabet)
	,course_id
	,course.name
	,course_length.name
FROM student
LEFT JOIN person ON person.id=student.person_id AND person.disable=0
LEFT JOIN course ON course.id=student.course_id AND course.disable=0
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='course_length'
)course_length ON course_length.code1=student.course_length_month
WHERE student.disable=0
ORDER BY student_no
;
