SELECT * FROM class_student WHERE school_term_id=0
;

UPDATE class_student
LEFT JOIN class on class.id = class_student.class_id AND class.disable=0
LEFT JOIN class_group on class_group.id = class.class_group_id AND class_group.disable=0
SET class_student.school_term_id=class_group.school_term_id
WHERE class_student.disable=0 AND class_student.school_term_id=0
;

TRUNCATE TABLE achievement_detail;
INSERT INTO achievement_detail(
achievement_id
,test_section_cd
,score
)
SELECT
achievement.id
,test_section_cd
,TEEL.score
FROM achievement
LEFT JOIN student ON student.person_id=achievement.person_id
LEFT JOIN class_student ON class_student.person_id=student.person_id AND class_student.class_id=achievement.class_id AND class_student.disable=0
LEFT JOIN content_test_list ON content_test_list.id=achievement.content_test_list_id
LEFT JOIN (
    SELECT
        excel_2_term_end_exam_list.school_term_id
        ,excel_2_term_end_exam_list.`学籍番号` as student_no
        ,end_exam_list_test_section.code2 as test_id
        ,end_exam_list_test_section.code1 as test_section_cd
        ,CASE end_exam_list_test_section.name
            WHEN '文字語彙' THEN excel_2_term_end_exam_list.`文字語彙`
            WHEN '文法' THEN excel_2_term_end_exam_list.`文法`
            WHEN '文字語彙文法' THEN excel_2_term_end_exam_list.`文字語彙文法`
            WHEN '読解' THEN excel_2_term_end_exam_list.`読解`
            WHEN '聴解' THEN excel_2_term_end_exam_list.`聴解`
        END as score
    FROM excel_2_term_end_exam_list 
    CROSS JOIN (
        SELECT
            system_code_detail.code1
            ,system_code_detail.code2
            ,system_code_detail.name
        FROM system_code
        INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
        WHERE system_code.disable=0
        AND system_code_detail.disable=0 
        AND system_code.column_name='test_section'
        AND system_code_detail.code2 IN (2,5)
    )end_exam_list_test_section
    ORDER BY excel_2_term_end_exam_list.school_term_id,excel_2_term_end_exam_list.`学籍番号`
)TEEL ON TEEL.school_term_id=class_student.school_term_id AND TEEL.student_no=student.student_no AND TEEL.test_id=content_test_list.test_id
WHERE score!=''
ORDER BY TEEL.school_term_id,TEEL.student_no,test_section_cd
;

-- --------------------------------------------------------------------------------------------------------------------------------------------------
-- add 2017/07 2017/10 2018/01
-- --------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO achievement_detail(
achievement_id
,test_section_cd
,score
)
SELECT
achievement.id
,test_section_cd
,TEEL.score
FROM achievement
LEFT JOIN student ON student.person_id=achievement.person_id
LEFT JOIN class_student ON class_student.person_id=student.person_id AND class_student.class_id=achievement.class_id AND class_student.disable=0
LEFT JOIN content_test_list ON content_test_list.id=achievement.content_test_list_id
LEFT JOIN (
    SELECT
        excel_2_term_end_exam_list.school_term_id
        ,excel_2_term_end_exam_list.`学籍番号` as student_no
        ,end_exam_list_test_section.code2 as test_id
        ,end_exam_list_test_section.code1 as test_section_cd
        ,CASE end_exam_list_test_section.name
            WHEN '文字語彙' THEN excel_2_term_end_exam_list.`文字語彙`
            WHEN '文法' THEN excel_2_term_end_exam_list.`文法`
            WHEN '文字語彙文法' THEN excel_2_term_end_exam_list.`文字語彙文法`
            WHEN '読解' THEN excel_2_term_end_exam_list.`読解`
            WHEN '聴解' THEN excel_2_term_end_exam_list.`聴解`
        END as score
    FROM excel_2_term_end_exam_list 
    CROSS JOIN (
        SELECT
            system_code_detail.code1
            ,system_code_detail.code2
            ,system_code_detail.name
        FROM system_code
        INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
        WHERE system_code.disable=0
        AND system_code_detail.disable=0 
        AND system_code.column_name='test_section'
        AND system_code_detail.code2 IN (2,5)
    )end_exam_list_test_section
    ORDER BY excel_2_term_end_exam_list.school_term_id,excel_2_term_end_exam_list.`学籍番号`
)TEEL ON TEEL.school_term_id=class_student.school_term_id AND TEEL.student_no=student.student_no AND TEEL.test_id=content_test_list.test_id
WHERE score!='' AND TEEL.school_term_id IN (130,131,132)
ORDER BY TEEL.school_term_id,TEEL.student_no,test_section_cd
;