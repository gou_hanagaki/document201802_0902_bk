INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`)
VALUES 
('成績', 'school_record_grade', 2000, NOW());
SET @id = (SELECT id FROM system_code WHERE column_name='school_record_grade');
INSERT INTO system_code_detail (`system_code_id`, `code1`,`sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) 
VALUES
(@id, 4,1, 'A', 'A', 2000, NOW()),
(@id, 3,2, 'B', 'B', 2000, NOW()),
(@id, 2,3, 'C', 'C', 2000, NOW()),
(@id, 1,4, 'D', 'D', 2000, NOW()),
(@id, 0,5, 'F', 'F', 2000, NOW())
;

TRUNCATE TABLE school_record_detail;
INSERT INTO school_record_detail(
school_record_id
,school_record_section_id
,grade
)
SELECT
school_record.id
,school_record_section_id
,system_code_detail.code1 as grade
FROM school_record
INNER JOIN student ON student.person_id=school_record.person_id
INNER JOIN excel_1_school_record ON excel_1_school_record.`学籍番号`=student.student_no
LEFT JOIN (
    SELECT
        excel_1_school_record.school_term_id
        ,excel_1_school_record.`学籍番号` as student_no
        ,school_record_section.id as school_record_section_id
        ,CASE school_record_section.name
            WHEN '文字語彙' then excel_1_school_record.`文字語彙`
            WHEN '文法' then excel_1_school_record.`文法`
            WHEN '読解' then excel_1_school_record.`読解`
            WHEN '聴解' then excel_1_school_record.`聴解`
            WHEN '会話' then excel_1_school_record.`会話`
            WHEN '作文' then excel_1_school_record.`作文`
            WHEN '平常点' then excel_1_school_record.`平常点`
            WHEN '総合' then excel_1_school_record.`総合`
        END as school_record_section_name
    FROM excel_1_school_record 
    CROSS JOIN school_record_section 
)SRS ON SRS.school_term_id=excel_1_school_record.school_term_id AND SRS.student_no=student.student_no
LEFT JOIN system_code_detail ON system_code_detail.name=school_record_section_name
LEFT JOIN system_code ON system_code.id=system_code_detail.system_code_id
WHERE system_code.column_name='school_record_grade'
ORDER BY school_record.id,school_record_section_id
;