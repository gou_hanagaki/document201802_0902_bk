UPDATE class SET name='大学院A' WHERE id='88';
UPDATE class SET name='大学A' WHERE id='89';
UPDATE class SET name='大学B' WHERE id='90';
UPDATE class SET name='進学準備' WHERE id='91';
UPDATE class SET name='美大' WHERE id='92';
UPDATE class SET name='EJU' WHERE id='93';

TRUNCATE TABLE school_record;
INSERT INTO school_record(
school_term_id
,person_id
,observation_internal
,observation_internal_account_id
,lastup_account_id
,create_datetime
,lastup_datetime
)
SELECT
excel_1_school_record.school_term_id
,student.person_id
,excel_1_school_record.`所見`
,teacher_person_id
,teacher_person_id as lastup_account_id
,NOW() as create_datetime
,TPI.end_date as lastup_datetime
FROM excel_1_school_record
INNER JOIN student ON student.student_no=excel_1_school_record.`学籍番号`
LEFT JOIN (
	SELECT
		school_term.id as school_term_id
		,class.id as class_id
		,class.name as class_name
		,class.teacher_person_id as teacher_person_id
		,school_term.end_date
	FROM school_term
	INNER JOIN class_group ON class_group.school_term_id = school_term.id
	INNER JOIN class ON class.class_group_id = class_group.id
	INNER JOIN class_student ON class_student.class_id = class.id
	GROUP BY school_term.id,class.id,class.name,class.teacher_person_id
)TPI ON TPI.school_term_id=excel_1_school_record.school_term_id AND TPI.class_name=excel_1_school_record.`クラス`
;

-- ----------------------------------------------------------------------------------------------------------------
-- add 2018/01
-- ----------------------------------------------------------------------------------------------------------------
UPDATE class SET name='大学3美術' WHERE id='212';	

INSERT INTO school_record(
school_term_id
,person_id
,observation_internal
,observation_internal_account_id
,lastup_account_id
,create_datetime
,lastup_datetime
)
SELECT
excel_1_school_record.school_term_id
,student.person_id
,excel_1_school_record.`所見`
,teacher_person_id
,teacher_person_id as lastup_account_id
,NOW() as create_datetime
,TPI.end_date as lastup_datetime
FROM excel_1_school_record
INNER JOIN student ON student.student_no=excel_1_school_record.`学籍番号`
LEFT JOIN (
	SELECT
		school_term.id as school_term_id
		,class.id as class_id
		,class.name as class_name
		,class.teacher_person_id as teacher_person_id
		,school_term.end_date
	FROM school_term
	INNER JOIN class_group ON class_group.school_term_id = school_term.id
	INNER JOIN class ON class.class_group_id = class_group.id
	INNER JOIN class_student ON class_student.class_id = class.id
	GROUP BY school_term.id,class.id,class.name,class.teacher_person_id
)TPI ON TPI.school_term_id=excel_1_school_record.school_term_id AND TPI.class_name=excel_1_school_record.`クラス`
WHERE excel_1_school_record.school_term_id=132
;

/*
TRUNCATE TABLE school_record;
INSERT INTO school_record(
school_term_id
,person_id
,observation_internal
,observation_internal_account_id
,lastup_account_id
,create_datetime
,lastup_datetime
)
SELECT
excel_1_school_record.school_term_id
,student.person_id
,excel_1_school_record.`所見`
,CASE 
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学院A' THEN 1824
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学A' THEN 1822
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='美大' THEN 1872
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学B' THEN 1859
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='進学準備' THEN 1820
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='EJU' THEN 1821
	ELSE teacher_person_id
END as teacher_person_id
,CASE 
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学院A' THEN 1824
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学A' THEN 1822
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='美大' THEN 1872
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='大学B' THEN 1859
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='進学準備' THEN 1820
	WHEN excel_1_school_record.school_term_id=128 AND excel_1_school_record.`クラス`='EJU' THEN 1821
	ELSE teacher_person_id
END as lastup_account_id
,NOW()
,CASE 
	WHEN TPI.end_date IS NULL THEN '2017-03-31'
	ELSE TPI.end_date
END
FROM excel_1_school_record
INNER JOIN student ON student.student_no=excel_1_school_record.`学籍番号`
LEFT JOIN (
	SELECT
		school_term.id as school_term_id
		,class.id as class_id
		,class.name as class_name
		,class.teacher_person_id as teacher_person_id
		,school_term.end_date
	FROM school_term
	INNER JOIN class_group ON class_group.school_term_id = school_term.id
	INNER JOIN class ON class.class_group_id = class_group.id
	INNER JOIN class_student ON class_student.class_id = class.id
	GROUP BY school_term.id,class.id,class.name,class.teacher_person_id
)TPI ON TPI.school_term_id=excel_1_school_record.school_term_id AND TPI.class_name=excel_1_school_record.`クラス`
;
*/