-- --------------------------------------------------------
-- 本番DBを取得して作業用tableの作成
-- --------------------------------------------------------
■本番データをdump
mysqldump -h rds.midream.local -u midream -p midream --default-character-set=utf8 --single-transaction --ignore-table=midream.tablet_application_update --ignore-table=midream.logs > 201802141530_midream_real.sql

mysql -u midream -p midream_work < 201802141530_midream_real.sql

-- --------------------------------------------------------
-- 作業用tableの作成
-- --------------------------------------------------------
■初期化
TRUNCATE TABLE system8_1_student_base;
TRUNCATE TABLE system8_2_student_other;
TRUNCATE TABLE system8_3_immigration;

■DROP CREATE
DROP TABLE IF EXISTS system8_1_student_base;
DROP TABLE IF EXISTS system8_2_student_other;
DROP TABLE IF EXISTS system8_3_immigration;
DROP TABLE IF EXISTS system8_4_attendance_rate;

CREATE TABLE `system8_1_student_base` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`学籍番号` text NOT NULL,
`登録番号` text NOT NULL,
`本国氏名書体` text NOT NULL,
`本国氏名1` text NOT NULL,
`本国氏名2` text NOT NULL,
`フリガナ名` text NOT NULL,
`性別` text NOT NULL,
`未既婚` text NOT NULL,
`国名` text NOT NULL,
`生年月日` text NOT NULL,
`紹介者名` text NOT NULL,
`本国住所書体` text NOT NULL,
`本国住所` text NOT NULL,
`本国電話` text NOT NULL,
`旅券番号` text NOT NULL,
`旅券有効期限` text NOT NULL,
`入国年月日` text NOT NULL,
`生VISA種類` text NOT NULL,
`VISA更新日(1)` text NOT NULL,
`VISA更新日(2)` text NOT NULL,
`VISA更新日(3)` text NOT NULL,
`VISA更新日(4)` text NOT NULL,
`入管許可番号` text NOT NULL,
`入管許可日` text NOT NULL,
`在留カード番号` text NOT NULL,
`入学日` text NOT NULL,
`卒修予定日` text NOT NULL,
`コース名` text NOT NULL,
`クラス名` text NOT NULL,
`退学年月日` text NOT NULL,
`除籍年月日` text NOT NULL,
`退学除籍理由` text NOT NULL,
`在籍卒業区分` text NOT NULL,
`以前住所〒` text NOT NULL,
`以前住所住` text NOT NULL,
`現住所〒` text NOT NULL,
`現住所住` text NOT NULL,
`現住所電話` text NOT NULL,
`携　帯電話` text NOT NULL,
`国民健康保険番号` text NOT NULL,
`障害保険有無` text NOT NULL,
`母国語` text NOT NULL,
`卒業後進路先` text NOT NULL,
`紹介者書体` text NOT NULL,
`紹介者名1` text NOT NULL,
`紹介者名2` text NOT NULL,
`紹介者住所1` text NOT NULL,
`紹介者住所2` text NOT NULL,
`紹介者TEL` text NOT NULL,
`紹介者FAX` text NOT NULL,
`J   ﾃｽﾄ` text NOT NULL,
`NAT ﾃｽﾄ` text NOT NULL,
`入学ﾃｽﾄ` text NOT NULL,
`組分ﾃｽﾄ` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `system8_2_student_other` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`学籍番号` text NOT NULL,
`登録番号` text NOT NULL,
`本国氏名書体` text NOT NULL,
`本国氏名1` text NOT NULL,
`本国氏名2` text NOT NULL,
`フリガナ名` text NOT NULL,
`性別` text NOT NULL,
`未既婚` text NOT NULL,
`国名` text NOT NULL,
`生年月日` text NOT NULL,
`紹介者名` text NOT NULL,
`保護者名書体` text NOT NULL,
`保護者名` text NOT NULL,
`保護者住所書体` text NOT NULL,
`保護者住所` text NOT NULL,
`保護者電話` text NOT NULL,
`保護者続柄` text NOT NULL,
`保護者職業` text NOT NULL,
`本人最終学歴` text NOT NULL,
`本人最終卒業日` text NOT NULL,
`本人最終修学年数` text NOT NULL,
`本人最終職業` text NOT NULL,
`本人最終会社名` text NOT NULL,
`修学予定年数` text NOT NULL,
`卒業後予定` text NOT NULL,
`希望進路先(1)` text NOT NULL,
`希望進路先(2)` text NOT NULL,
`希望進路先(3)` text NOT NULL,
`確認年月日1` text NOT NULL,
`職場名1` text NOT NULL,
`職場場所1` text NOT NULL,
`職場電話1` text NOT NULL,
`職種1` text NOT NULL,
`時給1` text NOT NULL,
`勤務時間1` text NOT NULL,
`許可1` text NOT NULL,
`確認年月日2` text NOT NULL,
`職場名2` text NOT NULL,
`職場場所2` text NOT NULL,
`職場電話2` text NOT NULL,
`職種2` text NOT NULL,
`時給2` text NOT NULL,
`勤務時間2` text NOT NULL,
`許可2` text NOT NULL,
`確認年月日3` text NOT NULL,
`職場名3` text NOT NULL,
`職場場所3` text NOT NULL,
`職場電話3` text NOT NULL,
`職種3` text NOT NULL,
`時給3` text NOT NULL,
`勤務時間3` text NOT NULL,
`許可3` text NOT NULL,
`確認年月日4` text NOT NULL,
`職場名4` text NOT NULL,
`職場場所4` text NOT NULL,
`職場電話4` text NOT NULL,
`職種4` text NOT NULL,
`時給4` text NOT NULL,
`勤務時間4` text NOT NULL,
`許可4` text NOT NULL,
`確認年月日5` text NOT NULL,
`職場名5` text NOT NULL,
`職場場所5` text NOT NULL,
`職場電話5` text NOT NULL,
`職種5` text NOT NULL,
`時給5` text NOT NULL,
`勤務時間5` text NOT NULL,
`許可5` text NOT NULL,
`確認年月日6` text NOT NULL,
`職場名6` text NOT NULL,
`職場場所6` text NOT NULL,
`職場電話6` text NOT NULL,
`職種6` text NOT NULL,
`時給6` text NOT NULL,
`勤務時間6` text NOT NULL,
`許可6` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `system8_3_immigration` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`学籍番号` text NOT NULL,
`登録番号` text NOT NULL,
`本国氏名書体` text NOT NULL,
`本国氏名1` text NOT NULL,
`本国氏名2` text NOT NULL,
`フリガナ名` text NOT NULL,
`性別` text NOT NULL,
`未既婚` text NOT NULL,
`国名` text NOT NULL,
`生年月日` text NOT NULL,
`紹介者名` text NOT NULL,
`出生地書体` text NOT NULL,
`出生地` text NOT NULL,
`本国書体` text NOT NULL,
`本国居住地` text NOT NULL,
`査証申請予定地書体` text NOT NULL,
`査証申請予定地` text NOT NULL,
`過去出入国歴有無` text NOT NULL,
`過去出入国回数` text NOT NULL,
`直近出入国年月日〜` text NOT NULL,
`〜直近出入国年月日` text NOT NULL,
`卒業までの所要年数` text NOT NULL,
`最終学歴状況（在・卒・休）` text NOT NULL,
`最終学歴状況（学校種別）` text NOT NULL,
`学校名書体` text NOT NULL,
`学校名` text NOT NULL,
`卒業・見込年月日` text NOT NULL,
`日本語能力試験（級）` text NOT NULL,
`日本語能力留学試験得点` text NOT NULL,
`日本語能力Ｊテスト結果` text NOT NULL,
`日本語教育機関書体` text NOT NULL,
`日本語教育機関名` text NOT NULL,
`日本語教育年月日〜` text NOT NULL,
`〜日本語教育年月日` text NOT NULL,
`支弁方法＿本人負担額` text NOT NULL,
`支弁方法＿外国から送金額` text NOT NULL,
`支弁方法＿外国から携行額` text NOT NULL,
`支弁方法＿携行者名書体` text NOT NULL,
`支弁方法＿携行者` text NOT NULL,
`支弁方法＿携行時期` text NOT NULL,
`支弁方法＿在日支弁負担額` text NOT NULL,
`支弁方法＿奨学金` text NOT NULL,
`支弁方法＿その他` text NOT NULL,
`支弁方法＿欄外入学金他前払` text NOT NULL,
`支弁者名1書体` text NOT NULL,
`支弁者名1` text NOT NULL,
`支弁者住所1書体` text NOT NULL,
`支弁者住所1` text NOT NULL,
`支弁者電話1` text NOT NULL,
`支弁者勤務先1書体` text NOT NULL,
`支弁者勤務先1` text NOT NULL,
`支弁者勤務先電話1` text NOT NULL,
`支弁者年収1` text NOT NULL,
`支弁者申請者との関係1` text NOT NULL,
`支弁者名2書体` text NOT NULL,
`支弁者名2` text NOT NULL,
`支弁者住所2書体` text NOT NULL,
`支弁者住所2` text NOT NULL,
`支弁者電話2` text NOT NULL,
`支弁者勤務先2書体` text NOT NULL,
`支弁者勤務先2` text NOT NULL,
`支弁者勤務先2電話` text NOT NULL,
`支弁者年収2` text NOT NULL,
`支弁者申請者との関係2` text NOT NULL,
`職業` text NOT NULL,
`旅券番号` text NOT NULL,
`旅券＿有効期限` text NOT NULL,
`修学年数` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
-- システムエイトCSVからテーブルを作成
-- --------------------------------------------------------
mysql -u midream -p midream_work --local-infile=1

LOAD DATA LOCAL INFILE '/home/wakka-dev/system8/system8_1_student_base.csv' INTO TABLE midream_work.system8_1_student_base FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/system8/system8_2_student_other.csv' INTO TABLE midream_work.system8_2_student_other FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

LOAD DATA LOCAL INFILE '/home/wakka-dev/system8/system8_3_immigration.csv' INTO TABLE midream_work.system8_3_immigration FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

-- --------------------------------------------------------
-- 不要データを除外
-- --------------------------------------------------------
DELETE FROM system8_1_student_base WHERE `学籍番号`='学籍番号' AND `登録番号`='登録番号';
DELETE FROM system8_2_student_other WHERE `学籍番号`='学籍番号' AND `登録番号`='登録番号';
DELETE FROM system8_3_immigration WHERE `学籍番号`='学籍番号' AND `登録番号`='登録番号';

-- 2018/02/22確認済み
DELETE FROM system8_1_student_base WHERE `学籍番号`='1804148' AND `登録番号`='0741';
DELETE FROM system8_2_student_other WHERE `学籍番号`='1804148' AND `登録番号`='0741';
DELETE FROM system8_3_immigration WHERE `学籍番号`='1804148' AND `登録番号`='0741';

DELETE FROM system8_1_student_base WHERE `学籍番号`='1804149' AND `登録番号`='0742';
DELETE FROM system8_2_student_other WHERE `学籍番号`='1804149' AND `登録番号`='0742';
DELETE FROM system8_3_immigration WHERE `学籍番号`='1804149' AND `登録番号`='0742';

-- --------------------------------------------------------
-- trim
-- --------------------------------------------------------
UPDATE system8_1_student_base SET `退学除籍理由`=TRIM(`退学除籍理由`);
UPDATE system8_1_student_base SET `退学除籍理由`=TRIM(TRAILING '　' FROM `退学除籍理由`);
UPDATE system8_1_student_base SET `退学除籍理由`=TRIM(LEADING '　' FROM `退学除籍理由`);

