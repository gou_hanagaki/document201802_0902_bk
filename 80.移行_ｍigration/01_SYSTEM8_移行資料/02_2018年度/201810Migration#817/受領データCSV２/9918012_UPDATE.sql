UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日`
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
-- person.type
person.country_id = '18'
,person.native_language=KIHON.native_language
-- ,person.parent_person_id
,person.birthday=KIHON.`生年月日`
,person.sex_cd=	CASE KIHON.sex
					WHEN '男' THEN 1
					WHEN '女' THEN 2
					ELSE 0
				END
,person.married_cd=	CASE KIHON.married
						WHEN '未婚' THEN 1
						WHEN '既婚' THEN 2
						ELSE 0
					END
,person.zipcode=KIHON.zipcode
,person.address_country_id='156' -- 156:日本
,person.address1=KIHON.address1
-- ,person.address2
-- ,person.address3
,person.phone_no= 
CASE KIHON.phone_no
	WHEN '無' THEN ''
	WHEN 'なし' THEN ''
	ELSE KIHON.phone_no
END
-- ,person.phone_no2 
 ,person.mobile_no=
CASE KIHON.mobile_no
	WHEN '無' THEN ''
	WHEN 'なし' THEN ''
	ELSE KIHON.mobile_no
END
,person.previous_address_zipcode=KIHON.previous_address_zipcode
,person.previous_address1=KIHON.previous_address1
,person.last_name = SUBSTRING_INDEX(KIHON.name, '　', 1)
,person.first_name = TRIM(LEADING '　' FROM REPLACE(KIHON.name,SUBSTRING_INDEX(KIHON.name, '　', 1),''))
,person.last_name_kana = SUBSTRING_INDEX(KIHON.name_kana, '　', 1)
,person.first_name_kana = TRIM(LEADING '　' FROM REPLACE(KIHON.name_kana,SUBSTRING_INDEX(KIHON.name_kana, '　', 1),''))
,person.last_name_alphabet = SUBSTRING_INDEX(KIHON.name_en, '　', 1)
,person.first_name_alphabet = TRIM(LEADING '　' FROM REPLACE(KIHON.name_en,SUBSTRING_INDEX(KIHON.name_en, '　', 1),''))
-- ,person.previous_address2
-- ,person.previous_address3
-- ,person.mail_address1
-- ,person.mail_address2
-- ,person.last_name -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
,person.name_font=KIHON.name_font
-- ,person.last_name_kana -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_kana -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.last_name_alphabet -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_alphabet -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.legal_registered_last_name -- 戸籍
-- ,person.legal_registered_first_name -- 戸籍
-- ,person.legal_registered_last_name_kana -- 戸籍
-- ,person.legal_registered_first_name_kana -- 戸籍
-- ,person.legal_registered_last_name_alphabet -- 戸籍
-- ,person.legal_registered_first_name_alphabet -- 戸籍
-- ,person.abbreviation_name -- 表示用
-- ,person.last_name_native -- 姓_母国語
-- ,person.first_name_native -- 名_母国語
,person.lastup_account_id=KIHON.student_no
,person.lastup_datetime=now()
WHERE student.student_no = '9918012'
;



UPDATE student
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` 
		,`本国氏名1` 
		,`本国氏名2` 
		,`フリガナ名` 
		,`性別` 
		,`未既婚` 
		,`国名` 
		,`生年月日` 
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` as old_class_name
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` 
		,`以前住所住` 
		,`現住所〒` 
		,`現住所住` 
		,`現住所電話` 
		,`携　帯電話` 
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` 
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
	WHERE `学籍番号` = '9918012'
)KIHON ON KIHON.student_no=student.student_no
SET 
-- student.person_id
-- ,student.agent_id
-- ,student.sales_staff_person_id
-- ,student.student_no

student.course_id=	CASE KIHON.course
					WHEN '進学2年' THEN 1
					WHEN '進学1年9ヶ月' THEN 1
					WHEN '進学1年6ヶ月' THEN 1
					WHEN '進学1年3ヶ月' THEN 1
					WHEN '短期6ヶ月日本語研修' THEN 2
					WHEN '短期3ヶ月日本語研修' THEN 2
					WHEN '短期2ヶ月日本語研修' THEN 2
					WHEN '短期1ヶ月日本語研修' THEN 2
					WHEN '一般日本語コース' THEN 3
					WHEN '一般2年' THEN 4
					WHEN '一般1年' THEN 4
					ELSE 0
			END
,student.course_length_month=	CASE KIHON.course
					WHEN '進学2年' THEN 1
					WHEN '進学1年9ヶ月' THEN 2
					WHEN '進学1年6ヶ月' THEN 3
					WHEN '進学1年3ヶ月' THEN 4
					WHEN '短期6ヶ月日本語研修' THEN 6
					WHEN '短期3ヶ月日本語研修' THEN 7
					WHEN '短期2ヶ月日本語研修' THEN 9
					WHEN '短期1ヶ月日本語研修' THEN 8
					WHEN '一般日本語コース' THEN 5
					WHEN '一般2年' THEN 1
					WHEN '一般1年' THEN 5
					ELSE 0
				END
-- ,student.ability_id
-- ,student.start_date_intended
,student.start_date= '2018-10-10'
-- CASE 
--						WHEN (student.status!=4) THEN KIHON.start_date
--					END
,student.end_date_intended=KIHON.end_date_intended
,student.end_date= '0000-00-00'
--	CASE 
--						WHEN (KIHON.end_date1!=00000000) THEN KIHON.end_date1
--						WHEN (KIHON.end_date2!=00000000) THEN KIHON.end_date2
--			END
-- ,student.weekly_study_hours
,student.status=CASE KIHON.status_name
					WHEN '予入' THEN 1
					WHEN '不入' THEN 11
					WHEN '在籍' THEN 4
					WHEN '卒業' THEN 5
					WHEN '修了' THEN 6
					WHEN '退学' THEN 7
					WHEN '除籍' THEN 8
					ELSE 3
				END
-- ,student.final_judgement -- 卒業判定
-- ,student.status_print -- 卒業修了証印刷済みフラグ
-- ,student.quit_reason_cd
,student.reason_for_exclusion=KIHON.reason_for_exclusion
,student.student_type_cd=	CASE -- 学生種別 0:進学 1:短期
								WHEN LEFT(KIHON.student_no,2)=99 THEN 1
								ELSE 0
							END 
,student.after_graduate_school=KIHON.after_graduate_school
-- ,student.general_observation -- 総合所見
-- ,student.general_observation_account_id -- 評価者
-- ,student.general_observation_date -- 評価日
-- ,student.priority_student_part_time_job_id -- 優先アルバイト情報ID
,student.start_preferred_date_intended = '135' -- 入学希望年学期 
-- ,student.part_time_job_name
-- ,student.part_time_job_address1
-- ,student.part_time_job_address2
-- ,student.part_time_job_address3
-- ,student.part_time_job_phone_no
-- ,student.continuous_absent_count
-- ,student.continuous_absent_class_id
-- ,student.old_class_name=KIHON.old_class_name
-- ,student.is_old_class
-- ,student.is_attendance_exclusion -- 出欠除外フラグ
-- ,student.art_class_name -- 美術クラス名
-- ,student.is_use_dormitory -- 寮使用有無
,student.lastup_account_id= KIHON.student_no
,student.lastup_datetime=now()
WHERE student.student_no = '9918012'
;