INSERT INTO person(
type
-- ,country_id
,native_language
-- ,parent_person_id
,birthday
,sex_cd
,married_cd
,zipcode
,address_country_id
,address1
-- ,address2
-- ,address3
,phone_no
-- ,phone_no2
,mobile_no
,previous_address_zipcode
,previous_address1
-- ,previous_address2
-- ,previous_address3
-- ,mail_address1
-- ,mail_address2
,last_name
,first_name
,name_font
,last_name_kana
,first_name_kana
,last_name_alphabet
,first_name_alphabet
-- ,legal_registered_last_name
-- ,legal_registered_first_name
-- ,legal_registered_last_name_kana
-- ,legal_registered_first_name_kana
-- ,legal_registered_last_name_alphabet
-- ,legal_registered_first_name_alphabet
-- ,abbreviation_name
-- ,last_name_native -- ©_êê
-- ,first_name_native -- ¼_êê
,lastup_account_id
)
SELECT 
1 as type -- 1:w¶
,KIHON.native_language
,KIHON.birthday
,CASE KIHON.sex
	WHEN 'j' THEN 1
	WHEN '' THEN 2
	ELSE 0
END as sex_cd
,CASE KIHON.married
	WHEN '¢¥' THEN 1
	WHEN 'ù¥' THEN 2
	ELSE 0
END as married_cd
,KIHON.zipcode
,'156' as address_country_id -- 156:ú{
,KIHON.address1
,CASE KIHON.phone_no
	WHEN '³' THEN ''
	WHEN 'Èµ' THEN ''
	ELSE KIHON.phone_no
END as phone_no
,CASE KIHON.mobile_no
	WHEN '³' THEN ''
	WHEN 'Èµ' THEN ''
	ELSE KIHON.mobile_no
END as mobile_no
,KIHON.previous_address_zipcode
,KIHON.previous_address1
,SUBSTRING_INDEX(KIHON.name, '@', 1) as last_name
,TRIM(LEADING '@' FROM REPLACE(KIHON.name,SUBSTRING_INDEX(KIHON.name, '@', 1),'')) as first_name
,KIHON.name_font
,SUBSTRING_INDEX(KIHON.name_kana, '@', 1) as last_name_kana
,TRIM(LEADING '@' FROM REPLACE(KIHON.name_kana,SUBSTRING_INDEX(KIHON.name_kana, '@', 1),'')) as first_name_kana
,SUBSTRING_INDEX(KIHON.name_en, '@', 1) as last_name_alphabet
,TRIM(LEADING '@' FROM REPLACE(KIHON.name_en,SUBSTRING_INDEX(KIHON.name_en, '@', 1),'')) as first_name_alphabet
,KIHON.student_no as lastup_account_id
FROM (
	SELECT
		`wÐÔ` as student_no
		,`o^Ô` 
		,`{¼Ì` as name_font
		,`{¼1` as name
		,`{¼2` as name_en
		,`tKi¼` as name_kana
		,`«Ê` as sex
		,`¢ù¥` as married
		,`¼` 
		,`¶Nú` as birthday
		,`ÐîÒ¼` 
		,`{ZÌ` 
		,`{Z` 
		,`{db` 
		,`·Ô` 
		,`·LøúÀ` 
		,`üNú` 
		,`¶VISAíÞ` 
		,`VISAXVú(1)` 
		,`VISAXVú(2)` 
		,`VISAXVú(3)` 
		,`VISAXVú(4)` 
		,`üÇÂÔ` 
		,`üÇÂú` 
		,`Ý¯J[hÔ` 
		,`üwú` as start_date
		,`²C\èú` as end_date_intended
		,`R[X¼` as course
		,`NX¼` 
		,`ÞwNú` as end_date1
		,`ÐNú` as end_date2
		,`ÞwÐR` as reason_for_exclusion
		,`ÝÐ²Ææª` as status_name
		,`ÈOZ§` as previous_address_zipcode
		,`ÈOZZ` as previous_address1
		,`»Z§` as zipcode
		,`»ZZ` as address1
		,`»Zdb` as phone_no
		,`g@Ñdb` as mobile_no
		,`¯NÛ¯Ô` 
		,`áQÛ¯L³` 
		,`êê` as native_language
		,`²ÆãiHæ` as after_graduate_school
		,`ÐîÒÌ` 
		,`ÐîÒ¼1` 
		,`ÐîÒ¼2` 
		,`ÐîÒZ1` 
		,`ÐîÒZ2` 
		,`ÐîÒTEL` 
		,`ÐîÒFAX` 
		,`J   Ã½Ä` 
		,`NAT Ã½Ä` 
		,`üwÃ½Ä` 
		,`gªÃ½Ä` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
WHERE student.student_no IS NULL
;





-- Vü¶
INSERT INTO student(
person_id
-- ,agent_id
-- ,sales_staff_person_id
,student_no
,course_id
,course_length_month
,student_type_cd -- w¶íÊ 0:iw 1:Zú
-- ,ability_id
-- ,start_date_intended
,start_date
,end_date_intended
-- ,end_date
-- ,weekly_study_hours
,status
-- ,final_judgement -- ²Æ»è
-- ,status_print -- ²ÆC¹ØóüÏÝtO
,quit_reason_cd
,reason_for_exclusion
,after_graduate_school
-- ,general_observation -- ©
-- ,general_observation_account_id -- ]¿Ò
-- ,general_observation_date -- ]¿ú
-- ,priority_student_part_time_job_id -- DæAoCgîñID
-- ,start_preferred_date_intended
-- ,part_time_job_name
-- ,part_time_job_address1
-- ,part_time_job_address2
-- ,part_time_job_address3
-- ,part_time_job_phone_no
-- ,continuous_absent_count
-- ,continuous_absent_class_id
-- ,old_class_name
-- ,is_old_class
-- ,is_attendance_exclusion -- oOtO
-- ,art_class_name -- üpNX¼
-- ,is_use_dormitory -- ¾gpL³
)
SELECT 
person.id as person_id
,KIHON.student_no

,CASE KIHON.course
	WHEN 'iw2N' THEN 1
	WHEN 'iw1N9' THEN 1
	WHEN 'iw1N6' THEN 1
	WHEN 'iw1N3' THEN 1
	WHEN 'Zú6ú{ê¤C' THEN 2
	WHEN 'Zú3ú{ê¤C' THEN 2
	WHEN 'Zú2ú{ê¤C' THEN 2
	WHEN 'Zú1ú{ê¤C' THEN 2
	WHEN 'êÊú{êR[X' THEN 3
	WHEN 'êÊ2N' THEN 4
	WHEN 'êÊ1N' THEN 4
	ELSE 0
END as course_id

,CASE KIHON.course
	WHEN 'iw2N' THEN 1
	WHEN 'iw1N9' THEN 2
	WHEN 'iw1N6' THEN 3
	WHEN 'iw1N3' THEN 4
	WHEN 'Zú6ú{ê¤C' THEN 6
	WHEN 'Zú3ú{ê¤C' THEN 7
	WHEN 'Zú2ú{ê¤C' THEN 9
	WHEN 'Zú1ú{ê¤C' THEN 8
	WHEN 'êÊú{êR[X' THEN 5
	WHEN 'êÊ2N' THEN 1
	WHEN 'êÊ1N' THEN 5
	ELSE 0
END as course_length_month
,CASE KIHON.course
	WHEN 'iw2N' THEN 1
	WHEN 'iw1N9' THEN 1
	WHEN 'iw1N6' THEN 1
	WHEN 'iw1N3' THEN 1
	WHEN 'Zú6ú{ê¤C' THEN 2
	WHEN 'Zú3ú{ê¤C' THEN 2
	WHEN 'Zú2ú{ê¤C' THEN 2
	WHEN 'Zú1ú{ê¤C' THEN 2
	WHEN 'êÊú{êR[X' THEN 2
	WHEN 'êÊ2N' THEN 2
	WHEN 'êÊ1N' THEN 2
	ELSE 0
END as student_type_cd
,KIHON.start_date
,KIHON.end_date_intended
,CASE KIHON.status_name
	WHEN '\ü' THEN 1
	WHEN 'sü' THEN 11
	WHEN 'ÝÐ' THEN 4
	WHEN '²Æ' THEN 5
	WHEN 'C¹' THEN 6
	WHEN 'Þw' THEN 7
	WHEN 'Ð' THEN 8
	ELSE 3
END as status
,'' as quit_reason_cd
,KIHON.reason_for_exclusion
,KIHON.after_graduate_school
FROM (
	SELECT
		`wÐÔ` as student_no
		,`o^Ô` 
		,`{¼Ì` 
		,`{¼1` 
		,`{¼2` 
		,`tKi¼` 
		,`«Ê` 
		,`¢ù¥` 
		,`¼` 
		,`¶Nú` 
		,`ÐîÒ¼` 
		,`{ZÌ` 
		,`{Z` 
		,`{db` 
		,`·Ô` 
		,`·LøúÀ` 
		,`üNú` 
		,`¶VISAíÞ` 
		,`VISAXVú(1)` 
		,`VISAXVú(2)` 
		,`VISAXVú(3)` 
		,`VISAXVú(4)` 
		,`üÇÂÔ` 
		,`üÇÂú` 
		,`Ý¯J[hÔ` 
		,`üwú` as start_date
		,`²C\èú` as end_date_intended
		,`R[X¼` as course
		,`NX¼` 
		,`ÞwNú` as end_date1
		,`ÐNú` as end_date2
		,`ÞwÐR` as reason_for_exclusion
		,`ÝÐ²Ææª` as status_name
		,`ÈOZ§` 
		,`ÈOZZ` 
		,`»Z§` 
		,`»ZZ` 
		,`»Zdb` 
		,`g@Ñdb` 
		,`¯NÛ¯Ô` 
		,`áQÛ¯L³` 
		,`êê` 
		,`²ÆãiHæ` as after_graduate_school
		,`ÐîÒÌ` 
		,`ÐîÒ¼1` 
		,`ÐîÒ¼2` 
		,`ÐîÒZ1` 
		,`ÐîÒZ2` 
		,`ÐîÒTEL` 
		,`ÐîÒFAX` 
		,`J   Ã½Ä` 
		,`NAT Ã½Ä` 
		,`üwÃ½Ä` 
		,`gªÃ½Ä` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
LEFT JOIN person ON person.lastup_account_id=KIHON.student_no
WHERE student.student_no IS NULL
;
