SET @student_no:='1707069';
SET @csw_date:='2018-08-27';
SET @school_term:='134';

SELECT *
FROM class_student
LEFT JOIN student ON student.person_id = class_student.person_id
WHERE student.student_no = @student_no
AND class_student.disable = 0
AND class_student.school_term_id  = @school_term
;

select shift_attendance.*
from shift_attendance
left join class_shift_work on class_shift_work.id = shift_attendance.class_shift_work_id
left join student on student.person_id = shift_attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;

select attendance.*
from attendance
left join class_shift_work on class_shift_work.id = attendance.class_shift_work_id
left join student on student.person_id = attendance.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;

select class_entry_exit.*
from class_entry_exit
left join class_shift_work on class_shift_work.id = class_entry_exit.class_shift_work_id
left join student on student.person_id = class_entry_exit.person_id
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;
-----------------------------------------------------------------------------------------------------------------
SET @student_no:='1707069';
SET @csw_date:='2018-08-27';
SET @school_term:='134';


UPDATE class_student
LEFT JOIN student ON student.person_id = class_student.person_id
SET
 class_student.date_to = @csw_date,
 class_student.lastup_account_id = 2000,
 class_student.lastup_datetime = now()
WHERE student.student_no = @student_no
AND class_student.school_term_id  = @school_term
;

UPDATE shift_attendance
LEFT JOIN class_shift_work ON class_shift_work.id = shift_attendance.class_shift_work_id
LEFT JOIN student on student.person_id = shift_attendance.person_id
SET shift_attendance.disable=1,shift_attendance.lastup_account_id=2000,shift_attendance.lastup_datetime=now()
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;

UPDATE attendance
LEFT JOIN class_shift_work ON class_shift_work.id = attendance.class_shift_work_id
LEFT JOIN student on student.person_id = attendance.person_id
SET attendance.disable=1,attendance.lastup_account_id=2000,attendance.lastup_datetime=now()
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;

UPDATE class_entry_exit
LEFT JOIN class_shift_work ON class_shift_work.id = class_entry_exit.class_shift_work_id
LEFT JOIN student on student.person_id = class_entry_exit.person_id
SET class_entry_exit.disable=1,class_entry_exit.lastup_account_id=2000,class_entry_exit.lastup_datetime=now()
WHERE student_no = @student_no
and class_shift_work.date > @csw_date
;

UPDATE student
SET status = '7', end_date_intended = @csw_date, end_date = @csw_date, lastup_account_id=2000, lastup_datetime=now()
WHERE student_no = @student_no
;

