-- ------------------------------------------------------------------------------------------------------------
-- 10月度新入生　入学日、卒業予定日の更新
-- ------------------------------------------------------------------------------------------------------------
SELECT *
FROM `student`
WHERE `student_no` IN (1810001,1810002,1810003,1810004,1810005,1810006,1810007,1810008,1810009,1810010,1810011,1810012,1810013,1810014,1810015,1810016,1810017,1810018,1810019,1810020,1810021,1810022,1810023,1810024,1810025,1810026,1810027,1810028,1810029,1810030,1810031,1810032,1810033,1810034,1810035,1810036,1810037,1810038,1810039,1810040,1810041,1810042,1810043,1810044,1810045,1810046,1810047,1810048,1810049,1810050,1810051,1810052,1810053,1810054,1810055,1810056,1810057,1810058,1810059,1810060,1810061,1810062,1810063,1810064,1810065,1810066,1810067,1810068,1810069,1810070,1810071)
and disable = 0


UPDATE student
SET student.start_date = '2018-10-10', student.end_date_intended = '2020-03-31'
WHERE `student_no` IN (1810001,1810002,1810003,1810004,1810005,1810006,1810007,1810008,1810009,1810010,1810011,1810012,1810013,1810014,1810015,1810016,1810017,1810018,1810019,1810020,1810021,1810022,1810023,1810024,1810025,1810026,1810027,1810028,1810029,1810030,1810031,1810032,1810033,1810034,1810035,1810036,1810037,1810038,1810039,1810040,1810041,1810042,1810043,1810044,1810045,1810046,1810047,1810048,1810049,1810050,1810051,1810052,1810053,1810054,1810055,1810056,1810057,1810058,1810059,1810060,1810061,1810062,1810063,1810064,1810065,1810066,1810067,1810068,1810069,1810070,1810071)
and disable = 0;

-- ------------------------------------------------------------------------------------------------------------
-- 入学日の更新
-- ------------------------------------------------------------------------------------------------------------
UPDATE student SET start_date =20180123 WHERE student.student_no = 1801026;
UPDATE student SET start_date =20180412 WHERE student.student_no = 1804045;
UPDATE student SET start_date =20180406 WHERE student.student_no = 1804046;
UPDATE student SET start_date =20180417 WHERE student.student_no = 1804058;
UPDATE student SET start_date =20180411 WHERE student.student_no = 1804170;
UPDATE student SET start_date =20180411 WHERE student.student_no = 1804214;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804215;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804216;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804217;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804218;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804219;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804220;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804221;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804222;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804223;
UPDATE student SET start_date =20180418 WHERE student.student_no = 1804224;
UPDATE student SET start_date =20180508 WHERE student.student_no = 1804234;
UPDATE student SET start_date =20180507 WHERE student.student_no = 1804235;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804238;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804239;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804240;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804241;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804242;
UPDATE student SET start_date =20180415 WHERE student.student_no = 1804246;
UPDATE student SET start_date =20180411 WHERE student.student_no = 1804247;
UPDATE student SET start_date =20180508 WHERE student.student_no = 1804251;
UPDATE student SET start_date =20180508 WHERE student.student_no = 1804252;
UPDATE student SET start_date =20180507 WHERE student.student_no = 1804253;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804254;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804255;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804256;
UPDATE student SET start_date =20180420 WHERE student.student_no = 1804258;
UPDATE student SET start_date =20180420 WHERE student.student_no = 1804259;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804260;
UPDATE student SET start_date =20180420 WHERE student.student_no = 1804263;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804264;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804265;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804269;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804270;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804271;
UPDATE student SET start_date =20180507 WHERE student.student_no = 1804273;
UPDATE student SET start_date =20180420 WHERE student.student_no = 1804275;
UPDATE student SET start_date =20180417 WHERE student.student_no = 1804276;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804277;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804279;
UPDATE student SET start_date =20180416 WHERE student.student_no = 1804281;
UPDATE student SET start_date =20180419 WHERE student.student_no = 1804283;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804285;
UPDATE student SET start_date =20180411 WHERE student.student_no = 1804286;
UPDATE student SET start_date =20180410 WHERE student.student_no = 1804287;
UPDATE student SET start_date =20180711 WHERE student.student_no = 1807016;
UPDATE student SET start_date =20180711 WHERE student.student_no = 1807017;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807028;
UPDATE student SET start_date =20180821 WHERE student.student_no = 1807030;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807031;
UPDATE student SET start_date =20180718 WHERE student.student_no = 1807061;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807089;
UPDATE student SET start_date =20180712 WHERE student.student_no = 1807090;
UPDATE student SET start_date =20180705 WHERE student.student_no = 1807100;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807101;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807108;
UPDATE student SET start_date =20180702 WHERE student.student_no = 1807127;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807131;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807158;
UPDATE student SET start_date =20180710 WHERE student.student_no = 1807167;
UPDATE student SET start_date =20180709 WHERE student.student_no = 1807178;
UPDATE student SET start_date =20180725 WHERE student.student_no = 1807189;



-- ------------------------------------------------------------------------------------------------------------
-- 在籍ステータスの変更--不入学
-- ------------------------------------------------------------------------------------------------------------
UPDATE student SET student.status = 11 WHERE student.student_no =1807020;
UPDATE student SET student.status = 11 WHERE student.student_no =1810001;
UPDATE student SET student.status = 11 WHERE student.student_no =1810009;


-- ------------------------------------------------------------------------------------------------------------
-- 在籍ステータスの変更--不要学生の削除 student,person の disable = 1
-- ------------------------------------------------------------------------------------------------------------
UPDATE student s
LEFT JOIN person p ON p.id = s.person_id  
SET s.disable = '1',p.disable='1'
WHERE s.student_no IN (1810073,1810074,1810075,1810076,1810077,1810078,1810079,1810080,1810081,1810082,1810083,1810084,1810085,1810086,1810087,1810088,1810089,1810090,1810091,1810092,1810093,1810094,1810095,1810096,1810097,1810098,1810099,1810100);


-- check
SELECT s.student_no,s.disable,p.disable
FROM student s
LEFT JOIN person p ON p.id = s.person_id  
WHERE s.student_no IN (1810073,1810074,1810075,1810076,1810077,1810078,1810079,1810080,1810081,1810082,1810083,1810084,1810085,1810086,1810087,1810088,1810089,1810090,1810091,1810092,1810093,1810094,1810095,1810096,1810097,1810098,1810099,1810100);


-- ------------------------------------------------------------------------------------------------------------
-- 電話番号の更新 携帯電話　今回はあまりにsystem8データがごちゃごちゃなので個別アップデートにした
-- ------------------------------------------------------------------------------------------------------------
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-2337-5932' WHERE student.student_no = '1510026';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1263-9597' WHERE student.student_no = '1607054';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-2151-1246' WHERE student.student_no = '1704123';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1904-0323' WHERE student.student_no = '1707040';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3962-0708' WHERE student.student_no = '1707089';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4413-4543' WHERE student.student_no = '1710040';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3420-2037' WHERE student.student_no = '1710063';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9444-6019' WHERE student.student_no = '1710069';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-2155-1112' WHERE student.student_no = '1710123';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9582-4849' WHERE student.student_no = '1710162';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9696-9869' WHERE student.student_no = '1710178';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3862-1015' WHERE student.student_no = '1710208';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-4724-9888' WHERE student.student_no = '1710209';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-7475-1285' WHERE student.student_no = '1710219';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1668-4166' WHERE student.student_no = '1710221';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9362-6105' WHERE student.student_no = '1710222';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9863-0483' WHERE student.student_no = '1710235';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9863-1302' WHERE student.student_no = '1710236';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9863-0266' WHERE student.student_no = '1710239';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-6045-0625' WHERE student.student_no = '1710240';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-5388-6585' WHERE student.student_no = '1710241';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1486-3004' WHERE student.student_no = '1710242';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7698-0559' WHERE student.student_no = '1710244';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7833-3626' WHERE student.student_no = '1710245';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9882-1997' WHERE student.student_no = '1710246';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9811-3611' WHERE student.student_no = '1710247';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9811-3611' WHERE student.student_no = '1710248';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9811-3336' WHERE student.student_no = '1710249';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9048-2938' WHERE student.student_no = '1710251';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9048-4990' WHERE student.student_no = '1710253';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3552-0296' WHERE student.student_no = '1804004';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3482-1998' WHERE student.student_no = '1804010';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1576-7588' WHERE student.student_no = '1804011';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3723-4927' WHERE student.student_no = '1804021';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1402-5881' WHERE student.student_no = '1804036';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3507-2048' WHERE student.student_no = '1804037';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3551-9712' WHERE student.student_no = '1804038';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3296-9689' WHERE student.student_no = '1804042';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4125-9595' WHERE student.student_no = '1804045';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9682-0910' WHERE student.student_no = '1804046';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4658-8009' WHERE student.student_no = '1804057';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4772-9705' WHERE student.student_no = '1804059';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3340-5008' WHERE student.student_no = '1804066';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-2810-0647' WHERE student.student_no = '1804073';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-9324-3918' WHERE student.student_no = '1804080';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4393-5756' WHERE student.student_no = '1804086';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4321-0768' WHERE student.student_no = '1804087';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7844-5735' WHERE student.student_no = '1804089';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-1790-0714' WHERE student.student_no = '1804100';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4379-9806' WHERE student.student_no = '1804104';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-8412-0125' WHERE student.student_no = '1804111';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7885-1168' WHERE student.student_no = '1804133';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1385-0419' WHERE student.student_no = '1804141';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4121-0719' WHERE student.student_no = '1804144';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3314-0222' WHERE student.student_no = '1804147';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4555-0626' WHERE student.student_no = '1804148';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-6557-9793' WHERE student.student_no = '1804152';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1556-7433' WHERE student.student_no = '1804164';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7836-3169' WHERE student.student_no = '1804178';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4389-1737' WHERE student.student_no = '1804202';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1446-6120' WHERE student.student_no = '1804206';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1548-0865' WHERE student.student_no = '1804209';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4138-7204' WHERE student.student_no = '1804211';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-5887-0677' WHERE student.student_no = '1804213';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-4170-3286' WHERE student.student_no = '1804221';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1253-4984' WHERE student.student_no = '1804228';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4157-8984' WHERE student.student_no = '1804269';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1449-2401' WHERE student.student_no = '1804277';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-2566-1717' WHERE student.student_no = '1804287';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-8962-6258' WHERE student.student_no = '1807009';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7941-4585' WHERE student.student_no = '1807011';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-6039-6767' WHERE student.student_no = '1807012';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-9965-2861' WHERE student.student_no = '1807014';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-6525-9285' WHERE student.student_no = '1807015';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3586-6048' WHERE student.student_no = '1807017';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4717-2136' WHERE student.student_no = '1807021';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3355-9554' WHERE student.student_no = '1807024';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9426-0217' WHERE student.student_no = '1807025';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3917-5230' WHERE student.student_no = '1807028';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4791-5493' WHERE student.student_no = '1807037';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9437-1121' WHERE student.student_no = '1807041';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9416-2333' WHERE student.student_no = '1807042';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3648-8836' WHERE student.student_no = '1807044';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3322-7676' WHERE student.student_no = '1807045';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4152-0932' WHERE student.student_no = '1807049';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3158-6918' WHERE student.student_no = '1807054';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3871-5309' WHERE student.student_no = '1807055';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7945-8658' WHERE student.student_no = '1807059';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9681-0815' WHERE student.student_no = '1807066';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4185-5234' WHERE student.student_no = '1807067';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3344-0605' WHERE student.student_no = '1807069';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4165-7517' WHERE student.student_no = '1807070';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-2387-8607' WHERE student.student_no = '1807072';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4933-9966' WHERE student.student_no = '1807078';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3573-5130' WHERE student.student_no = '1807081';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-7471-3428' WHERE student.student_no = '1807088';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4814-7333' WHERE student.student_no = '1807090';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3002-8462' WHERE student.student_no = '1807092';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4087-0300' WHERE student.student_no = '1807093';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4608-8894' WHERE student.student_no = '1807094';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4857-8811' WHERE student.student_no = '1807096';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1302-7888' WHERE student.student_no = '1807106';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-5492-1875' WHERE student.student_no = '1807125';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3108-1994' WHERE student.student_no = '1807126';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3319-9177' WHERE student.student_no = '1807132';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-7932-2505' WHERE student.student_no = '1807135';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3107-0712' WHERE student.student_no = '1807138';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3170-8778' WHERE student.student_no = '1807139';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4027-2258' WHERE student.student_no = '1807140';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3106-0627' WHERE student.student_no = '1807144';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-4472-2997' WHERE student.student_no = '1807154';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9191-0877' WHERE student.student_no = '1807155';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3081-7162' WHERE student.student_no = '1807157';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-7471-5342' WHERE student.student_no = '1807159';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1251-1118' WHERE student.student_no = '1807160';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9663-0271' WHERE student.student_no = '1807161';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3717-0702' WHERE student.student_no = '1807166';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3273-5728' WHERE student.student_no = '1807168';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-3238-2767' WHERE student.student_no = '1807174';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-9657-0830' WHERE student.student_no = '1807175';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1189-6653' WHERE student.student_no = '1807176';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4072-5961' WHERE student.student_no = '1807180';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-3255-0430' WHERE student.student_no = '1807186';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-4619-0822' WHERE student.student_no = '1807187';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '070-1304-8055' WHERE student.student_no = '1807196';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '090-5789-1368' WHERE student.student_no = '9918009';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET mobile_no = '080-2275-6149' WHERE student.student_no = '9918010';


-- ------------------------------------------------------------------------------------------------------------
-- 電話番号の更新 自宅電話　今回はあまりにsystem8データがごちゃごちゃなので個別アップデートにした
-- ------------------------------------------------------------------------------------------------------------
UPDATE person  INNER JOIN student ON student.person_id = person.id SET phone_no = '070-4120-2766' WHERE student.student_no = '1704123';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET phone_no = '' WHERE student.student_no = '1710162';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET phone_no = '070-4202-2437' WHERE student.student_no = '1710222';
UPDATE person  INNER JOIN student ON student.person_id = person.id SET phone_no = '03-3368-49361' WHERE student.student_no = '1804252';

-- ------------------------------------------------------------------------------------------------------------
-- 電話番号の更新 本国電話　今回はあまりにsystem8データがごちゃごちゃなので個別アップデートにした
-- ------------------------------------------------------------------------------------------------------------
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-2151-1246' WHERE student.student_no = '1704123';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1904-0323' WHERE student.student_no = '1707040';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3962-0708' WHERE student.student_no = '1707089';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4413-4543' WHERE student.student_no = '1710040';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3420-2037' WHERE student.student_no = '1710063';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-2155-1112' WHERE student.student_no = '1710123';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9582-4849' WHERE student.student_no = '1710162';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9696-9869' WHERE student.student_no = '1710178';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3862-1015' WHERE student.student_no = '1710208';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-4724-9888' WHERE student.student_no = '1710209';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-7475-1285' WHERE student.student_no = '1710219';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1668-4166' WHERE student.student_no = '1710221';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9362-6105' WHERE student.student_no = '1710222';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3552-0296' WHERE student.student_no = '1804004';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3482-1998' WHERE student.student_no = '1804010';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1576-7588' WHERE student.student_no = '1804011';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3695-7033' WHERE student.student_no = '1804018';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3723-4927' WHERE student.student_no = '1804021';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3308-0116' WHERE student.student_no = '1804032';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1402-5881' WHERE student.student_no = '1804036';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3507-2048' WHERE student.student_no = '1804037';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3551-9712' WHERE student.student_no = '1804038';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3602-3859' WHERE student.student_no = '1804041';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4125-9595' WHERE student.student_no = '1804045';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9682-0910' WHERE student.student_no = '1804046';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4658-8009' WHERE student.student_no = '1804057';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4772-9705' WHERE student.student_no = '1804059';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3340-5008' WHERE student.student_no = '1804066';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-9324-3918' WHERE student.student_no = '1804080';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4393-5756' WHERE student.student_no = '1804086';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4321-0768' WHERE student.student_no = '1804087';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7844-5735' WHERE student.student_no = '1804089';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-1790-0714' WHERE student.student_no = '1804100';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-8412-0125' WHERE student.student_no = '1804111';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1529-8724' WHERE student.student_no = '1804131';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7885-1168' WHERE student.student_no = '1804133';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1385-0419' WHERE student.student_no = '1804141';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4121-0719' WHERE student.student_no = '1804144';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3314-0222' WHERE student.student_no = '1804147';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4555-0626' WHERE student.student_no = '1804148';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-6557-9793' WHERE student.student_no = '1804152';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1556-7433' WHERE student.student_no = '1804164';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7836-3169' WHERE student.student_no = '1804178';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4389-1737' WHERE student.student_no = '1804202';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1446-6120' WHERE student.student_no = '1804206';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1548-0865' WHERE student.student_no = '1804209';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4138-7204' WHERE student.student_no = '1804211';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4539-9696' WHERE student.student_no = '1804212';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-5887-0677' WHERE student.student_no = '1804213';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1253-4984' WHERE student.student_no = '1804228';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9678-6809' WHERE student.student_no = '1804237';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9368-2482' WHERE student.student_no = '1804239';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3356-8504' WHERE student.student_no = '1804241';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9356-9396' WHERE student.student_no = '1804246';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4543-3926' WHERE student.student_no = '1804251';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4543-4071' WHERE student.student_no = '1804252';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4203-8192' WHERE student.student_no = '1804253';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-2670-3997' WHERE student.student_no = '1804254';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4343-2852' WHERE student.student_no = '1804255';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3556-4879' WHERE student.student_no = '1804256';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-2165-6717' WHERE student.student_no = '1804258';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3525-4268' WHERE student.student_no = '1804259';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-2806-7060' WHERE student.student_no = '1804260';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4157-3495' WHERE student.student_no = '1804265';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4157-8984' WHERE student.student_no = '1804269';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3604-5774' WHERE student.student_no = '1804273';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9391-8816' WHERE student.student_no = '1804276';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1449-2401' WHERE student.student_no = '1804277';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4412-9133' WHERE student.student_no = '1804278';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-8962-6258' WHERE student.student_no = '1807009';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7941-4585' WHERE student.student_no = '1807011';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-6039-6767' WHERE student.student_no = '1807012';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-9965-2861' WHERE student.student_no = '1807014';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-6525-9285' WHERE student.student_no = '1807015';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3586-6048' WHERE student.student_no = '1807017';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4717-2136' WHERE student.student_no = '1807021';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3355-9554' WHERE student.student_no = '1807024';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9426-0217' WHERE student.student_no = '1807025';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3917-5230' WHERE student.student_no = '1807028';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1522-5207' WHERE student.student_no = '1807031';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4791-5493' WHERE student.student_no = '1807037';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4612-6417' WHERE student.student_no = '1807038';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3326-1011' WHERE student.student_no = '1807040';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9437-1121' WHERE student.student_no = '1807041';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9416-2333' WHERE student.student_no = '1807042';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3648-8836' WHERE student.student_no = '1807044';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3322-7676' WHERE student.student_no = '1807045';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4152-0932' WHERE student.student_no = '1807049';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3158-6918' WHERE student.student_no = '1807054';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3871-5309' WHERE student.student_no = '1807055';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7945-8658' WHERE student.student_no = '1807059';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3578-1130' WHERE student.student_no = '1807060';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9681-0815' WHERE student.student_no = '1807066';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4185-5234' WHERE student.student_no = '1807067';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3344-0605' WHERE student.student_no = '1807069';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4165-7517' WHERE student.student_no = '1807070';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-2387-8607' WHERE student.student_no = '1807072';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4933-9966' WHERE student.student_no = '1807078';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3573-5130' WHERE student.student_no = '1807081';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-7471-3428' WHERE student.student_no = '1807088';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4814-7333' WHERE student.student_no = '1807090';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3002-8462' WHERE student.student_no = '1807092';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4087-0300' WHERE student.student_no = '1807093';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4608-8894' WHERE student.student_no = '1807094';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4857-8811' WHERE student.student_no = '1807096';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-9973-2964' WHERE student.student_no = '1807098';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1391-6665' WHERE student.student_no = '1807102';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1302-7888' WHERE student.student_no = '1807106';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4732-1314' WHERE student.student_no = '1807109';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-5492-1875' WHERE student.student_no = '1807125';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3108-1994' WHERE student.student_no = '1807126';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3319-9177' WHERE student.student_no = '1807132';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-7932-2505' WHERE student.student_no = '1807135';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3107-0712' WHERE student.student_no = '1807138';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3170-8778' WHERE student.student_no = '1807139';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4027-2258' WHERE student.student_no = '1807140';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3106-0627' WHERE student.student_no = '1807144';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4472-2997' WHERE student.student_no = '1807154';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9191-0877' WHERE student.student_no = '1807155';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3081-7162' WHERE student.student_no = '1807157';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-7471-5342' WHERE student.student_no = '1807159';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1251-1118' WHERE student.student_no = '1807160';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9663-0271' WHERE student.student_no = '1807161';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3717-0702' WHERE student.student_no = '1807166';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3273-5728' WHERE student.student_no = '1807168';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-3238-2767' WHERE student.student_no = '1807174';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-9657-0830' WHERE student.student_no = '1807175';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-1189-6653' WHERE student.student_no = '1807176';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '070-4192-0523' WHERE student.student_no = '1807177';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4072-5961' WHERE student.student_no = '1807180';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-3255-0430' WHERE student.student_no = '1807186';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-4619-0822' WHERE student.student_no = '1807187';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '135-5886-7114' WHERE student.student_no = '1904001';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '090-5789-1368' WHERE student.student_no = '9918009';
UPDATE immigration INNER JOIN student ON student.person_id = immigration.person_id SET home_country_phone_no1 = '080-2275-6149' WHERE student.student_no = '9918010';




