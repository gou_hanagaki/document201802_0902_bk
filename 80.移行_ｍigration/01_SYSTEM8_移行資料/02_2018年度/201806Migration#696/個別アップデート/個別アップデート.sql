UPDATE student SET student.status = 5 WHERE student.student_no = 1607093;
UPDATE student SET student.status = 8 WHERE student.student_no = 1610049;
UPDATE student SET student.status = 5, student.end_date_intended = '2018-06-22', student.end_date = '2018-06-22'  WHERE student.student_no = 1707077;
UPDATE student SET student.start_date = '2018-04-16' WHERE student.student_no = 1804080;
UPDATE student SET student.status = 6 WHERE student.student_no = 1704119;


-- ------------------------------------------------------------------------------------------------------------
-- 在籍ステータスの変更--予備入学から在籍
-- ------------------------------------------------------------------------------------------------------------
UPDATE student SET student.start_date = '2018-05-07', student.end_date_intended ='2020-03-31' WHERE student.student_no = 1804234;
UPDATE student SET student.start_date = '2018-05-07', student.end_date_intended ='2020-03-31' WHERE student.student_no = 1804251;
UPDATE student SET student.start_date = '2018-05-07', student.end_date_intended ='2020-03-31' WHERE student.student_no = 1804252;
UPDATE student SET student.start_date = '2018-07-05', student.end_date_intended ='2020-03-31' WHERE student.student_no = 1807010;
UPDATE student SET student.start_date = '2018-07-05', student.end_date_intended ='2020-03-31' WHERE student.student_no = 1807188;


-- ------------------------------------------------------------------------------------------------------------
-- 在籍ステータスの変更--不入学
-- ------------------------------------------------------------------------------------------------------------
UPDATE student SET student.status = 11 WHERE student.student_no =1804023;
UPDATE student SET student.status = 11 WHERE student.student_no =1804069;
UPDATE student SET student.status = 11 WHERE student.student_no =1804119;
UPDATE student SET student.status = 11 WHERE student.student_no =1804173;
UPDATE student SET student.status = 11 WHERE student.student_no =1804188;
UPDATE student SET student.status = 11 WHERE student.student_no =1804225;
UPDATE student SET student.status = 11 WHERE student.student_no =1804229;
UPDATE student SET student.status = 11 WHERE student.student_no =1804230;
UPDATE student SET student.status = 11 WHERE student.student_no =1804231;
UPDATE student SET student.status = 11 WHERE student.student_no =1804232;
UPDATE student SET student.status = 11 WHERE student.student_no =1804233;
UPDATE student SET student.status = 11 WHERE student.student_no =1804236;
UPDATE student SET student.status = 11 WHERE student.student_no =1804243;
UPDATE student SET student.status = 11 WHERE student.student_no =1804244;
UPDATE student SET student.status = 11 WHERE student.student_no =1804245;
UPDATE student SET student.status = 11 WHERE student.student_no =1804248;
UPDATE student SET student.status = 11 WHERE student.student_no =1804249;
UPDATE student SET student.status = 11 WHERE student.student_no =1804250;
UPDATE student SET student.status = 11 WHERE student.student_no =1804257;
UPDATE student SET student.status = 11 WHERE student.student_no =1804261;
UPDATE student SET student.status = 11 WHERE student.student_no =1804262;
UPDATE student SET student.status = 11 WHERE student.student_no =1804266;
UPDATE student SET student.status = 11 WHERE student.student_no =1804267;
UPDATE student SET student.status = 11 WHERE student.student_no =1804272;
UPDATE student SET student.status = 11 WHERE student.student_no =1804274;
UPDATE student SET student.status = 11 WHERE student.student_no =1804280;
UPDATE student SET student.status = 11 WHERE student.student_no =1804282;
UPDATE student SET student.status = 11 WHERE student.student_no =1804284;
UPDATE student SET student.status = 11 WHERE student.student_no =1807001;
UPDATE student SET student.status = 11 WHERE student.student_no =1807003;
UPDATE student SET student.status = 11 WHERE student.student_no =1807004;
UPDATE student SET student.status = 11 WHERE student.student_no =1807007;
UPDATE student SET student.status = 11 WHERE student.student_no =1807103;
UPDATE student SET student.status = 11 WHERE student.student_no =1807136;
UPDATE student SET student.status = 11 WHERE student.student_no =1807153;
UPDATE student SET student.status = 11 WHERE student.student_no =1807165;
UPDATE student SET student.status = 11 WHERE student.student_no =1807169;
UPDATE student SET student.status = 11 WHERE student.student_no =1807193;
UPDATE student SET student.status = 11 WHERE student.student_no =9918003;
