SELECT * 
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN account ON account.person_id=person.id
WHERE account.login_id IS NULL
;

-- 新入生
INSERT INTO account(
account_group_id
,person_id
,login_id
,password
)
SELECT 
'1'
,person.id
,student.student_no
,MD5('password0')
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN account ON account.person_id=person.id
WHERE account.login_id IS NULL
;
