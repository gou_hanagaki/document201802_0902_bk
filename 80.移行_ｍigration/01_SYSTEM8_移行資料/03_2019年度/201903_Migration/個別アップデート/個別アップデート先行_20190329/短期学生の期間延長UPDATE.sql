/*  短期学生(9918008)の期間延長 退学学生を在籍へ変更する 卒業予定日:2019/03/20⇒2020/03/14　1年コース⇒2年コース */
UPDATE student 
SET student.status = '4',
student.course_length_month = '1',
student.end_date_intended = '2020-03-14',
student.end_date = '0000-00-00',
student.final_judgement = '0',
student.final_judgement_all_attend = '0',
student.final_judgement_all_absent = '0'
WHERE student.student_no ='9918008'
;

/* 短期学生(9918013)の期間延長　卒業予定日:2019/03/31⇒2019/09/30 、6ヵ月コース⇒1年コース */
UPDATE student 
SET
student.course_length_month = '5',
student.end_date_intended = '2019-09-30',
student.final_judgement = '0',
student.final_judgement_all_attend = '0',
student.final_judgement_all_absent = '0'
WHERE student.student_no ='9918013'
;

/* 短期学生(9918002)の期間延長　卒業予定日:2019/03/31⇒2019/09/30 、1年月コース⇒2年コース */
UPDATE student 
SET
student.course_length_month = '5',
student.end_date_intended = '2019-09-30',
student.final_judgement = '0',
student.final_judgement_all_attend = '0',
student.final_judgement_all_absent = '0'
WHERE student.student_no ='9918002'
;
